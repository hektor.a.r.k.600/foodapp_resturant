import 'package:get_storage/get_storage.dart';

class ApiAddress {
  static final String FOOD_DEFAULT_URL =
      'https://cdn.iconscout.com/icon/free/png-256/fast-food-1851561-1569286.png';
  static final String BASE_URL = 'http://storeapi.makanavenue.com/';
  static final String LOGIN = 'User/Login';
  static final String GET_USERS = 'User/GetUsers';
  static final String GET_USERS_BY_ID = 'User/GetUserById';
  static final String Get_TABLE = 'Table/GetTables';
  static final String CHANGE_TABLE_STATUS = 'Table/ChangeTableStatus';
  static final String GET_STORE_FOOD_CATEGORY = 'Food/GetStoreFoodCategories';
  static final String ADD_STORE_FOOD_CATEGORY = 'Food/AddStoreFoodCategory';
  static final String EDIT_STORE_FOOD_CATEGORY = 'Food/EditStoreFoodCategory';
  static final String DELETE_STORE_FOOD_CATEGORY =
      'Food/DeleteStoreFoodCategory';
  static final String ADD_FOOD_STORE = 'Food/AddStoreFood';
  static final String EDIT_FOOD_STORE = 'Food/EditStoreFood';
  static final String GET_FOOD_CATEGORIES = 'Food/GetFoodCategories';
  static final String ADD_FOOD_INGREDIENT = 'Food/AddFoodIngredient';
  static final String GET_FOOD_INGREDIENT = 'Food/GetFoodIngredients';
  static final String REMOVE_FOOD_INGREDIENT = 'Food/RemoveFoodIngredient';
  static final String EDIT_STORE_PROFILE = 'Store/EditStoreProfile';
  static final String GET_STORE_BY_ID = 'Store/GetStoreById';
  static final String GET_ORDERS = 'Order/GetOrders';
  static final String GET_STORE_FOOD = 'Food/GetStoreFoods';
  static final String GET_STORE_FOOD_BY_ID = 'Food/GetStoreFoodById';
  static final String ADD_ADDITIONAL = 'Food/AddAditional';
  static final String GET_ORDER_BY_ID = 'Order/GetOrderById';
  static final String CHANGE_STORE_FOOD_STATUS = 'Food/ChangeStoreFoodStatus';
  static final String GET_STORE_WITHDRAW_REQUEST =
      'WithDraw/GetStoreWithDrawRequests';
  static final String ADD_STORE_WITHDRAW_REQUEST =
      'WithDraw/AddStoreWithDrawRequest';
  static final String ADD_TICKET = 'Ticket/AddTicket';
  static final String GET_TICKETS = 'Ticket/GetTickets';
  static final String GET_TICKETS_BY_ID = 'Ticket/GetTicketById';
  static final String GET_DISCOUNT = 'Discount​/GetDiscounts';
  static final String ADD_DISCOUNT = 'Discount/AddDiscount';
  static final String REMOVE_DISCOUNT = 'Discount/RemoveDiscount';
  static final String GET_NOTIFICATION = 'Notification/GetUserNotifications';
  static final String GET_ORDER_COUNT = 'Order/GetOrdersCount';
  static final String GET_STORE_RATE = 'Rate/GetStoreRates';
  static final String GET_ORDERS_BY_TABLE = 'Order/GetOrdersByTable';
  static final String GET_STORE_TRANSACTION = 'Store/GetStoreTransactions';
  static final String REMOVE_STORE_FOOD = 'Food/RemoveStoreFood';
  static final String CHANGE_ORDER_STATUS = 'Order/ChangeOrderStatus';
  static final String CHANGE_FOOD_ORDER_STATUS =
      '/Order/ChangeOrderStoreStatus';
  static final String EDIT_FOOD_DISCOUNT = 'Discount/EditFoodDiscount';
  static final String ADD_ATTENDENCE = 'Attendance/AddAttendence';
  static final String GET_STORE_ORDERS = 'Order/StoreGetOrders';
  static final String ADD_ANSWER_TICKET = 'Ticket/AddAnswerTicket';
  static final String GET_BALANCE = 'Store/GetStoreBalance';
  static final String CHECK_UPDATE = 'Update/StoreCheckUpdate';
  static final String CHANGE_LOGIN_STATUS = 'User/ChangeLoginStatus';
  static final String USER_EDIT_PROFILE = 'User/EditProfile';
  static final String SEARCH_ORDERS = 'Order/SearchOrder';
  static final String ChangeOrderApprovalStatus =
      'Order/ChangeOrderCashAprovalStatus';
}
