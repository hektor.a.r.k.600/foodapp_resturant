
class MassageError {
  static const String SERVER_FAILURE_MASSAGE = "There was a problem communicating with the server";
  static const String OFFLINE_FAILURE_MASSAGE = "لطفا دسترسی به اینترنت خود را بررسی کنید";
  static const String WRONG_PASSWORD_MASSAGE = "کد وارد شده صحیح نیست";
  static const String EXIST_USER_MASSAGE = "کاربر وجئد دارد";
  static const String WRONG_CODE_MASSAGE = "کد وارد شده صحیح نیست";
  static const String FIELD_IS_EMPTY = "لطفا فیلد های بالا را پر کنید";
  static const String INVALID_VALUE_MASSAGE = "مقدار وارد شده معتبر نیست";
  static const String UNEXPECTED_MASSAGE = "مشکلی بوجد آمده است لطفا با پشتیبانی تماس بگیرید";
  static const String Authentication_Massage = "مشکلی بوجد آمده است لطفا با پشتیبانی تماس بگیرید";
  static const String CHANGES_APPLIED = "تغییرات اعمال شد";
  static const String UNKNOWN_MASSAGE = "";

}
