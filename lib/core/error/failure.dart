
abstract class Failure{
  Failure([List properties = const <dynamic>[]]);
}

class ServerFailure extends Failure{}
class OfflineFailure extends Failure{}
class InValidFailure extends Failure{}
class UnKnownUser extends Failure{}
