import 'package:data_connection_checker/data_connection_checker.dart';
import 'package:food_resturant/core/config/global_setting.dart';
import 'package:food_resturant/feature/server/api_helper.dart';
import 'package:food_resturant/library/platforms/notification_service.dart';
import 'package:food_resturant/library/platforms/shared_preferences.dart';
import 'package:get_it/get_it.dart';
import 'package:get_storage/get_storage.dart';
import 'package:shared_preferences/shared_preferences.dart';


final sl = GetIt.instance;

Future<void> init() async {
  //! Features
  GetStorage getStorage = GetStorage();
  sl.registerLazySingleton<DataStorage>(
          () => DataStorageGetXImpl(getStorage: getStorage));
  // sl.registerLazySingleton<DataStorage>(
  //     () => DataStorageGetXImpl(getStorage: sl()));

  ApiHelper apiHelper = ApiHelper();
  sl.registerLazySingleton<ApiHelper>(() => apiHelper);

  NotificationService notificationService = NotificationService();
  sl.registerLazySingleton<NotificationService>(() => notificationService);


  GlobalSetting globalSetting = GlobalSetting();
  sl.registerLazySingleton<GlobalSetting>(() => globalSetting);

  // ! Externals
  // * DataConnectionChecker packages
  sl.registerLazySingleton<DataConnectionChecker>(
      () => DataConnectionChecker());
  SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
  sl.registerLazySingleton<SharedPreferences>(() => sharedPreferences);
}
