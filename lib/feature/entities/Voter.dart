class Voter {
  String id;
  String userName;
  String name;
  String familyName;
  String profileImage;

  Voter(
      {this.id, this.userName, this.name, this.familyName, this.profileImage});

  Voter.fromJson(dynamic json) {
    id = json["id"];
    userName = json["userName"];
    name = json["name"];
    familyName = json["familyName"];
    profileImage = json["profileImage"];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["id"] = id;
    map["userName"] = userName;
    map["name"] = name;
    map["familyName"] = familyName;
    map["profileImage"] = profileImage;
    return map;
  }
}
