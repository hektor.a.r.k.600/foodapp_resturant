import 'package:food_resturant/feature/entities/food.dart';
import 'package:food_resturant/feature/entities/icon_object.dart';
import 'package:food_resturant/feature/entities/store.dart';
import 'package:food_resturant/feature/server/params/params.dart';

class AddAndEditStoreFoodParams extends Params{
  Store store;
  Food food;

  AddAndEditStoreFoodParams({
      this.store, 
      this.food});

  AddAndEditStoreFoodParams.fromJson(dynamic json) {
    store = json["store"] != null ? Store.fromJson(json["store"]) : null;
    food = json["food"] != null ? Food.fromJson(json["food"]) : null;
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    if (store != null) {
      map["store"] = store.toJson();
    }
    if (food != null) {
      map["food"] = food.toJson();
    }
    return map;
  }

}









