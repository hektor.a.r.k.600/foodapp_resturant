import 'package:food_resturant/feature/entities/icon_object.dart';
import 'package:food_resturant/feature/entities/main_category.dart';
import 'package:food_resturant/feature/entities/store.dart';

class Category {
  Store store;
  String name;
  IconObject icon;
  int discountValue;
  MainCategory mainCategory;
  String id;
  int createdAt;

  Category({
    this.store,
    this.name,
    this.icon,
    this.discountValue,
    this.mainCategory,
    this.id,
    this.createdAt});

  Category.fromJson(dynamic json) {
    store = json["store"] != null ? Store.fromJson(json["store"]) : null;
    name = json["name"];
    icon = json["icon"] != null ? IconObject.fromJson(json["icon"]) : null;
    discountValue = json["discountValue"];
    mainCategory = json["mainCategory"] != null ? MainCategory.fromJson(json["mainCategory"]) : null;
    id = json["id"];
    createdAt = json["createdAt"];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    if (store != null) {
      map["store"] = store.toJson();
    }
    map["name"] = name;
    if (icon != null) {
      map["icon"] = icon.toJson();
    }
    map["discountValue"] = discountValue;
    if (mainCategory != null) {
      map["mainCategory"] = mainCategory.toJson();
    }
    map["id"] = id;
    map["createdAt"] = createdAt;
    return map;
  }

}