class ChangeLoginStatusModel {
  String userId;
  int loginStatus;

  ChangeLoginStatusModel({this.userId, this.loginStatus});

  ChangeLoginStatusModel.fromJson(Map<String, dynamic> json) {
    userId = json['userId'];
    loginStatus = json['loginStatus'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['userId'] = this.userId;
    data['loginStatus'] = this.loginStatus;
    return data;
  }
}
