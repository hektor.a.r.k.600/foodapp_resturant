class UpdateModel {
  Update object;
  bool status;
  String message;

  UpdateModel({this.object, this.status, this.message});

  UpdateModel.fromJson(Map<String, dynamic> json) {
    object =
        json['object'] != null ? new Update.fromJson(json['object']) : null;
    status = json['status'];
    message = json['message'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.object != null) {
      data['object'] = this.object.toJson();
    }
    data['status'] = this.status;
    data['message'] = this.message;
    return data;
  }
}

class Update {
  String version;
  String description;
  bool restricted;
  String link;
  String id;
  int createdAt;

  Update(
      {this.version,
      this.description,
      this.restricted,
      this.link,
      this.id,
      this.createdAt});

  Update.fromJson(Map<String, dynamic> json) {
    version = json['version'];
    description = json['description'];
    restricted = json['restricted'];
    link = json['link'];
    id = json['id'];
    createdAt = json['createdAt'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['version'] = this.version;
    data['description'] = this.description;
    data['restricted'] = this.restricted;
    data['link'] = this.link;
    data['id'] = this.id;
    data['createdAt'] = this.createdAt;
    return data;
  }
}
