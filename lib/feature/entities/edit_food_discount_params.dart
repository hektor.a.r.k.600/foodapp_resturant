import 'package:food_resturant/feature/server/params/params.dart';

class EditFoodDiscountParams extends Params{
  String storeFoodId;
  int discountValue;

  EditFoodDiscountParams({
      this.storeFoodId, 
      this.discountValue});

  EditFoodDiscountParams.fromJson(dynamic json) {
    storeFoodId = json["storeFoodId"];
    discountValue = json["discountValue"];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["storeFoodId"] = storeFoodId;
    map["discountValue"] = discountValue;
    return map;
  }

}