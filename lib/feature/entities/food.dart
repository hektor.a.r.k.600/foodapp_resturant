import 'package:food_resturant/feature/entities/category.dart';
import 'package:food_resturant/feature/entities/icon_object.dart';
import 'package:food_resturant/feature/entities/main_category.dart';
import 'package:food_resturant/feature/entities/store.dart';
import 'package:food_resturant/library/enums/enums.dart';
import 'package:food_resturant/library/extention/enum_extention.dart';
import 'package:food_resturant/library/extention/int_extention.dart';

class Food {
  String name;
  IconObject icon;
  num totalPrice;
  int cookingTime;
  int status;
  num discountValue;
  num discountPrice;
  String id;
  int createdAt;
  Store store;
  OrderStatus storeStatus;

  Food(
      {this.name,
      this.icon,
      this.totalPrice,
      this.cookingTime,
      this.status,
      this.discountValue,
      this.discountPrice,
      this.store,
      this.id,
      this.storeStatus,
      this.createdAt});

  Food.fromJson(dynamic json) {
    name = json["name"];
    icon = json["icon"] != null ? IconObject.fromJson(json["icon"]) : null;
    store = json["store"] != null ? Store.fromJson(json["store"]) : null;
    totalPrice = json["totalPrice"];
    discountPrice = json["discountPrice"];
    cookingTime = json["cookingTime"];
    status = json["status"];
    discountValue = json["discountValue"];
    id = json["id"];
    createdAt = json["createdAt"];
    int statusInt = json["storeStatus"];
    storeStatus = statusInt.enumOrderStatus;
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["name"] = name;
    if (icon != null) {
      map["icon"] = icon.toJson();
    }
    if (store != null) {
      map["store"] = store.toJson();
    }
    map["totalPrice"] = totalPrice;
    map["discountPrice"] = discountPrice;
    map["cookingTime"] = cookingTime;
    map["status"] = status;
    map["discountValue"] = discountValue;
    map["id"] = id;
    map["createdAt"] = createdAt;
    map["storeStatus"] = storeStatus.orderStatusToInt;
    return map;
  }
}
