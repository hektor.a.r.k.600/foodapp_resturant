import 'package:food_resturant/feature/entities/additional.dart';
import 'package:food_resturant/feature/entities/icon_object.dart';
import 'package:food_resturant/feature/entities/optional.dart';
import 'package:food_resturant/feature/entities/store.dart';

class Foods {
  String id;
  String name;
  IconObject icon;
  Optionals optionals;
  List<Additionals> additionals;
  num totalPrice;
  num discountPrice;
  Store store;

  Foods({
    this.id,
    this.name,
    this.icon,
    this.optionals,
    this.additionals,
    this.totalPrice,
    this.discountPrice,
    this.store});

  Foods.fromJson(dynamic json) {
    id = json["id"];
    name = json["name"];
    icon = json["icon"] != null ? IconObject.fromJson(json["icon"]) : null;
    optionals = json["optionals"] != null ? Optionals.fromJson(json["optionals"]) : null;
    if (json["additionals"] != null) {
      additionals = [];
      json["additionals"].forEach((v) {
        additionals.add(Additionals.fromJson(v));
      });
    }
    totalPrice = json["totalPrice"];
    discountPrice = json["discountPrice"];
    store = json["store"] != null ? Store.fromJson(json["store"]) : null;
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["id"] = id;
    map["name"] = name;
    if (icon != null) {
      map["icon"] = icon.toJson();
    }
    if (optionals != null) {
      map["optionals"] = optionals.toJson();
    }
    if (additionals != null) {
      map["additionals"] = additionals.map((v) => v.toJson()).toList();
    }
    map["totalPrice"] = totalPrice;
    map["discountPrice"] = discountPrice;
    if (store != null) {
      map["store"] = store.toJson();
    }
    return map;
  }

}
