import 'package:food_resturant/feature/entities/additional.dart';
import 'package:food_resturant/feature/entities/food.dart';
import 'package:food_resturant/feature/entities/icon_object.dart';
import 'package:food_resturant/feature/entities/image_obj.dart';
import 'package:food_resturant/feature/entities/ingredient.dart';
import 'package:food_resturant/feature/entities/main_category.dart';
import 'package:food_resturant/feature/entities/optional.dart';
import 'package:food_resturant/feature/server/params/add_and_edit_store_food_params.dart';

class FullFoodObj {
  Food food;
  MainCategory mainCategory;
  List<Ingredients> ingredients;
  List<ImageObj> images;
  List<Optionals> optionals;
  List<Additionals> additionals;
  List<Rates> rates;
  IconObject icon;

  FullFoodObj({
    this.food,
    this.mainCategory,
    this.ingredients,
    this.images,
    this.optionals,
    this.additionals,
    this.rates,
    this.icon});

  FullFoodObj.fromJson(dynamic json) {
    food = json["food"] != null ? Food.fromJson(json["food"]) : null;
    mainCategory = json["mainCategory"] != null ? MainCategory.fromJson(json["mainCategory"]) : null;
    if (json["ingredients"] != null) {
      ingredients = [];
      json["ingredients"].forEach((v) {
        ingredients.add(Ingredients.fromJson(v));
      });
    }
    if (json["images"] != null) {
      images = [];
      json["images"].forEach((v) {
        images.add(ImageObj.fromJson(v));
      });
    }
    if (json["optionals"] != null) {
      optionals = [];
      json["optionals"].forEach((v) {
        optionals.add(Optionals.fromJson(v));
      });
    }
    if (json["additionals"] != null) {
      additionals = [];
      json["additionals"].forEach((v) {
        additionals.add(Additionals.fromJson(v));
      });
    }
    if (json["rates"] != null) {
      rates = [];
      json["rates"].forEach((v) {
        rates.add(Rates.fromJson(v));
      });
    }
    icon = json["icon"] != null ? IconObject.fromJson(json["icon"]) : null;
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    if (food != null) {
      map["food"] = food.toJson();
    }
    if (mainCategory != null) {
      map["mainCategory"] = mainCategory.toJson();
    }
    if (ingredients != null) {
      map["ingredients"] = ingredients.map((v) => v.toJson()).toList();
    }
    if (images != null) {
      map["images"] = images.map((v) => v.toJson()).toList();
    }
    if (optionals != null) {
      map["optionals"] = optionals.map((v) => v.toJson()).toList();
    }
    if (additionals != null) {
      map["additionals"] = additionals.map((v) => v.toJson()).toList();
    }
    if (rates != null) {
      map["rates"] = rates.map((v) => v.toJson()).toList();
    }
    if (icon != null) {
      map["icon"] = icon.toJson();
    }
    return map;
  }

}
