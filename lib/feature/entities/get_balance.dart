class GetBalance {
  Balance object;
  bool status;
  String message;

  GetBalance({this.object, this.status, this.message});

  GetBalance.fromJson(Map<String, dynamic> json) {
    object =
        json['object'] != null ? new Balance.fromJson(json['object']) : null;
    status = json['status'];
    message = json['message'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.object != null) {
      data['object'] = this.object.toJson();
    }
    data['status'] = this.status;
    data['message'] = this.message;
    return data;
  }
}

class Balance {
  double balance;

  Balance({this.balance});

  Balance.fromJson(Map<String, dynamic> json) {
    balance = json['balance'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['balance'] = this.balance;
    return data;
  }
}
