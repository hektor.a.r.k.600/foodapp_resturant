import 'package:food_resturant/feature/entities/icon_object.dart';
import 'package:food_resturant/feature/entities/main_category.dart';

class GetFoodCategoriesEntity {
  List<MainCategory> getFoodCategoriesObject;
  int blockSize;
  bool status;
  dynamic message;

  GetFoodCategoriesEntity({
      this.getFoodCategoriesObject,
      this.blockSize, 
      this.status, 
      this.message});

  GetFoodCategoriesEntity.fromJson(dynamic json) {
    if (json["object"] != null) {
      getFoodCategoriesObject = [];
      json["object"].forEach((v) {
        getFoodCategoriesObject.add(MainCategory.fromJson(v));
      });
    }
    blockSize = json["blockSize"];
    status = json["status"];
    message = json["message"];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    if (getFoodCategoriesObject != null) {
      map["object"] = getFoodCategoriesObject.map((v) => v.toJson()).toList();
    }
    map["blockSize"] = blockSize;
    map["status"] = status;
    map["message"] = message;
    return map;
  }

}

