import 'package:food_resturant/feature/entities/ingredient.dart';

class GetFoodIngredientsEntity {
  List<GetFoodIngredientsObject> getFoodIngredientsObject;
  int blockSize;
  bool status;
  String message;

  GetFoodIngredientsEntity({
      this.getFoodIngredientsObject,
      this.blockSize, 
      this.status, 
      this.message});

  GetFoodIngredientsEntity.fromJson(dynamic json) {
    if (json["object"] != null) {
      getFoodIngredientsObject = [];
      json["object"].forEach((v) {
        getFoodIngredientsObject.add(GetFoodIngredientsObject.fromJson(v));
      });
    }
    blockSize = json["blockSize"];
    status = json["status"];
    message = json["message"];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    if (GetFoodIngredientsObject != null) {
      map["object"] = getFoodIngredientsObject.map((v) => v.toJson()).toList();
    }
    map["blockSize"] = blockSize;
    map["status"] = status;
    map["message"] = message;
    return map;
  }

}

class GetFoodIngredientsObject {
  Ingredients ingredient;
  String unit;
  double value;
  String id;
  int createdAt;

  GetFoodIngredientsObject({
      this.ingredient, 
      this.unit, 
      this.value, 
      this.id, 
      this.createdAt});

  GetFoodIngredientsObject.fromJson(dynamic json) {
    ingredient = json["ingredient"] != null ? Ingredients.fromJson(json["ingredient"]) : null;
    unit = json["unit"];
    value = json["value"];
    id = json["id"];
    createdAt = json["createdAt"];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    if (ingredient != null) {
      map["ingredient"] = ingredient.toJson();
    }
    map["unit"] = unit;
    map["value"] = value;
    map["id"] = id;
    map["createdAt"] = createdAt;
    return map;
  }

}

