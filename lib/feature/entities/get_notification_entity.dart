import 'package:food_resturant/feature/entities/entities.dart';

class GetNotificationEntity extends Entities{
  List<GetNotificationObject> getNotificationObject;
  int blockSize;
  bool status;
  String message;

  GetNotificationEntity({
      this.getNotificationObject,
      this.blockSize, 
      this.status, 
      this.message});

  GetNotificationEntity.fromJson(dynamic json) {
    if (json["object"] != null) {
      getNotificationObject = [];
      json["object"].forEach((v) {
        getNotificationObject.add(GetNotificationObject.fromJson(v));
      });
    }
    blockSize = json["blockSize"];
    status = json["status"];
    message = json["message"];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    if (getNotificationObject != null) {
      map["object"] = getNotificationObject.map((v) => v.toJson()).toList();
    }
    map["blockSize"] = blockSize;
    map["status"] = status;
    map["message"] = message;
    return map;
  }

}

class GetNotificationObject {
  String id;
  int createdAt;
  String description;
  String title;
  String link;
  int roleId;

  GetNotificationObject({
      this.id, 
      this.createdAt, 
      this.description, 
      this.title, 
      this.link, 
      this.roleId});

  GetNotificationObject.fromJson(dynamic json) {
    id = json["id"];
    createdAt = json["createdAt"];
    description = json["description"];
    title = json["title"];
    link = json["link"];
    roleId = json["roleId"];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["id"] = id;
    map["createdAt"] = createdAt;
    map["description"] = description;
    map["title"] = title;
    map["link"] = link;
    map["roleId"] = roleId;
    return map;
  }

}