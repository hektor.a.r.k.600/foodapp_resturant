import 'package:food_resturant/feature/entities/food.dart';
import 'package:food_resturant/feature/entities/get_orders_entity.dart';
import 'package:food_resturant/feature/entities/store.dart';
import 'package:food_resturant/feature/entities/table.dart';
import 'package:food_resturant/feature/entities/user.dart';
import 'package:food_resturant/library/enums/enums.dart';
import 'package:food_resturant/library/extention/int_extention.dart';
import 'package:food_resturant/library/extention/enum_extention.dart';

class GetOrderByIdEntity {
  GetOrderByIdObject getOrderByIdObject;
  bool status;
  String message;

  GetOrderByIdEntity({this.getOrderByIdObject, this.status, this.message});

  GetOrderByIdEntity.fromJson(dynamic json) {
    getOrderByIdObject = json["object"] != null
        ? GetOrderByIdObject.fromJson(json["object"])
        : null;
    status = json["status"];
    message = json["message"];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    if (getOrderByIdObject != null) {
      map["object"] = getOrderByIdObject.toJson();
    }
    map["status"] = status;
    map["message"] = message;
    return map;
  }
}

class GetOrderByIdObject {
  String id;
  int createdAt;
  User user;
  Table table;
  Store store;
  UserAddress userAddress;
  OrderStatus status;
  OrderTypes orderType;
  int peymentType;
  num totalPrice;
  String description;
  num timeToPrepare;
  num chashAprovalStatus;

  List<Food> foods;

  GetOrderByIdObject(
      {this.id,
      this.createdAt,
      this.user,
      this.table,
      this.store,
      this.userAddress,
      this.status,
      this.orderType,
      this.peymentType,
      this.totalPrice,
      this.description,
      this.timeToPrepare,
      this.chashAprovalStatus,
      this.foods});

  GetOrderByIdObject.fromJson(dynamic json) {
    id = json["id"];
    createdAt = json["createdAt"];
    user = json["user"] != null ? User.fromJson(json["user"]) : null;
    table = json["table"] != null ? Table.fromJson(json["table"]) : null;
    store = json["store"] != null ? Store.fromJson(json["store"]) : null;
    int statusInt = json["status"];
    status = statusInt.enumOrderStatus;

    int orderTypeInt = json["orderType"];
    orderType = orderTypeInt.enumOrderTypes;

    userAddress = json["userAddress"] != null
        ? UserAddress.fromJson(json["userAddress"])
        : null;
    peymentType = json["peymentType"];
    totalPrice = json["totalPrice"];
    chashAprovalStatus = json["chashAprovalStatus"];
    description = json["description"];
    timeToPrepare = json["timeToPrepare"];
    if (json["foods"] != null) {
      foods = [];
      json["foods"].forEach((v) {
        foods.add(Food.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["id"] = id;
    map["createdAt"] = createdAt;
    if (user != null) {
      map["user"] = user.toJson();
    }
    if (userAddress != null) {
      map["userAddress"] = userAddress.toJson();
    }
    if (table != null) {
      map["table"] = table.toJson();
    }
    if (store != null) {
      map["store"] = store.toJson();
    }
    map["status"] = status.orderStatusToInt;
    map["orderType"] = orderType.orderTypeToInt;
    map["peymentType"] = peymentType;
    map["totalPrice"] = totalPrice;
    map["description"] = description;
    map["chashAprovalStatus"] = chashAprovalStatus;
    map["timeToPrepare"] = timeToPrepare;
    if (foods != null) {
      map["foods"] = foods.map((v) => v.toJson()).toList();
    }
    return map;
  }
}
