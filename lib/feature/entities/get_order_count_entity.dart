import 'package:food_resturant/feature/entities/entities.dart';

class GetOrderCountEntity extends Entities{
  List<GetOrderCountObject> getOrderCountObject;
  bool status;
  String message;

  GetOrderCountEntity({
      this.getOrderCountObject,
      this.status, 
      this.message});

  GetOrderCountEntity.fromJson(dynamic json) {
    if (json["object"] != null) {
      getOrderCountObject = [];
      json["object"].forEach((v) {
        getOrderCountObject.add(GetOrderCountObject.fromJson(v));
      });
    }
    status = json["status"];
    message = json["message"];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    if (getOrderCountObject != null) {
      map["object"] = getOrderCountObject.map((v) => v.toJson()).toList();
    }
    map["status"] = status;
    map["message"] = message;
    return map;
  }

}

class GetOrderCountObject {
  String id;
  String userId;
  num price;

  GetOrderCountObject({
      this.id, 
      this.userId, 
      this.price});

  GetOrderCountObject.fromJson(dynamic json) {
    id = json["id"];
    userId = json["userId"];
    price = json["price"];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["id"] = id;
    map["userId"] = userId;
    map["price"] = price;
    return map;
  }

}