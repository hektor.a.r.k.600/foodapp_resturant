import 'package:food_resturant/feature/entities/entities.dart';
import 'package:food_resturant/feature/entities/food.dart';
import 'package:food_resturant/feature/entities/foods.dart';
import 'package:food_resturant/feature/entities/store.dart';
import 'package:food_resturant/feature/entities/table.dart';
import 'package:food_resturant/feature/entities/user.dart';
import 'package:food_resturant/library/enums/enums.dart';
import 'package:food_resturant/library/extention/int_extention.dart';

class GetOrdersByTableEntity {
  List<GetOrdersByTableObject> getOrdersByTableObject;
  int blockSize;
  bool status;
  String message;

  GetOrdersByTableEntity({
      this.getOrdersByTableObject,
      this.blockSize, 
      this.status, 
      this.message});

  GetOrdersByTableEntity.fromJson(dynamic json) {
    if (json["object"] != null) {
      getOrdersByTableObject = [];
      json["object"].forEach((v) {
        getOrdersByTableObject.add(GetOrdersByTableObject.fromJson(v));
      });
    }
    blockSize = json["blockSize"];
    status = json["status"];
    message = json["message"];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    if (getOrdersByTableObject != null) {
      map["object"] = getOrdersByTableObject.map((v) => v.toJson()).toList();
    }
    map["blockSize"] = blockSize;
    map["status"] = status;
    map["message"] = message;
    return map;
  }

}

class GetOrdersByTableObject {
  String id;
  int createdAt;
  User user;
  Table table;
  Store store;
  UserAddress userAddress;
  OrderStatus status;
  OrderTypes orderType;
  int peymentType;
  num totalPrice;
  String description;
  int timeToPrepare;
  List<Foods> foods;

  GetOrdersByTableObject({
      this.id, 
      this.createdAt, 
      this.user, 
      this.table, 
      this.store, 
      this.userAddress, 
      this.status, 
      this.orderType, 
      this.peymentType, 
      this.totalPrice, 
      this.description, 
      this.timeToPrepare, 
      this.foods});

  GetOrdersByTableObject.fromJson(dynamic json) {
    id = json["id"];
    createdAt = json["createdAt"];
    user = json["user"] != null ? User.fromJson(json["user"]) : null;
    table = json["table"] != null ? Table.fromJson(json["table"]) : null;
    store = json["store"] != null ? Store.fromJson(json["store"]) : null;
    userAddress = json["userAddress"] != null ? UserAddress.fromJson(json["userAddress"]) : null;

    int orderStatus = json["status"];
    status = orderStatus.enumOrderStatus;

    int order = json["orderType"];
    orderType = order.enumOrderTypes;



    peymentType = json["peymentType"];
    totalPrice = json["totalPrice"];
    description = json["description"];
    timeToPrepare = json["timeToPrepare"];
    if (json["foods"] != null) {
      foods = [];
      json["foods"].forEach((v) {
        foods.add(Foods.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["id"] = id;
    map["createdAt"] = createdAt;
    if (user != null) {
      map["user"] = user.toJson();
    }
    if (table != null) {
      map["table"] = table.toJson();
    }
    if (store != null) {
      map["store"] = store.toJson();
    }
    if (userAddress != null) {
      map["userAddress"] = userAddress.toJson();
    }
    map["status"] = status;
    map["orderType"] = orderType;
    map["peymentType"] = peymentType;
    map["totalPrice"] = totalPrice;
    map["description"] = description;
    map["timeToPrepare"] = timeToPrepare;
    if (foods != null) {
      map["foods"] = foods.map((v) => v.toJson()).toList();
    }
    return map;
  }

}


class UserAddress {
  User user;
  String title;
  String text;
  int lat;
  int lon;
  String id;
  int createdAt;

  UserAddress({
      this.user, 
      this.title, 
      this.text, 
      this.lat, 
      this.lon, 
      this.id, 
      this.createdAt});

  UserAddress.fromJson(dynamic json) {
    user = json["user"] != null ? User.fromJson(json["user"]) : null;
    title = json["title"];
    text = json["text"];
    lat = json["lat"];
    lon = json["lon"];
    id = json["id"];
    createdAt = json["createdAt"];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    if (user != null) {
      map["user"] = user.toJson();
    }
    map["title"] = title;
    map["text"] = text;
    map["lat"] = lat;
    map["lon"] = lon;
    map["id"] = id;
    map["createdAt"] = createdAt;
    return map;
  }

}
