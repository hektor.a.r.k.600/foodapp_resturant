import 'package:food_resturant/feature/entities/additional.dart';
import 'package:food_resturant/feature/entities/foods.dart';
import 'package:food_resturant/feature/entities/icon_object.dart';
import 'package:food_resturant/feature/entities/optional.dart';
import 'package:food_resturant/feature/entities/store.dart';
import 'package:food_resturant/feature/entities/table.dart';
import 'package:food_resturant/feature/entities/user.dart';
import 'package:food_resturant/library/enums/enums.dart';
import 'package:food_resturant/library/extention/int_extention.dart';

class GetOrdersEntity {
  List<GetOrdersObject> getOrdersObject;
  int blockSize;
  bool status;
  String message;

  GetOrdersEntity(
      {this.getOrdersObject, this.blockSize, this.status, this.message});

  GetOrdersEntity.fromJson(dynamic json) {
    if (json["object"] != null) {
      getOrdersObject = [];
      json["object"].forEach((v) {
        getOrdersObject.add(GetOrdersObject.fromJson(v));
      });
    }
    blockSize = json["blockSize"];
    status = json["status"];
    message = json["message"];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    if (getOrdersObject != null) {
      map["object"] = getOrdersObject.map((v) => v.toJson()).toList();
    }
    map["blockSize"] = blockSize;
    map["status"] = status;
    map["message"] = message;
    return map;
  }
}

class GetOrdersObject {
  String id;
  int createdAt;
  User user;
  Table table;
  String serialNo;
  UserAddress userAddress;
  OrderStatus status;
  OrderTypes orderType;
  num peymentType;
  num totalPrice;
  num totalDiscount;
  String description;
  num timeToPrepare;
  num chashAprovalStatus;
  List<Foods> foods;

  GetOrdersObject(
      {this.id,
      this.createdAt,
      this.user,
      this.serialNo,
      this.table,
      this.userAddress,
      this.status,
      this.orderType,
      this.peymentType,
      this.totalPrice,
      this.totalDiscount,
      this.description,
      this.timeToPrepare,
      this.foods});

  GetOrdersObject.fromJson(dynamic json) {
    id = json["id"];
    createdAt = json["createdAt"];
    user = json["user"] != null ? User.fromJson(json["user"]) : null;
    table = json["table"] != null ? Table.fromJson(json["table"]) : null;
    userAddress = json["userAddress"] != null
        ? UserAddress.fromJson(json["userAddress"])
        : null;
    int statusInt = json["status"];
    status = statusInt.enumOrderStatus;
    int type = json["orderType"];
    orderType = type.enumOrderTypes;
    peymentType = json["peymentType"];
    chashAprovalStatus = json["chashAprovalStatus"];
    serialNo = json["serialNo"];
    totalPrice = json["totalPrice"];
    totalDiscount = json["totalDiscount"];
    description = json["description"];
    timeToPrepare = json["timeToPrepare"];
    if (json["foods"] != null) {
      foods = [];
      json["foods"].forEach((v) {
        foods.add(Foods.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["id"] = id;
    map["createdAt"] = createdAt;
    if (user != null) {
      map["user"] = user.toJson();
    }
    if (table != null) {
      map["table"] = table.toJson();
    }
    if (userAddress != null) {
      map["userAddress"] = userAddress.toJson();
    }
    map["status"] = status;
    map["orderType"] = orderType;
    map["peymentType"] = peymentType;
    map["totalPrice"] = totalPrice;
    map["chashAprovalStatus"] = chashAprovalStatus;
    map["totalDiscount"] = totalDiscount;
    map["serialNo"] = serialNo;
    map["description"] = description;
    map["timeToPrepare"] = timeToPrepare;
    if (foods != null) {
      map["foods"] = foods.map((v) => v.toJson()).toList();
    }
    return map;
  }
}

class UserAddress {
  User user;
  String title;
  String text;
  num lat;
  num lon;
  String id;
  int createdAt;

  UserAddress(
      {this.user,
      this.title,
      this.text,
      this.lat,
      this.lon,
      this.id,
      this.createdAt});

  UserAddress.fromJson(dynamic json) {
    user = json["user"] != null ? User.fromJson(json["user"]) : null;
    title = json["title"];
    text = json["text"];
    lat = json["lat"];
    lon = json["lon"];
    id = json["id"];
    createdAt = json["createdAt"];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    if (user != null) {
      map["user"] = user.toJson();
    }
    map["title"] = title;
    map["text"] = text;
    map["lat"] = lat;
    map["lon"] = lon;
    map["id"] = id;
    map["createdAt"] = createdAt;
    return map;
  }
}
