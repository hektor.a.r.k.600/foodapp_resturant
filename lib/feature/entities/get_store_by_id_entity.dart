import 'package:food_resturant/feature/entities/cover_image.dart';
import 'package:food_resturant/feature/entities/entities.dart';
import 'package:food_resturant/feature/entities/get_store_rate_entity.dart';
import 'package:food_resturant/feature/entities/icon_object.dart';

class GetStoreByIdEntity extends Entities{
  GetStoreByIdObject getStoreByIdObject;
  bool status;
  String message;

  GetStoreByIdEntity({
      this.getStoreByIdObject,
      this.status, 
      this.message});

  GetStoreByIdEntity.fromJson(dynamic json) {
    getStoreByIdObject = json["object"] != null ? GetStoreByIdObject.fromJson(json["object"]) : null;
    status = json["status"];
    message = json["message"];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    if (getStoreByIdObject != null) {
      map["object"] = getStoreByIdObject.toJson();
    }
    map["status"] = status;
    map["message"] = message;
    return map;
  }

}

class GetStoreByIdObject {
  String name;
  String address;
  PaymentInfo paymentInfo;
  String descriptions;
  int openingHour;
  int openingMinute;
  int closingHour;
  int closingMinute;
  int status;
  int lat;
  int lon;
  IconObject icon;
  CoverImage coverImage;
  List<Rate> rate;
  String id;
  int createdAt;

  GetStoreByIdObject({
      this.name, 
      this.address, 
      this.paymentInfo, 
      this.descriptions, 
      this.openingHour, 
      this.openingMinute, 
      this.closingHour, 
      this.closingMinute, 
      this.status, 
      this.lat, 
      this.lon, 
      this.icon, 
      this.coverImage, 
      this.rate, 
      this.id, 
      this.createdAt});

  GetStoreByIdObject.fromJson(dynamic json) {
    name = json["name"];
    address = json["address"];
    paymentInfo = json["paymentInfo"] != null ? PaymentInfo.fromJson(json["paymentInfo"]) : null;
    descriptions = json["descriptions"];
    openingHour = json["openingHour"];
    openingMinute = json["openingMinute"];
    closingHour = json["closingHour"];
    closingMinute = json["closingMinute"];
    status = json["status"];
    lat = json["lat"];
    lon = json["lon"];
    icon = json["icon"] != null ? IconObject.fromJson(json["icon"]) : null;
    coverImage = json["coverImage"] != null ? CoverImage.fromJson(json["coverImage"]) : null;
    if (json["rate"] != null) {
      rate = [];
      json["rate"].forEach((v) {
        rate.add(Rate.fromJson(v));
      });
    }
    id = json["id"];
    createdAt = json["createdAt"];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["name"] = name;
    map["address"] = address;
    if (paymentInfo != null) {
      map["paymentInfo"] = paymentInfo.toJson();
    }
    map["descriptions"] = descriptions;
    map["openingHour"] = openingHour;
    map["openingMinute"] = openingMinute;
    map["closingHour"] = closingHour;
    map["closingMinute"] = closingMinute;
    map["status"] = status;
    map["lat"] = lat;
    map["lon"] = lon;
    if (icon != null) {
      map["icon"] = icon.toJson();
    }
    if (coverImage != null) {
      map["coverImage"] = coverImage.toJson();
    }
    if (rate != null) {
      map["rate"] = rate.map((v) => v.toJson()).toList();
    }
    map["id"] = id;
    map["createdAt"] = createdAt;
    return map;
  }

}





class PaymentInfo {
  String bankName;
  String accountHolderName;
  String accountNumber;

  PaymentInfo({
      this.bankName, 
      this.accountHolderName, 
      this.accountNumber});

  PaymentInfo.fromJson(dynamic json) {
    bankName = json["bankName"];
    accountHolderName = json["accountHolderName"];
    accountNumber = json["accountNumber"];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["bankName"] = bankName;
    map["accountHolderName"] = accountHolderName;
    map["accountNumber"] = accountNumber;
    return map;
  }

}