import 'package:food_resturant/feature/entities/entities.dart';
import 'package:food_resturant/feature/entities/food.dart';
import 'package:food_resturant/feature/entities/full_food_obj.dart';
import 'package:food_resturant/feature/entities/icon_object.dart';
import 'package:food_resturant/feature/entities/image_obj.dart';
import 'package:food_resturant/feature/entities/store.dart';

class GetStoreFoodByIdEntity extends Entities{
  GetStoreFoodByIdObject getStoreFoodByIdObject;
  bool status;
  String message;

  GetStoreFoodByIdEntity({
      this.getStoreFoodByIdObject,
      this.status, 
      this.message});

  GetStoreFoodByIdEntity.fromJson(dynamic json) {
    getStoreFoodByIdObject = json["object"] != null ? GetStoreFoodByIdObject.fromJson(json["object"]) : null;
    status = json["status"];
    message = json["message"];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    if (getStoreFoodByIdObject != null) {
      map["object"] = getStoreFoodByIdObject.toJson();
    }
    map["status"] = status;
    map["message"] = message;
    return map;
  }

}

class GetStoreFoodByIdObject {
  Store store;
  FullFoodObj food;
  String id;
  int createdAt;

  GetStoreFoodByIdObject({
      this.store, 
      this.food, 
      this.id, 
      this.createdAt});

  GetStoreFoodByIdObject.fromJson(dynamic json) {
    store = json["store"] != null ? Store.fromJson(json["store"]) : null;
    food = json["food"] != null ? FullFoodObj.fromJson(json["food"]) : null;
    id = json["id"];
    createdAt = json["createdAt"];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    if (store != null) {
      map["store"] = store.toJson();
    }
    if (food != null) {
      map["food"] = food.toJson();
    }
    map["id"] = id;
    map["createdAt"] = createdAt;
    return map;
  }

}







