import 'package:food_resturant/feature/entities/category.dart';
import 'package:food_resturant/feature/entities/icon_object.dart';

class GetStoreFoodEntity {
  List<GetStoreFoodObject> getStoreFoodObject;
  int blockSize;
  bool status;
  String message;

  GetStoreFoodEntity({
      this.getStoreFoodObject,
      this.blockSize, 
      this.status, 
      this.message});

  GetStoreFoodEntity.fromJson(dynamic json) {
    if (json["object"] != null) {
      getStoreFoodObject = [];
      json["object"].forEach((v) {
        getStoreFoodObject.add(GetStoreFoodObject.fromJson(v));
      });
    }
    blockSize = json["blockSize"];
    status = json["status"];
    message = json["message"];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    if (getStoreFoodObject != null) {
      map["object"] = getStoreFoodObject.map((v) => v.toJson()).toList();
    }
    map["blockSize"] = blockSize;
    map["status"] = status;
    map["message"] = message;
    return map;
  }

}

class GetStoreFoodObject {
  String name;
  Category category;
  IconObject icon;
  num totalPrice;
  int cookingTime;
  int status;
  double discountValue;
  String id;
  int createdAt;
  bool isItemChecked = false;

  GetStoreFoodObject({
      this.name, 
      this.category, 
      this.icon, 
      this.totalPrice, 
      this.cookingTime, 
      this.status, 
      this.discountValue, 
      this.id, 
      this.createdAt});

  GetStoreFoodObject.fromJson(dynamic json) {
    name = json["name"];
    category = json["category"] != null ? Category.fromJson(json["category"]) : null;
    icon = json["icon"] != null ? IconObject.fromJson(json["icon"]) : null;
    totalPrice = json["totalPrice"];
    cookingTime = json["cookingTime"];
    status = json["status"];
    discountValue = json["discountValue"];
    id = json["id"];
    createdAt = json["createdAt"];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["name"] = name;
    if (category != null) {
      map["category"] = category.toJson();
    }
    if (icon != null) {
      map["icon"] = icon.toJson();
    }
    map["totalPrice"] = totalPrice;
    map["cookingTime"] = cookingTime;
    map["status"] = status;
    map["discountValue"] = discountValue;
    map["id"] = id;
    map["createdAt"] = createdAt;
    return map;
  }

}
