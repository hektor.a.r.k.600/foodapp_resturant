import 'package:food_resturant/feature/entities/Voter.dart';
import 'package:food_resturant/feature/entities/entities.dart';
import 'package:food_resturant/feature/entities/store.dart';

class GetStoreRateEntity extends Entities {
  List<GetStoreRateEntityObject> getStoreRateEntityObject;
  int blockSize;
  bool status;
  String message;

  GetStoreRateEntity(
      {this.getStoreRateEntityObject,
      this.blockSize,
      this.status,
      this.message});

  GetStoreRateEntity.fromJson(dynamic json) {
    if (json["object"] != null) {
      getStoreRateEntityObject = [];
      json["object"].forEach((v) {
        getStoreRateEntityObject.add(GetStoreRateEntityObject.fromJson(v));
      });
    }
    blockSize = json["blockSize"];
    status = json["status"];
    message = json["message"];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    if (getStoreRateEntityObject != null) {
      map["object"] = getStoreRateEntityObject.map((v) => v.toJson()).toList();
    }
    map["blockSize"] = blockSize;
    map["status"] = status;
    map["message"] = message;
    return map;
  }
}

class GetStoreRateEntityObject {
  Store store;
  Rate rate;
  Order order;
  Answer answer;
  String id;
  int createdAt;

  GetStoreRateEntityObject(
      {this.store,
      this.rate,
      this.order,
      this.answer,
      this.id,
      this.createdAt});

  GetStoreRateEntityObject.fromJson(dynamic json) {
    store = json["store"] != null ? Store.fromJson(json["store"]) : null;
    rate = json["rate"] != null ? Rate.fromJson(json["rate"]) : null;
    order = json["order"] != null ? Order.fromJson(json["order"]) : null;
    answer = json["answer"] != null ? Answer.fromJson(json["answer"]) : null;
    id = json["id"];
    createdAt = json["createdAt"];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    if (store != null) {
      map["store"] = store.toJson();
    }
    if (rate != null) {
      map["rate"] = rate.toJson();
    }
    if (order != null) {
      map["order"] = order.toJson();
    }
    if (answer != null) {
      map["answer"] = answer.toJson();
    }
    map["id"] = id;
    map["createdAt"] = createdAt;
    return map;
  }
}

class Answer {
  String title;
  int rateValue;
  String comment;
  int status;
  Voter voter;

  Answer({this.title, this.rateValue, this.comment, this.status, this.voter});

  Answer.fromJson(dynamic json) {
    title = json["title"];
    rateValue = json["rateValue"];
    comment = json["comment"];
    status = json["status"];
    voter = json["voter"] != null ? Voter.fromJson(json["voter"]) : null;
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["title"] = title;
    map["rateValue"] = rateValue;
    map["comment"] = comment;
    map["status"] = status;
    if (voter != null) {
      map["voter"] = voter.toJson();
    }
    return map;
  }
}


class Order {
  String id;
  String userId;
  num price;

  Order({this.id, this.userId, this.price});

  Order.fromJson(dynamic json) {
    id = json["id"];
    userId = json["userId"];
    price = json["price"];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["id"] = id;
    map["userId"] = userId;
    map["price"] = price;
    return map;
  }
}

class Rate {
  String title;
  num rateValue;
  String comment;
  int status;
  Voter voter;

  Rate({this.title, this.rateValue, this.comment, this.status, this.voter});

  Rate.fromJson(dynamic json) {
    title = json["title"];
    rateValue = json["rateValue"];
    comment = json["comment"];
    status = json["status"];
    voter = json["voter"] != null ? Voter.fromJson(json["voter"]) : null;
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["title"] = title;
    map["rateValue"] = rateValue;
    map["comment"] = comment;
    map["status"] = status;
    if (voter != null) {
      map["voter"] = voter.toJson();
    }
    return map;
  }
}
