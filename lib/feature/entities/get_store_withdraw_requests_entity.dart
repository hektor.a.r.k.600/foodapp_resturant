
import 'package:food_resturant/feature/entities/entities.dart';
import 'package:food_resturant/feature/entities/store.dart';
import 'package:food_resturant/library/enums/enums.dart';
import 'package:food_resturant/library/extention/int_extention.dart';

class GetStoreWithDrawRequestsEntity extends Entities{
  List<WithDrawGetStoreWithDrawRequestsObject> withDrawGetStoreWithDrawRequestsObject;
  int blockSize;
  bool status;
  String message;

  GetStoreWithDrawRequestsEntity({
      this.withDrawGetStoreWithDrawRequestsObject,
      this.blockSize, 
      this.status, 
      this.message});

  GetStoreWithDrawRequestsEntity.fromJson(dynamic json) {
    if (json["object"] != null) {
      withDrawGetStoreWithDrawRequestsObject = [];
      json["object"].forEach((v) {
        withDrawGetStoreWithDrawRequestsObject.add(WithDrawGetStoreWithDrawRequestsObject.fromJson(v));
      });
    }
    blockSize = json["blockSize"];
    status = json["status"];
    message = json["message"];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    if (withDrawGetStoreWithDrawRequestsObject != null) {
      map["object"] = withDrawGetStoreWithDrawRequestsObject.map((v) => v.toJson()).toList();
    }
    map["blockSize"] = blockSize;
    map["status"] = status;
    map["message"] = message;
    return map;
  }

}

class WithDrawGetStoreWithDrawRequestsObject {
  String id;
  int createdAt;
  Store store;
  int value;
  WithdrawStatus status;
  String reason;

  WithDrawGetStoreWithDrawRequestsObject({
      this.id, 
      this.createdAt, 
      this.store, 
      this.value, 
      this.status, 
      this.reason});

  WithDrawGetStoreWithDrawRequestsObject.fromJson(dynamic json) {
    id = json["id"];
    createdAt = json["createdAt"];
    store = json["store"] != null ? Store.fromJson(json["store"]) : null;
    value = json["value"];
    int statusInt = json["status"];
    status = statusInt.enumWithdrawStatus;
    reason = json["reason"];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["id"] = id;
    map["createdAt"] = createdAt;
    if (store != null) {
      map["store"] = store.toJson();
    }
    map["value"] = value;
    map["status"] = status;
    map["reason"] = reason;
    return map;
  }

}
