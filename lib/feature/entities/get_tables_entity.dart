import 'package:food_resturant/library/enums/enums.dart';
import 'package:food_resturant/library/extention/int_extention.dart';

class GetTablesEntity {
  List<TableObject> tableObject;
  int blockSize;
  bool status;
  String message;

  GetTablesEntity({this.tableObject, this.blockSize, this.status, this.message});

  GetTablesEntity.fromJson(Map<String, dynamic> json) {
    if (json['object'] != null) {
      tableObject = new List<TableObject>();
      json['object'].forEach((v) {
        tableObject.add(new TableObject.fromJson(v));
      });
    }
    blockSize = json['blockSize'];
    status = json['status'];
    message = json['message'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.tableObject != null) {
      data['object'] = this.tableObject.map((v) => v.toJson()).toList();
    }
    data['blockSize'] = this.blockSize;
    data['status'] = this.status;
    data['message'] = this.message;
    return data;
  }
}

class TableObject {
  String id;
  int no;
  int sitsCount;
  TableStatus status;
  String name;
  QrCodeImage qrCodeImage;

  TableObject(
      {this.id,
        this.no,
        this.sitsCount,
        this.status,
        this.name,
        this.qrCodeImage});

  TableObject.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    no = json['no'];
    sitsCount = json['sitsCount'];
    int statusInt = json["status"];
    status = statusInt.enumTableStatus;    name = json['name'];
    qrCodeImage = json['qrCodeImage'] != null
        ? new QrCodeImage.fromJson(json['qrCodeImage'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['no'] = this.no;
    data['sitsCount'] = this.sitsCount;
    data['status'] = this.status;
    data['name'] = this.name;
    if (this.qrCodeImage != null) {
      data['qrCodeImage'] = this.qrCodeImage.toJson();
    }
    return data;
  }
}

class QrCodeImage {
  String id;
  String location;
  int type;

  QrCodeImage({this.id, this.location, this.type});

  QrCodeImage.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    location = json['location'];
    type = json['type'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['location'] = this.location;
    data['type'] = this.type;
    return data;
  }
}
