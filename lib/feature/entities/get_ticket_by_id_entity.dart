import 'package:food_resturant/feature/entities/entities.dart';
import 'package:food_resturant/feature/entities/store.dart';
import 'package:food_resturant/feature/entities/ticket.dart';

class GetTicketByIdEntity extends Entities{
  GetTicketByIdObject getTicketByIdObject;
  bool status;
  String message;

  GetTicketByIdEntity({
      this.getTicketByIdObject,
      this.status, 
      this.message});

  GetTicketByIdEntity.fromJson(dynamic json) {
    getTicketByIdObject = json["object"] != null ? GetTicketByIdObject.fromJson(json["object"]) : null;
    status = json["status"];
    message = json["message"];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    if (getTicketByIdObject != null) {
      map["object"] = getTicketByIdObject.toJson();
    }
    map["status"] = status;
    map["message"] = message;
    return map;
  }

}

class GetTicketByIdObject {
  Store store;
  Ticket ticket;
  int status;

  GetTicketByIdObject({
      this.store, 
      this.ticket, 
      this.status});

  GetTicketByIdObject.fromJson(dynamic json) {
    store = json["store"] != null ? Store.fromJson(json["store"]) : null;
    ticket = json["ticket"] != null ? Ticket.fromJson(json["ticket"]) : null;
    status = json["status"];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    if (store != null) {
      map["store"] = store.toJson();
    }
    if (ticket != null) {
      map["ticket"] = ticket.toJson();
    }
    map["status"] = status;
    return map;
  }

}



