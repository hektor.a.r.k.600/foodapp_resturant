import 'package:food_resturant/feature/entities/entities.dart';
import 'package:food_resturant/feature/entities/messages.dart';
import 'package:food_resturant/feature/entities/store.dart';
import 'package:food_resturant/feature/entities/ticket.dart';

class GetTicketEntity extends Entities{
  List<GetTicketObject> getTicketEntityObject;
  int blockSize;
  bool status;
  String message;

  GetTicketEntity({
    this.getTicketEntityObject,
    this.blockSize,
    this.status,
    this.message});

  GetTicketEntity.fromJson(dynamic json) {
    if (json["object"] != null) {
      getTicketEntityObject = [];
      json["object"].forEach((v) {
        getTicketEntityObject.add(GetTicketObject.fromJson(v));
      });
    }
    blockSize = json["blockSize"];
    status = json["status"];
    message = json["message"];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    if (object != null) {
      map["object"] = object.map((v) => v.toJson()).toList();
    }
    map["blockSize"] = blockSize;
    map["status"] = status;
    map["message"] = message;
    return map;
  }

}

class GetTicketObject {
  String id;
  Store store;
  Ticket ticket;
  int status;
  int createAt;

  GetTicketObject({
    this.id,
    this.store,
    this.ticket,
    this.status,
    this.createAt});

  GetTicketObject.fromJson(dynamic json) {
    id = json["id"];
    store = json["store"] != null ? Store.fromJson(json["store"]) : null;
    ticket = json["ticket"] != null ? Ticket.fromJson(json["ticket"]) : null;
    status = json["status"];
    createAt = json["createAt"];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["id"] = id;
    if (store != null) {
      map["store"] = store.toJson();
    }
    if (ticket != null) {
      map["ticket"] = ticket.toJson();
    }
    map["status"] = status;
    map["createAt"] = createAt;
    return map;
  }

}




