
import 'package:food_resturant/feature/entities/entities.dart';
import 'package:food_resturant/feature/entities/get_user_object.dart';

class GetUserByIdEntity extends Entities{
  GetUsersObject getUsersObject;
  bool status;
  String message;

  GetUserByIdEntity({
      this.getUsersObject,
      this.status, 
      this.message});

  GetUserByIdEntity.fromJson(dynamic json) {
    getUsersObject = json["object"] != null ? GetUsersObject.fromJson(json["object"]) : null;
    status = json["status"];
    message = json["message"];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    if (getUsersObject != null) {
      map["object"] = getUsersObject.toJson();
    }
    map["status"] = status;
    map["message"] = message;
    return map;
  }

}
