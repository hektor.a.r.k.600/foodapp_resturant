import 'package:food_resturant/feature/entities/profile_image.dart';
import 'package:food_resturant/feature/entities/store.dart';
import 'package:food_resturant/feature/entities/token.dart';
import 'package:food_resturant/library/enums/enums.dart';
import 'package:food_resturant/library/extention/int_extention.dart';

class GetUsersObject {
  String id;
  String userName;
  String name;
  String familyName;
  int birthday;
  String mobile;
  int createdAt;
  int gender;
  Token token;
  ProfileImage profileImage;
  String email;
  int status;
  Store store;
  UserRole roleId;
  String description;

  GetUsersObject({
    this.id,
    this.userName,
    this.name,
    this.familyName,
    this.birthday,
    this.mobile,
    this.createdAt,
    this.gender,
    this.token,
    this.profileImage,
    this.email,
    this.status,
    this.store,
    this.roleId,
    this.description});

  GetUsersObject.fromJson(dynamic json) {
    id = json["id"];
    userName = json["userName"];
    name = json["name"];
    familyName = json["familyName"];
    birthday = json["birthday"];
    mobile = json["mobile"];
    createdAt = json["createdAt"];
    gender = json["gender"];
    token = json["token"] != null ? Token.fromJson(json["token"]) : null;
    profileImage = json["profileImage"] != null ? ProfileImage.fromJson(json["profileImage"]) : null;
    email = json["email"];
    status = json["status"];
    store = json["store"] != null ? Store.fromJson(json["store"]) : null;
    int role = json["roleId"];
    roleId = role.enumUserRole;
    description = json["description"];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["id"] = id;
    map["userName"] = userName;
    map["name"] = name;
    map["familyName"] = familyName;
    map["birthday"] = birthday;
    map["mobile"] = mobile;
    map["createdAt"] = createdAt;
    map["gender"] = gender;
    if (token != null) {
      map["token"] = token.toJson();
    }
    if (profileImage != null) {
      map["profileImage"] = profileImage.toJson();
    }
    map["email"] = email;
    map["status"] = status;
    if (store != null) {
      map["store"] = store.toJson();
    }
    map["roleId"] = roleId.index;
    map["description"] = description;
    return map;
  }

}
