import 'package:food_resturant/feature/entities/get_user_object.dart';
import 'package:food_resturant/feature/entities/store.dart';
import 'package:food_resturant/library/enums/enums.dart';
import 'package:food_resturant/library/extention/int_extention.dart';

class GetUsersEntity {
  List<GetUsersObject> object;
  int blockSize;
  bool status;
  String message;

  GetUsersEntity({
      this.object, 
      this.blockSize, 
      this.status, 
      this.message});

  GetUsersEntity.fromJson(dynamic json) {
    if (json["object"] != null) {
      object = [];
      json["object"].forEach((v) {
        object.add(GetUsersObject.fromJson(v));
      });
    }
    blockSize = json["blockSize"];
    status = json["status"];
    message = json["message"];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    if (object != null) {
      map["object"] = object.map((v) => v.toJson()).toList();
    }
    map["blockSize"] = blockSize;
    map["status"] = status;
    map["message"] = message;
    return map;
  }

}




