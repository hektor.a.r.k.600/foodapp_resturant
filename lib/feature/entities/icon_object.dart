class IconObject {
  String id;
  String location;
  int type;

  IconObject({
    this.id,
    this.location,
    this.type});

  IconObject.fromJson(dynamic json) {
    id = json["id"];
    location = json["location"];
    type = json["type"];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["id"] = id;
    map["location"] = location;
    map["type"] = type;
    return map;
  }

}