class Ingredients {
  Ingredient ingredient;
  String unit;
  num value;
  String id;
  int createdAt;

  Ingredients(
      {this.ingredient, this.unit, this.value, this.id, this.createdAt});

  Ingredients.fromJson(Map<String, dynamic> json) {
    ingredient = json['ingredient'] != null
        ? new Ingredient.fromJson(json['ingredient'])
        : null;
    unit = json['unit'];
    value = json['value'];
    id = json['id'];
    createdAt = json['createdAt'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.ingredient != null) {
      data['ingredient'] = this.ingredient.toJson();
    }
    data['unit'] = this.unit;
    data['value'] = this.value;
    data['id'] = this.id;
    data['createdAt'] = this.createdAt;
    return data;
  }
}

class Ingredient {
  String name;

  Ingredient({this.name});

  Ingredient.fromJson(Map<String, dynamic> json) {
    name = json['name'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['name'] = this.name;
    return data;
  }
}
