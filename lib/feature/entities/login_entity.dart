import 'package:food_resturant/feature/entities/store.dart';

class LoginEntity {
  LoginObject loginObject;
  bool status;
  String message;

  LoginEntity({
      this.loginObject,
      this.status, 
      this.message});

  LoginEntity.fromJson(dynamic json) {
    loginObject = json["object"] != null ? LoginObject.fromJson(json["object"]) : null;
    status = json["status"];
    message = json["message"];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    if (loginObject != null) {
      map["object"] = loginObject.toJson();
    }
    map["status"] = status;
    map["message"] = message;
    return map;
  }

}

class LoginObject {
  String id;
  String userName;
  String name;
  String familyName;
  int birthday;
  String mobile;
  int createdAt;
  int gender;
  Token token;
  ProfileImage profileImage;
  String email;
  int status;
  Store store;
  int roleId;
  String description;

  LoginObject({
      this.id, 
      this.userName, 
      this.name, 
      this.familyName, 
      this.birthday, 
      this.mobile, 
      this.createdAt, 
      this.gender, 
      this.token, 
      this.profileImage, 
      this.email, 
      this.status, 
      this.store, 
      this.roleId, 
      this.description});

  LoginObject.fromJson(dynamic json) {
    id = json["id"];
    userName = json["userName"];
    name = json["name"];
    familyName = json["familyName"];
    birthday = json["birthday"];
    mobile = json["mobile"];
    createdAt = json["createdAt"];
    gender = json["gender"];
    token = json["token"] != null ? Token.fromJson(json["token"]) : null;
    profileImage = json["profileImage"] != null ? ProfileImage.fromJson(json["profileImage"]) : null;
    email = json["email"];
    status = json["status"];
    store = json["store"] != null ? Store.fromJson(json["store"]) : null;
    roleId = json["roleId"];
    description = json["description"];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["id"] = id;
    map["userName"] = userName;
    map["name"] = name;
    map["familyName"] = familyName;
    map["birthday"] = birthday;
    map["mobile"] = mobile;
    map["createdAt"] = createdAt;
    map["gender"] = gender;
    if (token != null) {
      map["token"] = token.toJson();
    }
    if (profileImage != null) {
      map["profileImage"] = profileImage.toJson();
    }
    map["email"] = email;
    map["status"] = status;
    if (store != null) {
      map["store"] = store.toJson();
    }
    map["roleId"] = roleId;
    map["description"] = description;
    return map;
  }

}

class ProfileImage {
  String address;
  String id;

  ProfileImage({
      this.address, 
      this.id});

  ProfileImage.fromJson(dynamic json) {
    address = json["address"];
    id = json["id"];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["address"] = address;
    map["id"] = id;
    return map;
  }

}

class Token {
  String token;
  int expires;

  Token({
      this.token, 
      this.expires});

  Token.fromJson(dynamic json) {
    token = json["token"];
    expires = json["expires"];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["token"] = token;
    map["expires"] = expires;
    return map;
  }

}