import 'package:food_resturant/feature/entities/icon_object.dart';

class MainCategory {
  String name;
  IconObject icon;
  int foodCount;
  String id;
  int createdAt;

  MainCategory({
    this.name,
    this.icon,
    this.foodCount,
    this.id,
    this.createdAt});

  MainCategory.fromJson(dynamic json) {
    name = json["name"];
    icon = json["icon"] != null ? IconObject.fromJson(json["icon"]) : null;
    foodCount = json["foodCount"];
    id = json["id"];
    createdAt = json["createdAt"];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["name"] = name;
    if (icon != null) {
      map["icon"] = icon.toJson();
    }
    map["foodCount"] = foodCount;
    map["id"] = id;
    map["createdAt"] = createdAt;
    return map;
  }

}
