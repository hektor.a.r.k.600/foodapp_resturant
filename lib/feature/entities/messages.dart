import 'package:food_resturant/feature/entities/file.dart';

class Messages {
  String title;
  String text;
  FileDto file;
  bool byAdmin;

  Messages({
    this.title,
    this.text,
    this.file,
    this.byAdmin});

  Messages.fromJson(dynamic json) {
    title = json["title"];
    text = json["text"];
    file = json["file"] != null ? FileDto.fromJson(json["file"]) : null;
    byAdmin = json["byAdmin"];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["title"] = title;
    map["text"] = text;
    if (file != null) {
      map["file"] = file.toJson();
    }
    map["byAdmin"] = byAdmin;
    return map;
  }

}
