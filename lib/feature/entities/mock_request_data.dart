
import 'package:food_resturant/feature/entities/entities.dart';

class MockRequestData extends Entities<List<MockRequestDataObject>>  {
  List<MockRequestDataObject> getOrdersObject;
  bool status;
  String massage;

  MockRequestData({this.status, this.massage, this.getOrdersObject});

  MockRequestData.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    massage = json['massage'];
    if (json['object'] != null) {
      getOrdersObject = new List<MockRequestDataObject>();
      json['object'].forEach((v) {
        getOrdersObject.add(new MockRequestDataObject.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    data['massage'] = this.massage;
    if (this.getOrdersObject != null) {
      data['object'] = this.getOrdersObject.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class MockRequestDataObject {
  String orders;
  StartPosition startPosition;
  StartPosition endPosition;
  String image;

  MockRequestDataObject({this.orders, this.startPosition, this.endPosition, this.image});

  MockRequestDataObject.fromJson(Map<String, dynamic> json) {
    orders = json['orders'];
    startPosition = json['startPosition'] != null
        ? new StartPosition.fromJson(json['startPosition'])
        : null;
    endPosition = json['endPosition'] != null
        ? new StartPosition.fromJson(json['endPosition'])
        : null;
    image = json['image'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['orders'] = this.orders;
    if (this.startPosition != null) {
      data['startPosition'] = this.startPosition.toJson();
    }
    if (this.endPosition != null) {
      data['endPosition'] = this.endPosition.toJson();
    }
    data['image'] = this.image;
    return data;
  }
}

class StartPosition {
  double lat;
  double lon;

  StartPosition({this.lat, this.lon});

  StartPosition.fromJson(Map<String, dynamic> json) {
    lat = json['lat'];
    lon = json['lon'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['lat'] = this.lat;
    data['lon'] = this.lon;
    return data;
  }
}
