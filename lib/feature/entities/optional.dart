class Optionals {
  String name;
  num price;
  String id;
  int createdAt;

  Optionals({
    this.name,
    this.price,
    this.id,
    this.createdAt});

  Optionals.fromJson(dynamic json) {
    name = json["name"];
    price = json["price"];
    id = json["id"];
    createdAt = json["createdAt"];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["name"] = name;
    map["price"] = price;
    map["id"] = id;
    map["createdAt"] = createdAt;
    return map;
  }

}
