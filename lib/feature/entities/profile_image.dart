class ProfileImage {
  String address;
  String id;

  ProfileImage({
    this.address,
    this.id});

  ProfileImage.fromJson(dynamic json) {
    address = json["address"];
    id = json["id"];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["address"] = address;
    map["id"] = id;
    return map;
  }

}
