import 'package:food_resturant/feature/entities/entities.dart';

class PublicEntity extends Entities {
  bool status;
  String message;

  PublicEntity({this.status, this.message});

  PublicEntity.fromJson(dynamic json) {
    status = json["status"];
    message = json["message"];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["status"] = status;
    map["message"] = message;
    return map;
  }
}
