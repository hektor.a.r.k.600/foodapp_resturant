import 'package:food_resturant/feature/entities/Voter.dart';

class Rates {
  int rateValue;
  String comment;
  int status;
  Voter voter;
  String id;
  int createdAt;

  Rates({
    this.rateValue,
    this.comment,
    this.status,
    this.voter,
    this.id,
    this.createdAt});

  Rates.fromJson(dynamic json) {
    rateValue = json["rateValue"];
    comment = json["comment"];
    status = json["status"];
    voter = json["voter"] != null ? Voter.fromJson(json["voter"]) : null;
    id = json["id"];
    createdAt = json["createdAt"];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["rateValue"] = rateValue;
    map["comment"] = comment;
    map["status"] = status;
    if (voter != null) {
      map["voter"] = voter.toJson();
    }
    map["id"] = id;
    map["createdAt"] = createdAt;
    return map;
  }

}
