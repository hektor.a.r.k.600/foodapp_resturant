import 'package:food_resturant/feature/entities/icon_object.dart';

class Store {
  String id;
  String name;
  IconObject icon;

  Store({
    this.id,
    this.name,
    this.icon});

  Store.fromJson(dynamic json) {
    id = json["id"];
    name = json["name"];
    icon = json["icon"] != null ? IconObject.fromJson(json["icon"]) : null;
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["id"] = id;
    map["name"] = name;
    if (icon != null) {
      map["icon"] = icon.toJson();
    }
    return map;
  }

}