import 'package:food_resturant/feature/entities/entities.dart';

import 'get_store_rate_entity.dart';
import 'store.dart';
import 'user.dart';

class StoreGetStoreTransactionsEntity extends Entities {
  List<StoreGetStoreTransactionsObject> storeGetStoreTransactionsEntityObject;
  int blockSize;
  bool status;
  String message;

  StoreGetStoreTransactionsEntity(
      {this.storeGetStoreTransactionsEntityObject,
      this.blockSize,
      this.status,
      this.message});

  StoreGetStoreTransactionsEntity.fromJson(dynamic json) {
    if (json["object"] != null) {
      storeGetStoreTransactionsEntityObject = [];
      json["object"].forEach((v) {
        storeGetStoreTransactionsEntityObject
            .add(StoreGetStoreTransactionsObject.fromJson(v));
      });
    }
    blockSize = json["blockSize"];
    status = json["status"];
    message = json["message"];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    if (storeGetStoreTransactionsEntityObject != null) {
      map["object"] =
          storeGetStoreTransactionsEntityObject.map((v) => v.toJson()).toList();
    }
    map["blockSize"] = blockSize;
    map["status"] = status;
    map["message"] = message;
    return map;
  }
}

class StoreGetStoreTransactionsObject {
  User user;
  Store store;
  Order order;
  bool isSuccessful;
  String description;
  num commissionPrice;
  num totalPrice;
  num balance;
  num totalPriceWithDiscount;
  num totalDiscount;
  num peymentType;
  num status;
  num widthrawlAmount;
  String id;
  num createdAt;

  StoreGetStoreTransactionsObject(
      {this.user,
      this.store,
      this.order,
      this.isSuccessful,
      this.description,
      this.commissionPrice,
      this.totalPrice,
      this.balance,
      this.totalPriceWithDiscount,
      this.totalDiscount,
      this.peymentType,
      this.status,
      this.widthrawlAmount,
      this.id,
      this.createdAt});

  StoreGetStoreTransactionsObject.fromJson(Map<String, dynamic> json) {
    user = json['user'] != null ? new User.fromJson(json['user']) : null;
    store = json['store'] != null ? new Store.fromJson(json['store']) : null;
    order = json['order'] != null ? new Order.fromJson(json['order']) : null;
    isSuccessful = json['isSuccessful'];
    description = json['description'];
    commissionPrice = json['commissionPrice'];
    totalPrice = json['totalPrice'];
    balance = json['balance'];
    totalPriceWithDiscount = json['totalPriceWithDiscount'];
    totalDiscount = json['totalDiscount'];
    peymentType = json['peymentType'];
    status = json['status'];
    widthrawlAmount = json['widthrawlAmount'];
    id = json['id'];
    createdAt = json['createdAt'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.user != null) {
      data['user'] = this.user.toJson();
    }
    if (this.store != null) {
      data['store'] = this.store.toJson();
    }
    if (this.order != null) {
      data['order'] = this.order.toJson();
    }
    data['isSuccessful'] = this.isSuccessful;
    data['description'] = this.description;
    data['commissionPrice'] = this.commissionPrice;
    data['totalPrice'] = this.totalPrice;
    data['balance'] = this.balance;
    data['totalPriceWithDiscount'] = this.totalPriceWithDiscount;
    data['totalDiscount'] = this.totalDiscount;
    data['peymentType'] = this.peymentType;
    data['status'] = this.status;
    data['widthrawlAmount'] = this.widthrawlAmount;
    data['id'] = this.id;
    data['createdAt'] = this.createdAt;
    return data;
  }
}
