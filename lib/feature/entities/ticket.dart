import 'package:food_resturant/feature/entities/file.dart';
import 'package:food_resturant/feature/entities/messages.dart';

class Ticket {
  String title;
  String text;
  FileDto file;
  bool byAdmin;
  List<Messages> messages;

  Ticket({
    this.title,
    this.text,
    this.file,
    this.byAdmin,
    this.messages});

  Ticket.fromJson(dynamic json) {
    title = json["title"];
    text = json["text"];
    file = json["file"] != null ? FileDto.fromJson(json["file"]) : null;
    byAdmin = json["byAdmin"];
    if (json["messages"] != null) {
      messages = [];
      json["messages"].forEach((v) {
        messages.add(Messages.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["title"] = title;
    map["text"] = text;
    if (file != null) {
      map["file"] = file.toJson();
    }
    map["byAdmin"] = byAdmin;
    if (messages != null) {
      map["messages"] = messages.map((v) => v.toJson()).toList();
    }
    return map;
  }

}