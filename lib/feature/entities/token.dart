class Token {
  String token;
  int expires;

  Token({
    this.token,
    this.expires});

  Token.fromJson(dynamic json) {
    token = json["token"];
    expires = json["expires"];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["token"] = token;
    map["expires"] = expires;
    return map;
  }

}