import 'dart:async';
import 'dart:io';

import 'package:dio/dio.dart';
import 'package:food_resturant/core/config/api_address.dart';
import 'package:food_resturant/library/platforms/params_to_url.dart';


class ApiHelper {
  Dio _dio;
  final int TIME_OUT = 500000;
  ApiHelper() {
    BaseOptions options = BaseOptions(
        method: 'POST',
        contentType: 'application/json',
        receiveTimeout: TIME_OUT,
        connectTimeout: TIME_OUT);
    options.baseUrl = ApiAddress.BASE_URL;
    _dio = Dio(options);

    // cookieJar = CookieJar();
    // _dio.interceptors.add(CookieManager(cookieJar));
    _dio.interceptors.add(LogInterceptor(
        request: true,
        error: false,
        requestBody: true,
        responseBody: true,
        requestHeader: false));
  }

  Future<Response<dynamic>> get(String url) async {
    Response response;
    try {
      response =
          await _dio.get(url, options: Options(responseType: ResponseType.json));

    } on SocketException catch (_) {
      response = new Response();
      response.statusCode = ApiResponseCode.no_internet_connection;
      response.statusMessage = '';
    } on Exception {
      response = new Response();
      response.statusCode = ApiResponseCode.known;
      response.statusMessage = '';
    }
    return response;
  }

  Future<Response<dynamic>> getWithParam(
      String url, Map<String, dynamic> params) async {
    Response response;
    try {
      //  final result = await InternetAddress.lookup('google.com');
      // if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {

      response = await _dio.get(paramsToUrl(params: params, url: url),
          // queryParameters: params,
          options: Options(responseType: ResponseType.json));
      // }
    } on SocketException catch (_) {
      response = new Response();
      response.statusCode = ApiResponseCode.no_internet_connection;
      response.statusMessage = '';
    } on Exception {
      response = new Response();
      response.statusCode = ApiResponseCode.known;
      response.statusMessage = '';
    }
    // print('respo : '+response.toString());
    return response;
  }

  Future<Response> post(String url, Map<String, dynamic> params, [bool queryParameter= false]) async {
    if (queryParameter) {
      Response response = await _dio.post(paramsToUrl(params: params, url: url) );
      return response;

    } else{
      Response response = await _dio.post(url, data: params);
      return response;

    }
  }

  Future<Response> put(String url, Map<String, dynamic> params) async {
    Response response = await _dio.put(url,
        data: params, options: Options(responseType: ResponseType.json));
    return response;
  }

  Future<Response> delete(String url, Map<String, dynamic> params) async {
    Response response = await _dio.delete(url,
        data: params, options: Options(responseType: ResponseType.json));
    return response;
  }


  Future<Response> multiCallApi() async {
    // await Future.wait(getWithParam(url, params), dio.get("/token")]);

  }


}

class ApiResponseCode {
  static int success = 200;
  static int no_internet_connection = 999;
  static int known = 533;
}
