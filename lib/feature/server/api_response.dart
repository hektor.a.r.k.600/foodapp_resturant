import 'package:dio/dio.dart';
import 'package:food_resturant/core/config/routes.dart';
import 'package:get/get.dart' hide Response;// import 'package:get/get.dart';
import 'package:food_resturant/library/widgets/snackbar.dart';
import 'api_helper.dart';

class ApiResponse<T> {
  ApiStatus status;
  T data;
  String message;
  ApiError apierror;

  ApiResponse({ApiStatus status, T data, ApiError apierror}) {
    this.status = status;
    this.data = data;
    this.apierror = apierror;
  }

  static ApiResponse<T> loading<T>() {
    return ApiResponse<T>(status: ApiStatus.LOADING, data: null);
  }

  static ApiResponse success<T>(T data) {
    return ApiResponse<T>(status: ApiStatus.COMPLETED, data: data);
  }

  static dioError<T>(DioError error) {
    if (error?.response == null) {
      showSnackBar(text: error?.error?.message ?? error?.error?.toString());
      return;
    }

    if (error?.response?.statusCode == 403) {
      showSnackBar(text: error?.response?.statusMessage ?? 'خطایی رخ داده');
      return;
      // sl<GetStorage>()
      //     .erase()
      //     .then((value) => Get.offAllNamed(Routes.SplashScreen));
    }

    if (error?.response?.statusCode == 422) {
      if (error?.response?.data['error'] != null) {
        String errorMessage = error?.response?.data['error'];
        showSnackBar(text: errorMessage);
      }
      if (error?.response?.data['errors'] != null) {
        List<String> errorMessages = (error?.response?.data['errors'] as List)
            ?.map((item) => item as String)
            ?.toList();
        showSnackBar(text: errorMessages.join('\r\n'));
      }
    }
  }

  static ApiResponse<T> error<T>(
      {int errCode, String errMsg, String errBdy, T data}) {
    ApiError apiError = ApiError(
        statusCode: errCode,
        errorMessage: errMsg,
        errorBody: errBdy == null ? "" : errBdy);
    return ApiResponse<T>(
        status: ApiStatus.ERROR, apierror: apiError, data: data);
  }

  static ApiResponse<T> returnResponse<T>(Response response, T data) {
    if (response.statusCode == ApiResponseCode.no_internet_connection)
      return ApiResponse.error(
          errCode: response.statusCode,
          errMsg: '',
          data: null);
    else if (response.statusCode == ApiResponseCode.success)
      return ApiResponse.success(data);
    else if (response.statusCode == ApiResponseCode.known) {
      Get.offAndToNamed(Routes.LoginPage);
      showSnackBar(text: response?.statusMessage ?? '');
    } else {
      return ApiResponse.error(
          errCode: response.statusCode,
          errMsg: response.statusMessage,
          errBdy: response.data.toString(),
          data: null);
    }
  }

  @override
  String toString() {
    return "Status : $status \n Message : $message \n Data : $data";
  }

//  static checkDaa<T>(Response response, T datatt) {
//    try {
//      final dataRespo = apiHelper.returnResponse(response, datatt);
//      return dataRespo;
//    } catch (error, stacktrace) {
//      return error(
//          ApiError(533, error.toString(), stacktrace.toString()), null);
//    }
//  }
}

class ApiError {
  int statusCode;
  String errorMessage;
  String errorBody;

  ApiError({this.statusCode, this.errorMessage, this.errorBody});
}

enum ApiStatus { LOADING, COMPLETED, ERROR }
