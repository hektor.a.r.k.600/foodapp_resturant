import 'dart:async';
import 'package:flutter/material.dart';
import 'package:food_resturant/library/platforms/authentication.dart';
import 'package:http/http.dart' as http;
import 'package:flutter/services.dart' show rootBundle;
import 'package:http/http.dart';

abstract class IServer {
  Future<Response> getDataFromServer();
  Future<LocalResponse> getDataFromLocal();
}

class GetData implements IServer {
  final Map<String, String> headers;
  final String url;

  GetData({@required this.url, @required this.headers, dataConverter});

  @override
  Future<Response> getDataFromServer() async {
    print("Url : " + url);
    final response = await http.get(url, headers: headers);
    print("Status code : " + response.statusCode.toString());
    print("response : " + response.body.toString());
    return statusCodesDictionary(response)[response.statusCode]();
  }

  @override
  Future<LocalResponse> getDataFromLocal() async {
    await Future.delayed(Duration(seconds: 2));
    String dataJson = await rootBundle.loadString(url);
    LocalResponse localResponse = LocalResponse(dataJson, 200);
    return localResponse;
  }
}

class PostData implements IServer {
  final String params;
  final Map<String, String> headers;
  final String url;

  PostData({
    @required this.headers,
    @required this.params,
    @required this.url,
  });

  @override
  Future<Response> getDataFromServer() async {
    print("Url : " + url);
    print("params : " + params);
    final response = await http.post(url, headers: headers, body: params);
    print("Status code : " + response.statusCode.toString());
    print("response : " + response.body.toString());
    return statusCodesDictionary(response)[response.statusCode]();
  }

  @override
  Future<LocalResponse> getDataFromLocal() async {
    String json = await rootBundle.loadString(url);
    LocalResponse localResponse = LocalResponse(json, 200);
    return localResponse;
  }
}

class PutData implements IServer {
  final String params;
  final Map<String, String> headers;
  final String url;

  PutData({
    @required this.headers,
    @required this.params,
    @required this.url,
  });

  @override
  Future<Response> getDataFromServer() async {
    print("Url : " + url);
    print("params : " + params);
    final response = await http.put(url, headers: headers, body: params);
    print("Status code : " + response.statusCode.toString());
    print("response : " + response.body.toString());
    return statusCodesDictionary(response)[response.statusCode]();
  }

  @override
  Future<LocalResponse> getDataFromLocal() async {
    String json = await rootBundle.loadString(url);
    LocalResponse localResponse = LocalResponse(json, 200);
    return localResponse;
  }
}

class DeleteData implements IServer {
  final Map<String, String> headers;
  final String url;

  DeleteData({
    @required this.headers,
    @required this.url,
  });

  @override
  Future<Response> getDataFromServer() async {
    print("Url : " + url);
    final response = await http.delete(url, headers: headers);
    print("Status code : " + response.statusCode.toString());
    print("response : " + response.body.toString());
    return statusCodesDictionary(response)[response.statusCode]();
  }

  @override
  Future<LocalResponse> getDataFromLocal() async {
    String json = await rootBundle.loadString(url);
    LocalResponse localResponse = LocalResponse(json, 200);
    return localResponse;
  }
}

class LocalResponse extends Response {
  final String body;
  final int statusCode;

  LocalResponse(this.body, this.statusCode) : super(body, statusCode);
}
