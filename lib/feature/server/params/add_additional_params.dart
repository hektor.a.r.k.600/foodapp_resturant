import 'package:food_resturant/feature/entities/additional.dart';
import 'package:food_resturant/feature/entities/food.dart';
import 'package:food_resturant/feature/entities/full_food_obj.dart';
import 'package:food_resturant/feature/server/params/params.dart';
import 'package:food_resturant/feature/view/page/add_food_page/add_food_page_controller.dart';

class AddAdditionalParams extends Params{
  Food food;
  Additionals additional;

  AddAdditionalParams({
      this.food, 
      this.additional});

  AddAdditionalParams.fromJson(dynamic json) {
    food = json["food"] != null ? Food.fromJson(json["food"]) : null;
    additional = json["additional"] != null ? Additionals.fromJson(json["additional"]) : null;
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    if (food != null) {
      map["food"] = food.toJson();
    }
    if (additional != null) {
      map["additional"] = additional.toJson();
    }
    return map;
  }

}
