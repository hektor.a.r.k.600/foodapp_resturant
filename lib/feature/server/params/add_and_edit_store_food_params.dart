import 'package:food_resturant/feature/entities/food.dart';
import 'package:food_resturant/feature/entities/full_food_obj.dart';
import 'package:food_resturant/feature/entities/store.dart';
import 'package:food_resturant/feature/server/params/params.dart';

class AddAndEditStoreFoodParams extends Params{
  String storeFoodId;
  Store store;
  FullFoodObj food;

  AddAndEditStoreFoodParams({this.storeFoodId, this.store, this.food});

  AddAndEditStoreFoodParams.fromJson(Map<String, dynamic> json) {
    storeFoodId = json["storeFoodId"];
    store = json['store'] != null ? new Store.fromJson(json['store']) : null;
    food = json['food'] != null ? new FullFoodObj.fromJson(json['food']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data["storeFoodId"] = storeFoodId;
    if (this.store != null) {
      data['store'] = this.store.toJson();
    }
    if (this.food != null) {
      data['food'] = this.food.toJson();
    }
    return data;
  }
}


class Rates {
  int rateValue;
  String comment;
  int status;
  Voter voter;
  String id;
  int createdAt;

  Rates(
      {this.rateValue,
        this.comment,
        this.status,
        this.voter,
        this.id,
        this.createdAt});

  Rates.fromJson(Map<String, dynamic> json) {
    rateValue = json['rateValue'];
    comment = json['comment'];
    status = json['status'];
    voter = json['voter'] != null ? new Voter.fromJson(json['voter']) : null;
    id = json['id'];
    createdAt = json['createdAt'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['rateValue'] = this.rateValue;
    data['comment'] = this.comment;
    data['status'] = this.status;
    if (this.voter != null) {
      data['voter'] = this.voter.toJson();
    }
    data['id'] = this.id;
    data['createdAt'] = this.createdAt;
    return data;
  }
}

class Voter {
  String id;
  String userName;
  String name;
  String familyName;
  String profileImage;

  Voter(
      {this.id, this.userName, this.name, this.familyName, this.profileImage});

  Voter.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    userName = json['userName'];
    name = json['name'];
    familyName = json['familyName'];
    profileImage = json['profileImage'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['userName'] = this.userName;
    data['name'] = this.name;
    data['familyName'] = this.familyName;
    data['profileImage'] = this.profileImage;
    return data;
  }
}
