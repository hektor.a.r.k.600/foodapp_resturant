import 'package:food_resturant/feature/entities/file.dart';

class AddAnswerTicketParams {
  String storeTicketId;
  String title;
  String text;
  FileDto file;
  bool byAdmin;

  AddAnswerTicketParams({
      this.storeTicketId, 
      this.title, 
      this.text, 
      this.file, 
      this.byAdmin});

  AddAnswerTicketParams.fromJson(dynamic json) {
    storeTicketId = json["storeTicketId"];
    title = json["title"];
    text = json["text"];
    file = json["file"] != null ? FileDto.fromJson(json["file"]) : null;
    byAdmin = json["byAdmin"];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["storeTicketId"] = storeTicketId;
    map["title"] = title;
    map["text"] = text;
    if (file != null) {
      map["file"] = file.toJson();
    }
    map["byAdmin"] = byAdmin;
    return map;
  }

}
