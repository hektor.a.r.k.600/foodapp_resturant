import 'package:food_resturant/feature/entities/user.dart';
import 'package:food_resturant/feature/server/params/params.dart';

class AddAttendenceParams extends Params{
  int status;
  User user;
  String macAddress;
  String qrCodeId;

  AddAttendenceParams({
      this.status, 
      this.user, 
      this.macAddress ,this.qrCodeId});

  AddAttendenceParams.fromJson(dynamic json) {
    status = json["status"];
    user = json["user"] != null ? User.fromJson(json["user"]) : null;
    macAddress = json["macAddress"];
    qrCodeId = json["qrCodeId"];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["status"] = status;
    if (user != null) {
      map["user"] = user.toJson();
    }
    map["macAddress"] = macAddress;
    map["qrCodeId"] = qrCodeId;
    return map;
  }

}
