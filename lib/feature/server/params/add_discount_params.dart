import 'package:food_resturant/feature/server/params/params.dart';

class AddDiscountParams extends Params{
  String foodId;
  int discountValue;

  AddDiscountParams({
      this.foodId, 
      this.discountValue});

  AddDiscountParams.fromJson(dynamic json) {
    foodId = json["foodId"];
    discountValue = json["discountValue"];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["foodId"] = foodId;
    map["discountValue"] = discountValue;
    return map;
  }

}