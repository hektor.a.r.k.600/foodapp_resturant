import 'package:food_resturant/feature/entities/ingredient.dart';

class AddFoodIngredientParams {
  Ingredients ingredient;
  String unit;
  int value;

  AddFoodIngredientParams({
      this.ingredient, 
      this.unit, 
      this.value});

  AddFoodIngredientParams.fromJson(dynamic json) {
    ingredient = json["ingredient"] != null ? Ingredients.fromJson(json["ingredient"]) : null;
    unit = json["unit"];
    value = json["value"];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    if (ingredient != null) {
      map["ingredient"] = ingredient.toJson();
    }
    map["unit"] = unit;
    map["value"] = value;
    return map;
  }

}
