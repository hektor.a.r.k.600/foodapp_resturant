import 'package:food_resturant/feature/server/params/params.dart';

class AddStoreFoodCategoryParams extends Params{
  String storeId;
  String mainCategoryId;
  String name;
  int discountValue;

  AddStoreFoodCategoryParams({
      this.storeId, 
      this.mainCategoryId, 
      this.name, 
      this.discountValue});

  AddStoreFoodCategoryParams.fromJson(dynamic json) {
    storeId = json["storeId"];
    mainCategoryId = json["mainCategoryId"];
    name = json["name"];
    discountValue = json["discountValue"];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["storeId"] = storeId;
    map["mainCategoryId"] = mainCategoryId;
    map["name"] = name;
    map["discountValue"] = discountValue;
    return map;
  }

}