import 'package:food_resturant/feature/entities/store.dart';
import 'package:food_resturant/feature/entities/ticket.dart';
import 'package:food_resturant/feature/server/params/params.dart';

class AddTicketParams extends Params{
  Store store;
  Ticket ticket;

  AddTicketParams({
      this.store, 
      this.ticket});

  AddTicketParams.fromJson(dynamic json) {
    store = json["store"] != null ? Store.fromJson(json["store"]) : null;
    ticket = json["ticket"] != null ? Ticket.fromJson(json["ticket"]) : null;
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    if (store != null) {
      map["store"] = store.toJson();
    }
    if (ticket != null) {
      map["ticket"] = ticket.toJson();
    }
    return map;
  }

}





