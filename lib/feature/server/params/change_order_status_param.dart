import 'package:food_resturant/feature/server/params/params.dart';

class ChangeOrderStatusParam extends Params {
  String orderId;
  int status;

  ChangeOrderStatusParam({this.orderId, this.status});

  ChangeOrderStatusParam.fromJson(dynamic json) {
    orderId = json["orderId"];
    status = json["status"];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["orderId"] = orderId;
    map["status"] = status;
    return map;
  }
}

class ChangeFoodOrderStatusParam extends Params {
  String orderId;
  String storeId;
  int orderStoreStatus;

  ChangeFoodOrderStatusParam(
      {this.orderId, this.storeId, this.orderStoreStatus});

  ChangeFoodOrderStatusParam.fromJson(Map<String, dynamic> json) {
    orderId = json['orderId'];
    storeId = json['storeId'];
    orderStoreStatus = json['orderStoreStatus'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['orderId'] = this.orderId;
    data['storeId'] = this.storeId;
    data['orderStoreStatus'] = this.orderStoreStatus;
    return data;
  }
}
