class ChangeStatusApprovalParams {
  String orderId;
  int cashStatus;

  ChangeStatusApprovalParams({this.orderId, this.cashStatus});

  ChangeStatusApprovalParams.fromJson(Map<String, dynamic> json) {
    orderId = json['orderId'];
    cashStatus = json['cashStatus'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['orderId'] = this.orderId;
    data['cashStatus'] = this.cashStatus;
    return data;
  }
}
