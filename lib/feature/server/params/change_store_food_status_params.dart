class ChangeStoreFoodStatusParams {
  String storeFoodId;
  int status;

  ChangeStoreFoodStatusParams({
      this.storeFoodId, 
      this.status});

  ChangeStoreFoodStatusParams.fromJson(dynamic json) {
    storeFoodId = json["storeFoodId"];
    status = json["status"];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["storeFoodId"] = storeFoodId;
    map["status"] = status;
    return map;
  }

}