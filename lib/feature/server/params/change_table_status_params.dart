import 'package:food_resturant/feature/server/params/params.dart';

/// userName : "string"
/// password : "string"
/// pushId : "string"

class ChangeTableStatusParams extends Params{
  String tableId;
  int status;


  ChangeTableStatusParams({this.tableId, this.status});

  ChangeTableStatusParams.fromJson(dynamic json) {
    tableId = json["TableId"];
    status = json["Status"];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["TableId"] = tableId;
    map["Status"] = status;
    return map;
  }

}