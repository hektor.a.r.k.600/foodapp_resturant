import 'package:food_resturant/feature/server/params/params.dart';

class EditStoreFoodCategoryParams extends Params{
  String storeFoodCategoryId;
  String name;
  int discountValue;

  EditStoreFoodCategoryParams({
      this.storeFoodCategoryId,
      this.name, 
      this.discountValue});

  EditStoreFoodCategoryParams.fromJson(dynamic json) {
    storeFoodCategoryId = json["storeFoodCategoryId"];
    name = json["name"];
    discountValue = json["discountValue"];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["storeFoodCategoryId"] = storeFoodCategoryId;
    map["name"] = name;
    map["discountValue"] = discountValue;
    return map;
  }

}