import 'package:food_resturant/feature/entities/cover_image.dart';
import 'package:food_resturant/feature/entities/icon_object.dart';

class EditStoreProfile {
  String storeId;
  String name;
  String address;
  String descriptions;
  int openingHour;
  int openingMinute;
  int closingHour;
  int closingMinute;
  int lat;
  int lon;
  IconObject icon;
  CoverImage coverImage;

  EditStoreProfile({
      this.storeId, 
      this.name, 
      this.address, 
      this.descriptions, 
      this.openingHour, 
      this.openingMinute, 
      this.closingHour, 
      this.closingMinute, 
      this.lat, 
      this.lon, 
      this.icon, 
      this.coverImage});

  EditStoreProfile.fromJson(dynamic json) {
    storeId = json["storeId"];
    name = json["name"];
    address = json["address"];
    descriptions = json["descriptions"];
    openingHour = json["openingHour"];
    openingMinute = json["openingMinute"];
    closingHour = json["closingHour"];
    closingMinute = json["closingMinute"];
    lat = json["lat"];
    lon = json["lon"];
    icon = json["icon"] != null ? IconObject.fromJson(json["icon"]) : null;
    coverImage = json["coverImage"] != null ? CoverImage.fromJson(json["coverImage"]) : null;
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["storeId"] = storeId;
    map["name"] = name;
    map["address"] = address;
    map["descriptions"] = descriptions;
    map["openingHour"] = openingHour;
    map["openingMinute"] = openingMinute;
    map["closingHour"] = closingHour;
    map["closingMinute"] = closingMinute;
    map["lat"] = lat;
    map["lon"] = lon;
    if (icon != null) {
      map["icon"] = icon.toJson();
    }
    if (coverImage != null) {
      map["coverImage"] = coverImage.toJson();
    }
    return map;
  }

}



class Icon {
  String id;
  String location;
  int type;

  Icon({
      this.id, 
      this.location, 
      this.type});

  Icon.fromJson(dynamic json) {
    id = json["id"];
    location = json["location"];
    type = json["type"];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["id"] = id;
    map["location"] = location;
    map["type"] = type;
    return map;
  }

}