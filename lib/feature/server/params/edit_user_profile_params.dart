class UserEditProfileParam {
  String userId;
  String name;
  String familyName;
  String mobile;
  String email;

  UserEditProfileParam(
      {this.userId, this.name, this.familyName, this.mobile, this.email});

  UserEditProfileParam.fromJson(Map<String, dynamic> json) {
    userId = json['userId'];
    name = json['name'];
    familyName = json['familyName'];
    mobile = json['mobile'];
    email = json['email'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['userId'] = this.userId;
    data['name'] = this.name;
    data['familyName'] = this.familyName;
    data['mobile'] = this.mobile;
    data['email'] = this.email;
    return data;
  }
}
