import 'package:food_resturant/feature/entities/ingredient.dart';
import 'package:food_resturant/feature/server/params/params.dart';

class FoodCategoryIdParams extends Params{
  String storeFoodCategoryId;



  FoodCategoryIdParams({
      this.storeFoodCategoryId,
      });

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["storeFoodCategoryId"] = storeFoodCategoryId;
    return map;
  }

}
