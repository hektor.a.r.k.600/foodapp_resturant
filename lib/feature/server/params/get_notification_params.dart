import 'package:food_resturant/feature/server/params/params.dart';

class GetNotificationParams extends Params{
  int roleId;
  int pageNo;

  GetNotificationParams({
      this.roleId,
      this.pageNo});

  GetNotificationParams.fromJson(dynamic json) {
    roleId = json["RoleId"];
    pageNo = json["PageNo"];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["RoleId"] = roleId;
    map["PageNo"] = pageNo;
    return map;
  }

}