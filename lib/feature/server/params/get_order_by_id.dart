import 'package:food_resturant/feature/entities/food.dart';
import 'package:food_resturant/feature/entities/full_food_obj.dart';
import 'package:food_resturant/feature/server/params/params.dart';
import 'package:food_resturant/feature/view/page/add_food_page/add_food_page_controller.dart';

class GetOrderByIdParams extends Params{
  String orderId;

  GetOrderByIdParams({
      this.orderId,});

  GetOrderByIdParams.fromJson(dynamic json) {
    orderId = json["OrderId"];

  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["OrderId"] = orderId;
    return map;
  }

}
