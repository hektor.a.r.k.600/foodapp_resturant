import 'package:food_resturant/feature/server/params/params.dart';
import 'package:food_resturant/library/enums/enums.dart';
import 'package:food_resturant/library/extention/enum_extention.dart';
import 'package:food_resturant/library/extention/int_extention.dart';

class GetOrderByTableParams extends Params{
  String tableId;
  OrderStatus status;
  OrderTypes type;
  int pageNo;


  GetOrderByTableParams({this.tableId, this.status, this.type, this.pageNo});

  GetOrderByTableParams.fromJson(dynamic json) {
    tableId = json["TableId"];
    int orderStatus = json["Status"];
    status = orderStatus.enumOrderStatus;

    int orderType = json["Type"];
    type = orderType.enumOrderTypes;


    pageNo = json["PageNo"];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["TableId"] = tableId;
    map["status"] = status.orderStatusToInt;
    map["PageNo"] = pageNo;
    map["Type"] = type.orderTypeToInt;
    return map;
  }

}