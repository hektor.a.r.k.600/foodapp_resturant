import 'package:food_resturant/feature/entities/ingredient.dart';
import 'package:food_resturant/feature/server/params/params.dart';
import 'package:food_resturant/library/enums/enums.dart';
import 'package:food_resturant/library/extention/int_extention.dart';
import 'package:food_resturant/library/extention/enum_extention.dart';

class GetOrdersCountParams extends Params {
  int createdAt;
  String storeId;

  GetOrdersCountParams({this.createdAt, this.storeId});

  GetOrdersCountParams.fromJson(dynamic json) {
    createdAt = json["CreatedAt"];
    storeId = json["StoreId"];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["CreatedAt"] = createdAt;
    map["StoreId"] = storeId;
    return map;
  }
}
