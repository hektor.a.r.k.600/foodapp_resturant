import 'package:food_resturant/feature/entities/ingredient.dart';
import 'package:food_resturant/feature/server/params/params.dart';
import 'package:food_resturant/library/enums/enums.dart';
import 'package:food_resturant/library/extention/int_extention.dart';
import 'package:food_resturant/library/extention/enum_extention.dart';

class GetOrdersParams extends Params {
  String userId;
  String storeId;
  OrderStatus status;
  OrderTypes orderType;
  int chashAprovalStatus;
  int pageNo;

  GetOrdersParams(
      {this.userId,
      this.storeId,
      this.chashAprovalStatus,
      this.status = OrderStatus.ALL,
      this.orderType = OrderTypes.ALL,
      this.pageNo});

  GetOrdersParams.fromJson(dynamic json) {
    userId = json["UserId"];
    storeId = json["StoreId"];
    chashAprovalStatus = json["ChashAprovalStatus"];
    int orderStatus = json["Status"];
    status = orderStatus.enumOrderStatus;

    int oType = json["OrderType"];
    orderType = oType.enumOrderTypes;
    pageNo = json["PageNo"];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["UserId"] = userId;
    map["ChashAprovalStatus"] = chashAprovalStatus;
    map["StoreId"] = storeId;
    map["Status"] = status.orderStatusToInt;
    map["OrderType"] = orderType.orderTypeToInt;
    map["PageNo"] = pageNo;
    return map;
  }
}
