import 'package:food_resturant/feature/entities/ingredient.dart';
import 'package:food_resturant/feature/server/params/params.dart';
import 'package:food_resturant/library/enums/enums.dart';
import 'package:food_resturant/library/extention/int_extention.dart';
import 'package:food_resturant/library/extention/enum_extention.dart';

class GetStoreFoodByIdParams extends Params{
  String storeFoodId;


  GetStoreFoodByIdParams({this.storeFoodId});

  GetStoreFoodByIdParams.fromJson(dynamic json) {
    storeFoodId = json["StoreFoodId"];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["StoreFoodId"] = storeFoodId;
    return map;
  }

}
