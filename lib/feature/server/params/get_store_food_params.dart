import 'package:food_resturant/feature/entities/ingredient.dart';

class GetStoreFoodParams {
  String soreId;
  String mainCategoryId;
  int pageNo;


  GetStoreFoodParams({this.soreId, this.mainCategoryId, this.pageNo});

  GetStoreFoodParams.fromJson(dynamic json) {
    soreId = json["StoreId"];
    mainCategoryId = json["MainCategoryId"];
    pageNo = json["PageNo"];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["StoreId"] = soreId;
    map["MainCategoryId"] = mainCategoryId;
    map["PageNo"] = pageNo;
    return map;
  }

}
