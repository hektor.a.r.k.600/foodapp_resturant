import 'package:food_resturant/feature/server/params/params.dart';

class GetStoreRateParams extends Params{
  String storeId;
  int pageNo;

  GetStoreRateParams({
      this.storeId,
      this.pageNo});

  GetStoreRateParams.fromJson(dynamic json) {
    storeId = json["StoreId"];
    pageNo = json["PageNo"];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["StoreId"] = storeId;
    map["PageNo"] = pageNo;
    return map;
  }

}