import 'package:food_resturant/feature/server/params/params.dart';

class GetStoreTransactionsParams extends Params {
  String storeId;
  int from;
  int to;

  GetStoreTransactionsParams({this.storeId, this.from, this.to});

  GetStoreTransactionsParams.fromJson(dynamic json) {
    storeId = json["StoreId"];
    from = json["From"];
    to = json["To"];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["StoreId"] = storeId;
    map["To"] = to;
    map["From"] = from;
    return map;
  }
}
