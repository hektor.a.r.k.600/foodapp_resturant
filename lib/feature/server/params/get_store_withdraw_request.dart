import 'package:food_resturant/feature/entities/food.dart';
import 'package:food_resturant/feature/entities/full_food_obj.dart';
import 'package:food_resturant/feature/server/params/params.dart';
import 'package:food_resturant/feature/view/page/add_food_page/add_food_page_controller.dart';

class AddAdditionalParams extends Params{
  String storeId;
  int createdAt;
  int pageNo;


  AddAdditionalParams.fromJson(dynamic json) {
    storeId = json["StoreId"];
    createdAt = json["CreatedAt"];
    pageNo = json["PageNo"];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["StoreId"] = storeId;
    map["CreatedAt"] = createdAt;
    map["PageNo"] = pageNo;
    return map;
  }

}
