/// userName : "string"
/// password : "string"
/// pushId : "string"

class GetTablesParams {
  int status;
  int pageNo;


  GetTablesParams({this.status, this.pageNo});

  GetTablesParams.fromJson(dynamic json) {
    status = json["Status"];
    pageNo = json["PageNo"];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["Status"] = status;
    map["PageNo"] = pageNo;
    return map;
  }

}