import 'package:food_resturant/feature/entities/full_food_obj.dart';
import 'package:food_resturant/feature/entities/get_store_food_by_id_entity.dart';
import 'package:food_resturant/feature/entities/store.dart';
import 'package:food_resturant/feature/server/params/params.dart';

class GetTicketByIdParams extends Params{
  String storeTicketId;

  GetTicketByIdParams({
      this.storeTicketId,});

  GetTicketByIdParams.fromJson(dynamic json) {
    storeTicketId = json["StoreTicketId"];

  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["StoreTicketId"] = storeTicketId;

    return map;
  }

}
