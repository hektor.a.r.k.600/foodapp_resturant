import 'package:food_resturant/feature/server/params/params.dart';

class GetTicketParams extends Params{
  int status;
  int total;
  int currentCount;
  int pageNo;

  GetTicketParams({
      this.status,
      this.total,
      this.currentCount,
      this.pageNo});

  GetTicketParams.fromJson(dynamic json) {
    status = json["Status"];
    total = json["Total"];
    currentCount = json["CurrentCount"];
    pageNo = json["PageNo"];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["Status"] = status;
    map["Total"] = total;
    map["CurrentCount"] = currentCount;
    map["PageNo"] = pageNo;
    return map;
  }

}