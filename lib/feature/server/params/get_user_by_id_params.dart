/// userName : "string"
/// password : "string"
/// pushId : "string"

class GetUserByIdParams {
  String userId;


  GetUserByIdParams({this.userId});

  GetUserByIdParams.fromJson(dynamic json) {
    userId = json["UserId"];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["UserId"] = userId;
    return map;
  }

}