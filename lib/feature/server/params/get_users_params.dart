/// userName : "string"
/// password : "string"
/// pushId : "string"

class GetUsersParams {
  int status;
  int roleId;
  String storeId;
  int pageNo;


  GetUsersParams({this.status, this.roleId, this.storeId, this.pageNo});

  GetUsersParams.fromJson(dynamic json) {
    status = json["Status"];
    roleId = json["RoleId"];
    storeId = json["StoreId"];
    pageNo = json["PageNo"];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["Status"] = status;
    map["RoleId"] = roleId;
    map["StoreId"] = storeId;
    map["PageNo"] = pageNo;
    return map;
  }

}