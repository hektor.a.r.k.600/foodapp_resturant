import 'package:food_resturant/feature/entities/ingredient.dart';
import 'package:food_resturant/feature/server/params/params.dart';

class GetWithdrawStoreRequestParams extends Params{
  String storeId;
  int createdAt;
  int pageNo;


  GetWithdrawStoreRequestParams({this.storeId, this.createdAt, this.pageNo});

  GetWithdrawStoreRequestParams.fromJson(dynamic json) {
    storeId = json["StoreId"];
    createdAt = json["CreatedAt"];
    pageNo = json["PageNo"];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["StoreId"] = storeId;
    map["CreatedAt"] = createdAt;
    map["PageNo"] = pageNo;
    return map;
  }

}
