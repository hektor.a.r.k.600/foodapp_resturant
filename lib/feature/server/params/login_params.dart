import 'package:food_resturant/feature/server/params/params.dart';

/// userName : "string"
/// password : "string"
/// pushId : "string"

class LoginParams extends Params{
  String _userName;
  String _password;
  String _pushId;

  String get userName => _userName;
  String get password => _password;
  String get pushId => _pushId;

  LoginParams({
      String userName, 
      String password, 
      String pushId}){
    _userName = userName;
    _password = password;
    _pushId = pushId;
}

  LoginParams.fromJson(dynamic json) {
    _userName = json["userName"];
    _password = json["password"];
    _pushId = json["pushId"];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["userName"] = _userName;
    map["password"] = _password;
    map["pushId"] = _pushId;
    return map;
  }

}