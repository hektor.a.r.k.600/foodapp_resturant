import 'package:food_resturant/feature/server/params/params.dart';

class PublicParams extends Params{
  int pageNo;


  PublicParams({this.pageNo});

  PublicParams.fromJson(dynamic json) {
    pageNo = json["PageNo"];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["PageNo"] = pageNo;
    return map;
  }
}