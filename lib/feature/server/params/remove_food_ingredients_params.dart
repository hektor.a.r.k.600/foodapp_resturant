class RemoveFoodIngredientsParams {
  String foodIngredientId;

  RemoveFoodIngredientsParams({
      this.foodIngredientId});

  RemoveFoodIngredientsParams.fromJson(dynamic json) {
    foodIngredientId = json["foodIngredientId"];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["foodIngredientId"] = foodIngredientId;
    return map;
  }

}