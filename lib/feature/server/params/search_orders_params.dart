class SearchOrdersParams {
  String filter;

  SearchOrdersParams({this.filter});

  SearchOrdersParams.fromJson(dynamic json) {
    filter = json["filter"];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["filter"] = filter;
    return map;
  }
}
