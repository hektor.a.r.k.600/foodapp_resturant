import 'package:food_resturant/feature/entities/user.dart';

class WithDrawAddStoreWithDrawRequestParams {
  String storId;
  String reason;
  int value;
  User user;

  WithDrawAddStoreWithDrawRequestParams(
      {this.storId, this.reason, this.value, this.user});

  WithDrawAddStoreWithDrawRequestParams.fromJson(dynamic json) {
    storId = json["storId"];
    reason = json["reason"];
    value = json["value"];
    user = json["user"];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["storId"] = storId;
    map["reason"] = reason;
    map["user"] = user;
    map["value"] = value;
    return map;
  }
}
