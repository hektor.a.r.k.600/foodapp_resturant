import 'dart:convert';

import 'package:dartz/dartz.dart';
import 'package:food_resturant/core/error/failure.dart';
import 'package:food_resturant/feature/entities/entities.dart';
import 'package:food_resturant/feature/view/bloc/bloc_bloc.dart';
import 'package:food_resturant/library/platforms/try_to_get_data.dart';


class GetDataRepositories {
  final BlocParams repositoriesImplParams;
  GetDataRepositories(this.repositoriesImplParams);

  Future<Either<Failure, Entities>> repostiroriesfunction() async {
    try {
      TryToGetData tryToGetData =
          TryToGetData(repositoriesImplParams.serverMethod);

      final result = repositoriesImplParams.isLocal
          ? await tryToGetData.getDataFromLocal()
          : await tryToGetData.getDataFromServer();

      Entities data = await repositoriesImplParams.dataConverter
          .convertEntityData(json.decode(result.body));
  
      return Right(data);
    } catch (e) {
      print(e);
      return Left(ServerFailure());
    }
  }
}
