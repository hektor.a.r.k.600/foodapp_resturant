import 'dart:isolate';
import 'package:dartz/dartz.dart';
import 'package:food_resturant/core/error/failure.dart';
import 'package:food_resturant/feature/entities/entities.dart';
import 'package:food_resturant/feature/view/bloc/bloc_bloc.dart';


import 'get_data_repositories.dart';

class IsolateImpl {
  final receivePort = ReceivePort();
  Isolate _isolate;
  final BlocParams blocParams;

  IsolateImpl(this.blocParams);

  void stop() {
    print("isolate closed.");
    receivePort.close();
    _isolate.kill(priority: Isolate.immediate);
  }

  Future<dynamic> start() async {
        print("isolate started ");

    Map map = {
      'port': receivePort.sendPort,
      'blocParams': blocParams,
    };
    _isolate = await Isolate.spawn(_entryPoint, map);
    return await receivePort.first;
  }

  static void _entryPoint(Map map) async {
    SendPort port = map['port'];
    GetDataRepositories getDataRepositories =
        GetDataRepositories(map["blocParams"]);
    Either<Failure, Entities<dynamic>> result =
        await getDataRepositories.repostiroriesfunction();
    port.send(result);
  }
}
