import 'dart:async';
import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:food_resturant/feature/entities/entities.dart';
import 'package:food_resturant/feature/server/connections/get_data.dart';
import 'package:food_resturant/feature/server/repositories/ConvertData.dart';
import 'package:food_resturant/feature/server/repositories/get_data_repositories.dart';
import 'package:food_resturant/feature/server/repositories/isolate_impl.dart';
import 'package:food_resturant/library/platforms/error_to_massage.dart';

part 'bloc_state.dart';

class FoodDeliveryBloc extends Bloc<BlocParams, BlocState> {
  FoodDeliveryBloc(BlocState initialState) : super(initialState);

  @override
  Stream<BlocState> mapEventToState(BlocParams blocParams) => blocParams.isLocal
      ? blocImpl(
          blocParams: blocParams,
        )
      : blocImplWithIsolate(
          blocParams: blocParams,
        );
}

Stream<BlocState> blocImplWithIsolate(
    {@required BlocParams blocParams}) async* {
  yield Loading();

  IsolateImpl isolateImpl = IsolateImpl(blocParams);
  final result = await isolateImpl.start();
  yield result.fold((failure) => Error(massage: failureToMassage(failure)),
      (result) {
    isolateImpl.stop();
    if (result.status) {
      return Loaded(entities: result);
    } else {
      return Error(massage: result.message);
    }
  });
}

Stream<BlocState> blocImpl({@required BlocParams blocParams}) async* {
  yield Loading();
  GetDataRepositories getDataRepositories = GetDataRepositories(blocParams);
  final result = await getDataRepositories.repostiroriesfunction();
  yield result.fold((failure) => Error(massage: failureToMassage(failure)),
      (result) {
    if (result.status) {
      return Loaded(entities: result);
    } else {
      return Error(massage: result.message);
    }
  });
}

class BlocParams {
  final IServer serverMethod;
  final DataConverter dataConverter;
  final bool isLocal;

  BlocParams(
      {@required this.serverMethod,
      @required this.dataConverter,
      this.isLocal = false});
}
