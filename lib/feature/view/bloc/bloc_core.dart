import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:food_resturant/feature/entities/entities.dart';
import 'package:food_resturant/feature/server/connections/get_data.dart';
import 'package:food_resturant/feature/server/repositories/ConvertData.dart';
import 'package:food_resturant/library/widgets/custom_loading_widget.dart';
import 'package:food_resturant/library/widgets/snackbar.dart';
import 'package:meta/meta.dart';

import 'bloc_bloc.dart';

Map<Type, Widget> dictionaryBlocBuilder(
        {@required Widget emptyWidget, Widget looadingWidget}) =>
    {
      Empty: emptyWidget,
      Error: emptyWidget,
      Loading: looadingWidget ??
          Center(
            child: customLoadingWidget(50.0),
          ),
      Loaded: emptyWidget,
    };

Map<Type, Function> dictionaryBlocListener(BuildContext context,
        {Widget customWidget}) =>
    {
      Empty: (Empty empty) {},
      Error: (Error error) {
        showSnackBar(text: error.massage);
      },
      Loading: (Loading loading) {},
      Loaded: (Loaded loaded) {
        return loaded.entities;
      },
    };

// blocProvider Handler : font handle the blocs;

class BlocProviderHandler<T extends Entities> extends StatelessWidget {
  final Widget Function(BuildContext) emptyWidget;
  final IServer serverMethod;
  final DataConverter dataConverter;
  final Widget looadingWidget;
  final bool isLocal;
  final void Function(T) getResponse;
  BlocProviderHandler(this.emptyWidget,
      {Key key,
      this.serverMethod,
      this.dataConverter,
      this.looadingWidget,
      this.isLocal = false,
      @required this.getResponse})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (_) => FoodDeliveryBloc(Empty()),
      child:
          BlocListener<FoodDeliveryBloc, BlocState>(listener: (context, state) {
        if (state is Loaded) {
          getResponse(
              dictionaryBlocListener(context)[state.runtimeType](state));
        } else {
          dictionaryBlocListener(context)[state.runtimeType](state);
        }
      }, child: BlocBuilder<FoodDeliveryBloc, BlocState>(
        builder: (context, state) {
          if (state is Empty && serverMethod != null && dataConverter != null) {
            BlocParams blocParams = BlocParams(
                dataConverter: dataConverter,
                serverMethod: serverMethod,
                isLocal: isLocal);
            BlocProvider.of<FoodDeliveryBloc>(context).add(blocParams);
          }
          return dictionaryBlocBuilder(
              looadingWidget: looadingWidget,
              emptyWidget: emptyWidget(
                context,
              ))[state.runtimeType];
        },
      )),
    );
  }
}
