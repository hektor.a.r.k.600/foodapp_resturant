part of 'bloc_bloc.dart';

abstract class BlocState extends Equatable {
  @override
  List<Object> get props => [];
}

class Empty extends BlocState {

}

class Loading extends BlocState {}

class Error extends BlocState {
  final String massage;
  Error({@required this.massage});
}

class Loaded extends BlocState {
  final Entities entities;
  Loaded({@required this.entities});
}
