import 'package:flutter/material.dart';
import 'package:food_resturant/core/config/routes.dart';
import 'package:food_resturant/feature/view/page/add_discount_page/add_discount_page_controller.dart';
import 'package:food_resturant/feature/view/page/add_discount_page/add_discount_page_controller.dart';
import 'package:food_resturant/feature/view/page/discount_page/restaurant_discounts_page_controller.dart';
import 'package:food_resturant/feature/view/page/stocks_page/stocks_page_controller.dart';
import 'package:food_resturant/feature/view/page/tables_page/tables_page_controller.dart';
import 'package:food_resturant/feature/view/widget/app_bar.dart';
import 'package:food_resturant/feature/view/widget/box_shadow_widget.dart';
import 'package:food_resturant/library/widgets/button_widget.dart';
import 'package:food_resturant/library/widgets/custom_drop_list/custom_drop_down_list.dart';
import 'package:food_resturant/library/widgets/text_widget.dart';
import 'package:get/get.dart';
import 'package:qr_flutter/qr_flutter.dart';

import '../../../../library/widgets/text_field_widget.dart';

class AddDiscountPage extends StatelessWidget {
  final controller = Get.put(AddDiscountPageController());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // floatingActionButton: ,
      // floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
      body: SingleChildScrollView(
        child:
        GetBuilder<AddDiscountPageController>(builder: (x) {
          return Container(
            height: Get.height,
            padding: EdgeInsets.only(bottom: 30.0),
            child: Column(
              children: [
                BuildAppBarWidget(
                  title: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      CustomText(
                        'Add Discount',
                        color: Colors.white,
                        size: 25.0,
                        fontWeight: FontWeight.bold,
                      ),
                      SizedBox(
                        height: 2.0,
                      ),
                      CustomText(
                        'Add a Discount Code',
                        color: Colors.white,
                        size: 18.0,
                        fontWeight: FontWeight.w500,
                      ),
                    ],
                  ),
                ),
                Container(
                  padding: EdgeInsets.all(15.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      // Row(
                      //   mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      //   children: [
                      //     CustomText(
                      //       'Send Notification To Users',
                      //       color: Colors.black,
                      //       size: 20.0,
                      //       fontWeight: FontWeight.bold,
                      //     ),
                      //       Checkbox(
                      //         checkColor: Get.theme.primaryColorDark,
                      //         activeColor: Get.theme.primaryColorLight,
                      //         value: x.isCheckedSendNotificationToUsers,
                      //         onChanged: (bool isCheck) {
                      //           x.isCheckedSendNotificationToUsers = isCheck;
                      //           controller.update();
                      //         },
                      //       ),
                      //   ],
                      // ),
                      // SizedBox(
                      //   height: 20,
                      // ),
                      // CustomText(
                      //   'Notification text',
                      //   color: Colors.black,
                      //   size: 20.0,
                      //   fontWeight: FontWeight.bold,
                      //   textAlign: TextAlign.start,
                      // ),
                      // CustomTextField(
                      //     hint: "We Have Discount For You",
                      //     height: 100.0,
                      //     maxLine: 2,
                      //     fontSize: 18.0,
                      //     isBold: false),
                      // SizedBox(
                      //   height: 20,
                      // ),
                      CustomText(
                        'Discount Percent',
                        color: Colors.black,
                        size: 20.0,
                        fontWeight: FontWeight.bold,
                        textAlign: TextAlign.start,
                      ),
                      CustomTextField(
                        textEditingController: controller.txtDiscountValueController,
                          hint: "20",
                          textInputType: TextInputType.number,
                          height: 50.0,
                          maxLine: 1,
                          fontSize: 18.0,
                          isBold: false),
                      SizedBox(
                        height: 20,
                      ),
                      // SizedBox(
                      //     height: 40.0,
                      //     width: Get.width * .9,
                      //     child: CustomButton(
                      //       onTap: () {
                      //         Get.toNamed(Routes.SelectFoodPage).then((value) => {
                      //           if(value != null){
                      //         controller.selectedFoodItem = value,
                      //           }
                      //         });
                      //       },
                      //       type: 2,
                      //       borderRadios: 10.0,
                      //       child: CustomText(
                      //         'Select Foods',
                      //         color: Colors.black54,
                      //         size: 18.0,
                      //         fontWeight: FontWeight.bold,
                      //       ),
                      //     )),
                    ],
                  ),
                ),
                Expanded(
                  child: Align(
                    alignment: Alignment.bottomCenter,
                    child: SizedBox(
                        width: Get.width * 0.3,
                        height: 40.0,
                        child: CustomButton(
                          borderRadios: 10.0,
                          onTap: () {
                           controller.addDiscountFood();
                          },
                          type: 2,
                          child: CustomText(
                            'Done',
                            color: Colors.black54,
                            size: 18.0,
                            fontWeight: FontWeight.bold,
                          ),
                        )),
                  ),
                )
              ],
            ),
          );
        }),
      ),
    );
  }
}
