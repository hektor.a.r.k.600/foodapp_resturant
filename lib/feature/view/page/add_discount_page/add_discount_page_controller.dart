import 'package:flutter/material.dart';
import 'package:food_resturant/core/config/api_address.dart';
import 'package:food_resturant/core/config/global_setting.dart';
import 'package:food_resturant/feature/depenency_injection.dart';
import 'package:food_resturant/feature/entities/edit_food_discount_params.dart';
import 'package:food_resturant/feature/entities/get_store_food_entity.dart';
import 'package:food_resturant/feature/entities/login_entity.dart';
import 'package:food_resturant/feature/entities/public_entity.dart';
import 'package:food_resturant/feature/server/api_helper.dart';
import 'package:food_resturant/feature/server/api_response.dart';
import 'package:food_resturant/feature/server/params/add_discount_params.dart';
import 'package:food_resturant/feature/server/params/get_store_food_params.dart';
import 'package:food_resturant/library/platforms/loading_overlay.dart';
import 'package:food_resturant/library/widgets/snackbar.dart';
import 'package:food_resturant/library/widgets/text_widget.dart';
import 'package:get/get.dart';

class AddDiscountPageController extends GetxController {
  final overlay = LoadingOverlay.of(Get.context);

  bool isCheckedSendNotificationToUsers = false;
  RxList<GetStoreFoodObject> storeFood = <GetStoreFoodObject>[].obs;
  // List<GetStoreFoodObject> selectedFood = new List();
  GetStoreFoodObject selectedFoodItem;


  LoginObject user;

  TextEditingController txtDiscountValueController =
      TextEditingController(text: '');

  GetStoreFoodObject arguments;

  @override
  void onInit() {
    arguments = Get.arguments;
    user = sl<GlobalSetting>().user;
    super.onInit();
  }

  @override
  void onReady() {
    if (arguments != null) {
      txtDiscountValueController.text =
          arguments.discountValue.toString() ?? '';
    }
    super.onReady();
  }

  getAllStoreFood() async {
    var getStoreFoodParams = GetStoreFoodParams(soreId: user?.store?.id ?? '');

    final response = await overlay.during(sl<ApiHelper>()
        .getWithParam(ApiAddress.GET_STORE_FOOD, getStoreFoodParams.toJson()));
    var result = ApiResponse.returnResponse(
        response, GetStoreFoodEntity.fromJson(response.data));
    if (result.status == ApiStatus.COMPLETED && result?.data?.status) {
      // showSnackBar(text: result?.data?.message ?? '');
      storeFood.value = result.data?.getStoreFoodObject;
      // pizzaStoreFood.value = result.data?.getStoreFoodObject?.where((element) => element.)

    } else {
      showSnackBar(text: result?.data?.message ?? '');
    }
  }

  addDiscountFood() async {
    if (txtDiscountValueController.text.isEmpty) {
      showSnackBar(text: 'complete Discount value' ?? '');
      return;
    }
    // if (selectedFoodItem ==null) {
    //   showSnackBar(text: 'selected a food' ?? '');
    //   return;
    // }

    //todo because accept one food id we have to call each
    var discountParams = EditFoodDiscountParams(
        discountValue: int.tryParse(txtDiscountValueController.text),
        storeFoodId: arguments?.id);

    final response = await overlay.during(sl<ApiHelper>().post(
        ApiAddress.EDIT_FOOD_DISCOUNT, discountParams.toJson()));
    var result = ApiResponse.returnResponse(
        response, PublicEntity.fromJson(response.data));
    if (result.status == ApiStatus.COMPLETED && result?.data?.status) {
      Get.back(result: true);
    } else {
      showSnackBar(text: result?.data?.message ?? '');
    }


    // selectedFood.forEach((element) async {
    //   var discountParams = EditFoodDiscountParams(
    //       discountValue: int.tryParse(txtDiscountValueController.text),
    //       storeFoodId: element?.id);
    //
    //   final response = await overlay.during(sl<ApiHelper>().post(
    //       ApiAddress.EDIT_FOOD_DISCOUNT, discountParams.toJson()));
    //   var result = ApiResponse.returnResponse(
    //       response, PublicEntity.fromJson(response.data));
    //   if (result.status == ApiStatus.COMPLETED && result?.data?.status) {
    //   } else {
    //     showSnackBar(text: result?.data?.message ?? '');
    //   }
    // });

  }
}
