import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:food_resturant/feature/entities/additional.dart';
import 'package:food_resturant/feature/entities/full_food_obj.dart';
import 'package:food_resturant/feature/entities/optional.dart';
import 'package:food_resturant/feature/view/page/add_food_page/add_food_page_controller.dart';
import 'package:food_resturant/feature/view/page/add_food_page/add_food_page_controller.dart';
import 'package:food_resturant/feature/view/page/balance_page/balance_page_controller.dart';
import 'package:food_resturant/feature/view/widget/app_bar.dart';
import 'package:food_resturant/library/enums/enums.dart';
import 'package:food_resturant/library/platforms/image_picker.dart';
import 'package:food_resturant/library/platforms/upload_file.dart';
import 'package:food_resturant/library/widgets/button_widget.dart';
import 'package:food_resturant/library/widgets/cached_network_image_widget.dart';
import 'package:food_resturant/library/widgets/custom_drop_list/custom_drop_down_list.dart';
import 'package:food_resturant/library/widgets/show_bottom_sheet.dart';
import 'package:food_resturant/library/widgets/text_field_widget.dart';
import 'package:food_resturant/library/widgets/text_widget.dart';
import 'package:food_resturant/library/extention/list_extention.dart';
import 'package:food_resturant/library/extention/enum_extention.dart';
import 'package:food_resturant/library/extention/money_extention.dart';
import 'package:uuid/uuid.dart';

import 'package:get/get.dart';
import 'package:queries/collections.dart';

class AddFoodPage extends StatelessWidget {
  final controller = Get.put(AddFoodPageController());

  @override
  Widget build(BuildContext context) {
    return GetBuilder<AddFoodPageController>(
        init: AddFoodPageController(),
        builder: (controller) {
          return Scaffold(
            body: SingleChildScrollView(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  InkWell(
                    onTap: () {
                      pickImage().then((file) => {
                            if (file != null)
                              {
                                controller.selectedImage = file,
                                controller.update(),
                              }
                          });
                    },
                    child: BuildAppBarWidget(
                      title: controller?.selectedImage != null
                          ? CircleAvatar(
                              radius: 60,
                              backgroundImage:
                                  FileImage(controller.selectedImage),
                            )
                          : BuildCachedImageWidget(
                              imageUrl:
                                  controller.storeFoodObject?.icon?.location ??
                                      '',
                              height: 60.0,
                              width: 60.0,
                              borderRadius: 40.0,
                            ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(16.0),
                    child: Form(
                      key: controller.addFoodFormKey,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          CustomText(
                            'Food Name',
                            color: Colors.black,
                            size: 20.0,
                            fontWeight: FontWeight.bold,
                          ),
                          CustomTextField(
                              hint: "Spicy Chicken Pizza",
                              height: 40.0,
                              maxLine: 1,
                              textEditingController:
                                  controller.txtFoodNameController,
                              fontSize: 18.0,
                              isBold: false),
                          CustomText(
                            'Food Category',
                            color: Colors.black,
                            size: 20.0,
                            fontWeight: FontWeight.bold,
                          ),
                          controller.foodCategories != null &&
                                  controller.selectedFoodCategory != null
                              ? SizedBox(
                                  height: 50.0,
                                  child: CustomDropDownList(
                                    padding: EdgeInsets.only(top: 8.0),
                                    context: context,
                                    dropdownColor: Get.theme.primaryColorLight
                                        .withAlpha(150),
                                    onChange: (c) {
                                      controller.selectedFoodCategory =
                                          controller
                                              .foodCategories.listCollection
                                              .firstOrDefault(
                                                  (arg1) => arg1?.name == c);
                                    },
                                    valueItem:
                                        controller.selectedFoodCategory?.name ??
                                            '',
                                    items: controller.foodCategories
                                        ?.map((e) => e.name)
                                        ?.toList(),
                                  ),
                                )
                              : SizedBox(),
                          SizedBox(
                            height: 8.0,
                          ),
                          CustomText(
                            'Cooking Time (min)',
                            color: Colors.black,
                            size: 20.0,
                            fontWeight: FontWeight.bold,
                          ),
                          CustomTextField(
                              hint: "20",
                              textEditingController:
                                  controller.txtCookingTimeController,
                              height: 40.0,
                              maxLine: 1,
                              fontSize: 18.0,
                              textInputType: TextInputType.number,
                              isBold: false),
                          CustomText(
                            'Price (RM)',
                            color: Colors.black,
                            size: 20.0,
                            fontWeight: FontWeight.bold,
                          ),
                          CustomTextField(
                              hint: "28",
                              textEditingController:
                                  controller.txtPriceController,
                              height: 40.0,
                              inputFormatters: [
                                WhitelistingTextInputFormatter(
                                    RegExp(r'^(\d+)?\.?\d{0,2}')),
                              ],
                              textInputType: TextInputType.number,
                              maxLine: 1,
                              fontSize: 18.0,
                              isBold: false),
                          CustomText(
                            'Discount (%)',
                            color: Colors.black,
                            size: 20.0,
                            fontWeight: FontWeight.bold,
                          ),
                          CustomTextField(
                              hint: "0",
                              textEditingController:
                                  controller.txtDiscountController,
                              height: 40.0,
                              maxLenght: 2,
                              maxLine: 1,
                              textInputType: TextInputType.number,
                              fontSize: 18.0,
                              isBold: false),
                          CustomText(
                            'Additional List',
                            color: Colors.black,
                            size: 20.0,
                            fontWeight: FontWeight.bold,
                          ),
                          SizedBox(
                            height: 8.0,
                          ),
                          Container(
                            decoration: BoxDecoration(
                                color:
                                    Get.theme.primaryColorLight.withAlpha(150),
                                borderRadius: BorderRadius.circular(15)),
                            child: Column(
                              children: [
                                ListView.builder(
                                  itemCount:
                                      controller.additionalList.length ?? 0,
                                  shrinkWrap: true,
                                  padding: EdgeInsets.all(8.0),
                                  physics: NeverScrollableScrollPhysics(),
                                  itemBuilder: (context, index) {
                                    final item =
                                        controller.additionalList[index];
                                    return additionalItem(item);
                                  },
                                ),
                                CustomButton(
                                  onTap: () {
                                    showCustomBottomSheet(
                                        additionalBottomSheet());
                                  },
                                  color: Get.theme.accentColor,
                                  child: CustomText(
                                    'Add Additional',
                                    color: Get.theme.primaryColorDark,
                                    size: 18.0,
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                                SizedBox(
                                  height: 10.0,
                                )
                              ],
                            ),
                          ),
                          SizedBox(
                            height: 8.0,
                          ),
                          CustomText(
                            'Optional List',
                            color: Colors.black,
                            size: 20.0,
                            fontWeight: FontWeight.bold,
                          ),
                          SizedBox(
                            height: 8.0,
                          ),
                          Container(
                            decoration: BoxDecoration(
                                color:
                                    Get.theme.primaryColorLight.withAlpha(150),
                                borderRadius: BorderRadius.circular(15)),
                            child: Column(
                              children: [
                                ListView.builder(
                                  itemCount:
                                      controller.optionalList.length ?? 0,
                                  shrinkWrap: true,
                                  padding: EdgeInsets.all(8.0),
                                  physics: NeverScrollableScrollPhysics(),
                                  itemBuilder: (context, index) {
                                    return optionalItem(
                                        controller?.optionalList[index]);
                                  },
                                ),
                                CustomButton(
                                  onTap: () {
                                    showCustomBottomSheet(
                                        optionalBottomSheet());
                                  },
                                  color: Get.theme.accentColor,
                                  child: CustomText(
                                    'Add Optional',
                                    color: Get.theme.primaryColorDark,
                                    size: 18.0,
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                                SizedBox(
                                  height: 10.0,
                                )
                              ],
                            ),
                          ),
                          SizedBox(
                            height: 50,
                          ),
                          SizedBox(
                              height: 40.0,
                              width: Get.width * .9,
                              child: CustomButton(
                                onTap: () {
                                  if (controller?.selectedImage != null) {
                                    controller.overlay.show();
                                    uploadFile(
                                            imageFile:
                                                controller?.selectedImage)
                                        .then((value) {
                                      if (value != null) {
                                        controller.overlay.hide();
                                        controller.addStoreFood(value);
                                      }
                                    });
                                  } else {
                                    controller.addStoreFood();
                                  }
                                },
                                type: 2,
                                borderRadios: 10.0,
                                child: CustomText(
                                  controller?.isEdit ? 'Edit Food' : 'Add Food',
                                  color: Colors.black54,
                                  size: 18.0,
                                  fontWeight: FontWeight.bold,
                                ),
                              )),
                          SizedBox(
                            height: 20,
                          ),
                          SizedBox(
                              height: 40.0,
                              width: Get.width * .9,
                              child: CustomButton(
                                onTap: () {
                                  showCustomBottomSheet(changeFoodStatus());
                                },
                                type: 2,
                                borderRadios: 10.0,
                                child: CustomText(
                                  'Change Status',
                                  color: Colors.black54,
                                  size: 18.0,
                                  fontWeight: FontWeight.bold,
                                ),
                              ))
                        ],
                      ),
                    ),
                  )
                ],
              ),
            ),
          );
        });
  }

  Padding additionalItem(Additionals additionals) {
    return Padding(
        padding: const EdgeInsets.all(8.0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Expanded(
              flex: 7,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  CustomText(
                    additionals?.price?.moneyFormatter ?? '',
                    color: Colors.black,
                    size: 18.0,
                    textAlign: TextAlign.start,
                    fontWeight: FontWeight.bold,
                  ),
                  CustomText(
                    additionals?.name ?? '',
                    color: Colors.black,
                    size: 18.0,
                    textAlign: TextAlign.start,
                    fontWeight: FontWeight.bold,
                  ),
                ],
              ),
            ),
            Expanded(
              flex: 4,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  GestureDetector(
                      onTap: () {
                        showCustomBottomSheet(
                            additionalBottomSheet(additional: additionals));
                      },
                      child: Icon(Icons.edit)),
                  SizedBox(
                    width: 10.0,
                  ),
                  GestureDetector(
                    onTap: () {
                      controller.additionalList
                          ?.removeWhere((element) => element == additionals);
                      controller.update();
                    },
                    child: Icon(
                      Icons.delete,
                      color: Colors.red,
                    ),
                  ),
                ],
              ),
            )
          ],
        ));
  }

  Padding optionalItem(Optionals optionals) {
    return Padding(
        padding: const EdgeInsets.all(8.0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Expanded(
              flex: 7,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  CustomText(
                    optionals?.price?.moneyFormatter ?? '',
                    color: Colors.black,
                    size: 18.0,
                    textAlign: TextAlign.start,
                    fontWeight: FontWeight.bold,
                  ),
                  CustomText(
                    optionals?.name ?? '',
                    color: Colors.black,
                    size: 18.0,
                    textAlign: TextAlign.start,
                    fontWeight: FontWeight.bold,
                  ),
                ],
              ),
            ),
            Expanded(
              flex: 4,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  GestureDetector(
                      onTap: () {
                        showCustomBottomSheet(
                            optionalBottomSheet(optionals: optionals));
                      },
                      child: Icon(Icons.edit)),
                  SizedBox(
                    width: 10.0,
                  ),
                  GestureDetector(
                    onTap: () {
                      controller.optionalList
                          ?.removeWhere((element) => element == optionals);
                      controller.update();
                    },
                    child: Icon(
                      Icons.delete,
                      color: Colors.red,
                    ),
                  ),
                ],
              ),
            )
          ],
        ));
  }

  ListView additionalBottomSheet({Additionals additional}) {
    controller.txtTitleAdditional.text = additional?.name ?? '';
    controller.txtPriceAdditional.text = additional?.price?.toString() ?? '';
    return ListView(
      padding: const EdgeInsets.all(15.0),
      children: [
        CustomText(
          'Additional Item',
          color: Colors.black,
          size: 30.0,
          fontWeight: FontWeight.bold,
        ),
        SizedBox(
          height: 10,
        ),
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            CustomText(
              'Title',
              color: Colors.black,
              size: 20.0,
              fontWeight: FontWeight.bold,
              textAlign: TextAlign.left,
            ),
            CustomTextField(
                hint: "Title Addtion",
                height: 40.0,
                textEditingController: controller.txtTitleAdditional,
                maxLine: 1,
                fontSize: 18.0,
                isBold: false),
            SizedBox(
              height: 10,
            ),
            CustomText(
              'Amount',
              color: Colors.black,
              size: 20.0,
              fontWeight: FontWeight.bold,
              textAlign: TextAlign.left,
            ),
            CustomTextField(
                hint: "Price",
                height: 40.0,
                inputFormatters: [
                  WhitelistingTextInputFormatter(RegExp(r'^(\d+)?\.?\d{0,2}')),
                ],
                // keyboardType: TextInputType.numberWithOptions(decimal: true),
                textInputType: TextInputType.number,
                textEditingController: controller.txtPriceAdditional,
                maxLine: 1,
                fontSize: 18.0,
                isBold: false),
          ],
        ),
        SizedBox(
            height: 40.0,
            width: Get.width * .4,
            child: CustomButton(
              onTap: () {
                if (controller.additionalList
                    .any((element) => element == additional)) {
                  controller.additionalList
                      ?.removeWhere((element) => element == additional);
                }

                var additionalObj = Additionals(
                    createdAt: 1111,
                    id: Uuid().v4(),
                    name: controller.txtTitleAdditional.text,
                    price: controller.txtPriceAdditional.text.isEmpty
                        ? 0.00
                        : num.tryParse(controller.txtPriceAdditional.text));
                controller.additionalList.add(additionalObj);
                controller.update();
                Get.back();
              },
              type: 2,
              borderRadios: 10.0,
              child: CustomText(
                'Add Additional',
                color: Colors.black,
                size: 18.0,
                fontWeight: FontWeight.bold,
              ),
            ))
      ],
    );
  }

  ListView optionalBottomSheet({Optionals optionals}) {
    controller.txtTitleOptional.text = optionals?.name ?? '';
    controller.txtPriceOptional.text = optionals?.price?.toString() ?? '';
    return ListView(
      padding: const EdgeInsets.all(15.0),
      children: [
        CustomText(
          'Optional Item',
          color: Colors.black,
          size: 30.0,
          fontWeight: FontWeight.bold,
        ),
        SizedBox(
          height: 10,
        ),
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            CustomText(
              'Title',
              color: Colors.black,
              size: 20.0,
              fontWeight: FontWeight.bold,
              textAlign: TextAlign.left,
            ),
            CustomTextField(
                hint: "Title Option",
                height: 40.0,
                textEditingController: controller.txtTitleOptional,
                maxLine: 1,
                fontSize: 18.0,
                isBold: false),
            SizedBox(
              height: 10,
            ),
            CustomText(
              'Amount',
              color: Colors.black,
              size: 20.0,
              fontWeight: FontWeight.bold,
              textAlign: TextAlign.left,
            ),
            CustomTextField(
                hint: "Price",
                height: 40.0,
                inputFormatters: [
                  WhitelistingTextInputFormatter(RegExp(r'^(\d+)?\.?\d{0,2}')),
                ],
                textInputType: TextInputType.number,
                textEditingController: controller.txtPriceOptional,
                maxLine: 1,
                fontSize: 18.0,
                isBold: false),
          ],
        ),
        SizedBox(
            height: 40.0,
            width: Get.width * .4,
            child: CustomButton(
              onTap: () {
                if (controller.optionalList
                    .any((element) => element == optionals)) {
                  controller.optionalList
                      ?.removeWhere((element) => element == optionals);
                }
                var optionalsObj = Optionals(
                    createdAt: 1111,
                    id: Uuid().v4(),
                    name: controller.txtTitleOptional.text,
                    price: controller.txtPriceOptional.text.isEmpty
                        ? 0.00
                        : num.tryParse(controller.txtPriceOptional.text));
                controller.optionalList.add(optionalsObj);
                controller.update();
                Get.back();
              },
              type: 2,
              borderRadios: 10.0,
              child: CustomText(
                'Add Optional',
                color: Colors.black,
                size: 18.0,
                fontWeight: FontWeight.bold,
              ),
            ))
      ],
    );
  }

  changeFoodStatus() {
    return GetBuilder<AddFoodPageController>(
        init: AddFoodPageController(),
        builder: (x) {
          return Padding(
            padding: const EdgeInsets.all(15.0),
            child: Column(
              children: [
                CustomText(
                  'Change Food Status',
                  color: Colors.black,
                  size: 30.0,
                  fontWeight: FontWeight.bold,
                ),
                SizedBox(
                  height: 10,
                ),
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: x?.foodStatusRadioTitle
                        ?.map<RadioListTile>((e) => RadioListTile<FoodStatus>(
                              activeColor: Get.theme.primaryColor,
                              title: Text(e?.foodStatusToString),
                              value: FoodStatus.values
                                  ?.firstWhere((element) => element == e),
                              groupValue: x.selectedFoodStatus,
                              onChanged: x.foodChangeStatus,
                            ))
                        ?.toList(),
                  ),
                ),
                SizedBox(
                    height: 40.0,
                    width: Get.width * .5,
                    child: CustomButton(
                      onTap: () {
                        x.changeFoodStatus();

                      },
                      type: 2,
                      borderRadios: 10.0,
                      child: CustomText(
                        'Change Food Status',
                        color: Colors.black,
                        size: 18.0,
                        fontWeight: FontWeight.bold,
                      ),
                    ))
              ],
            ),
          );
        });
  }
}
