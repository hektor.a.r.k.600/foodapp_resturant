import 'dart:convert';
import 'dart:developer';
import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:food_resturant/core/config/api_address.dart';
import 'package:food_resturant/core/config/global_setting.dart';
import 'package:food_resturant/feature/entities/additional.dart';
import 'package:food_resturant/feature/entities/image_obj.dart';
import 'package:food_resturant/feature/entities/optional.dart';
import 'package:food_resturant/feature/server/params/change_store_food_status_params.dart';
import 'package:food_resturant/library/enums/enums.dart';
import 'package:uuid/uuid.dart';
import 'package:food_resturant/feature/depenency_injection.dart';
import 'package:food_resturant/feature/entities/category.dart';
import 'package:food_resturant/feature/entities/food.dart';
import 'package:food_resturant/feature/entities/full_food_obj.dart';
import 'package:food_resturant/feature/entities/get_food_categories_entity.dart';
import 'package:food_resturant/feature/entities/get_store_food_by_id_entity.dart';
import 'package:food_resturant/feature/entities/get_store_food_entity.dart';
import 'package:food_resturant/feature/entities/icon_object.dart';
import 'package:food_resturant/feature/entities/login_entity.dart';
import 'package:food_resturant/feature/entities/main_category.dart';
import 'package:food_resturant/feature/entities/public_entity.dart';
import 'package:food_resturant/feature/server/api_helper.dart';
import 'package:food_resturant/feature/server/api_response.dart';
import 'package:food_resturant/feature/server/params/add_additional_params.dart';
import 'package:food_resturant/feature/server/params/add_and_edit_store_food_params.dart';
import 'package:food_resturant/feature/server/params/get_store_food_by_id_params.dart';
import 'package:food_resturant/library/platforms/loading_overlay.dart';
import 'package:food_resturant/library/extention/enum_extention.dart';
import 'package:food_resturant/library/extention/int_extention.dart';
import 'package:food_resturant/library/platforms/shared_preferences.dart';
import 'package:food_resturant/library/widgets/snackbar.dart';
import 'package:get/get.dart';

class AddFoodPageController extends GetxController {
  final overlay = LoadingOverlay.of(Get.context);

  // List<Additional> additionalList = List<Additional>();
  // List<int> additionalEditList = List<int>();
  List<Additionals> additionalList = <Additionals>[];
  List<Optionals> optionalList = <Optionals>[];

  final addFoodFormKey = GlobalKey<FormState>();

  TextEditingController txtFoodNameController = TextEditingController(text: '');
  TextEditingController txtDiscountController = TextEditingController(text: '');
  TextEditingController txtPriceController = TextEditingController(text: '');
  TextEditingController txtCookingTimeController =
      TextEditingController(text: '');

  TextEditingController txtTitleAdditional = TextEditingController(text: '');
  TextEditingController txtPriceAdditional = TextEditingController(text: '');
  TextEditingController txtTitleOptional = TextEditingController(text: '');
  TextEditingController txtPriceOptional = TextEditingController(text: '');

  MainCategory selectedFoodCategory;
  FoodStatus selectedFoodStatus;

  List<MainCategory> foodCategories;

  File selectedImage;
  String foodImage;

  GetStoreFoodObject storeFoodObject;

  bool isEdit = false;

  LoginObject user;
  List<FoodStatus> foodStatusRadioTitle = [];

  var txtControllerValue = TextEditingController(text: '');

  @override
  void onInit() {
    foodStatusRadioTitle = FoodStatus?.values;
    user = sl<GlobalSetting>().user;
    foodCategories = sl<GlobalSetting>().foodCategoriesObject;
    selectedFoodCategory = foodCategories != null && foodCategories.isNotEmpty
        ? foodCategories.first
        : null;
    update();
    super.onInit();
  }

  @override
  void onReady() {
    storeFoodObject = Get.arguments;
    if (storeFoodObject != null) {
      isEdit = true;
      getStoreFoodById();
    }
  }

  addStoreFood([IconObject foodIconObject]) async {
    if (sl<GlobalSetting>().user == null) return;

    if (addFoodFormKey.currentState.validate()) {
      var foodCategoryParams = AddAndEditStoreFoodParams(
          storeFoodId: isEdit ? storeFoodObject?.id : Uuid().v4(),
          store: user?.store,
          food: FullFoodObj(
              icon: isEdit && foodIconObject == null
                  ? storeFoodObject?.icon
                  : foodIconObject,
              additionals: additionalList,
              optionals: optionalList,
              food: Food(
                icon: isEdit && foodIconObject == null
                    ? storeFoodObject?.icon
                    : foodIconObject,
                id: isEdit ? storeFoodObject?.id : Uuid().v4(),
                status: 1,
                createdAt: 0,
                totalPrice: num.tryParse(txtPriceController.text) ?? 0.0,
                cookingTime: num.tryParse(txtCookingTimeController.text) ?? 0,
                name: txtFoodNameController.text,
                discountPrice: 0,
                discountValue: num.tryParse(txtDiscountController.text),
              ),
              mainCategory: MainCategory(
                  foodCount: 0,
                  icon: isEdit
                      ? storeFoodObject?.category?.icon
                      : IconObject(location: '', type: 1, id: Uuid().v4()),
                  createdAt: 0,
                  name: selectedFoodCategory?.name,
                  id: selectedFoodCategory?.id)));

      log(json.encode(foodCategoryParams.toJson()));

      final response = await overlay.during(sl<ApiHelper>().post(
          isEdit ? ApiAddress.EDIT_FOOD_STORE : ApiAddress.ADD_FOOD_STORE,
          foodCategoryParams.toJson()));
      var result = ApiResponse.returnResponse(
          response, PublicEntity.fromJson(response.data));
      if (result.status == ApiStatus.COMPLETED && result?.data?.status) {
        // showSnackBar(text: result?.data?.message ?? '');
        Get.back(result: selectedFoodCategory);
      } else {
        showSnackBar(text: result?.data?.message ?? '');
      }
    }
  }

  getStoreFoodById() async {
    var getStoreFoodByIdParams =
        GetStoreFoodByIdParams(storeFoodId: storeFoodObject?.id);
    final response = await overlay.during(sl<ApiHelper>().getWithParam(
        ApiAddress.GET_STORE_FOOD_BY_ID, getStoreFoodByIdParams.toJson()));
    var result = ApiResponse.returnResponse(
        response, GetStoreFoodByIdEntity.fromJson(response.data));
    if (result.status == ApiStatus.COMPLETED && result?.data?.status) {
      if (result.data != null) {
        final objectFood = result.data?.getStoreFoodByIdObject?.food;
        selectedFoodStatus =  objectFood?.food?.status?.enumFoodStatus;
        txtFoodNameController.text = (objectFood?.food?.name) ?? '';
        txtDiscountController.text =
            (objectFood?.food?.discountValue?.toString()) ?? '';
        txtPriceController.text =
            (objectFood?.food?.totalPrice?.toString()) ?? '';
        txtCookingTimeController.text =
            (objectFood?.food?.cookingTime?.toString()) ?? '';
        selectedFoodCategory = objectFood?.mainCategory;
        optionalList = objectFood?.optionals;
        additionalList = objectFood?.additionals;

        update();
      }
    } else {
      showSnackBar(text: result?.data?.message ?? '');
    }
  }

  void foodChangeStatus(FoodStatus value) {
    selectedFoodStatus = value;
    update();
  }

  changeFoodStatus() async{
    var changeFoodStatus = ChangeStoreFoodStatusParams(
        storeFoodId: storeFoodObject?.id,
        status: selectedFoodStatus?.foodStatusToInt);
    final response = await overlay.during(sl<ApiHelper>().post(
        ApiAddress.CHANGE_STORE_FOOD_STATUS, changeFoodStatus.toJson()));
    var result = ApiResponse.returnResponse(
        response, PublicEntity.fromJson(response.data));
    if (result.status == ApiStatus.COMPLETED && result?.data?.status) {
      Get.back();
    } else {
      showSnackBar(text: result?.data?.message ?? '');
    }
  }
}
