import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:food_resturant/core/config/routes.dart';
import 'package:food_resturant/feature/entities/get_store_rate_entity.dart';
import 'package:food_resturant/feature/entities/get_ticket_entity.dart';
import 'package:food_resturant/feature/view/page/all_tickets/all_tickets_page_controller.dart';
import 'package:food_resturant/feature/view/page/food_list_page/food_list_page_controller.dart';
import 'package:food_resturant/feature/view/page/order_history_page/order_history_page_controller.dart';
import 'package:food_resturant/feature/view/page/tables_page/tables_page_controller.dart';
import 'package:food_resturant/feature/view/page/withdraw_page/withdraw_page_controller.dart';
import 'package:food_resturant/feature/view/widget/app_bar.dart';
import 'package:food_resturant/feature/view/widget/box_shadow_widget.dart';
import 'package:food_resturant/library/widgets/button_widget.dart';
import 'package:food_resturant/library/widgets/custom_drop_list/custom_drop_down_list.dart';
import 'package:food_resturant/library/widgets/text_widget.dart';
import 'package:get/get.dart';
import 'package:qr_flutter/qr_flutter.dart';

class AllTicketsPage extends StatelessWidget {
  final controller = Get.put(AllTicketsPageController());

  @override
  Widget build(BuildContext context) {
    return GetBuilder<AllTicketsPageController>(
        init: AllTicketsPageController(),
        builder: (x) {
          return Scaffold(
              floatingActionButton: controller.selectedIndex == 0
                  ? FloatingActionButton(
                      onPressed: () {
                        Get.toNamed(Routes.CreateTicketPage).then((value) => {
                              if (value != null)
                                {
                                  controller.getTicketApi(),
                                }
                            });
                      },
                      backgroundColor: Get.theme.primaryColor,
                      child: Center(
                        child: Icon(
                          Icons.message,
                          color: Colors.white,
                        ),
                      ),
                    )
                  : SizedBox(),
              appBar: AppBar(
                iconTheme: IconThemeData(opacity: 0.0),
                toolbarHeight: 200.0,
                backgroundColor: Get.theme.scaffoldBackgroundColor,
                elevation: 0.0,
                bottom: TabBar(
                    controller: controller.tabController,
                    indicatorColor: Get.theme.primaryColor,
                    tabs: controller.tabBuilder()),
                flexibleSpace: BuildAppBarWidget(
                  title: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      CustomText(
                        'All Tickets',
                        color: Colors.white,
                        size: 25.0,
                        fontWeight: FontWeight.bold,
                      ),
                      SizedBox(
                        height: 2.0,
                      ),
                      CustomText(
                        'View Customers Tickets',
                        color: Colors.white,
                        size: 18.0,
                        fontWeight: FontWeight.w500,
                      ),
                    ],
                  ),
                ),
              ),
              body: Padding(
                padding: const EdgeInsets.all(5.0),
                child: Obx(() => TabBarView(
                      controller: controller.tabController,
                      children: [
                        RefreshIndicator(
                          backgroundColor: Get.theme.primaryColorDark,
                          onRefresh: () {
                            return controller.getTicketApi();
                          },
                          child: ListView.builder(
                            padding: EdgeInsets.all(5.0),
                            itemCount:
                                controller.ticketList?.value?.length ?? 0,
                            itemBuilder: (context, index) =>
                                buildListItemTicketAndSupport(
                                    controller?.ticketList?.value[index]),
                          ),
                        ),
                        //  RefreshIndicator(
                        //   backgroundColor: Get.theme.primaryColorDark,
                        //   onRefresh: (){
                        //     return controller.getTicketApi();
                        //   },
                        //   child: ListView.builder(
                        //         padding: EdgeInsets.all(5.0),
                        //         itemCount: controller.supportList?.value?.length??0,
                        //         itemBuilder: (context, index) =>
                        //             buildListItemTicketAndSupport(
                        //                 controller.supportList.value[index]),
                        //       ),
                        // ),
                        RefreshIndicator(
                          backgroundColor: Get.theme.primaryColorDark,
                          onRefresh: () {
                            return controller.getStoreRate();
                          },
                          child: ListView.builder(
                            padding: EdgeInsets.all(5),
                            itemCount: controller?.comment?.value?.length ?? 0,
                            itemBuilder: (context, index) {
                              final item = controller?.comment?.value[index];
                              return buildListItemComment(item);
                            },
                          ),
                        ),
                      ],
                    )),
              ));
        });
  }

  //todo add item object instead of index
  Widget buildListItemTicketAndSupport(GetTicketObject ticketObject) {
    return InkWell(
      onTap: () {
        Get.toNamed(Routes.ChatPage, arguments: ticketObject);
      },
      child: Card(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10.0),
        ),
        color: Get.theme.accentColor,
        elevation: 4,
        margin: EdgeInsets.all(5.0),
        child: Container(
          height: 80.0,
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                CustomText(
                  ticketObject?.ticket?.title ?? '',
                  color: Colors.black,
                  size: 20.0,
                  fontWeight: FontWeight.bold,
                ),
                CustomText(
                  ticketObject?.ticket?.text ?? '',
                  color: Colors.black,
                  size: 20.0,
                  fontWeight: FontWeight.normal,
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget buildListItemComment(GetStoreRateEntityObject item) {
    return InkWell(
      child: Card(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10.0),
        ),
        color: Get.theme.accentColor,
        elevation: 4,
        margin: EdgeInsets.all(5.0),
        child: Container(
          height: item?.answer != null ? 250.0 : 200,
          child: Padding(
            padding: const EdgeInsets.all(15.0),
            child: Column(
              children: [
                ListTile(
                    dense: true,
                    contentPadding: EdgeInsets.zero,
                    trailing: Row(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        RatingBar.builder(
                          ignoreGestures: true,
                          itemSize: 24,
                          initialRating: item?.rate?.rateValue ?? 0,
                          minRating: 1,
                          direction: Axis.horizontal,
                          allowHalfRating: true,
                          itemCount: 5,
                          itemBuilder: (context, _) => Icon(
                            Icons.star,
                            color: Colors.yellow,
                          ),
                          onRatingUpdate: (rating) {
                            print(rating);
                          },
                        ),
                        // IconButton(
                        //   iconSize: 28,
                        //   icon: Icon(Icons.delete),
                        //   onPressed: () {},
                        //   color: Colors.red,
                        // ),
                      ],
                    ),
                    title: Row(
                      children: [
                        CustomText(
                          (item?.rate?.voter?.name ?? '') +
                              (item?.rate?.voter?.familyName ?? ''),
                          color: Colors.black,
                          size: 20.0,
                          fontWeight: FontWeight.bold,
                        ),
                      ],
                    ),
                    subtitle: Row(
                      children: [
                        CustomText(
                          item?.rate?.voter?.userName ?? '',
                          color: Colors.black,
                          size: 18.0,
                        ),
                      ],
                    ),
                    enabled: true,
                    onTap: () {
                      /* react to the tile being tapped */
                    }),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 30),
                  child: Divider(
                    color: Colors.grey[500],
                  ),
                ),
                Align(
                  alignment: Alignment.centerLeft,
                  child: CustomText(
                    item?.rate?.comment ?? '',
                    color: Colors.black,
                    textAlign: TextAlign.left,
                    characterCount: 100,
                    size: 18.0,
                  ),
                ),
                SizedBox(
                  height: 10.0,
                ),
                // controller.selectedCommentStatus == 0
                //     ?
                item?.answer == null
                    ? SizedBox()
                    : Container(
                        height: 80,
                        width: Get.width,
                        decoration: BoxDecoration(
                            color: Colors.grey.withOpacity(.1),
                            borderRadius: BorderRadius.circular(10.0)),
                        child: Padding(
                          padding: const EdgeInsets.all(10.0),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              CustomText(
                                'Answer',
                                color: Colors.blue,
                                size: 20.0,
                                fontWeight: FontWeight.bold,
                              ),
                              CustomText(
                                item?.answer?.comment ?? '',
                                color: Colors.blue,
                                size: 18.0,
                                textAlign: TextAlign.start,
                                characterCount: 100,
                              ),
                            ],
                          ),
                        ),
                      )
                // : Expanded(
                //     child: Align(
                //       alignment: Alignment.bottomRight,
                //       child: InkWell(
                //         onTap: () {},
                //         child: Padding(
                //           padding: const EdgeInsets.all(8.0),
                //           child: Row(
                //             mainAxisSize: MainAxisSize.min,
                //             children: [
                //               CustomText(
                //                 'Reply',
                //                 color: Colors.blue,
                //                 textAlign: TextAlign.start,
                //                 size: 20.0,
                //                 fontWeight: FontWeight.bold,
                //               ),
                //               Icon(
                //                 Icons.arrow_forward_ios_rounded,
                //                 color: Colors.blue,
                //                 size: 18,
                //               )
                //             ],
                //           ),
                //         ),
                //       ),
                //     ),
                //   )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
