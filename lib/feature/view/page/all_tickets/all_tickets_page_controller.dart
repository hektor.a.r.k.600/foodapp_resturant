import 'package:flutter/material.dart';
import 'package:food_resturant/core/config/api_address.dart';
import 'package:food_resturant/core/config/global_setting.dart';
import 'package:food_resturant/feature/depenency_injection.dart';
import 'package:food_resturant/feature/entities/get_store_rate_entity.dart';
import 'package:food_resturant/feature/entities/get_ticket_entity.dart';
import 'package:food_resturant/feature/entities/get_ticket_entity.dart';
import 'package:food_resturant/feature/entities/login_entity.dart';
import 'package:food_resturant/feature/entities/store.dart';
import 'package:food_resturant/feature/entities/ticket.dart';
import 'package:food_resturant/feature/server/api_helper.dart';
import 'package:food_resturant/feature/server/api_response.dart';
import 'package:food_resturant/feature/server/params/get_store_rate_params.dart';
import 'package:food_resturant/feature/server/params/get_ticket_params.dart';
import 'package:food_resturant/library/platforms/loading_overlay.dart';
import 'package:food_resturant/library/widgets/snackbar.dart';
import 'package:food_resturant/library/widgets/text_widget.dart';
import 'package:get/get.dart';

class AllTicketsPageController extends GetxController
    with SingleGetTickerProviderMixin {
  int selectedIndex = 0;
  final overlay = LoadingOverlay.of(Get.context);

  RxList<GetTicketObject> ticketList = <GetTicketObject>[].obs;
  // RxList<GetTicketObject> supportList = <GetTicketObject>[].obs;

  List<String> commentStatusRadioTitle = List<String>(2);
  // int selectedCommentStatus = 0;

  List<String> tabsTitle = List<String>(2);
  TabController tabController;

  LoginObject user;

  RxList<GetStoreRateEntityObject> comment = <GetStoreRateEntityObject>[].obs;

  addItemsToTabs() {
    tabsTitle[0] = ("Tickets");
    // tabsTitle[1] = ("Support");
    tabsTitle[1] = ("Comments");
  }

  List<Widget> tabBuilder() {
    List<Widget> listTabs = List<Widget>();
    tabsTitle.forEach((title) {
      listTabs.add(
        Padding(
          padding: const EdgeInsets.only(bottom: 12.0, top: 11.0),
          child: CustomText(
            title,
            color: Colors.black,
            size: 20.0,
            fontWeight: FontWeight.normal,
          ),
        ),
      );
    });
    return listTabs;
  }

  // addCommentStatusToRadios() {
  //   commentStatusRadioTitle[0] = ("Answered");
  //   commentStatusRadioTitle[1] = ("Not Answered");
  // }

  // List<Widget> commentStatusRadioBuilder(List<String> radiosTitles) {
  //   List<Widget> listRadios = List<Widget>();
  //   for (var i = 0; i < radiosTitles.length; i++) {
  //     listRadios.add(
  //       Row(
  //         children: [
  //           SizedBox(
  //             height: 30.0,
  //             width: 30.0,
  //             child: Radio(
  //               activeColor: Get.theme.primaryColor,
  //               value: i,
  //               groupValue: selectedCommentStatus,
  //               onChanged: radioButtonOnChange,
  //             ),
  //           ),
  //           CustomText(
  //             radiosTitles[i],
  //             color: Colors.black,
  //             size: 18.0,
  //             fontWeight: FontWeight.normal,
  //           )
  //         ],
  //       ),
  //     );
  //   }
  //
  //   return listRadios;
  // }

  radioButtonOnChange(var selectedId) {
    // selectedCommentStatus = selectedId;
    update();
  }

  @override
  void onInit() {
    user = sl<GlobalSetting>().user;

    tabController = TabController(length: 2, vsync: this);
    addItemsToTabs();
    // addCommentStatusToRadios();
    tabController.addListener(() {
      selectedIndex = tabController.index;
      update();
    });

    super.onInit();
  }

  @override
  void onReady() async {
    getTicketApi();
    await getStoreRate();
    super.onReady();
  }

  getTicketApi() async {
    ticketList.value = [];
    // supportList.value =[];
    var getTicketParams = GetTicketParams(pageNo: -1, status: -1);
    try {
      final response = await overlay.during(sl<ApiHelper>()
          .getWithParam(ApiAddress.GET_TICKETS, getTicketParams.toJson()));
      var result = ApiResponse.returnResponse(
          response, GetTicketEntity.fromJson(response.data));
      if (result.status == ApiStatus.COMPLETED) {
        // showSnackBar(
        //     text: result?.message ?? '', snackBarType: SnackBarType.Report);
        ticketList.value = result.data?.getTicketEntityObject;
        // supportList.value = result.data?.getTicketObject
        //     ?.where((element) => element.status == 1)
        //     ?.toList();

      }
    } on Exception catch (e) {}
  }

  getStoreRate() async {
    var storerate = GetStoreRateParams(storeId: user?.store?.id);

    final response = await overlay.during(sl<ApiHelper>()
        .getWithParam(ApiAddress.GET_STORE_RATE, storerate.toJson()));
    var result = ApiResponse.returnResponse(
        response, GetStoreRateEntity.fromJson(response.data));
    if (result.status == ApiStatus.COMPLETED && result?.data?.status) {
      // showSnackBar(text: result?.data?.message ?? '');
      comment.value = result.data?.getStoreRateEntityObject;
    } else {
      showSnackBar(text: result?.data?.message ?? '');
    }
  }
}
