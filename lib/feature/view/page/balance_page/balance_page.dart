import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:food_resturant/feature/entities/additional.dart';
import 'package:food_resturant/feature/entities/full_food_obj.dart';
import 'package:food_resturant/feature/entities/optional.dart';
import 'package:food_resturant/feature/view/page/add_food_page/add_food_page_controller.dart';
import 'package:food_resturant/feature/view/page/add_food_page/add_food_page_controller.dart';
import 'package:food_resturant/feature/view/page/balance_page/balance_page_controller.dart';
import 'package:food_resturant/feature/view/widget/app_bar.dart';
import 'package:food_resturant/library/platforms/image_picker.dart';
import 'package:food_resturant/library/platforms/upload_file.dart';
import 'package:food_resturant/library/widgets/button_widget.dart';
import 'package:food_resturant/library/widgets/cached_network_image_widget.dart';
import 'package:food_resturant/library/widgets/custom_drop_list/custom_drop_down_list.dart';
import 'package:food_resturant/library/widgets/show_bottom_sheet.dart';
import 'package:food_resturant/library/widgets/text_field_widget.dart';
import 'package:food_resturant/library/widgets/text_widget.dart';
import 'package:food_resturant/library/extention/list_extention.dart';
import 'package:food_resturant/library/extention/money_extention.dart';
import 'package:uuid/uuid.dart';

import 'package:get/get.dart';
import 'package:queries/collections.dart';

class BalancePage extends StatelessWidget {
  final controller = Get.put(BalancePageController());

  @override
  Widget build(BuildContext context) {
    return GetBuilder<BalancePageController>(
        init: BalancePageController(),
        builder: (controller) {
          return Scaffold(
            body: SingleChildScrollView(
              child: Column(
                children: [
                  BuildAppBarWidget(
                    title: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        CustomText(
                          'Balance',
                          color: Colors.white,
                          size: 25.0,
                          fontWeight: FontWeight.bold,
                        ),
                      ],
                    ),
                  ),
                  Container(
                    width: Get.width * .9,
                    height: 80,
                    margin: const EdgeInsets.only(
                        right: 16.0, left: 16.0, top: 16.0, bottom: 16.0),
                    decoration: BoxDecoration(
                      color: Get.theme.accentColor,
                      borderRadius: BorderRadius.circular(10.0),
                    ),
                    child: Column(
                      children: [
                        SizedBox(
                          height: 25,
                        ),
                        CustomText(
                          'RM' +
                              (controller?.balance?.object?.balance == null
                                  ? "0.0"
                                  : controller.balance.object.balance
                                      .toStringAsFixed(2)),
                          color: Colors.black,
                          size: 30.0,
                          fontWeight: FontWeight.bold,
                        ),
                        SizedBox(
                          height: 10,
                        ),
                        // Row(
                        //   mainAxisAlignment: MainAxisAlignment.spaceAround,
                        //   children: [
                        //     Container(
                        //       decoration: BoxDecoration(
                        //         color: Get.theme.accentColor,
                        //         border: Border.all(
                        //             color: Get.theme.primaryColorLight,
                        //             width: 3),
                        //         borderRadius: BorderRadius.circular(5.0),
                        //       ),
                        //       padding: EdgeInsets.all(10),
                        //       child:   CustomText(
                        //         'Ticket',
                        //         color: Colors.black,
                        //         size: 20.0,
                        //         fontWeight: FontWeight.bold,
                        //       ),
                        //     ),
                        //     Container(
                        //       decoration: BoxDecoration(
                        //         color: Get.theme.accentColor,
                        //         border: Border.all(
                        //             color: Get.theme.primaryColorLight,
                        //             width: 3),
                        //         borderRadius: BorderRadius.circular(5.0),
                        //       ),
                        //       padding: EdgeInsets.all(10),
                        //       child:   CustomText(
                        //         'Ticket',
                        //         color: Colors.black,
                        //         size: 20.0,
                        //         fontWeight: FontWeight.bold,
                        //       ),
                        //     ),
                        //     Container(
                        //       decoration: BoxDecoration(
                        //         color: Get.theme.accentColor,
                        //         border: Border.all(
                        //             color: Get.theme.primaryColorLight,
                        //             width: 3),
                        //         borderRadius: BorderRadius.circular(5.0),
                        //       ),
                        //       padding: EdgeInsets.all(10),
                        //       child:   CustomText(
                        //         'Ticket',
                        //         color: Colors.black,
                        //         size: 20.0,
                        //         fontWeight: FontWeight.bold,
                        //       ),
                        //     ),
                        //   ],
                        // )
                      ],
                    ),
                  ),
                  // Container(
                  //   width: Get.width * .9,
                  //   child: Row(
                  //     children: [
                  //       Container(
                  //           decoration: BoxDecoration(
                  //             color: Get.theme.accentColor,
                  //             borderRadius: BorderRadius.circular(5.0),
                  //           ),
                  //           width: 40,
                  //           height: 40,
                  //           child: IconButton(
                  //               icon: Icon(Icons.add), onPressed: () => {})),
                  //       SizedBox(
                  //         width: 10,
                  //       ),
                  //       Expanded(
                  //         child: CustomTextField(
                  //             textEditingController:
                  //                 controller.txtControllerValue,
                  //             height: 50.0,
                  //             textInputType: TextInputType.number,
                  //             textAlign: TextAlign.center,
                  //             maxLine: 1,
                  //             fontSize: 18.0,
                  //             isBold: false),
                  //       ),
                  //       SizedBox(
                  //         width: 10,
                  //       ),
                  //       Container(
                  //           decoration: BoxDecoration(
                  //             color: Get.theme.accentColor,
                  //             borderRadius: BorderRadius.circular(5.0),
                  //           ),
                  //           width: 40,
                  //           height: 40,
                  //           child: IconButton(
                  //               icon: Icon(Icons.minimize),
                  //               onPressed: () => {})),
                  //     ],
                  //   ),
                  // ),
                ],
              ),
            ),
          );
        });
  }

  Padding additionalItem(Additionals additionals) {
    return Padding(
        padding: const EdgeInsets.all(8.0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Expanded(
              flex: 7,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  CustomText(
                    additionals?.price?.moneyFormatter ?? '',
                    color: Colors.black,
                    size: 18.0,
                    textAlign: TextAlign.start,
                    fontWeight: FontWeight.bold,
                  ),
                  CustomText(
                    additionals?.name ?? '',
                    color: Colors.black,
                    size: 18.0,
                    textAlign: TextAlign.start,
                    fontWeight: FontWeight.bold,
                  ),
                ],
              ),
            ),
            Expanded(
              flex: 4,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  GestureDetector(
                      onTap: () {
                        showCustomBottomSheet(
                            additionalBottomSheet(additional: additionals));
                      },
                      child: Icon(Icons.edit)),
                  SizedBox(
                    width: 10.0,
                  ),
                  GestureDetector(
                    onTap: () {
                      controller.additionalList
                          ?.removeWhere((element) => element == additionals);
                      controller.update();
                    },
                    child: Icon(
                      Icons.delete,
                      color: Colors.red,
                    ),
                  ),
                ],
              ),
            )
          ],
        ));
  }

  Padding optionalItem(Optionals optionals) {
    return Padding(
        padding: const EdgeInsets.all(8.0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Expanded(
              flex: 7,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  CustomText(
                    optionals?.price?.moneyFormatter ?? '',
                    color: Colors.black,
                    size: 18.0,
                    textAlign: TextAlign.start,
                    fontWeight: FontWeight.bold,
                  ),
                  CustomText(
                    optionals?.name ?? '',
                    color: Colors.black,
                    size: 18.0,
                    textAlign: TextAlign.start,
                    fontWeight: FontWeight.bold,
                  ),
                ],
              ),
            ),
            Expanded(
              flex: 4,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  GestureDetector(
                      onTap: () {
                        showCustomBottomSheet(
                            optionalBottomSheet(optionals: optionals));
                      },
                      child: Icon(Icons.edit)),
                  SizedBox(
                    width: 10.0,
                  ),
                  GestureDetector(
                    onTap: () {
                      controller.optionalList
                          ?.removeWhere((element) => element == optionals);
                      controller.update();
                    },
                    child: Icon(
                      Icons.delete,
                      color: Colors.red,
                    ),
                  ),
                ],
              ),
            )
          ],
        ));
  }

  ListView additionalBottomSheet({Additionals additional}) {
    controller.txtTitleAdditional.text = additional?.name ?? '';
    controller.txtPriceAdditional.text = additional?.price?.toString() ?? '';
    return ListView(
      padding: const EdgeInsets.all(15.0),
      children: [
        CustomText(
          'Additional Item',
          color: Colors.black,
          size: 30.0,
          fontWeight: FontWeight.bold,
        ),
        SizedBox(
          height: 10,
        ),
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            CustomText(
              'Title',
              color: Colors.black,
              size: 20.0,
              fontWeight: FontWeight.bold,
              textAlign: TextAlign.left,
            ),
            CustomTextField(
                hint: "Title Addtion",
                height: 40.0,
                textEditingController: controller.txtTitleAdditional,
                maxLine: 1,
                fontSize: 18.0,
                isBold: false),
            SizedBox(
              height: 10,
            ),
            CustomText(
              'Amount',
              color: Colors.black,
              size: 20.0,
              fontWeight: FontWeight.bold,
              textAlign: TextAlign.left,
            ),
            CustomTextField(
                hint: "Price",
                height: 40.0,
                inputFormatters: [
                  WhitelistingTextInputFormatter(RegExp(r'^(\d+)?\.?\d{0,2}')),
                ],
                // keyboardType: TextInputType.numberWithOptions(decimal: true),
                textInputType: TextInputType.number,
                textEditingController: controller.txtPriceAdditional,
                maxLine: 1,
                fontSize: 18.0,
                isBold: false),
          ],
        ),
        SizedBox(
            height: 40.0,
            width: Get.width * .4,
            child: CustomButton(
              onTap: () {
                if (controller.additionalList
                    .any((element) => element == additional)) {
                  controller.additionalList
                      ?.removeWhere((element) => element == additional);
                }

                var additionalObj = Additionals(
                    createdAt: 1111,
                    id: Uuid().v4(),
                    name: controller.txtTitleAdditional.text,
                    price: num.tryParse(controller.txtPriceAdditional.text));
                controller.additionalList.add(additionalObj);
                controller.update();
                Get.back();
              },
              type: 2,
              borderRadios: 10.0,
              child: CustomText(
                'Add Additional',
                color: Colors.black,
                size: 18.0,
                fontWeight: FontWeight.bold,
              ),
            ))
      ],
    );
  }

  ListView optionalBottomSheet({Optionals optionals}) {
    controller.txtTitleOptional.text = optionals?.name ?? '';
    controller.txtPriceOptional.text = optionals?.price?.toString() ?? '';
    return ListView(
      padding: const EdgeInsets.all(15.0),
      children: [
        CustomText(
          'Optional Item',
          color: Colors.black,
          size: 30.0,
          fontWeight: FontWeight.bold,
        ),
        SizedBox(
          height: 10,
        ),
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            CustomText(
              'Title',
              color: Colors.black,
              size: 20.0,
              fontWeight: FontWeight.bold,
              textAlign: TextAlign.left,
            ),
            CustomTextField(
                hint: "Title Option",
                height: 40.0,
                textEditingController: controller.txtTitleOptional,
                maxLine: 1,
                fontSize: 18.0,
                isBold: false),
            SizedBox(
              height: 10,
            ),
            CustomText(
              'Amount',
              color: Colors.black,
              size: 20.0,
              fontWeight: FontWeight.bold,
              textAlign: TextAlign.left,
            ),
            CustomTextField(
                hint: "Price",
                height: 40.0,
                inputFormatters: [
                  WhitelistingTextInputFormatter(RegExp(r'^(\d+)?\.?\d{0,2}')),
                ],
                textInputType: TextInputType.number,
                textEditingController: controller.txtPriceOptional,
                maxLine: 1,
                fontSize: 18.0,
                isBold: false),
          ],
        ),
        SizedBox(
            height: 40.0,
            width: Get.width * .4,
            child: CustomButton(
              onTap: () {
                if (controller.optionalList
                    .any((element) => element == optionals)) {
                  controller.optionalList
                      ?.removeWhere((element) => element == optionals);
                }
                var optionalsObj = Optionals(
                    createdAt: 1111,
                    id: Uuid().v4(),
                    name: controller.txtTitleOptional.text,
                    price: num.tryParse(controller.txtPriceOptional.text));
                controller.optionalList.add(optionalsObj);
                controller.update();
                Get.back();
              },
              type: 2,
              borderRadios: 10.0,
              child: CustomText(
                'Add Optional',
                color: Colors.black,
                size: 18.0,
                fontWeight: FontWeight.bold,
              ),
            ))
      ],
    );
  }
}
