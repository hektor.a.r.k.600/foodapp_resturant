import 'dart:convert';
import 'dart:developer';
import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:food_resturant/core/config/api_address.dart';
import 'package:food_resturant/core/config/global_setting.dart';
import 'package:food_resturant/feature/entities/additional.dart';
import 'package:food_resturant/feature/entities/get_balance.dart';
import 'package:food_resturant/feature/entities/image_obj.dart';
import 'package:food_resturant/feature/entities/optional.dart';
import 'package:food_resturant/library/enums/enums.dart';
import 'package:uuid/uuid.dart';
import 'package:food_resturant/feature/depenency_injection.dart';
import 'package:food_resturant/feature/entities/category.dart';
import 'package:food_resturant/feature/entities/food.dart';
import 'package:food_resturant/feature/entities/full_food_obj.dart';
import 'package:food_resturant/feature/entities/get_food_categories_entity.dart';
import 'package:food_resturant/feature/entities/get_store_food_by_id_entity.dart';
import 'package:food_resturant/feature/entities/get_store_food_entity.dart';
import 'package:food_resturant/feature/entities/icon_object.dart';
import 'package:food_resturant/feature/entities/login_entity.dart';
import 'package:food_resturant/feature/entities/main_category.dart';
import 'package:food_resturant/feature/entities/public_entity.dart';
import 'package:food_resturant/feature/server/api_helper.dart';
import 'package:food_resturant/feature/server/api_response.dart';
import 'package:food_resturant/feature/server/params/add_additional_params.dart';
import 'package:food_resturant/feature/server/params/add_and_edit_store_food_params.dart';
import 'package:food_resturant/feature/server/params/get_store_food_by_id_params.dart';
import 'package:food_resturant/library/platforms/loading_overlay.dart';
import 'package:food_resturant/library/platforms/shared_preferences.dart';
import 'package:food_resturant/library/widgets/snackbar.dart';
import 'package:get/get.dart';

class BalancePageController extends GetxController {
  final overlay = LoadingOverlay.of(Get.context);

  // List<Additional> additionalList = List<Additional>();
  // List<int> additionalEditList = List<int>();
  List<Additionals> additionalList = <Additionals>[];
  List<Optionals> optionalList = <Optionals>[];
  GetBalance balance;

  TextEditingController txtTitleAdditional = TextEditingController(text: '');
  TextEditingController txtPriceAdditional = TextEditingController(text: '');
  TextEditingController txtTitleOptional = TextEditingController(text: '');
  TextEditingController txtPriceOptional = TextEditingController(text: '');

  MainCategory selectedFoodCategory;
  FoodStatus selectedFoodStatus;

  List<MainCategory> foodCategories;

  GetStoreFoodObject storeFoodObject;

  bool isEdit = false;

  LoginObject user;
  List<FoodStatus> foodStatusRadioTitle = [];

  var txtControllerValue = TextEditingController(text: '');

  @override
  void onInit() {
    user = sl<GlobalSetting>().user;
    foodCategories = sl<GlobalSetting>().foodCategoriesObject;
    selectedFoodCategory = foodCategories != null && foodCategories.isNotEmpty
        ? foodCategories.first
        : null;
    update();
    super.onInit();
  }

  @override
  void onReady() {
    storeFoodObject = Get.arguments;
    getBallance();
  }

  void foodChangeStatus(FoodStatus value) {
    selectedFoodStatus = value;
    update();
  }

  getBallance() async {
    final response = await overlay.during(sl<ApiHelper>().getWithParam(
        ApiAddress.GET_BALANCE, {"StoreId": user?.store?.id ?? ""}));
    var result = ApiResponse.returnResponse(
        response, GetBalance.fromJson(response.data));
    if (result.status == ApiStatus.COMPLETED && result.data.status) {
      balance = result.data;
      update();
    } else {
      showSnackBar(text: result?.data?.message ?? '');
    }
  }
}
