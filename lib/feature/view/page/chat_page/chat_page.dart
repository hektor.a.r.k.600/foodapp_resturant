import 'package:flutter/material.dart';
import 'package:food_resturant/feature/entities/messages.dart';
import 'package:food_resturant/feature/view/widget/app_bar.dart';
import 'package:food_resturant/feature/view/widget/bubble.dart';
import 'package:food_resturant/library/platforms/image_picker.dart';
import 'package:food_resturant/library/widgets/button_widget.dart';
import 'package:food_resturant/library/widgets/text_widget.dart';
import 'package:get/get.dart';

import 'chat_page_controller.dart';

class ChatPage extends StatelessWidget {
  final controller = Get.put(ChatPageController());

  @override
  Widget build(BuildContext context) {
    return GetBuilder<ChatPageController>(
        init: ChatPageController(),
        builder: (x) {
          return Scaffold(
              floatingActionButton: sendMessageWidget(),
              floatingActionButtonLocation:
                  FloatingActionButtonLocation.centerFloat,
              body: Column(
                children: [
                  BuildAppBarWidget(
                    title: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        CustomText(
                          'Tickets',
                          color: Colors.white,
                          size: 25.0,
                          fontWeight: FontWeight.bold,
                        ),
                        SizedBox(
                          height: 2.0,
                        ),
                        // CustomText(
                        //   'Ticket Subject Here',
                        //   color: Colors.white,
                        //   size: 18.0,
                        //   fontWeight: FontWeight.w500,
                        // ),
                      ],
                    ),
                  ),
                  Expanded(
                    child: Obx(() => RefreshIndicator(
                          backgroundColor: Get.theme.primaryColorDark,
                          onRefresh: () {
                            return controller.getTicketById();
                          },
                          child: ListView.builder(
                            padding: EdgeInsets.only(
                                right: 8.0, left: 8.0, top: 8.0, bottom: 90),
                            itemCount:
                                controller?.getMessageList?.value?.length ?? 0,
                            itemBuilder: (context, index) {
                              final item =
                                  controller?.getMessageList?.value[index];
                              return chatItem(item);
                            },
                          ),
                        )),
                  )
                ],
              ));
        });
  }

  BubbleStyle someoneStyle() => BubbleStyle(
        margin: BubbleEdges.all(15),
        alignment: Alignment.bottomLeft,
        elevation: 10,
        padding: BubbleEdges.all(15),
        radius: Radius.circular(10),
        nip: BubbleNip.leftBottom,
        color: Colors.green.withAlpha(150),
      );

  BubbleStyle MyStyle() => BubbleStyle(
      alignment: Alignment.bottomRight,
      margin: BubbleEdges.all(15),
      elevation: 10,
      padding: BubbleEdges.all(15),
      radius: Radius.circular(10),
      nip: BubbleNip.rightBottom,
      color: Colors.grey.withAlpha(150));

  sendMessageWidget() {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 15.0),
      child: Container(
        decoration: BoxDecoration(
            color: Get.theme.accentColor,
            borderRadius: BorderRadius.circular(15)),
        height: 60,
        child: Row(
          children: [
            InkWell(
              onTap: () async {
                pickImage().then((file) => {
                      if (file != null)
                        {
                          controller.ticketAttachment = file,
                          controller.update(),
                        }
                    });
              },
              child: Padding(
                padding: const EdgeInsets.fromLTRB(8.0, 10.0, 8.0, 8.0),
                child: Stack(
                  children: [
                    Icon(
                      Icons.attach_file,
                      color: Colors.grey[500],
                    ),
                    controller?.ticketAttachment == null ? SizedBox():Positioned(child: Icon(Icons.error , color: Colors.red,size: 12,))
                  ],
                ),
              ),
            ),
            Expanded(
              child: Padding(
                padding: const EdgeInsets.all(10.0),
                child: Container(
                  height: 60,
                  child: Form(
                    child: TextFormField(
                      textInputAction: TextInputAction.newline,
                      keyboardType: TextInputType.multiline,
                      controller: controller.messageController,
                      style: TextStyle(
                        fontSize: 20,
                        color: Colors.blue[900],
                      ),
                      autofocus: false,
                      decoration: InputDecoration(
                        border: InputBorder.none,
                        focusedBorder: InputBorder.none,
                        enabledBorder: InputBorder.none,
                        errorBorder: InputBorder.none,
                        disabledBorder: InputBorder.none,
                        contentPadding: EdgeInsets.fromLTRB(12, 8, 12, 8),
                        hintText: "Write a Message",
                        hintStyle: TextStyle(
                          fontSize: 15,
                          // color: Colors.grey[200],
                        ),
                      ),
                    ),
                  ),
                ),
              ),
            ),
            InkWell(
              onTap: () async {
                controller.createTicket();
              },
              child: Container(
                decoration: BoxDecoration(
                    color: Get.theme.primaryColor,
                    borderRadius: BorderRadius.circular(15.0)),
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Icon(
                    Icons.send,
                    color: Colors.white,
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget chatItem(Messages messages) {
    // return Bubble(
    //    alignment: Alignment.center,
    //    color: Color.fromARGB(255, 212, 234, 244),
    //    elevation: 1,
    //    margin: BubbleEdges.only(top: 8.0),
    //    child: Text('TODAY', style: TextStyle(fontSize: 10)),
    //  );
    return Column(
      children: [
        Bubble(
          style: messages?.byAdmin ? someoneStyle() : MyStyle(),
          child: Text(messages?.text ?? ''),
        ),
        messages?.file == null || messages?.file?.location == null
            ? SizedBox()
            : Align(
          alignment: messages?.byAdmin ? Alignment.bottomLeft : Alignment.bottomRight,
              child: Container(
                  width: 40,
                  height: 40,
                  child: Image.network(messages?.file?.location),
                ),
            )
      ],
    );
  }
}
