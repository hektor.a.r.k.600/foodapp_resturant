import 'dart:io';

import 'package:flutter/material.dart';
import 'package:food_resturant/core/config/api_address.dart';
import 'package:food_resturant/feature/depenency_injection.dart';
import 'package:food_resturant/feature/entities/file.dart';
import 'package:food_resturant/feature/entities/get_ticket_by_id_entity.dart';
import 'package:food_resturant/feature/entities/get_ticket_entity.dart';
import 'package:food_resturant/feature/entities/icon_object.dart';
import 'package:food_resturant/feature/entities/messages.dart';
import 'package:food_resturant/feature/entities/public_entity.dart';
import 'package:food_resturant/feature/entities/ticket.dart';
import 'package:food_resturant/feature/server/api_helper.dart';
import 'package:food_resturant/feature/server/api_response.dart';
import 'package:food_resturant/feature/server/params/add_answer_ticket_params.dart';
import 'package:food_resturant/feature/server/params/add_ticket_params.dart';
import 'package:food_resturant/feature/server/params/get_ticket_by_id_params.dart';
import 'package:food_resturant/library/platforms/loading_overlay.dart';
import 'package:food_resturant/library/platforms/upload_file.dart';
import 'package:food_resturant/library/widgets/snackbar.dart';
import 'package:food_resturant/library/widgets/text_widget.dart';
import 'package:get/get.dart';

class ChatPageController extends GetxController {
  TextEditingController messageController = new TextEditingController(text: '');
  final overlay = LoadingOverlay.of(Get.context);

  File ticketAttachment;

  IconObject uploadImage;

  GetTicketObject argTicket;

  RxList<Messages> getMessageList = <Messages>[].obs;

  @override
  void onReady() {
    argTicket = Get.arguments;
    getTicketById();
    super.onReady();
  }

  getTicketById() async {
    var getTicketById = GetTicketByIdParams(storeTicketId: argTicket?.id);
    try {
      final response = await overlay.during(sl<ApiHelper>()
          .getWithParam(ApiAddress.GET_TICKETS_BY_ID, getTicketById.toJson()));
      var result = ApiResponse.returnResponse(
          response, GetTicketByIdEntity.fromJson(response.data));
      if (result.status == ApiStatus.COMPLETED) {
        // showSnackBar(text: result?.message ?? '',snackBarType: SnackBarType.Report);
        getMessageList
            .assignAll(result?.data?.getTicketByIdObject?.ticket?.messages);
      }
    } on Exception catch (e) {}
  }

  createTicket() async {
    if (messageController.text.isEmpty || messageController.text.isEmpty) {
      showSnackBar(text: 'please enter value');
      return;
    }

    if (ticketAttachment != null) {
      overlay.show(isUpload: true);
      uploadImage = await uploadFile(imageFile: ticketAttachment);
      overlay.hide();
    }
    var addTicketParams = AddAnswerTicketParams(
      text: messageController.text,
      byAdmin: false,
      storeTicketId: argTicket?.id,
      file: uploadImage == null
          ? null
          : FileDto(
              type: uploadImage?.type,
              id: uploadImage.id,
              location: uploadImage.location),
    );
    try {
      final response = await overlay.during(sl<ApiHelper>()
          .post(ApiAddress.ADD_ANSWER_TICKET, addTicketParams.toJson()));
      var result = ApiResponse.returnResponse(
          response, PublicEntity.fromJson(response.data));
      if (result.status == ApiStatus.COMPLETED) {
        messageController.text = '';
        ticketAttachment = null;
        update();
        getTicketById();
        // showSnackBar(
        //     text: result?.message ?? '', snackBarType: SnackBarType.Report);
      }
    } on Exception catch (e) {
      ApiResponse.dioError(e);
    }
  }
}
