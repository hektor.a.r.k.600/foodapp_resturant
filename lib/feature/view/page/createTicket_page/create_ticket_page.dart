import 'package:flutter/material.dart';
import 'package:food_resturant/feature/server/params/edit_store_profile.dart' hide Icon;
import 'package:food_resturant/feature/view/widget/app_bar.dart';
import 'package:food_resturant/library/platforms/image_picker.dart';
import 'package:food_resturant/library/widgets/button_widget.dart';
import 'package:food_resturant/library/widgets/cached_network_image_widget.dart';
import 'package:food_resturant/library/widgets/text_widget.dart';
import 'package:get/get.dart';

import '../../../../library/widgets/text_field_widget.dart';
import 'create_ticket_page_controller.dart';

class CreateTicketPage extends StatelessWidget {
  final controller = Get.put(CreateTicketPageController());

  @override
  Widget build(BuildContext context) {
    return GetBuilder<CreateTicketPageController>(
        init: CreateTicketPageController(),
        builder: (x) {
          return Scaffold(
            floatingActionButtonLocation:
                FloatingActionButtonLocation.centerFloat,
            floatingActionButton: Align(
              alignment: Alignment.bottomCenter,
              child: SizedBox(
                  height: 40.0,
                  width: Get.width * .4,
                  child: CustomButton(
                    onTap: () {
                      controller.createTicket();
                    },
                    type: 2,
                    borderRadios: 10.0,
                    child: CustomText(
                      'Create Ticket',
                      color: Colors.black54,
                      size: 18.0,
                      fontWeight: FontWeight.bold,
                    ),
                  )),
            ),
            body: SafeArea(
              child: ListView(
                children: [
                  BuildAppBarWidget(
                    appBarHeight: 200,
                    padding: EdgeInsets.all(0.0),
                    title: StreamBuilder<Object>(
                        stream: null,
                        builder: (context, snapshot) {
                          return Column(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              CustomText(
                                'Create Ticket',
                                color: Colors.white,
                                size: 25.0,
                                fontWeight: FontWeight.bold,
                              ),
                              SizedBox(
                                height: 2.0,
                              ),
                              CustomText(
                                'Create New Ticket For Problems',
                                color: Colors.white,
                                size: 18.0,
                                fontWeight: FontWeight.w500,
                              ),
                            ],
                          );
                        }),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(16.0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        CustomText(
                          'Ticket Subject',
                          color: Colors.black,
                          size: 20.0,
                          fontWeight: FontWeight.bold,
                        ),
                        CustomTextField(
                            hint: "Ticket Subject",
                            textEditingController:
                                controller.txtTicketController,
                            height: 40.0,
                            maxLine: 1,
                            fontSize: 18.0,
                            isBold: false),
                        SizedBox(
                          height: 10.0,
                        ),
                        // CustomText(
                        //   'Support Department',
                        //   color: Colors.black,
                        //
                        //   size: 20.0,
                        //   fontWeight: FontWeight.bold,
                        // ),
                        // SizedBox(
                        //   height: 50.0,
                        //   child: CustomDropDownList(
                        //     padding: EdgeInsets.only(top: 8.0),
                        //     context: context,
                        //     dropdownColor: Get.theme.primaryColorLight.withAlpha(150),
                        //     onChange: (c) {},
                        //     valueItem: "Selected",
                        //     items: ['Selected'],
                        //   ),
                        // ),
                        // SizedBox(
                        //   height: 10.0,
                        // ),
                        // CustomText(
                        //   'Ticket Priority',
                        //   color: Colors.black,
                        //
                        //   size: 20.0,
                        //   fontWeight: FontWeight.bold,
                        // ),
                        // SizedBox(
                        //   height: 50.0,
                        //   child: CustomDropDownList(
                        //     padding: EdgeInsets.only(top: 8.0),
                        //     context: context,
                        //     dropdownColor: Get.theme.primaryColorLight.withAlpha(150),
                        //     onChange: (c) {},
                        //     valueItem: "Selected",
                        //     items: ['Selected'],
                        //   ),
                        // ),
                        CustomText(
                          'Description',
                          color: Colors.black,
                          size: 20.0,
                          fontWeight: FontWeight.bold,
                        ),
                        CustomTextField(
                            // hint: "Description",

                            textEditingController:
                                controller.txtTicketDescription,
                            height: 100.0,
                            maxLine: 3,
                            fontSize: 18.0,
                            isBold: false),

                        SizedBox(
                          height: 10.0,
                        ),

                        SizedBox(
                          height: 40,
                          width: Get.width,
                          child: CustomButton(
                            borderRadios: 10.0,
                            onTap: () {
                              pickImage().then((file) => {
                                    if (file != null)
                                      {
                                        controller.ticketAttachment = file,
                                        controller.update(),
                                      }
                                  });

                              // loadImage();
                            },
                            type: 2,
                            child: CustomText(
                              'Select Image',
                              color: Colors.black,
                            ),
                          ),
                        ),
                        SizedBox(
                          height: 10.0,
                        ),
                        controller.ticketAttachment != null
                            ? Center(
                                child: Stack(
                                  children: [
                                    Container(
                                      width: 200,
                                      height: 150,
                                      child: Image.file(
                                          controller.ticketAttachment,fit:BoxFit.cover ,),
                                    ),
                                    Positioned(
                                      top: 5,
                                        left: 5,
                                        child: InkWell(
                                            onTap: () {
                                              controller.ticketAttachment =null;
                                              controller.update();

                                            },
                                            child: Icon(
                                              Icons.close_sharp,
                                              color: Colors.red,
                                            ))),
                                  ],
                                ),
                              )
                            : SizedBox()

                        // Obx(() =>
                        //     GridView.builder(
                        //       shrinkWrap: true,
                        //       padding: EdgeInsets.all(8.0),
                        //       gridDelegate:
                        //       SliverGridDelegateWithFixedCrossAxisCount(
                        //           crossAxisCount: 3),
                        //       itemCount: controller?.images?.length ?? 0,
                        //       itemBuilder: (context, index) {
                        //         final item = controller.images
                        //             ?.value[index];
                        //         return Padding(
                        //           padding: const EdgeInsets.all(8.0),
                        //           child: itemImageBuilder(item),
                        //         );
                        //       },
                        //     )),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          );
        });
  }

// loadImage() async {
//   List<Asset> resultList = <Asset>[];
//   String error = 'No Error Detected';
//   try {
//     resultList = await MultiImagePicker.pickImages(
//       maxImages: 300,
//       enableCamera: true,
//       selectedAssets: controller.images?.value,
//       cupertinoOptions: CupertinoOptions(takePhotoIcon: "chat"),
//       materialOptions: MaterialOptions(
//         actionBarTitleColor: "#00D589",
//         textOnNothingSelected: 'Not Selected Yet',
//         actionBarColor: "#abcdef",
//         actionBarTitle: "Food Resturant",
//         allViewTitle: "All Photos",
//         useDetailsView: true,
//         selectCircleStrokeColor: "#000000",
//       ),
//     );
//   } on Exception catch (e) {
//     error = e.toString();
//   }
//   controller.images?.value = resultList;
// }

// itemImageBuilder(Asset asset) {
//   return Stack(
//     children: [
//       AssetThumb(
//         asset: asset,
//         width: 300,
//         height: 300,
//       ),
//       Positioned(child: InkWell(onTap: () {
//         List<Asset> list = List.from(controller.images.value);
//         list?.removeWhere((element) => element.name == asset.name);
//         controller.images.value.clear();
//         controller.images.value = list;
//
//       }, child: Icon(Icons.close_sharp, color: Colors.red,))),
//
//     ],
//   );
// }
}
