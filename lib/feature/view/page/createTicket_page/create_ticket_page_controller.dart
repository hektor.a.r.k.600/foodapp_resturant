import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:food_resturant/core/config/api_address.dart';
import 'package:food_resturant/core/config/global_setting.dart';
import 'package:food_resturant/feature/depenency_injection.dart';
import 'package:food_resturant/feature/entities/file.dart';
import 'package:food_resturant/feature/entities/icon_object.dart';
import 'package:food_resturant/feature/entities/login_entity.dart';
import 'package:food_resturant/feature/entities/messages.dart';
import 'package:food_resturant/feature/entities/public_entity.dart';
import 'package:food_resturant/feature/entities/ticket.dart';
import 'package:food_resturant/feature/server/api_helper.dart';
import 'package:food_resturant/feature/server/api_response.dart';
import 'package:food_resturant/feature/server/params/add_ticket_params.dart';
import 'package:food_resturant/library/platforms/loading_overlay.dart';
import 'package:food_resturant/library/platforms/upload_file.dart';
import 'package:food_resturant/library/platforms/utils.dart';
import 'package:food_resturant/library/widgets/snackbar.dart';
import 'package:food_resturant/library/widgets/text_widget.dart';
import 'package:get/get.dart';
import 'package:multi_image_picker/src/asset.dart';

class CreateTicketPageController extends GetxController {
  final overlay = LoadingOverlay.of(Get.context);

  LoginObject loginObject;

  TextEditingController txtTicketController = TextEditingController(text: '');

  TextEditingController txtTicketDescription = TextEditingController(text: '');

  // RxList<Asset> images = <Asset>[].obs;

  File ticketAttachment;

  IconObject uploadImage;

  createTicket() async {
    if (txtTicketController.text.isEmpty || txtTicketDescription.text.isEmpty) {
      showSnackBar(text: 'please enter value');
      return;
    }

    if (ticketAttachment != null) {
      overlay.show(isUpload: true);
      uploadImage = await uploadFile(imageFile: ticketAttachment);
      overlay.hide();
    }

    var messages = Messages(text: txtTicketDescription.text, byAdmin: false);

    var addTicketParams = AddTicketParams(
        store: loginObject.store,
        ticket: Ticket(
            file: uploadImage == null
                ? null
                : FileDto(
                    type: uploadImage?.type,
                    id: uploadImage.id,
                    location: uploadImage.location),
            byAdmin: false,
            text: txtTicketController.text,
            title: txtTicketController.text,
            messages: [messages]));
    printInfo(info: json.encode(addTicketParams.toJson()));
    try {
      final response = await overlay.during(sl<ApiHelper>()
          .post(ApiAddress.ADD_TICKET, addTicketParams.toJson()));
      var result = ApiResponse.returnResponse(
          response, PublicEntity.fromJson(response.data));
      if (result.status == ApiStatus.COMPLETED) {
        Get.back(result: true);
        // showSnackBar(
        //     text: result?.message ?? '', snackBarType: SnackBarType.Report);
      }
    } on Exception catch (e) {
      ApiResponse.dioError(e);
    }
  }

  @override
  void onInit() {
    loginObject = sl<GlobalSetting>().user;
    update();
    super.onInit();
  }
}
