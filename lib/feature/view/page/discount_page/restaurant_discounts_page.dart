import 'package:flutter/material.dart';
import 'package:food_resturant/core/config/routes.dart';
import 'package:food_resturant/feature/entities/get_store_food_entity.dart';
import 'package:food_resturant/feature/view/page/discount_page/restaurant_discounts_page_controller.dart';
import 'package:food_resturant/feature/view/page/stocks_page/stocks_page_controller.dart';
import 'package:food_resturant/feature/view/page/tables_page/tables_page_controller.dart';
import 'package:food_resturant/feature/view/widget/app_bar.dart';
import 'package:food_resturant/feature/view/widget/box_shadow_widget.dart';
import 'package:food_resturant/library/widgets/button_widget.dart';
import 'package:food_resturant/library/widgets/custom_drop_list/custom_drop_down_list.dart';
import 'package:food_resturant/library/widgets/text_widget.dart';
import 'package:get/get.dart';
import 'package:qr_flutter/qr_flutter.dart';

import '../../../../library/widgets/text_field_widget.dart';

class RestaurantDiscountsPage extends StatelessWidget {
  final controller = Get.put(RestaurantDiscountPageController());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // floatingActionButton: FloatingActionButton(
      //   onPressed: () {
      //    Get.toNamed(Routes.AddDiscountsPage).then((value) => {
      //      if(value){
      //       controller.getAllStoreFood(),
      //      }
      //    });
      //   },
      //   backgroundColor: Get.theme.primaryColor,
      //   child: Center(
      //     child: Icon(
      //       Icons.add,
      //       size: 30.0,
      //     ),
      //   ),
      // ),
      body: Column(
        children: [
          BuildAppBarWidget(
            title: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                CustomText(
                  'Store Discounts',
                  color: Colors.white,
                  size: 25.0,
                  fontWeight: FontWeight.bold,
                ),
                SizedBox(
                  height: 2.0,
                ),
                CustomText(
                  'View, Edit, Add, Remove Discounts',
                  color: Colors.white,
                  size: 18.0,
                  fontWeight: FontWeight.w500,
                ),
              ],
            ),
          ),
          Expanded(
            child: RefreshIndicator(
              backgroundColor: Get.theme.primaryColorDark,
              onRefresh: (){
                return controller.getAllStoreFood();
              },
              child: Obx(() => ListView.builder(
                padding: EdgeInsets.all(5.0),
                itemCount: controller.storeFood?.value?.length??0,
                itemBuilder: (context, index) {
                  final item = controller.storeFood?.value[index];
                 return buildDiscountItem(item);
                },
              )),
            ),
          ),
        ],
      ),
    );
  }

  buildDiscountItem(GetStoreFoodObject item) {
    return InkWell(
      onTap: () {
        Get.toNamed(Routes.AddDiscountsPage ,arguments: item).then((value) => {
          if(value != null && value){
            controller.getAllStoreFood(),
          }
        });
      },
      child: Card(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10.0),
        ),
        color: Get.theme.accentColor,
        elevation: 4,
        margin: EdgeInsets.all(5.0),
        child: Container(
          height: 80.0,

          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    CustomText(
                      item?.name??'',
                      color: Colors.black,
                      size: 20.0,
                      fontWeight: FontWeight.bold,
                    ),
                    CustomText(
                       item?.discountValue?.toString() + ' % Discount '??'',
                      color: Colors.black,
                      size: 20.0,
                      fontWeight: FontWeight.normal,
                    ),
                  ],
                ),

                // IconButton(
                //   iconSize: 32,
                //   icon: Icon(Icons.delete),
                //   onPressed: () {},
                //   color: Colors.red,
                // ),

              ],
            ),
          ),
        ),
      ),
    );





  }
}
