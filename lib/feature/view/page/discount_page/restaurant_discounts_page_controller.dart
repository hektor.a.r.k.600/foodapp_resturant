import 'package:food_resturant/core/config/api_address.dart';
import 'package:food_resturant/core/config/global_setting.dart';
import 'package:food_resturant/feature/entities/get_store_food_entity.dart';
import 'package:food_resturant/feature/entities/login_entity.dart';
import 'package:food_resturant/feature/entities/public_entity.dart';
import 'package:food_resturant/feature/server/api_helper.dart';
import 'package:food_resturant/feature/server/api_response.dart';
import 'package:food_resturant/feature/server/params/get_store_food_params.dart';
import 'package:food_resturant/library/platforms/loading_overlay.dart';
import 'package:food_resturant/library/widgets/snackbar.dart';
import 'package:get/get.dart';

import '../../../depenency_injection.dart';

class RestaurantDiscountPageController extends GetxController {
  final overlay = LoadingOverlay.of(Get.context);

  LoginObject user;

  RxList<GetStoreFoodObject> storeFood = <GetStoreFoodObject>[].obs;


  @override
  void onInit() {
    user = sl<GlobalSetting>().user;
    super.onInit();
  }

  @override
  void onReady() {
    getAllStoreFood();
    super.onReady();
  }

  getAllStoreFood() async {
    var getStoreFoodParams = GetStoreFoodParams(soreId: user?.store?.id ?? '');

    final response = await overlay.during(sl<ApiHelper>()
        .getWithParam(ApiAddress.GET_STORE_FOOD, getStoreFoodParams.toJson()));
    var result = ApiResponse.returnResponse(
        response, GetStoreFoodEntity.fromJson(response.data));
    if (result.status == ApiStatus.COMPLETED && result?.data?.status) {
      // showSnackBar(text: result?.data?.message ?? '');
      storeFood.assignAll(result.data?.getStoreFoodObject);
      // pizzaStoreFood.value = result.data?.getStoreFoodObject?.where((element) => element.)

    } else {
      showSnackBar(text: result?.data?.message ?? '');
    }
  }

}

