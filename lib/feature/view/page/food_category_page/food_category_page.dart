import 'package:flutter/material.dart';
import 'package:food_resturant/core/config/routes.dart';
import 'package:food_resturant/feature/entities/get_food_categories_entity.dart';
import 'package:food_resturant/feature/entities/main_category.dart';
import 'package:food_resturant/feature/view/page/discount_page/restaurant_discounts_page_controller.dart';
import 'package:food_resturant/feature/view/page/food_category_page/food_category_page_controller.dart';
import 'package:food_resturant/feature/view/page/stocks_page/stocks_page_controller.dart';
import 'package:food_resturant/feature/view/page/tables_page/tables_page_controller.dart';
import 'package:food_resturant/feature/view/widget/app_bar.dart';
import 'package:food_resturant/feature/view/widget/box_shadow_widget.dart';
import 'package:food_resturant/library/widgets/button_widget.dart';
import 'package:food_resturant/library/widgets/custom_drop_list/custom_drop_down_list.dart';
import 'package:food_resturant/library/widgets/show_bottom_sheet.dart';
import 'package:food_resturant/library/widgets/text_widget.dart';
import 'package:get/get.dart';
import 'package:qr_flutter/qr_flutter.dart';

import '../../../../library/widgets/text_field_widget.dart';

class FoodCategoryPage extends StatelessWidget {
  final controller = Get.put(FoodCategoryPageController());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          showCustomBottomSheet(bottomSheetAddFoodCategory(false));
        },
        backgroundColor: Get.theme.primaryColor,
        child: Center(
          child: Icon(
            Icons.add,
            size: 30.0,
          ),
        ),
      ),
      body: Column(
        children: [
          BuildAppBarWidget(
            title: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                CustomText(
                  'Add/Edit Category',
                  color: Colors.white,
                  size: 25.0,
                  fontWeight: FontWeight.bold,
                ),
                SizedBox(
                  height: 2.0,
                ),
                CustomText(
                  'View, Edit, Add, Remove Category',
                  color: Colors.white,
                  size: 18.0,
                  fontWeight: FontWeight.w500,
                ),
              ],
            ),
          ),
          Expanded(
            child: Obx(() => RefreshIndicator(
              backgroundColor: Get.theme.primaryColorDark,
              onRefresh: () {
               return controller.getFoodCategory();
              },
              child: ListView.builder(
                    padding: EdgeInsets.all(5.0),
                    itemCount: controller.getFoodCatList.length??0,
                    itemBuilder: (context, index) {
                      MainCategory item =controller?.getFoodCatList?.value[index];
                     return InkWell(
                        onTap: () {
                          controller.txtFoodCategoryName.text = item.name??'';
                          showCustomBottomSheet(bottomSheetAddFoodCategory(true,item));

                        },
                        child: Card(
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(10.0),
                          ),
                          color: Get.theme.accentColor,
                          elevation: 4,
                          margin: EdgeInsets.all(5.0),
                          child: Container(
                            height: 80.0,
                            child: Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Row(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  Column(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    mainAxisAlignment:
                                    MainAxisAlignment.spaceEvenly,
                                    children: [
                                      CustomText(
                                       item?.name??'',
                                        color: Colors.black,
                                        size: 20.0,
                                        fontWeight: FontWeight.bold,
                                      ),
                                      CustomText(
                                        '',
                                        color: Colors.black,
                                        size: 20.0,
                                        fontWeight: FontWeight.normal,
                                      ),
                                    ],
                                  ),
                                  IconButton(
                                    iconSize: 32,
                                    icon: Icon(Icons.delete),
                                    onPressed: () {

                                      controller.deleteFoodCategory(item);

                                    },
                                    color: Colors.red,
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ),
                      );


                    },
                  ),
            )),
          ),
        ],
      ),
    );
  }

  Widget bottomSheetAddFoodCategory(bool isEdit, [MainCategory item]) {
    return Column(
      children: [
        Expanded(
          child: ListView(
            padding: const EdgeInsets.all(15.0),
            children: [
              CustomText(
                'Add Food Category',
                color: Colors.black,
                size: 30.0,
                fontWeight: FontWeight.bold,
              ),
              SizedBox(
                height: 10,
              ),
              CustomText(
                'Category Name',
                color: Colors.black,
                size: 20.0,
                fontWeight: FontWeight.bold,
                textAlign: TextAlign.left,
              ),
              CustomTextField(
                  hint: "Category Name",
                  height: 40.0,
                  textEditingController:controller.txtFoodCategoryName ,
                  maxLine: 1,
                  fontSize: 18.0,
                  isBold: false),

            ],
          ),
        ),
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: SizedBox(
              height: 40.0,
              width: Get.width * 6,
              child: CustomButton(
                onTap: () {
                  controller.addFoodCategoryApi(isEdit,item);
                },
                type: 2,
                borderRadios: 10.0,
                child: CustomText(
                  'Add Food Category',
                  color: Colors.black,
                  size: 18.0,
                  fontWeight: FontWeight.bold,
                ),
              )),
        )
      ],
    );
  }
}
