import 'package:flutter/material.dart';
import 'package:food_resturant/core/config/api_address.dart';
import 'package:food_resturant/core/config/global_setting.dart';
import 'package:food_resturant/feature/depenency_injection.dart';
import 'package:food_resturant/feature/entities/get_food_categories_entity.dart';
import 'package:food_resturant/feature/entities/main_category.dart';
import 'package:food_resturant/feature/entities/public_entity.dart';
import 'package:food_resturant/feature/server/api_helper.dart';
import 'package:food_resturant/feature/server/api_response.dart';
import 'package:food_resturant/feature/server/params/add_store_food_category_params.dart';
import 'package:food_resturant/feature/server/params/edit_store_food_category_params.dart';
import 'package:food_resturant/feature/server/params/food_category_id_params.dart';
import 'package:food_resturant/feature/server/params/public_params.dart';
import 'package:food_resturant/library/platforms/loading_overlay.dart';
import 'package:food_resturant/library/widgets/snackbar.dart';
import 'package:food_resturant/library/widgets/text_widget.dart';
import 'package:get/get.dart';

class FoodCategoryPageController extends GetxController {
  TextEditingController txtFoodCategoryName = TextEditingController(text: '');
  final overlay = LoadingOverlay.of(Get.context);

  RxList<MainCategory> getFoodCatList = <MainCategory>[].obs;

  addFoodCategoryApi(isEdit, [MainCategory mainCategory]) async {
    var store = sl<GlobalSetting>()?.user?.store;

    if (txtFoodCategoryName.text?.isEmpty) {
      showSnackBar(text: 'Please Fill The Blank');
      return;
    }

    var foodCategoryParams = AddStoreFoodCategoryParams(
        name: txtFoodCategoryName.text.toString(),
        storeId: store?.id ?? '',discountValue: 0,mainCategoryId: '3fa85f64-5717-4562-b3fc-2c963f66afa6');


      var editFoodCategoryParams = EditStoreFoodCategoryParams(
        name: txtFoodCategoryName.text.toString(),
        storeFoodCategoryId: mainCategory?.id ?? '', discountValue: 0,);


    final response = await overlay.during(sl<ApiHelper>().post(
        isEdit
            ? ApiAddress.EDIT_STORE_FOOD_CATEGORY
            : ApiAddress.ADD_STORE_FOOD_CATEGORY,
        isEdit ? editFoodCategoryParams.toJson() :foodCategoryParams.toJson()));
    var result = ApiResponse.returnResponse(
        response, PublicEntity.fromJson(response.data));
    if (result.status == ApiStatus.COMPLETED && result?.data?.status) {
      // showSnackBar(text: result?.data?.message ?? '');
      getFoodCategory();
    } else {
      showSnackBar(text: result?.data?.message ?? '');
    }
    Get.back();
  }

  @override
  void onReady() {
    getFoodCategory();
  }

  getFoodCategory() async {
    var publicParams = PublicParams();

    final response = await overlay.during(sl<ApiHelper>()
        .getWithParam(ApiAddress.GET_STORE_FOOD_CATEGORY, publicParams.toJson()));
    var result = ApiResponse.returnResponse(
        response, GetFoodCategoriesEntity.fromJson(response.data));
    if (result.status == ApiStatus.COMPLETED && result?.data?.status) {
      getFoodCatList.value = result.data?.getFoodCategoriesObject;

      // showSnackBar(text: result?.data?.message ?? '');
    } else {
      showSnackBar(text: result?.data?.message ?? '');
    }
  }

  deleteFoodCategory(MainCategory item) async {
    var categoryIdParams = FoodCategoryIdParams(storeFoodCategoryId: item.id);

    final response = await overlay.during(sl<ApiHelper>().post(
        ApiAddress.DELETE_STORE_FOOD_CATEGORY, categoryIdParams.toJson()));
    var result = ApiResponse.returnResponse(
        response, PublicEntity.fromJson(response.data));
    if (result.status == ApiStatus.COMPLETED && result?.data?.status) {
      // showSnackBar(text: result?.data?.message ?? '');
      getFoodCategory();
    } else {
      showSnackBar(text: result?.data?.message ?? '');
    }
  }
}
