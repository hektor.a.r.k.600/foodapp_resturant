import 'dart:convert';
import 'dart:developer';

import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:food_resturant/feature/entities/additional.dart';
import 'package:food_resturant/feature/entities/foods.dart';
import 'package:food_resturant/feature/entities/optional.dart';
import 'package:food_resturant/library/extention/money_extention.dart';
import 'package:food_resturant/feature/view/page/food_list_page/food_list_page_controller.dart';
import 'package:food_resturant/feature/view/widget/app_bar.dart';
import 'package:food_resturant/feature/view/widget/box_shadow_widget.dart';
import 'package:food_resturant/library/widgets/button_widget.dart';
import 'package:food_resturant/library/widgets/custom_drop_list/custom_drop_down_list.dart';
import 'package:food_resturant/library/widgets/text_widget.dart';
import 'package:get/get.dart';
import 'package:food_resturant/library/extention/enum_extention.dart';
import 'package:get_it/get_it.dart';
import 'package:queries/queries.dart';
import 'package:food_resturant/library/extention/list_extention.dart';

class FoodListPage extends StatelessWidget {
  final controller = Get.put(FoodListPageController());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
      // floatingActionButton: SizedBox(
      //     height: 40.0,
      //     width: Get.width * .5,
      //     child: CustomButton(
      //       color: Colors.red,
      //       onTap: () {
      //         showChangeOrderStatusBottomSheet(context);
      //       },
      //       type: 2,
      //       borderRadios: 10.0,
      //       child: CustomText(
      //         'Change Order Status',
      //         color: Colors.white,
      //         size: 18.0,
      //         fontWeight: FontWeight.bold,
      //       ),
      //     )),
      body: SafeArea(
        child: SingleChildScrollView(
          child: Column(
            children: [
              BuildAppBarWidget(
                title: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    CustomText(
                      'Food List',
                      color: Colors.white,
                      size: 25.0,
                      fontWeight: FontWeight.bold,
                    ),
                    SizedBox(
                      height: 2.0,
                    ),
                    CustomText(
                      'View Ordered Food List',
                      color: Colors.white,
                      size: 18.0,
                      fontWeight: FontWeight.w500,
                    ),
                  ],
                ),
              ),
              Container(
                margin: const EdgeInsets.only(
                    right: 16.0, left: 16.0, top: 16.0, bottom: 50.0),
                decoration: BoxDecoration(
                  color: Get.theme.accentColor,
                  boxShadow: [buildBoxShadow()],
                  borderRadius: BorderRadius.circular(10.0),
                ),
                child: GetBuilder<FoodListPageController>(
                    init: FoodListPageController(),
                    builder: (x) {
                      return Padding(
                        padding: const EdgeInsets.all(10.0),
                        child: Column(
                          // physics: AlwaysScrollableScrollPhysics(),
                          // padding: EdgeInsets.all(10.0),
                          // mainAxisSize: MainAxisSize.max,
                          // crossAxisAlignment: CrossAxisAlignment.stretch,
                          children: [
                            CustomText(
                              'Food',
                              color: Colors.black,
                              size: 25.0,
                              fontWeight: FontWeight.bold,
                              textAlign: TextAlign.left,
                            ),
                            Divider(
                              color: Colors.grey[500],
                            ),
                            ListView.builder(
                                padding: EdgeInsets.all(5.0),
                                physics: NeverScrollableScrollPhysics(),
                                shrinkWrap: true,
                                itemCount: controller.groupList?.length ?? 0,
                                itemBuilder: (BuildContext ctx, int index) {
                                  return listItem(controller.groupList[index]);
                                }),
                            SizedBox(
                              height: 100,
                            ),
                            Divider(
                              color: Colors.grey[500],
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                CustomText(
                                  'Saving',
                                  color: Colors.black,
                                  size: 18.0,
                                  fontWeight: FontWeight.bold,
                                  textAlign: TextAlign.left,
                                ),
                                CustomText(
                                  // controller.argOrdersObject?.foods?.map((e) => e.discountValue),
                                  controller.argOrdersObject?.totalDiscount
                                          ?.moneyFormatter ??
                                      '',
                                  color: Colors.black,
                                  size: 18.0,
                                  fontWeight: FontWeight.bold,
                                  textAlign: TextAlign.left,
                                ),
                              ],
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                CustomText(
                                  'Total',
                                  color: Colors.black,
                                  size: 18.0,
                                  fontWeight: FontWeight.bold,
                                  textAlign: TextAlign.left,
                                ),
                                CustomText(
                                  controller.argOrdersObject?.totalPrice
                                          ?.moneyFormatter ??
                                      '',
                                  color: Colors.black,
                                  size: 18.0,
                                  fontWeight: FontWeight.bold,
                                  textAlign: TextAlign.left,
                                ),
                              ],
                            ),
                          ],
                        ),
                      );
                    }),
              ),
            ],
          ),
        ),
      ),
    );
  }

  listItem(IGrouping<String, Foods> groupListStore) {
    List<IGrouping<String, Foods>> groupListFood = groupListStore
        ?.asIterable()
        ?.toList()
        ?.listCollection
        ?.groupBy((arg1) => json.encode(arg1?.toJson()))
        ?.toList();

    Map<int, Foods> mapFood = Map<int, Foods>();

    groupListFood.forEach((element) {
      mapFood.addAll(
          {element?.asIterable()?.length: element?.asIterable()?.first});
    });

    // log(groupListStore
    //     ?.asIterable()
    //     ?.toList()
    //     ?.listCollection
    //     ?.groupBy((arg1) => json.encode(arg1?.toJson())).toString());

    return Column(mainAxisAlignment: MainAxisAlignment.start, children: [
      Align(
        alignment: Alignment.centerLeft,
        child: CustomText(
          groupListStore?.key ?? '',
          color: Colors.black,
          size: 22.0,
          fontWeight: FontWeight.bold,
          textAlign: TextAlign.left,
        ),
      ),
      SizedBox(
        height: 10,
      ),
      Padding(
        padding: const EdgeInsets.only(left: 8),
        child: ListView.builder(
            padding: EdgeInsets.all(5.0),
            physics: NeverScrollableScrollPhysics(),
            shrinkWrap: true,
            itemCount: mapFood?.length ?? 0,
            itemBuilder: (BuildContext ctx, int index) {
              MapEntry<int, Foods> item = mapFood?.entries?.elementAt(index);
              return Column(
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Expanded(
                        child: CustomText(
                          (item?.value?.name ?? '') +
                              (item?.key > 1 ? '* ${item?.key?.toString()}' : ''),
                          color: Colors.black,
                          size: 22.0,
                          fontWeight: FontWeight.bold,
                          textAlign: TextAlign.left,
                        ),
                      ),
                      SizedBox(
                        height: 5,
                      ),
                      Expanded(
                        child: CustomText(
                          item?.value?.totalPrice?.moneyFormatter ?? '',
                          color: Colors.black,
                          size: 22.0,
                          fontWeight: FontWeight.bold,
                          textAlign: TextAlign.right,
                        ),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 10),
                    child: Column(
                      children: [
                        Align(
                          alignment: Alignment.centerLeft,
                          child: CustomText(
                            'Options :',
                            color: Colors.black,
                            size: 18.0,
                            fontWeight: FontWeight.bold,
                            textAlign: TextAlign.left,
                          ),
                        ),
                        SizedBox(
                          height: 5,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            CustomText(
                              item?.value?.optionals?.name ?? '',
                              color: Colors.black,
                              size: 18.0,
                              fontWeight: FontWeight.normal,
                              textAlign: TextAlign.left,
                            ),

                            CustomText(
                              item?.value?.optionals?.price?.moneyFormatter ?? '',
                              color: Colors.black,
                              size: 18.0,
                              fontWeight: FontWeight.normal,
                              textAlign: TextAlign.left,
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  item?.value?.additionals?.length == 0
                      ? SizedBox()
                      : Padding(
                    padding: const EdgeInsets.only(left: 10),
                        child: Column(
                            children: [
                              Align(
                                alignment: Alignment.centerLeft,
                                child: CustomText(
                                  'Additionals :',
                                  color: Colors.black,
                                  size: 18.0,
                                  fontWeight: FontWeight.bold,
                                  textAlign: TextAlign.left,
                                ),
                              ),
                              ListView.builder(
                                  padding: EdgeInsets.all(5.0),
                                  physics: NeverScrollableScrollPhysics(),
                                  shrinkWrap: true,
                                  itemCount: item?.value?.additionals?.length ?? 0,
                                  itemBuilder: (BuildContext ctx, int i) {
                                    Additionals additional =
                                        item?.value?.additionals[i];
                                    return Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: [
                                        CustomText(
                                          additional?.name ?? '',
                                          color: Colors.black,
                                          size: 18.0,
                                          fontWeight: FontWeight.normal,
                                          textAlign: TextAlign.left,
                                        ),

                                        CustomText(
                                          additional?.price?.moneyFormatter ?? '',
                                          color: Colors.black,
                                          size: 18.0,
                                          fontWeight: FontWeight.normal,
                                          textAlign: TextAlign.left,
                                        ),
                                      ],
                                    );
                                  }),
                            ],
                          ),
                      ),

                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 40),
                    child: Divider(
                      color: Colors.grey[500],
                    ),
                  )

                ],
              );
            }),
      ),
      controller?.groupList?.length > 1
          ? Divider(
              color: Colors.grey[500],
            )
          : SizedBox()
    ]);
  }

  showChangeOrderStatusBottomSheet(BuildContext context) {
    return showModalBottomSheet<String>(
      enableDrag: true,
      context: context,
      builder: (context) => Container(
        child: Padding(
          padding: const EdgeInsets.all(15.0),
          child: Column(
            children: [
              CustomText(
                'Change Order Status',
                color: Colors.black,
                size: 30.0,
                fontWeight: FontWeight.bold,
              ),
              SizedBox(
                height: 10,
              ),
              GetBuilder<FoodListPageController>(builder: (x) {
                return Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Column(
                    children: x.orderStatusRadioBuilder(x.orderStatusRadioTitle
                        .map((e) => e.orderStatusToString)
                        .toList()),
                  ),
                );
              }),
              Expanded(
                child: Align(
                  alignment: Alignment.bottomCenter,
                  child: SizedBox(
                      height: 40.0,
                      width: Get.width * .4,
                      child: CustomButton(
                        onTap: () {
                          controller.changeStatusOrder();
                          Get.back();
                        },
                        type: 2,
                        borderRadios: 10.0,
                        child: CustomText(
                          'Change Status',
                          color: Colors.black,
                          size: 18.0,
                          fontWeight: FontWeight.bold,
                        ),
                      )),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
