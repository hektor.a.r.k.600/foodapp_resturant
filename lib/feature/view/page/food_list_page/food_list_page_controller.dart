import 'package:flutter/material.dart';
import 'package:food_resturant/core/config/api_address.dart';
import 'package:food_resturant/core/config/routes.dart';
import 'package:food_resturant/feature/depenency_injection.dart';
import 'package:food_resturant/feature/entities/foods.dart';
import 'package:food_resturant/feature/entities/get_orders_entity.dart';
import 'package:food_resturant/feature/entities/public_entity.dart';
import 'package:food_resturant/library/extention/list_extention.dart';
import 'package:food_resturant/feature/server/api_helper.dart';
import 'package:food_resturant/feature/server/api_response.dart';
import 'package:food_resturant/feature/server/params/change_order_status_param.dart';
import 'package:food_resturant/feature/server/params/change_store_food_status_params.dart';
import 'package:food_resturant/library/enums/enums.dart';
import 'package:food_resturant/library/platforms/loading_overlay.dart';
import 'package:food_resturant/library/widgets/snackbar.dart';
import 'package:food_resturant/library/widgets/text_widget.dart';
import 'package:get/get.dart';
import 'package:queries/queries.dart';

class FoodListPageController extends GetxController {
  int selectedOrderStatus;
  List<OrderStatus> orderStatusRadioTitle = List<OrderStatus>();
  final overlay = LoadingOverlay.of(Get.context);

  List<IGrouping<String, Foods>> groupList;

  GetOrdersObject argOrdersObject;


  List<Widget> orderStatusRadioBuilder(List<String> radiosTitles) {
    List<Widget> listRadios = List<Widget>();
    for (var i = 0; i < radiosTitles.length; i++) {
      listRadios.add(
        Row(
          children: [
            SizedBox(
              height: 30.0,
              width: 30.0,
              child: Radio(
                activeColor: Get.theme.primaryColor,
                value: i,
                groupValue: selectedOrderStatus,
                onChanged: radioButtonOnChange,
              ),
            ),
            CustomText(
              radiosTitles[i],
              color: Colors.black,
              size: 18.0,
              fontWeight: FontWeight.normal,
            )
          ],
        ),
      );
    }

    return listRadios;
  }

  radioButtonOnChange(int orderStatus) {
    selectedOrderStatus = orderStatus;
    update();
  }

  @override
  void onInit() {
    argOrdersObject = Get.arguments;

     groupList = argOrdersObject?.foods?.listCollection?.groupBy((arg1) => arg1?.store?.name)?.toList();
    orderStatusRadioTitle = OrderStatus.values.toList();
    super.onInit();
  }

  @override
  void onReady() {

  }



  changeStatusOrder() async {
    //todo change order status
    var changeOrder = ChangeOrderStatusParam(
        orderId: argOrdersObject?.id ?? '', status: selectedOrderStatus + 1);

    final response = await overlay.during(sl<ApiHelper>()
        .post(ApiAddress.CHANGE_ORDER_STATUS, changeOrder.toJson()));
    var result = ApiResponse.returnResponse(
        response, PublicEntity.fromJson(response.data));
    if (result.status == ApiStatus.COMPLETED && result?.data?.status) {
      // showSnackBar(text: result?.message ?? '');
      Get.back(result: true);
    }
  }


  // changeStatusOrder() async {
  //   var changeOrder = ChangeStoreFoodStatusParams(
  //       storeFoodId: argOrdersObject?.?.first?.id ?? '',
  //       status: selectedOrderStatus + 1);
  //
  //   final response = await overlay.during(sl<ApiHelper>()
  //       .post(ApiAddress.CHANGE_STORE_FOOD_STATUS, changeOrder.toJson()));
  //   var result = ApiResponse.returnResponse(
  //       response, PublicEntity.fromJson(response.data));
  //   if (result.status == ApiStatus.COMPLETED && result?.data?.status) {
  //     showSnackBar(text: result?.message ?? ''
  //     );
  //     Get.back();
  //   }
  // }

}
