import 'package:bubble_tab_indicator/bubble_tab_indicator.dart';
import 'package:flutter/material.dart';
import 'package:food_resturant/core/config/global_setting.dart';
import 'package:food_resturant/core/config/routes.dart';
import 'package:food_resturant/feature/entities/get_orders_entity.dart';
import 'package:food_resturant/feature/view/widget/box_shadow_widget.dart';
import 'package:food_resturant/library/enums/enums.dart';
import 'package:food_resturant/library/widgets/cached_network_image_widget.dart';
import 'package:food_resturant/library/widgets/text_widget.dart';
import 'package:get/get.dart';
import 'package:food_resturant/feature/view/page/home_page/home_page_controller.dart';
import 'package:food_resturant/library/extention/enum_extention.dart';
import 'package:food_resturant/library/extention/money_extention.dart';
import 'package:food_resturant/library/extention/int_extention.dart';

import '../../../depenency_injection.dart';

class HomePage extends StatelessWidget {
  final controller = Get.put(HomePageController());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ListView(
        // crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          buildHomeAppbar(
            context,
          ),
          GetBuilder<HomePageController>(
            builder: (controller) => GridView.builder(
                shrinkWrap: true,
                padding: EdgeInsets.only(top: 15.0),
                physics: NeverScrollableScrollPhysics(),
                gridDelegate: SliverGridDelegateWithMaxCrossAxisExtent(
                    maxCrossAxisExtent: 150,
                    childAspectRatio: 1,
                    crossAxisSpacing: 10,
                    mainAxisSpacing: 10),
                itemCount: controller.optionsValue.length,
                itemBuilder: (BuildContext ctx, index) {
                  return GestureDetector(
                    onTap: () => controller
                        .onOptionsTap(controller.optionsValue[index].pageRoute),
                    child: Column(
                      children: [
                        Container(
                          height: 80.0,
                          width: 80.0,
                          margin: EdgeInsets.only(bottom: 5.0),
                          decoration: BoxDecoration(
                              shape: BoxShape.circle,
                              border: Border.all(
                                  color: Get.theme.primaryColorLight, width: 3),
                              color:
                                  Get.theme.primaryColorLight.withOpacity(0.4)),
                          child: Center(
                            child: Icon(
                              controller.optionsValue[index].icon,
                              size: 32,
                              color: Get.theme.primaryColorDark,
                            ),
                          ),
                        ),
                        CustomText(
                          controller.optionsValue[index].title,
                          color: Colors.black,
                          fontWeight: FontWeight.bold,
                          size: 18.0,
                        )
                      ],
                    ),
                  );
                }),
          ),
        ],
      ),
    );
  }

  GestureDetector ordersCardItems(GetOrdersObject order) {
    return GestureDetector(
      onTap: () {
        Get.toNamed(Routes.OrderStatusPage, arguments: order);
      },
      child: Padding(
        padding: const EdgeInsets.all(5.0),
        child: Container(
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(20.0),
              boxShadow: [buildBoxShadow()],
              color: Get.theme.accentColor),
          child: Row(
            children: [
              Container(
                height: 140.0,
                width: 40.0,
                decoration: BoxDecoration(
                    color: Color(0xff0065D9),
                    borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(20.0),
                        bottomLeft: Radius.circular(20.0))),
                child: Center(
                  child: RotatedBox(
                    quarterTurns: -45,
                    child: CustomText(
                      order.status.orderStatusToString,
                      size: 20.0,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
              ),
              // Container(
              //    height: 140.0,
              //    width: 40.0,
              //    decoration: BoxDecoration(
              //        color: Colors.green,
              //        borderRadius: BorderRadius.only(
              //            topLeft: Radius.circular(20.0),
              //            bottomLeft: Radius.circular(20.0))),
              //    child: Center(
              //      child: RotatedBox(
              //        quarterTurns: -45,
              //        child: CustomText(
              //          'Cooking',
              //          size: 20.0,
              //          fontWeight: FontWeight.bold,
              //        ),
              //      ),
              //    ),
              //  ),
              Column(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  SizedBox(
                    width: Get.width - 84.0,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Row(
                          children: [
                            Padding(
                              padding: const EdgeInsets.only(
                                  top: 8.0, left: 4.0, right: 4.0),
                              child: BuildCachedImageWidget(
                                imageUrl: order?.user?.profileImage ?? '',
                                height: 70.0,
                                width: 70.0,
                                borderRadius: 60.0,
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(top: 5.0),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  CustomText(
                                    '',
                                    color: Colors.black,
                                    size: 18.0,
                                    fontWeight: FontWeight.bold,
                                  ),
                                  CustomText(
                                    order?.user?.userName ?? '',
                                    color: Colors.black,
                                    size: 18.0,
                                    fontWeight: FontWeight.normal,
                                  )
                                ],
                              ),
                            ),
                          ],
                        ),
                        // Padding(
                        //   padding: const EdgeInsets.only(top: 5.0),
                        //   child: Icon(
                        //     Icons.announcement,
                        //     color: Colors.red,
                        //   ),
                        // )
                      ],
                    ),
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      SizedBox(
                        width: Get.width - 120,
                        child: Divider(
                          thickness: 1,
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(
                            top: 6.0, right: 12.0, left: 12.0),
                        child: SizedBox(
                          width: Get.width - 84.0,
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              CustomText(
                                order.orderType.orderTypeToString ?? '',
                                color: Colors.black,
                                size: 18.0,
                                fontWeight: FontWeight.bold,
                              ),
                              CustomText(
                                'Total : ${order.totalPrice.moneyFormatter ?? ''}',
                                color: Colors.black,
                                size: 18.0,
                                fontWeight: FontWeight.bold,
                              ),
                            ],
                          ),
                        ),
                      )
                    ],
                  )
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}

GetBuilder buildHomeOptions(BuildContext context) {
  return GetBuilder<HomePageController>(
      init: HomePageController(),
      builder: (controller) {
        return Padding(
          padding: const EdgeInsets.symmetric(horizontal: 10.0),
          child: SizedBox(
            width: MediaQuery.of(context).size.width,
            height: 180.0,
            child: GridView.builder(
              padding: EdgeInsets.only(top: 15.0),
              physics: NeverScrollableScrollPhysics(),
              itemCount: controller.optionsValue.length,
              gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                  crossAxisCount: 5, childAspectRatio: 0.8),
              itemBuilder: (context, index) {
                return GestureDetector(
                  onTap: () => controller
                      .onOptionsTap(controller.optionsValue[index].pageRoute),
                  child: Column(
                    children: [
                      Container(
                        height: 55.0,
                        width: 55.0,
                        margin: EdgeInsets.only(bottom: 5.0),
                        decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            border: Border.all(
                                color: Get.theme.primaryColorLight, width: 3),
                            color:
                                Get.theme.primaryColorLight.withOpacity(0.4)),
                        child: Center(
                          child: Icon(
                            controller.optionsValue[index].icon,
                            size: 25,
                            color: Get.theme.primaryColorDark,
                          ),
                        ),
                      ),
                      CustomText(
                        controller.optionsValue[index].title,
                        color: Colors.black,
                        size: 18.0,
                      )
                    ],
                  ),
                );
              },
            ),
          ),
        );
      });
}

GetBuilder buildHomeAppbar(
  BuildContext context,
) {
  return GetBuilder<HomePageController>(
    builder: (controller) => Container(
      height: sl<GlobalSetting>().user.roleId.enumUserRole ==
                  UserRole.CASHIER ||
              sl<GlobalSetting>().user.roleId.enumUserRole == UserRole.WAITER
          ? 100.0
          : 160.0,
      width: Get.width,
      color: Get.theme.primaryColor,
      padding: EdgeInsets.only(top: 30.0),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Row(
                  children: [
                    // IconButton(
                    //   icon: Icon(Icons.menu),
                    //   onPressed: () {},
                    //   color: Colors.white,
                    // ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        // CustomText(
                        //   "Store Name",
                        //   fontWeight: FontWeight.bold,
                        //   size: 22.0,
                        // ),
                        CustomText(
                          sl<GlobalSetting>().user.store?.name ??
                              sl<GlobalSetting>().user.userName ??
                              "",
                          fontWeight: FontWeight.bold,
                          size: 25.0,
                        ),
                        CustomText(
                          sl<GlobalSetting>().user.roleId.enumUserRole ==
                                  UserRole.WAITER
                              ? "Waiter"
                              : sl<GlobalSetting>().user.roleId.enumUserRole ==
                                      UserRole.CASHIER
                                  ? "Cashier"
                                  : "Store Management",
                          size: 20.0,
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              Row(
                children: [
                  IconButton(
                    icon: Icon(
                      Icons.qr_code_scanner,
                      size: 32,
                    ),
                    onPressed: () {
                      Get.toNamed(Routes.QRCodePage);
                    },
                    color: Colors.white,
                  ),
                  IconButton(
                    icon: Icon(Icons.notifications, size: 32),
                    onPressed: () {
                      Get.toNamed(Routes.RestaurantNotificationPage);
                    },
                    color: Colors.white,
                  ),
                ],
              ),
            ],
          ),
          sl<GlobalSetting>().user.roleId.enumUserRole == UserRole.CASHIER ||
                  sl<GlobalSetting>().user.roleId.enumUserRole ==
                      UserRole.WAITER
              ? SizedBox()
              : Padding(
                  padding: const EdgeInsets.only(bottom: 15.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: [
                      Column(
                        children: [
                          Obx(() => CustomText(
                                controller?.getOrderCount?.value?.length
                                        ?.toString() ??
                                    '',
                                fontWeight: FontWeight.bold,
                                size: 22.0,
                              )),
                          CustomText(
                            "Today Orders",
                            size: 22.0,
                          ),
                        ],
                      ),
                      // Column(
                      //   children: [
                      //     CustomText(
                      //       "1800.63\$",
                      //       fontWeight: FontWeight.bold,
                      //       size: 22.0,
                      //     ),
                      //     CustomText(
                      //       "Today Sale",
                      //       size: 18.0,
                      //     ),
                      //   ],
                      // ),
                      Column(
                        children: [
                          Obx(() => CustomText(
                                controller?.storeRateComment?.value?.length
                                        ?.toString() ??
                                    '',
                                fontWeight: FontWeight.bold,
                                size: 22.0,
                              )),
                          CustomText(
                            "Tickets",
                            size: 22.0,
                          ),
                        ],
                      ),
                    ],
                  ),
                )
        ],
      ),
    ),
  );
}

class HomeOptions extends SliverPersistentHeaderDelegate {
  final double expandedHeight;

  HomeOptions({@required this.expandedHeight});

  @override
  Widget build(
      BuildContext context, double shrinkOffset, bool overlapsContent) {
    return Material(
      color: ((shrinkOffset / expandedHeight) - 1).abs() <= 0.2
          ? Get.theme.accentColor
          : Get.theme.scaffoldBackgroundColor,
      elevation: (shrinkOffset / expandedHeight * 3),
      child: Stack(
        fit: StackFit.expand,
        overflow: Overflow.visible,
        children: [
          Transform.translate(
              offset: Offset(0.0, -shrinkOffset * 0.6),
              child: buildHomeOptions(context)),
          Column(
            mainAxisAlignment: MainAxisAlignment.end,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              ((shrinkOffset / expandedHeight) - 1).abs() < 1
                  ? SizedBox()
                  : Padding(
                      padding: const EdgeInsets.only(right: 20.0, left: 20.0),
                      child: Divider(
                        color: Colors.black,
                      ),
                    ),
              Padding(
                padding: const EdgeInsets.only(left: 20.0, bottom: 0.0),
                child: CustomText(
                  'All Orders',
                  color: Colors.black,
                  size: 20.0,
                  textAlign: TextAlign.start,
                  fontWeight: FontWeight.bold,
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(left: 20.0, bottom: 10.0),
                child: CustomText(
                  'You Can Sort By Order Category',
                  color: Colors.black,
                  size: 18.0,
                  textAlign: TextAlign.start,
                  fontWeight: FontWeight.normal,
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }

  @override
  double get maxExtent => expandedHeight;

  @override
  double get minExtent => kToolbarHeight;

  @override
  bool shouldRebuild(SliverPersistentHeaderDelegate oldDelegate) => true;
}
