import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:food_resturant/core/config/api_address.dart';
import 'package:food_resturant/core/config/global_setting.dart';
import 'package:food_resturant/core/config/routes.dart';
import 'package:food_resturant/feature/depenency_injection.dart';
import 'package:food_resturant/feature/entities/get_order_count_entity.dart';
import 'package:food_resturant/feature/entities/get_orders_entity.dart';
import 'package:food_resturant/feature/entities/get_store_rate_entity.dart';
import 'package:food_resturant/feature/entities/login_entity.dart';
import 'package:food_resturant/feature/server/api_helper.dart';
import 'package:food_resturant/feature/server/api_response.dart';
import 'package:food_resturant/feature/server/params/get_order_count_params.dart';
import 'package:food_resturant/feature/server/params/get_orders_params.dart';
import 'package:food_resturant/feature/server/params/get_store_rate_params.dart';
import 'package:food_resturant/feature/view/page/orders_page/orders_page.dart';
import 'package:food_resturant/library/enums/enums.dart';
import 'package:food_resturant/library/extention/enum_extention.dart';
import 'package:food_resturant/library/extention/int_extention.dart';
import 'package:food_resturant/library/platforms/loading_overlay.dart';
import 'package:food_resturant/library/widgets/snackbar.dart';
import 'package:food_resturant/library/widgets/text_widget.dart';
import 'package:get/get.dart';

import '../../../../core/config/routes.dart';

class HomePageController extends GetxController
    with SingleGetTickerProviderMixin {
  final overlay = LoadingOverlay.of(Get.context);

  List<HomeOptionsInfo> optionsValue = [
    HomeOptionsInfo(
        title: "Tables", icon: Icons.table_rows, pageRoute: Routes.TablesPage),
    // HomeOptionsInfo(
    //     title: "Staffs", icon: Icons.person, pageRoute: Routes.StaffsPage),
    HomeOptionsInfo(
        title: "Menu", icon: Icons.local_dining, pageRoute: Routes.MenuPage),
    // HomeOptionsInfo(
    //     title: "Stocks", icon: Icons.list, pageRoute: Routes.StocksPage),
    HomeOptionsInfo(
        title: "Statistics",
        icon: Icons.leaderboard,
        pageRoute: Routes.StatisticsPage),
    HomeOptionsInfo(
        title: "Profile",
        icon: Icons.account_circle,
        pageRoute: Routes.ProfilePage),
    HomeOptionsInfo(
        title: "Orders", icon: Icons.group, pageRoute: Routes.OrdersPage),
    HomeOptionsInfo(
        title: "Withdraw",
        icon: Icons.nature_people,
        pageRoute: Routes.WithdrawPage),
    HomeOptionsInfo(
        title: "Inbox", icon: Icons.email, pageRoute: Routes.AllTicketsPages),
    HomeOptionsInfo(
        title: "Discounts",
        icon: FontAwesome.percent,
        pageRoute: Routes.DiscountsPage),
    HomeOptionsInfo(
        title: "Balance",
        icon: Icons.account_balance_wallet_sharp,
        pageRoute: Routes.BalancePage),
  ];
  List<String> tabsTitle = List<String>(4);

  TabController tabController;
  RxList<GetOrderCountObject> getOrderCount = <GetOrderCountObject>[].obs;
  RxList<GetStoreRateEntityObject> storeRateComment =
      <GetStoreRateEntityObject>[].obs;
  RxList<GetOrdersObject> allOrder = <GetOrdersObject>[].obs;
  RxList<GetOrdersObject> deliveryOrder = <GetOrdersObject>[].obs;
  RxList<GetOrdersObject> servinOrder = <GetOrdersObject>[].obs;
  RxList<GetOrdersObject> takeoutOrder = <GetOrdersObject>[].obs;

  LoginObject user;

  addItemsToTabs() {
    tabsTitle[0] = OrderTypes.ALL.orderTypeToString;
    tabsTitle[1] = OrderTypes.DELIVERY.orderTypeToString;
    tabsTitle[2] = OrderTypes.SERVEIN.orderTypeToString;
    tabsTitle[3] = OrderTypes.TAKEOUT.orderTypeToString;
  }

  tabListener() {
    tabController.addListener(() {
      update();
    });
  }

  onOptionsTap(String pageRoute) {
    if (pageRoute != null) {
      print("to");

      Get.toNamed(pageRoute).then((value) {
        print("back");
        update();
      });
    }
  }

  List<Widget> tabTitleWidgetBuilder() {
    List<Widget> tabWidgetTitle = List<Widget>();
    for (int i = 0; i < tabsTitle.length; i++) {
      tabWidgetTitle.add(Tab(
        icon: Container(
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(30.0),
              border: Border.all(color: Get.theme.primaryColor)),
          child: Padding(
            padding: const EdgeInsets.only(
                top: 8.0, bottom: 8.0, left: 20.0, right: 20.0),
            child: CustomText(
              tabsTitle[i],
              size: 20.0,
              fontWeight: FontWeight.bold,
              color: tabController.index == i
                  ? Colors.white
                  : Get.theme.primaryColor,
            ),
          ),
        ),
      ));
    }
    return tabWidgetTitle;
  }

  @override
  void onInit() {
    user = sl<GlobalSetting>().user;
    if (user?.roleId?.enumUserRole == UserRole.WAITER ||
        user?.roleId?.enumUserRole == UserRole.CASHIER) {
      optionsValue = [];
      optionsValue = [
        HomeOptionsInfo(
            title: "Tables",
            icon: Icons.table_rows,
            pageRoute: Routes.TablesPage),
        // HomeOptionsInfo(
        //     title: "Staffs", icon: Icons.person, pageRoute: Routes.StaffsPage),
        HomeOptionsInfo(
            title: "Orders", icon: Icons.group, pageRoute: Routes.OrdersPage),
        HomeOptionsInfo(
            title: "Profile",
            icon: Icons.account_circle,
            pageRoute: Routes.ProfilePage),
      ];
    }
    // else if (user?.roleId?.enumUserRole == UserRole.CASHIER) {
    //   optionsValue = [];
    //   optionsValue = [
    //     HomeOptionsInfo(
    //         title: "Tables",
    //         icon: Icons.table_rows,
    //         pageRoute: Routes.TablesPage),
    //     // HomeOptionsInfo(
    //     //     title: "Staffs", icon: Icons.person, pageRoute: Routes.StaffsPage),
    //     HomeOptionsInfo(
    //         title: "Orders", icon: Icons.group, pageRoute: Routes.OrdersPage),
    //     HomeOptionsInfo(
    //         title: "Statistics",
    //         icon: Icons.leaderboard,
    //         pageRoute: Routes.StatisticsPage),
    //   ];
    // }

    addItemsToTabs();
    tabController = TabController(length: tabsTitle?.length, vsync: this);
    tabListener();
    super.onInit();
  }

  @override
  void onReady() async {
    await getOrdersApi();
    await getTicketCount();
    await getOrderCountApi();
  }

  getOrdersApi() async {
    // if (sl<GlobalSetting>().user == null) return;
    // var user = sl<GlobalSetting>().user;
    // var orderSParams = GetOrdersParams(storeId: user.store.id, userId: user.id);
    var orderSParams = GetOrdersParams(
        orderType: OrderTypes.ALL, pageNo: -1, storeId: user?.store?.id);

    final response = await overlay.during(sl<ApiHelper>()
        .getWithParam(ApiAddress.GET_STORE_ORDERS, orderSParams.toJson()));
    var result = ApiResponse.returnResponse(
        response, GetOrdersEntity.fromJson(response.data));
    if (result.status == ApiStatus.COMPLETED && result?.data?.status) {
      allOrder.value = result.data?.getOrdersObject;
      deliveryOrder.value = result.data.getOrdersObject
          .where((arg1) => arg1.orderType == OrderTypes.DELIVERY)
          .toList();

      servinOrder.value = result.data.getOrdersObject
          .where((arg1) => arg1.orderType == OrderTypes.SERVEIN)
          .toList();

      takeoutOrder.value = result.data.getOrdersObject
          .where((arg1) => arg1.orderType == OrderTypes.TAKEOUT)
          .toList();
    } else {
      showSnackBar(text: result?.data?.message ?? '');
    }
  }

  getTicketCount() async {
    var storerate = GetStoreRateParams(storeId: user?.store?.id);

    final response = await sl<ApiHelper>()
        .getWithParam(ApiAddress.GET_STORE_RATE, storerate.toJson());
    var result = ApiResponse.returnResponse(
        response, GetStoreRateEntity.fromJson(response.data));
    if (result.status == ApiStatus.COMPLETED && result?.data?.status) {
      // showSnackBar(text: result?.data?.message ?? '');
      storeRateComment.value = result?.data?.getStoreRateEntityObject
          ?.where((element) => element?.answer == null)
          ?.toList();
    } else {
      showSnackBar(text: result?.data?.message ?? '');
    }
  }

  getOrderCountApi() async {
    var orderCount = GetOrdersCountParams(
        storeId: user?.store?.id ?? "",
        createdAt: (DateTime?.now()?.millisecondsSinceEpoch) ~/ 1000);

    final response = await sl<ApiHelper>()
        .getWithParam(ApiAddress.GET_ORDER_COUNT, orderCount.toJson());
    var result = ApiResponse.returnResponse(
        response, GetOrderCountEntity.fromJson(response.data));
    if (result.status == ApiStatus.COMPLETED && result?.data?.status) {
      // showSnackBar(text: result?.data?.message ?? '');
      getOrderCount.value = result?.data?.getOrderCountObject;
    } else {
      showSnackBar(text: result?.data?.message ?? '');
    }
  }
}

class HomeOptionsInfo {
  final String title;
  final IconData icon;
  final String pageRoute;

  HomeOptionsInfo({@required this.title, @required this.icon, this.pageRoute});
}
