import 'package:flutter/material.dart';
import 'package:food_resturant/core/config/config.dart';
import 'package:food_resturant/core/config/routes.dart';
import 'package:food_resturant/feature/view/page/add_discount_page/add_discount_page_controller.dart';
import 'package:food_resturant/feature/view/page/add_discount_page/add_discount_page_controller.dart';
import 'package:food_resturant/feature/view/page/discount_page/restaurant_discounts_page_controller.dart';
import 'package:food_resturant/feature/view/page/login_page/login_page_controller.dart';
import 'package:food_resturant/feature/view/page/stocks_page/stocks_page_controller.dart';
import 'package:food_resturant/feature/view/page/tables_page/tables_page_controller.dart';
import 'package:food_resturant/feature/view/widget/app_bar.dart';
import 'package:food_resturant/feature/view/widget/box_shadow_widget.dart';
import 'package:food_resturant/library/widgets/button_widget.dart';
import 'package:food_resturant/library/widgets/custom_drop_list/custom_drop_down_list.dart';
import 'package:food_resturant/library/widgets/text_widget.dart';
import 'package:get/get.dart';
import 'package:qr_flutter/qr_flutter.dart';

import '../../../../library/widgets/text_field_widget.dart';

class LoginPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // floatingActionButton: ,
      // floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
      body: Container(
        constraints: BoxConstraints.expand(),
        child: GetBuilder<LoginPageController>(
            init: LoginPageController(),
            builder: (controller) {
              return Form(
                key: controller.formKey,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    Expanded(
                      child: ListView(
                        padding: EdgeInsets.all(30),
                        children: [
                          SizedBox(height: Get.height * .1),
                          Align(
                              alignment: Alignment.centerLeft,
                              child: Image.asset(Images.login)),
                          CustomText(
                            'Proced With Your',
                            color: Colors.black,
                            textAlign: TextAlign.start,
                            size: 32.0,
                            fontWeight: FontWeight.normal,
                          ),
                          CustomText(
                            'Login',
                            color: Colors.black,
                            textAlign: TextAlign.start,
                            size: 32.0,
                            fontWeight: FontWeight.bold,
                          ),
                          SizedBox(
                            height: 30,
                          ),
                          CustomTextField(
                              lable: 'User Name',
                              textEditingController:
                                  controller.userNameTextController,
                              enableBorder: true,
                              backgroundColor: Colors.transparent,
                              suffixIcon: Icon(
                                Icons.person,
                                size: 28,
                              ),
                              maxLine: 1,
                              textColor: Colors.black,
                              fontSize: 18.0,
                              hintColor: Get.theme.primaryColorDark,
                              // validator: (value) {
                              //   if (value.isEmpty) {
                              //     return 'Please enter some text';
                              //   }
                              //   return null;
                              // },
                              isBold: true),
                          CustomTextField(
                              lable: 'Password',
                              textEditingController:
                                  controller.passwordTextController,
                              enableBorder: true,
                              backgroundColor: Colors.transparent,
                              suffixIcon: Icon(
                                Icons.lock,
                                size: 28,
                              ),
                              maxLine: 1,
                              textColor: Colors.black,
                              hintColor: Get.theme.primaryColorDark,
                              fontSize: 18.0,
                              isBold: true),
                        ],
                      ),
                    ),
                    Container(
                      height: 60,
                      padding: EdgeInsets.fromLTRB(20, 0, 20, 10),
                      child: CustomButton(
                        onTap: () {
                          controller.login();
                        },
                        color: Get.theme.primaryColor,
                        child: CustomText(
                          'Sign In',
                          color: Get.theme.accentColor,
                          size: 22.0,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    )
                  ],
                ),
              );
            }),
      ),
    );
  }
}
