import 'dart:convert';
import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:food_resturant/core/config/api_address.dart';
import 'package:food_resturant/core/config/global_setting.dart';
import 'package:food_resturant/core/config/keys.dart';
import 'package:food_resturant/core/config/routes.dart';
import 'package:food_resturant/feature/depenency_injection.dart';
import 'package:food_resturant/feature/entities/login_entity.dart';
import 'package:food_resturant/feature/server/api_helper.dart';
import 'package:food_resturant/feature/server/api_response.dart';
import 'package:food_resturant/feature/server/params/login_params.dart';
import 'package:food_resturant/library/platforms/loading_overlay.dart';
import 'package:food_resturant/library/platforms/notification_service.dart';
import 'package:food_resturant/library/platforms/shared_preferences.dart';
import 'package:food_resturant/library/widgets/snackbar.dart';
import 'package:food_resturant/library/widgets/text_widget.dart';
import 'package:get/get.dart';

class LoginPageController extends GetxController {
  final overlay = LoadingOverlay.of(Get.context);
  final formKey = GlobalKey<FormState>();

  TextEditingController userNameTextController =
      TextEditingController(text: '');
  TextEditingController passwordTextController =
      TextEditingController(text: '');

  login() async {
    if (formKey.currentState.validate()) {
      var pushId = await sl<NotificationService>()?.getPushId();
      log('PUSH ID : ' + pushId ?? '');

      var loginParams = LoginParams(
          userName: userNameTextController?.text?.toString(),
          password: passwordTextController?.text?.toString(),
          pushId: pushId);

      final loginResponse = await overlay
          .during(sl<ApiHelper>().post(ApiAddress.LOGIN, loginParams.toJson()));
      var loginResult = ApiResponse.returnResponse(
          loginResponse, LoginEntity.fromJson(loginResponse.data));
      if (loginResult.status == ApiStatus.COMPLETED) {
        if (loginResult.data.status) {
          sl<DataStorage>().setData(
              key: USER, value: json.encode(loginResult?.data?.toJson()));
          sl<GlobalSetting>().user = loginResult?.data?.loginObject;

          Get.offAndToNamed(Routes.HomePage);
        } else {
          showSnackBar(text: loginResult?.data?.message ?? '');
        }
      }
    }
  }
}
