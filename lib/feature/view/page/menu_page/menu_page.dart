import 'package:flutter/material.dart';
import 'package:food_resturant/core/config/api_address.dart';
import 'package:food_resturant/core/config/routes.dart';
import 'package:food_resturant/feature/entities/get_store_food_category_entity.dart';
import 'package:food_resturant/feature/entities/get_store_food_entity.dart';
import 'package:food_resturant/feature/entities/main_category.dart';
import 'package:food_resturant/feature/view/page/menu_page/menu_page_controller.dart';
import 'package:food_resturant/feature/view/widget/app_bar.dart';
import 'package:food_resturant/library/platforms/splash_handler.dart';
import 'package:food_resturant/library/platforms/will_pop.dart';
import 'package:food_resturant/library/widgets/cached_network_image_widget.dart';
import 'package:food_resturant/library/widgets/text_widget.dart';
import 'package:get/get.dart';
import 'package:food_resturant/library/extention/money_extention.dart';

class MenuPage extends StatelessWidget {
  final controller = Get.put(MenuPageController());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Get.toNamed(Routes.AddFoodPage).then((mainCategory) {
            if (mainCategory != null) {
              controller?.getAllStoreFood();
            }
          });
        },
        backgroundColor: Get.theme.primaryColor,
        child: Center(
          child: Icon(Icons.add),
        ),
      ),
      appBar: AppBar(
        iconTheme: IconThemeData(opacity: 0.0),
        toolbarHeight: 190.0,
        backgroundColor: Get.theme.scaffoldBackgroundColor,
        elevation: 0.0,
        bottom: TabBar(
            isScrollable: controller?.tabsTitle?.length > 4 ? true : false,
            onTap: (index) {
              controller.tabSwitch(index);
            },
            controller: controller.tabController,
            indicatorColor: Get.theme.primaryColor,
            tabs: controller.postTabBuilder()),
        flexibleSpace: BuildAppBarWidget(
          title: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              CustomText(
                'Store Menu',
                size: 25.0,
                fontWeight: FontWeight.bold,
              ),
              CustomText(
                'View, Edit, Add, Remove Foods',
                size: 19.0,
                fontWeight: FontWeight.normal,
              )
            ],
          ),
        ),
      ),
      body: TabBarView(
          physics: NeverScrollableScrollPhysics(),
          controller: controller.tabController,
          children: controller.tabsTitle
              .map((e) => Container(
                    child: Column(
                      children: [
                        Expanded(
                          child: Obx(() => ListView.builder(
                                itemCount: controller.storeFood.length ?? 0,
                                itemBuilder: (context, index) =>
                                    foodItemBuilder(
                                        context, controller.storeFood[index]),
                              )),
                        )
                      ],
                    ),
                  ))
              .toList()),
    );
  }

  Widget foodItemBuilder(BuildContext context, GetStoreFoodObject foodObject) {
    return Card(
        color: Get.theme.accentColor,
        elevation: 4,
        margin: EdgeInsets.all(8.0),
        child: ListTile(
            enabled: true,
            leading: BuildCachedImageWidget(
              imageUrl: (foodObject.icon?.location == null ||
                      foodObject.icon?.location.isEmpty)
                  ? ApiAddress.FOOD_DEFAULT_URL
                  : foodObject.icon?.location,
              height: 50.0,
              width: 50.0,
              borderRadius: 60.0,
            ),
            trailing: IconButton(
              icon: Icon(Icons.delete),
              color: Colors.red,
              onPressed: () {
                Get.defaultDialog(
                    title: 'Confirmation',
                    middleText: 'Are you sure delete food?',
                    textCancel: 'no',
                    cancelTextColor: Colors.blueAccent,
                    confirmTextColor: Colors.blueAccent,
                    textConfirm: 'yes',
                    onConfirm: (){
                      controller.deleteStoreFood(foodObject?.id);

                    },
                );

              },
            ),
            title: Align(
              child: CustomText(
                foodObject?.name ?? '',
                color: Colors.black,
                size: 18.0,
                fontWeight: FontWeight.bold,
              ),
              alignment: Alignment(-1.0, 0),
            ),
            subtitle: Align(
              alignment: Alignment(-1.0, 0),
              child: CustomText(
                foodObject?.totalPrice?.moneyFormatter ?? '',
                color: Colors.black,
                size: 16.0,
                fontWeight: FontWeight.normal,
              ),
            ),
            dense: false,
            onTap: () {
              Get.toNamed(Routes.AddFoodPage, arguments: foodObject)
                  .then((mainCategory) {
                // if (mainCategory) {
                controller?.getAllStoreFood();

                // }
              });
            }));
  }
}
