import 'package:flutter/material.dart';
import 'package:food_resturant/core/config/api_address.dart';
import 'package:food_resturant/core/config/global_setting.dart';
import 'package:food_resturant/core/config/routes.dart';
import 'package:food_resturant/feature/depenency_injection.dart';
import 'package:food_resturant/feature/entities/get_food_categories_entity.dart';
import 'package:food_resturant/feature/entities/get_store_food_category_entity.dart';
import 'package:food_resturant/feature/entities/get_store_food_entity.dart';
import 'package:food_resturant/feature/entities/login_entity.dart';
import 'package:food_resturant/feature/entities/main_category.dart';
import 'package:food_resturant/feature/entities/public_entity.dart';
import 'package:food_resturant/feature/server/api_helper.dart';
import 'package:food_resturant/feature/server/api_response.dart';
import 'package:food_resturant/feature/server/params/get_store_food_params.dart';
import 'package:food_resturant/feature/server/params/public_params.dart';
import 'package:food_resturant/library/platforms/loading_overlay.dart';
import 'package:food_resturant/library/widgets/snackbar.dart';
import 'package:food_resturant/library/widgets/text_widget.dart';
import 'package:get/get.dart';
import 'package:queries/collections.dart';

class MenuPageController extends GetxController
    with SingleGetTickerProviderMixin {
  final overlay = LoadingOverlay.of(Get.context);
  List<String> tabsTitle = List<String>();
  TabController tabController;

  RxList<GetStoreFoodObject> storeFood = <GetStoreFoodObject>[].obs;
  List<MainCategory> foodCategoriesObject;

  LoginObject user;

  int lastTabIndex = 0;

  MainCategory lastMainCategorySelection;

  List<Widget> postTabBuilder() {
    List<Widget> listTabs = List<Widget>();
    tabsTitle.forEach((title) {
      listTabs.add(
        Padding(
          padding: const EdgeInsets.only(bottom: 12.0, top: 11.0),
          child: CustomText(
            title,
            color: Colors.black,
            size: 20.0,
            fontWeight: FontWeight.normal,
          ),
        ),
      );
    });
    return listTabs;
  }

  @override
  void onInit() {
    user = sl<GlobalSetting>().user;
    update();
    tabsTitle.add('All');
    foodCategoriesObject = sl<GlobalSetting>().foodCategoriesObject;
    tabsTitle.addAll(foodCategoriesObject?.map((e) => e.name));
    // tabsTitle.add('Add/Edit');
    tabController = TabController(length: tabsTitle.length, vsync: this);
    super.onInit();
  }

  @override
  void onReady() {
    // getFoodCategoryApi();
    getAllStoreFood();
  }

  @override
  void onClose() {
    tabController.dispose();
  }

  deleteStoreFood([String id]) async {
    var removeStoreFoodParams = {"storeFoodId": id};

    final response = await overlay.during(sl<ApiHelper>()
        .post(ApiAddress.REMOVE_STORE_FOOD, removeStoreFoodParams));
    var result = ApiResponse.returnResponse(
        response, PublicEntity.fromJson(response.data));
    if (result.status == ApiStatus.COMPLETED && result?.data?.status) {
      if (lastTabIndex == 0)
        getAllStoreFood();
      else
        getPublicStoreFood(lastMainCategorySelection?.id ?? '');

      // showSnackBar(text: result?.data?.message ?? '');
    } else {
      showSnackBar(text: result?.data?.message ?? '');
    }
  }

  getAllStoreFood() async {
    var getStoreFoodParams =
        GetStoreFoodParams(soreId: sl<GlobalSetting>().user?.store?.id ?? '');
    final response = await overlay.during(sl<ApiHelper>()
        .getWithParam(ApiAddress.GET_STORE_FOOD, getStoreFoodParams.toJson()));
    var result = ApiResponse.returnResponse(
        response, GetStoreFoodEntity.fromJson(response.data));
    if (result.status == ApiStatus.COMPLETED && result?.data?.status) {
      // showSnackBar(text: result?.data?.message ?? '');
      storeFood.value = result.data?.getStoreFoodObject;
      // pizzaStoreFood.value = result.data?.getStoreFoodObject?.where((element) => element.)

    } else {
      showSnackBar(text: result?.data?.message ?? '');
    }
  }

  getPublicStoreFood(String id) async {
    var getStoreFoodParams = GetStoreFoodParams(
        soreId: user?.store?.id ?? null, mainCategoryId: id, pageNo: -1);
    final response = await overlay.during(sl<ApiHelper>()
        .getWithParam(ApiAddress.GET_STORE_FOOD, getStoreFoodParams.toJson()));
    var result = ApiResponse.returnResponse(
        response, GetStoreFoodEntity.fromJson(response.data));
    if (result.status == ApiStatus.COMPLETED && result?.data?.status) {
      // showSnackBar(text: result?.data?.message ?? '');
      storeFood.value = result.data?.getStoreFoodObject;
    } else {
      showSnackBar(text: result?.data?.message ?? '');
    }
  }

  tabSwitch(int index) {
    lastTabIndex = index;
    var title = tabsTitle[index];
    if (lastTabIndex == 0) {
      //SHOW ALL
      getAllStoreFood();
    }
    // if (tabsTitle.length == index + 1) {
    //ADD AND EDIT
    // Get.toNamed(Routes.FoodCategory);
    // } else {
    lastMainCategorySelection =
        foodCategoriesObject.firstWhere((element) => element?.name == title);
    getPublicStoreFood(lastMainCategorySelection?.id);
    // }
  }

  getFoodCategoryApi() async {
    var publicParams = PublicParams();

    final response = await sl<ApiHelper>()
        .getWithParam(ApiAddress.GET_FOOD_CATEGORIES, publicParams.toJson());
    var result = ApiResponse.returnResponse(
        response, GetFoodCategoriesEntity.fromJson(response.data));
    if (result.status == ApiStatus.COMPLETED && result?.data?.status) {
      sl<GlobalSetting>().foodCategoriesObject =
          result?.data?.getFoodCategoriesObject;
    } else {
      showSnackBar(text: result?.data?.message ?? '');
    }
  }
}
