import 'package:flutter/material.dart';
import 'package:food_resturant/core/config/routes.dart';
import 'package:food_resturant/feature/entities/get_orders_entity.dart';
import 'package:food_resturant/feature/view/page/food_list_page/food_list_page_controller.dart';
import 'package:food_resturant/feature/view/page/order_history_page/order_history_page_controller.dart';
import 'package:food_resturant/feature/view/page/tables_page/tables_page_controller.dart';
import 'package:food_resturant/feature/view/widget/app_bar.dart';
import 'package:food_resturant/feature/view/widget/box_shadow_widget.dart';
import 'package:food_resturant/library/platforms/conver_date.dart';
import 'package:food_resturant/library/widgets/button_widget.dart';
import 'package:food_resturant/library/widgets/custom_drop_list/custom_drop_down_list.dart';
import 'package:food_resturant/library/widgets/text_widget.dart';
import 'package:food_resturant/library/extention/money_extention.dart';
import 'package:get/get.dart';
import 'package:qr_flutter/qr_flutter.dart';


class OrderHistoryPage extends StatelessWidget {
  final controller = Get.put(OrderHistoryPageController());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          BuildAppBarWidget(
            title: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                CustomText(
                  'Order History',
                  color: Colors.white,
                  size: 25.0,
                  fontWeight: FontWeight.bold,
                ),
                SizedBox(
                  height: 2.0,
                ),
                CustomText(
                  'View User Order History',
                  color: Colors.white,
                  size: 18.0,
                  fontWeight: FontWeight.w500,
                ),
              ],
            ),
          ),

          Expanded(
            child: Obx(() =>
                ListView.builder(
                  padding: EdgeInsets.all(5),
                  itemCount: controller?.orderHistory?.value?.length ?? 0,
                  itemBuilder: (context, index) {
                    final item = controller?.orderHistory?.value[index];
                    return buildOrderItem(item);
                  },
                )),
          )

        ],
      ),
    );
  }

  GestureDetector buildOrderItem([GetOrdersObject item]) {
    return GestureDetector(
      onTap: () {
        Get.toNamed(Routes.FoodListPage);
      },
      child: Card(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10.0),
        ),
        color: Get.theme.accentColor,
        elevation: 4,
        margin: EdgeInsets.all(5.0),
        child: Container(
          height: 80,
          padding: EdgeInsets.all(8.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              CustomText(
                convertMilliSecToDateTime(item?.createdAt)??'',
                color: Colors.black,
                size: 18.0,
                fontWeight: FontWeight.bold,
              ),
              CustomText(
                item?.totalPrice?.moneyFormatter??'',
                color: Colors.black,
                size: 16.0,
                fontWeight: FontWeight.normal,
              ),
            ],
          ),
        ),
      ),
    );
  }

}
