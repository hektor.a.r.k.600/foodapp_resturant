import 'package:flutter/material.dart';
import 'package:food_resturant/core/config/api_address.dart';
import 'package:food_resturant/core/config/global_setting.dart';
import 'package:food_resturant/feature/depenency_injection.dart';
import 'package:food_resturant/feature/entities/get_orders_entity.dart';
import 'package:food_resturant/feature/entities/login_entity.dart';
import 'package:food_resturant/feature/server/api_helper.dart';
import 'package:food_resturant/feature/server/api_response.dart';
import 'package:food_resturant/feature/server/params/get_orders_params.dart';
import 'package:food_resturant/library/enums/enums.dart';
import 'package:food_resturant/library/platforms/loading_overlay.dart';
import 'package:food_resturant/library/widgets/snackbar.dart';
import 'package:food_resturant/library/widgets/text_widget.dart';
import 'package:get/get.dart';

class OrderHistoryPageController extends GetxController {
  LoginObject user;
  final overlay = LoadingOverlay.of(Get.context);
  RxList<GetOrdersObject> orderHistory = <GetOrdersObject>[].obs;
  GetOrdersObject ordersObject;

  @override
  void onInit() {
    user = sl<GlobalSetting>().user;
    ordersObject = Get.arguments;
    super.onInit();
  }

  @override
  void onReady() {
    getOrdersApi();
  }

  getOrdersApi() async {
    // if (sl<GlobalSetting>().user == null) return;
    // var user = sl<GlobalSetting>().user;

    // var orderSParams = GetOrdersParams(storeId: user.store.id, userId: user.id);
    var orderSParams = GetOrdersParams(
        orderType: OrderTypes.ALL,
        pageNo: -1,
        storeId: user?.store?.id,
        userId: ordersObject?.user?.id);

    final response = await overlay.during(sl<ApiHelper>()
        .getWithParam(ApiAddress.GET_STORE_ORDERS, orderSParams.toJson()));
    var result = ApiResponse.returnResponse(
        response, GetOrdersEntity.fromJson(response.data));
    if (result.status == ApiStatus.COMPLETED && result?.data?.status) {
      // showSnackBar(text: result?.data?.message ?? '');

      orderHistory.value = result.data?.getOrdersObject;
    } else {
      showSnackBar(text: result?.data?.message ?? '');
    }
  }
}
