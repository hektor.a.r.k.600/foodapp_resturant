import 'package:cached_network_image/cached_network_image.dart';
import 'package:dartz/dartz.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:food_resturant/core/config/config.dart';
import 'package:food_resturant/core/config/routes.dart';
import 'package:food_resturant/feature/view/page/order_status_page/order_status_page_controller.dart';
import 'package:food_resturant/feature/view/page/orders_page/orders_page.dart';
import 'package:food_resturant/feature/view/page/orders_page/orders_page_controller.dart';
import 'package:food_resturant/feature/view/page/profile_page/profile_page_controller.dart';
import 'package:food_resturant/feature/view/page/stocks_page/stocks_page_controller.dart';
import 'package:food_resturant/feature/view/page/tables_page/tables_page_controller.dart';
import 'package:food_resturant/feature/view/widget/app_bar.dart';
import 'package:food_resturant/feature/view/widget/box_shadow_widget.dart';
import 'package:food_resturant/library/enums/enums.dart';
import 'package:food_resturant/library/widgets/button_widget.dart';
import 'package:food_resturant/library/widgets/cached_network_image_widget.dart';
import 'package:food_resturant/library/widgets/custom_drop_list/custom_drop_down_list.dart';
import 'package:food_resturant/library/widgets/text_widget.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:qr_flutter/qr_flutter.dart';
import 'package:food_resturant/library/extention/enum_extention.dart';

import '../../../../library/widgets/text_field_widget.dart';

class OrderStatusPage extends StatelessWidget {
  final controller = Get.put(OrderStatusPageController());

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        return false;
      },
      child: Scaffold(
        body: SingleChildScrollView(
          child: GetBuilder<OrderStatusPageController>(
              init: OrderStatusPageController(),
              builder: (x) {
                return Column(
                  children: [
                    BuildAppBarWidget(
                      onBack: () {
                        Get.offNamed(Routes.OrdersPage);
                      },
                      title: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          CustomText(
                            controller.title ?? '',
                            size: 25.0,
                            fontWeight: FontWeight.bold,
                          ),
                          CustomText(
                            'Current Order Status',
                            size: 19.0,
                            fontWeight: FontWeight.normal,
                          )
                        ],
                      ),
                    ),

                    Padding(
                      padding:
                          const EdgeInsets.only(top: 16, right: 16, left: 16),
                      child: Container(
                        height: 120.0,
                        width: Get.width,
                        decoration: BoxDecoration(
                            color: Get.theme.primaryColorLight.withAlpha(150),
                            borderRadius: BorderRadius.circular(10.0)),
                        child: Padding(
                          padding: const EdgeInsets.all(16),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  CustomText(
                                    'First Name',
                                    color: Colors.black,
                                    size: 18.0,
                                    fontWeight: FontWeight.bold,
                                  ),
                                  CustomText(
                                    controller.firstName ?? '',
                                    color: Get.theme.primaryColorDark,
                                    size: 18.0,
                                    fontWeight: FontWeight.w500,
                                  ),
                                ],
                              ),
                              Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  CustomText(
                                    'Last Name',
                                    color: Colors.black,
                                    size: 18.0,
                                    fontWeight: FontWeight.bold,
                                  ),
                                  CustomText(
                                    controller.lastName ?? '',
                                    color: Get.theme.primaryColorDark,
                                    size: 18.0,
                                    fontWeight: FontWeight.w500,
                                  ),
                                ],
                              ),
                              Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  CustomText(
                                    'Phone Number',
                                    color: Colors.black,
                                    size: 18.0,
                                    fontWeight: FontWeight.bold,
                                  ),
                                  CustomText(
                                    controller.phoneNum ?? '',
                                    color: Get.theme.primaryColorDark,
                                    size: 18.0,
                                    fontWeight: FontWeight.w500,
                                  ),
                                ],
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                    buildOrderStatusItem(
                        label: "Order Date",
                        value: controller.createAt == null
                            ? ""
                            : DateFormat("y/M/d").add_jm().format(
                                DateTime.fromMillisecondsSinceEpoch(
                                    (controller.createAt ?? 0) * 1000))),
                    buildOrderStatusItem(
                        label: "Order Category",
                        value: controller.orderCategory ?? ''),

                    controller?.ordersObject?.orderType == OrderTypes?.DELIVERY
                        ? Padding(
                            padding: const EdgeInsets.only(
                                top: 16, right: 16, left: 16),
                            child: Container(
                              height: 100.0,
                              width: Get.width,
                              decoration: BoxDecoration(
                                  color: Get.theme.primaryColorLight
                                      .withAlpha(150),
                                  borderRadius: BorderRadius.circular(10.0)),
                              child: Padding(
                                padding: const EdgeInsets.all(16),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    CustomText(
                                      'Address',
                                      color: Colors.black,
                                      size: 18.0,
                                      fontWeight: FontWeight.bold,
                                    ),
                                    Flexible(
                                      child: CustomText(
                                        controller?.address ?? '',
                                        color: Get.theme.primaryColorDark,
                                        size: 18.0,
                                        fontWeight: FontWeight.bold,
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          )
                        : SizedBox(),

                    controller.isDelivery
                        ? SizedBox(
                            height: 100,
                          )
                        : SizedBox(),
                    //MAP

                    buildOrderStatusItem(
                        label: "Count Down",
                        value: controller.countDown ?? '' + 'Min'),
                    buildOrderStatusItem(
                        label: "Current Order Status",
                        value: controller.currentStatus ?? ''),
                    buildOrderStatusItem(
                        label: "Store Order Status",
                        value: controller.orderStoreStatus ?? ''),
                    buildOrderStatusItem(
                        label: "Peyment Type",
                        value: controller.peymentType ?? ''),
                    controller?.order?.chashAprovalStatus == null ||
                            controller.order.chashAprovalStatus ==
                                ChashAprovalStatus.OrderIsNotCash.index
                        ? SizedBox()
                        : buildOrderStatusItem(
                            label: "Chash Approval Status",
                            value: controller.order.chashAprovalStatus ==
                                    ChashAprovalStatus.Pending.index
                                ? "Pending"
                                : "Approved"),
                    // buildOrderStatusItem(label: "Saving" , value:controller.saveing??''),
                    buildOrderStatusItem(
                        label: "total Price",
                        value: controller.totalPrice ?? ''),
                    // buildOrderStatusItem(label: "User Rate" , value:controller.userRate??''),
                    Padding(
                      padding:
                          const EdgeInsets.only(top: 16, right: 16, left: 16),
                      child: Container(
                        height: 120.0,
                        width: Get.width,
                        decoration: BoxDecoration(
                            color: Get.theme.primaryColorLight.withAlpha(150),
                            borderRadius: BorderRadius.circular(10.0)),
                        child: Padding(
                          padding: const EdgeInsets.all(16),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              CustomText(
                                'User Comment',
                                color: Colors.black,
                                size: 18.0,
                                fontWeight: FontWeight.bold,
                              ),
                              CustomText(
                                controller?.description ?? '',
                                color: Get.theme.primaryColorDark,
                                size: 18.0,
                                fontWeight: FontWeight.bold,
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                    Padding(
                      padding:
                          const EdgeInsets.only(top: 16, right: 16, left: 16),
                      child: SizedBox(
                          width: Get.width,
                          height: 40.0,
                          child: CustomButton(
                            borderRadios: 10.0,
                            onTap: () {
                              Get.toNamed(Routes.FoodListPage,
                                      arguments: controller.ordersObject)
                                  .then((isChangeOrderStatus) {
                                if (isChangeOrderStatus != null &&
                                    isChangeOrderStatus) {
                                  controller.getOrderByIdApi();
                                }
                              });
                            },
                            type: 2,
                            child: CustomText(
                              'Food List',
                              color: Colors.black45,
                              size: 18.0,
                              fontWeight: FontWeight.bold,
                            ),
                          )),
                    ),
                    controller.user.loginObject.roleId ==
                                UserRole.CASHIER.index &&
                            controller?.order?.chashAprovalStatus != null &&
                            controller.order.chashAprovalStatus ==
                                ChashAprovalStatus.Pending.index
                        ? Padding(
                            padding: const EdgeInsets.only(
                                top: 16, right: 16, left: 16),
                            child: SizedBox(
                                width: Get.width,
                                height: 40.0,
                                child: CustomButton(
                                  borderRadios: 10.0,
                                  onTap: () async {
                                    await controller
                                        .changeOrderChashApprocalStatus();
                                  },
                                  type: 2,
                                  child: CustomText(
                                    'Approval',
                                    color: Colors.black45,
                                    size: 18.0,
                                    fontWeight: FontWeight.bold,
                                  ),
                                )),
                          )
                        : SizedBox(),
                    // Padding(
                    //   padding:
                    //       const EdgeInsets.only(top: 16, right: 16, left: 16),
                    //   child: SizedBox(
                    //       width: Get.width,
                    //       height: 40.0,
                    //       child: CustomButton(
                    //         borderRadios: 10.0,
                    //         onTap: () {
                    //           Get.toNamed(Routes.OrderHistoryPage,
                    //               arguments: controller.ordersObject);
                    //         },
                    //         type: 2,
                    //         child: CustomText(
                    //           'Order History',
                    //           color: Colors.black45,
                    //           size: 18.0,
                    //           fontWeight: FontWeight.bold,
                    //         ),
                    //       )),
                    // ),
                    controller.order?.status == null
                        ? SizedBox(
                            height: 25.0,
                          )
                        : controller.order.status == OrderStatus.CANCELED
                            ? SizedBox(
                                height: 25.0,
                              )
                            : Padding(
                                padding: const EdgeInsets.only(
                                    top: 25, right: 16, left: 16, bottom: 25),
                                child: SizedBox(
                                    width: Get.width * .5,
                                    height: 40.0,
                                    child: CustomButton(
                                      color: Colors.red,
                                      borderRadios: 10.0,
                                      onTap: () {
                                        showChangeOrderStatusBottomSheet(
                                            context, controller);
                                      },
                                      type: 2,
                                      child: CustomText(
                                        'Change Order Status',
                                        color: Colors.white,
                                        size: 18.0,
                                        fontWeight: FontWeight.bold,
                                      ),
                                    )),
                              )
                  ],
                );
              }),
        ),
      ),
    );
  }

  Padding buildOrderStatusItem({double height, String label, String value}) {
    return Padding(
      padding: const EdgeInsets.only(top: 16.0, right: 16.0, left: 16.0),
      child: Container(
        height: height,
        width: Get.width,
        decoration: BoxDecoration(
            color: Get.theme.primaryColorLight.withAlpha(150),
            borderRadius: BorderRadius.circular(10.0)),
        child: Padding(
          padding: const EdgeInsets.all(16.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              CustomText(
                label,
                color: Colors.black,
                size: 18.0,
                fontWeight: FontWeight.bold,
              ),
              Flexible(
                child: CustomText(
                  value,
                  color: Get.theme.primaryColorDark,
                  textAlign: TextAlign.center,
                  size: 18.0,
                  fontWeight: FontWeight.w500,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  showChangeOrderStatusBottomSheet(
      BuildContext context, OrderStatusPageController controller) {
    return showModalBottomSheet<String>(
      enableDrag: true,
      context: context,
      builder: (context) => Padding(
        padding: const EdgeInsets.all(15.0),
        child: SizedBox(
          height: 200.0,
          width: Get.width,
          child: Column(
            children: [
              CustomText(
                'Change Order Status',
                color: Colors.black,
                size: 30.0,
                fontWeight: FontWeight.bold,
              ),
              SizedBox(
                height: 10,
              ),
              SizedBox(
                height: 100.0,
                child: GetBuilder<OrderStatusPageController>(builder: (x) {
                  return Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: SizedBox(
                      height: 100,
                      width: Get.width,
                      child: SingleChildScrollView(
                        scrollDirection: Axis.horizontal,
                        child: Row(
                          children:
                              x.orderStatusRadioBuilder(x.orderStatusTypes()),
                        ),
                      ),
                    ),
                  );
                }),
              ),
              // Align(
              //   alignment: Alignment.bottomCenter,
              //   child: SizedBox(
              //       height: 40.0,
              //       width: Get.width * .4,
              //       child: CustomButton(
              //         onTap: () {
              //           controller.changeStatusOrder();

              //           // controller.getOrderByIdApi();
              //         },
              //         type: 2,
              //         borderRadios: 10.0,
              //         child: CustomText(
              //           'Change Status',
              //           color: Colors.black,
              //           size: 18.0,
              //           fontWeight: FontWeight.bold,
              //         ),
              //       )),
              // ),
            ],
          ),
        ),
      ),
    );
  }
}
