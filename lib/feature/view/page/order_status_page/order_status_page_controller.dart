import 'dart:convert';
import 'dart:io';

import 'package:dio/src/response.dart';
import 'package:flutter/material.dart';
import 'package:food_resturant/core/config/api_address.dart';
import 'package:food_resturant/core/config/global_setting.dart';
import 'package:food_resturant/core/config/keys.dart';
import 'package:food_resturant/core/config/routes.dart';
import 'package:food_resturant/feature/depenency_injection.dart';
import 'package:food_resturant/feature/entities/get_order_by_id_entity.dart';
import 'package:food_resturant/feature/entities/get_orders_entity.dart';
import 'package:food_resturant/feature/entities/get_user_by_id_entity.dart';
import 'package:food_resturant/feature/entities/login_entity.dart';
import 'package:food_resturant/feature/entities/public_entity.dart';
import 'package:food_resturant/feature/server/api_helper.dart';
import 'package:food_resturant/feature/server/api_response.dart';
import 'package:food_resturant/feature/server/params/change_order_status_param.dart';
import 'package:food_resturant/feature/server/params/change_status_approval_params.dart';
import 'package:food_resturant/feature/server/params/change_store_food_status_params.dart';
import 'package:food_resturant/feature/server/params/get_order_by_id.dart';
import 'package:food_resturant/feature/server/params/get_user_by_id_params.dart';
import 'package:food_resturant/library/enums/enums.dart';
import 'package:food_resturant/library/platforms/loading_overlay.dart';
import 'package:food_resturant/library/platforms/shared_preferences.dart';
import 'package:food_resturant/library/widgets/button_widget.dart';
import 'package:food_resturant/library/widgets/snackbar.dart';
import 'package:food_resturant/library/widgets/text_widget.dart';
import 'package:get/get.dart';
import 'package:food_resturant/library/extention/enum_extention.dart';
import 'package:food_resturant/library/extention/int_extention.dart';
import 'package:food_resturant/library/extention/money_extention.dart';

class OrderStatusPageController extends GetxController {
  bool isDelivery = false;
  List<OrderStatus> orderStatusRadioTitle = [];
  OrderStatus selectedOrderStatus;
  final overlay = LoadingOverlay.of(Get.context);
  GetOrderByIdObject order;
  String title;

  String firstName;

  String lastName;

  int createAt;

  String phoneNum;

  String orderCategory;

  String address;

  String currentStatus;
  String orderStoreStatus;

  String peymentType;

  String totalPrice;

  String saveing;

  String userRate;

  String description;

  String countDown;

  LoginEntity user;

  List<int> orderStatusTypes() {
    List types = [
      [
        [
          [2, 8, 4],
          [],
          [],
          []
        ],
        [
          [2, 8, 4],
          [],
          [],
          []
        ],
      ],
      [
        [
          [2, 3, 8],
          [],
          [9],
          []
        ],
        [
          [2, 3, 8],
          [],
          [],
          []
        ],
      ],
      [
        [
          [2, 5, 8],
          [],
          [9],
          []
        ],
        [
          [2, 5, 8],
          [],
          [],
          []
        ],
      ],
    ];
    try {
      List<int> selectedType = types[order.orderType.orderTypeToInt - 1]
          [order.peymentType - 1][sl<GlobalSetting>().user.roleId - 3];

      return selectedType;
    } catch (e) {
      return [];
    }
  }

  List<Widget> orderStatusRadioBuilder(List<int> radiosTitles) {
    List<Widget> listRadios = [];
    if (order.status.index >= 2) {
      radiosTitles.remove(8);
    }
    for (var i = 0; i < radiosTitles.length; i++) {
      print(radiosTitles[i]);
      listRadios.add(Padding(
        padding: const EdgeInsets.all(8.0),
        child: CustomButton(
          type: 2,
          onTap: () {
            selectedOrderStatus = radiosTitles[i].enumOrderStatus;
            changeStatusOrder();
            update();
          },
          child:
              CustomText(radiosTitles[i].enumOrderStatus.orderStatusToString),
        ),
      )
          // RadioListTile<OrderStatus>(
          //   activeColor: Get.theme.primaryColor,
          //   title: Text(radiosTitles[i]),
          //   value: OrderStatus.values.firstWhere(
          //       (element) => element.orderStatusToString == radiosTitles[i]),
          //   groupValue: selectedOrderStatus,
          //   onChanged: radioButtonOnChange,
          // ),

          // Row(
          //   children: [
          //     SizedBox(
          //       height: 30.0,
          //       width: 30.0,
          //       child: Radio(
          //         activeColor: Get.theme.primaryColor,
          //         value: i,
          //         groupValue: selectedOrderStatus - 1,
          //         onChanged: radioButtonOnChange,
          //       ),
          //     ),
          //     CustomText(
          //       radiosTitles[i],
          //       color: Colors.black,
          //       size: 18.0,
          //       fontWeight: FontWeight.normal,
          //     )
          //   ],
          // ),
          );
    }

    return listRadios;
  }

  radioButtonOnChange(OrderStatus orderStatus, {bool isInit = false}) {
    if (isInit) {
      selectedOrderStatus = orderStatus;
      update();
    } else {
      if (user?.loginObject?.roleId == UserRole.STORE_OWNER.userRoleToInt) {
        switch (ordersObject.orderType) {
          case OrderTypes.ALL:
            // TODO: Handle this case.
            break;

          case OrderTypes.DELIVERY:
            if (ordersObject.status == OrderStatus.PENDING &&
                (orderStatus == OrderStatus.COOKING ||
                    orderStatus == OrderStatus.CANCELED ||
                    orderStatus == OrderStatus.PENDING)) {
              selectedOrderStatus = orderStatus;
              update();
            } else {
              showSnackBar(text: 'you are not able to change status');
            }
            break;

          case OrderTypes.SERVEIN:
            if (ordersObject.status == OrderStatus.PENDING &&
                (orderStatus == OrderStatus.COOKING ||
                    orderStatus == OrderStatus.PENDING ||
                    orderStatus == OrderStatus.CANCELED)) {
              selectedOrderStatus = orderStatus;
              update();

              /////////////////////////////////////////////TACKOUT
            } else
              showSnackBar(text: 'you are not able to change status');
            break;
          case OrderTypes.TAKEOUT:
            if (ordersObject.status == OrderStatus.PENDING &&
                (orderStatus == OrderStatus.COOKING ||
                    orderStatus == OrderStatus.CANCELED ||
                    orderStatus == OrderStatus.PENDING)) {
              selectedOrderStatus = orderStatus;
              update();
            } else if (ordersObject.status == OrderStatus.COOKING &&
                (orderStatus == OrderStatus.PICKEDUP ||
                    orderStatus == OrderStatus.COOKING)) {
              selectedOrderStatus = orderStatus;
              update();
            } else {
              showSnackBar(text: 'you are not able to change status');
            }
            break;
        }

        //////////////////////////////////////////////////////////////TACKOUT
      } else if (user?.loginObject?.roleId == UserRole.WAITER.userRoleToInt) {
        if (ordersObject.orderType == OrderTypes.SERVEIN) {
          if (ordersObject.status == OrderStatus.COOKING &&
              (orderStatus == OrderStatus.SERVED ||
                  orderStatus == OrderStatus.COOKING)) {
            selectedOrderStatus = orderStatus;
            update();
          } else {
            showSnackBar(text: 'you are not able to change status');
          }
        }
      } else if (user?.loginObject?.roleId == UserRole.CASHIER.userRoleToInt) {
        if (ordersObject.orderType == OrderTypes.TAKEOUT) {
          if (ordersObject.status == OrderStatus.PICKEDUP &&
              ordersObject?.peymentType == 1 &&
              (orderStatus == OrderStatus.PAID ||
                  orderStatus == OrderStatus.PICKEDUP)) {
            selectedOrderStatus = orderStatus;
            update();
          } else {
            showSnackBar(text: 'you are not able to change status');
          }
        } else if (ordersObject.orderType == OrderTypes.SERVEIN) {
          if (ordersObject.status == OrderStatus.SERVED &&
              ordersObject?.peymentType == 1 &&
              (orderStatus == OrderStatus.PAID ||
                  orderStatus == OrderStatus.SERVED)) {
            selectedOrderStatus = orderStatus;
            update();
          } else {
            showSnackBar(text: 'you are not able to change status');
          }
        }
      }
    }
  }

  GetOrdersObject ordersObject;

  @override
  void onInit() {
    ordersObject = Get.arguments;
    if (ordersObject.orderType == OrderTypes.DELIVERY) {
      orderStatusRadioTitle = OrderStatus.values
          .where((element) =>
              element == OrderStatus.PENDING ||
              element == OrderStatus.COOKING ||
              element == OrderStatus.INDELIVERY ||
              element == OrderStatus.DELIVERED ||
              element == OrderStatus.COMPLETED ||
              element == OrderStatus.CANCELED)
          .toList();
    } else if (ordersObject.orderType == OrderTypes.SERVEIN) {
      orderStatusRadioTitle = OrderStatus.values
          .where((element) =>
              element == OrderStatus.PENDING ||
              element == OrderStatus.COOKING ||
              element == OrderStatus.SERVED ||
              element == OrderStatus.COMPLETED ||
              element == OrderStatus.CANCELED)
          .toList();
    } else if (ordersObject.orderType == OrderTypes.TAKEOUT) {
      orderStatusRadioTitle = OrderStatus.values
          .where((element) =>
              element == OrderStatus.PENDING ||
              element == OrderStatus.COOKING ||
              element == OrderStatus.PICKEDUP ||
              element == OrderStatus.COMPLETED ||
              element == OrderStatus.CANCELED)
          .toList();
    }

    if (ordersObject.peymentType == 1) {
      orderStatusRadioTitle.insert(3, OrderStatus.PAID);
    }

    // orderStatusRadioTitle = OrderStatus.values.where((element) => element != OrderStatus.ALL).toList();

    if (sl<DataStorage>().getData(key: USER) != null) {
      user = LoginEntity.fromJson(
          json.decode(sl<DataStorage>().getData(key: USER)));
      sl<GlobalSetting>().user = user?.loginObject;
    }

    // if(user?.loginObject?.roleId == UserRole.STORE_OWNER){
    //
    // }

    // selectedOrderStatus = getOrdersObject?.status?.orderStatusToInt;
    super.onInit();
  }

  Future<void> changeOrderChashApprocalStatus() async {
    var getOrderByIdParams =
        ChangeStatusApprovalParams(orderId: ordersObject?.id, cashStatus: 2);

    final response = await overlay.during(sl<ApiHelper>().post(
        ApiAddress.ChangeOrderApprovalStatus, getOrderByIdParams.toJson()));
    var result = ApiResponse.returnResponse(
        response, PublicEntity.fromJson(response.data));
    if (result.data.status) {
      getOrderByIdApi();
    } else {
      showSnackBar(text: result?.data?.message ?? '');
    }
  }

  @override
  void onReady() {
    if (ordersObject != null) getOrderByIdApi();
  }

  getOrderByIdApi() async {
    // GetUserByIdEntity user = await getUserById();
    print(ordersObject?.id);
    var getOrderByIdParams = GetOrderByIdParams(orderId: ordersObject?.id);

    final response = await overlay.during(sl<ApiHelper>()
        .getWithParam(ApiAddress.GET_ORDER_BY_ID, getOrderByIdParams.toJson()));
    var result = ApiResponse.returnResponse(
        response, GetOrderByIdEntity.fromJson(response.data));
    if (result.status == ApiStatus.COMPLETED && result?.data?.status) {
      var orderById = result?.data?.getOrderByIdObject;
      printError(info: json.encode(orderById.toJson()));
      order = orderById;
      title = orderById?.status?.orderStatusToString;
      firstName = orderById?.user?.name;
      lastName = orderById?.user?.familyName;
      createAt = orderById?.createdAt;
      phoneNum = user?.loginObject?.mobile ?? '';
      orderCategory = orderById?.orderType?.orderTypeToString;
      address = orderById?.userAddress?.text ?? '';
      //  = orderById?.user?.name;
      currentStatus = orderById?.status?.orderStatusToString;
      orderStoreStatus = orderById.foods
          .where((element) => element.store.id == user.loginObject.store.id)
          .toList()[0]
          .storeStatus
          ?.orderStatusToString;
      peymentType = orderById?.peymentType?.peymentTypeToString;
      // saveing = orderById?.s;
      totalPrice = orderById?.totalPrice?.moneyFormatter ?? '';
      // userRate = orderById?.foods[].;
      description = orderById?.description;
      selectedOrderStatus = orderById?.status;
      radioButtonOnChange(selectedOrderStatus, isInit: true);
      if (orderById.status == OrderStatus.DELIVERED) {
        int allCookingTime;
        orderById?.foods?.forEach((e) => allCookingTime = e.cookingTime);
        if (allCookingTime != null)
          countDown = (allCookingTime + 15).toString();
      }

      update();
    } else {
      showSnackBar(text: result?.data?.message ?? '');
    }
  }

  Future<GetUserByIdEntity> getUserById() async {
    var getUserById = GetUserByIdParams(userId: ordersObject?.user?.id);

    final responseUser = await overlay.during(sl<ApiHelper>()
        .getWithParam(ApiAddress.GET_USERS_BY_ID, getUserById.toJson()));
    var resultUser = ApiResponse.returnResponse(
        responseUser, GetUserByIdEntity.fromJson(responseUser.data));
    if (resultUser.status == ApiStatus.COMPLETED && resultUser?.data?.status)
      return resultUser?.data;
  }

  changeStatusOrder() async {
    //todo change order status
    List<int> theStatusesThatOnlyStoreCanChange = [2, 5, 8, 3, 4];
    var changeOrder = ChangeOrderStatusParam(
        orderId: ordersObject?.id ?? '',
        status: selectedOrderStatus.orderStatusToInt);

    var changeFoodOrder = ChangeFoodOrderStatusParam(
        orderId: ordersObject?.id ?? '',
        storeId: user.loginObject.store.id,
        orderStoreStatus: selectedOrderStatus.orderStatusToInt);
    var response;
    if (sl<GlobalSetting>().user.roleId.enumUserRole == UserRole.STORE_OWNER &&
        theStatusesThatOnlyStoreCanChange
            .contains(selectedOrderStatus.orderStatusToInt)) {
      response = await changeFoodOrderStatus(changeFoodOrder);
    } else {
      response = await changeOrderStatus(changeOrder);
    }
    var result = ApiResponse.returnResponse(
        response, PublicEntity.fromJson(response.data));
    if (result.status == ApiStatus.COMPLETED && result.data.status) {
      Get.back();
      getOrderByIdApi();
    }
  }

  Future<dynamic> changeOrderStatus(ChangeOrderStatusParam changeOrder) async {
    return await overlay.during(sl<ApiHelper>()
        .post(ApiAddress.CHANGE_ORDER_STATUS, changeOrder.toJson()));
  }

  Future<dynamic> changeFoodOrderStatus(
      ChangeFoodOrderStatusParam changeFoodOrder) async {
    return await overlay.during(sl<ApiHelper>()
        .post(ApiAddress.CHANGE_FOOD_ORDER_STATUS, changeFoodOrder.toJson()));
  }
}
