import 'package:flutter/material.dart';
import 'package:food_resturant/core/config/routes.dart';
import 'package:food_resturant/feature/entities/get_orders_by_table_entity.dart';
import 'package:food_resturant/feature/entities/get_orders_entity.dart';
import 'package:food_resturant/feature/view/page/orders_by_Table_page/orders_by_table_page_controller.dart';
import 'package:food_resturant/library/extention/enum_extention.dart';
import 'package:food_resturant/library/extention/enum_extention.dart';
import 'package:food_resturant/feature/view/widget/app_bar.dart';
import 'package:food_resturant/feature/view/widget/box_shadow_widget.dart';
import 'package:food_resturant/library/widgets/button_widget.dart';
import 'package:food_resturant/library/widgets/cached_network_image_widget.dart';
import 'package:food_resturant/library/widgets/custom_empty_widget.dart';
import 'package:food_resturant/library/widgets/text_widget.dart';
import 'package:get/get.dart';
import 'package:food_resturant/library/extention/money_extention.dart';

class OrdersByTablePage extends StatelessWidget {
  final controller = Get.put(OrdersByTablePageController());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // floatingActionButton: changeStatusButton(context),
      // floatingActionButtonLocation:
      // FloatingActionButtonLocation.centerFloat,
      appBar: AppBar(
        iconTheme: IconThemeData(opacity: 0.0),
        toolbarHeight: 110.0,
        backgroundColor: Get.theme.scaffoldBackgroundColor,
        elevation: 0.0,
        flexibleSpace: BuildAppBarWidget(
          title: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              CustomText(
                'Orders',
                size: 25.0,
                fontWeight: FontWeight.bold,
              ),
              CustomText(
                'View All Serve in Orders',
                size: 19.0,
                fontWeight: FontWeight.normal,
              )
            ],
          ),
        ),
      ),
      body: RefreshIndicator(
        backgroundColor: Get.theme.primaryColorDark,
        onRefresh: () {
          return controller.getOrdersByTableApi();
        },
        child: Obx(() => controller?.order?.length == 0
            ? customEmptyWidget()
            : ListView.builder(
                itemCount: controller.order.length ?? 0,
                itemBuilder: (context, index) =>
                    orderItem(controller.order[index]),
              )),
      ),
    );
  }

  GestureDetector orderItem(GetOrdersByTableObject order) {
    return GestureDetector(
      onTap: () {
        Get.toNamed(Routes.OrderStatusPage,
            arguments: GetOrdersObject(
                id: order?.id,
                user: order?.user,
                foods: order?.foods,
                status: order?.status,
                description: order?.description,
                orderType: order?.orderType));
      },
      child: Padding(
        padding: const EdgeInsets.all(5.0),
        child: Container(
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(20.0),
              boxShadow: [buildBoxShadow()],
              color: Get.theme.accentColor),
          child: Row(
            children: [
              Container(
                height: 140.0,
                width: 40.0,
                decoration: BoxDecoration(
                    color: Color(0xff0065D9),
                    borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(20.0),
                        bottomLeft: Radius.circular(20.0))),
                child: Center(
                  child: RotatedBox(
                    quarterTurns: -45,
                    child: CustomText(
                      order.status.orderStatusToString,
                      size: 20.0,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
              ),
              // Container(
              //    height: 140.0,
              //    width: 40.0,
              //    decoration: BoxDecoration(
              //        color: Colors.green,
              //        borderRadius: BorderRadius.only(
              //            topLeft: Radius.circular(20.0),
              //            bottomLeft: Radius.circular(20.0))),
              //    child: Center(
              //      child: RotatedBox(
              //        quarterTurns: -45,
              //        child: CustomText(
              //          'Cooking',
              //          size: 20.0,
              //          fontWeight: FontWeight.bold,
              //        ),
              //      ),
              //    ),
              //  ),
              Column(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  SizedBox(
                    width: Get.width - 84.0,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Row(
                          children: [
                            Padding(
                              padding: const EdgeInsets.only(
                                  top: 8.0, left: 4.0, right: 4.0),
                              child: BuildCachedImageWidget(
                                imageUrl: order?.user?.profileImage ?? '',
                                height: 70.0,
                                width: 70.0,
                                borderRadius: 60.0,
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(top: 5.0),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  CustomText(
                                    order?.user?.name ??
                                        '' + order?.user?.familyName ??
                                        '',
                                    color: Colors.black,
                                    size: 18.0,
                                    fontWeight: FontWeight.bold,
                                  ),
                                  CustomText(
                                    order?.user?.userName ?? '',
                                    color: Colors.black,
                                    size: 18.0,
                                    fontWeight: FontWeight.normal,
                                  )
                                ],
                              ),
                            ),
                          ],
                        ),
                        Padding(
                          padding: const EdgeInsets.only(top: 5.0),
                          child: Icon(
                            Icons.announcement,
                            color: Colors.red,
                          ),
                        )
                      ],
                    ),
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      SizedBox(
                        width: Get.width - 120,
                        child: Divider(
                          thickness: 1,
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(
                            top: 6.0, right: 12.0, left: 12.0),
                        child: SizedBox(
                          width: Get.width - 84.0,
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              CustomText(
                                order.orderType.orderTypeToString ?? '',
                                color: Colors.black,
                                size: 18.0,
                                fontWeight: FontWeight.bold,
                              ),
                              CustomText(
                                'Total : ${order.totalPrice.moneyFormatter ?? ''}',
                                color: Colors.black,
                                size: 18.0,
                                fontWeight: FontWeight.bold,
                              ),
                            ],
                          ),
                        ),
                      )
                    ],
                  )
                ],
              )
            ],
          ),
        ),
      ),
    );
  }

  changeStatusButton(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 15.0),
      child: SizedBox(
          width: Get.width * 0.5,
          height: 40.0,
          child: CustomButton(
            borderRadios: 10.0,
            onTap: () {
              showChangeTableStatusBottomSheet(context);
            },
            type: 2,
            color: Colors.red,
            splashColor: Colors.redAccent,
            child: CustomText(
              'Change Table Status',
              size: 18.0,
              fontWeight: FontWeight.bold,
            ),
          )),
    );
  }

  // showTableInformation(BuildContext context) {
  //   return showModalBottomSheet<String>(
  //     context: context,
  //     builder: (context) => Container(
  //       child: Padding(
  //         padding: const EdgeInsets.only(top: 30.0),
  //         child: Column(
  //           children: [
  //             CustomText(
  //               'Table Information',
  //               color: Colors.black,
  //               size: 25.0,
  //               fontWeight: FontWeight.bold,
  //             ),
  //             SizedBox(
  //               height: 10.0,
  //             ),
  //             Container(
  //                 height: 200,
  //                 width: 200,
  //                 child:BuildCachedImageWidget(
  //                   imageUrl: controller?.arguments?.qrCodeImage?.location??'',
  //                   loadingWidth: 10,
  //                   height: 70.0,
  //                   width: 70.0,
  //                 )
  //
  //
  //
  //
  //             ),
  //
  //
  //             // QrImage(
  //             //   data: tableObject.qrCode??'',
  //             //   version: QrVersions.auto,
  //             //   size: Get.width * 0.6,
  //             // ),
  //             SizedBox(
  //               height: 20.0,
  //             ),
  //             SizedBox(
  //                 width: Get.width * 0.5,
  //                 height: 40.0,
  //                 child: CustomButton(
  //                   borderRadios: 10.0,
  //                   onTap: () {
  //                     showChangeTableStatusBottomSheet(
  //                         context);
  //                   },
  //                   type: 2,
  //                   color: Colors.red,
  //                   splashColor: Colors.redAccent,
  //                   child: CustomText(
  //                     'Change Table Status',
  //                     size: 18.0,
  //                     fontWeight: FontWeight.bold,
  //                   ),
  //                 ))
  //           ],
  //         ),
  //       ),
  //     ),
  //   );
  // }

  showChangeTableStatusBottomSheet(BuildContext context) {
    return showModalBottomSheet<String>(
      context: context,
      builder: (context) => Container(
        child: Padding(
          padding: const EdgeInsets.only(top: 30.0),
          child: Column(
            children: [
              CustomText(
                'Change Table Status',
                color: Colors.black,
                size: 25.0,
                fontWeight: FontWeight.bold,
              ),
              GetBuilder<OrdersByTablePageController>(
                  init: OrdersByTablePageController(),
                  builder: (x) {
                    return Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Row(
                        children: [
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: controller
                                .tableStatusRadioWidgetBuilder(context),
                          ),
                        ],
                      ),
                    );
                  }),
              SizedBox(
                height: 50.0,
              ),
              SizedBox(
                  width: Get.width * 0.4,
                  height: 40.0,
                  child: CustomButton(
                    borderRadios: 10.0,
                    onTap: () {
                      controller.changeStatus(controller.arguments);
                    },
                    type: 2,
                    child: CustomText(
                      'Change Status',
                      color: Colors.black,
                      size: 18.0,
                      fontWeight: FontWeight.bold,
                    ),
                  ))
            ],
          ),
        ),
      ),
    );
  }
}
