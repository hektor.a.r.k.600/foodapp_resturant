import 'package:flutter/material.dart';
import 'package:food_resturant/core/config/api_address.dart';
import 'package:food_resturant/core/config/global_setting.dart';
import 'package:food_resturant/feature/depenency_injection.dart';
import 'package:food_resturant/feature/entities/get_orders_by_table_entity.dart';
import 'package:food_resturant/feature/entities/get_orders_entity.dart';
import 'package:food_resturant/feature/entities/get_tables_entity.dart';
import 'package:food_resturant/feature/entities/public_entity.dart';
import 'package:food_resturant/feature/server/api_helper.dart';
import 'package:food_resturant/feature/server/api_response.dart';
import 'package:food_resturant/feature/server/params/change_table_status_params.dart';
import 'package:food_resturant/feature/server/params/get_order_by_table_params.dart';
import 'package:food_resturant/feature/server/params/get_orders_params.dart';
import 'package:food_resturant/library/enums/enums.dart';
import 'package:food_resturant/library/extention/enum_extention.dart';
import 'package:food_resturant/library/extention/list_extention.dart';
import 'package:food_resturant/library/platforms/loading_overlay.dart';
import 'package:food_resturant/library/widgets/snackbar.dart';
import 'package:food_resturant/library/widgets/text_widget.dart';
import 'package:get/get.dart';

class OrdersByTablePageController extends GetxController {
  final overlay = LoadingOverlay.of(Get.context);

  TableObject arguments;
  var tableStatusId;

  RxList<GetOrdersByTableObject> order = <GetOrdersByTableObject>[].obs;

  List<Widget> tableStatusRadioWidgetBuilder(BuildContext context) {
    List<Widget> tableStatusRadioWidgets = List<Widget>();
    for (TableStatus item in TableStatus.values) {
      tableStatusRadioWidgets.add(tableStatusRadioWidget(context, item));
    }
    return tableStatusRadioWidgets;
  }

  Widget tableStatusRadioWidget(
      BuildContext context, TableStatus tablesStatus) {
    return SizedBox(
        height: 30.0,
        width: MediaQuery.of(context).size.width * 0.5,
        child: Row(
          children: [
            Radio(
              groupValue: tableStatusId,
              onChanged: onTableStatusRadioChange,
              value: tablesStatus.index,
              activeColor: Get.theme.primaryColor,
            ),
            CustomText(
              tablesStatus.stringTableStatus,
              color: Colors.black,
              size: 18.0,
              fontWeight: FontWeight.bold,
            ),
          ],
        ));
  }

  void onTableStatusRadioChange(id) {
    tableStatusId = id;
    print(tableStatusId);
    update();
  }

  @override
  void onInit() {
    arguments = Get.arguments;
    super.onInit();
  }

  @override
  void onReady() {
    getOrdersByTableApi();
  }

  getOrdersByTableApi() async {
    var orderParams = GetOrderByTableParams(
        // type: OrderTypes.SERVEIN,
        // status: OrderStatus.COOKING,
        tableId: arguments?.id);

    final response = await overlay.during(sl<ApiHelper>()
        .getWithParam(ApiAddress.GET_ORDERS_BY_TABLE, orderParams.toJson()));
    var result = ApiResponse.returnResponse(
        response, GetOrdersByTableEntity.fromJson(response.data));
    if (result.status == ApiStatus.COMPLETED && result?.data?.status) {
      order.value = result.data?.getOrdersByTableObject;
      // showSnackBar(text: result?.data?.message ?? '');

    } else {
      showSnackBar(text: result?.data?.message ?? '');
    }
  }

  changeStatus(TableObject tablesModel) async {
    var tablesParams = ChangeTableStatusParams(
        tableId: tablesModel?.id, status: (tableStatusId + 1) ?? 1);

    final response = await overlay.during(sl<ApiHelper>()
        .post(ApiAddress.CHANGE_TABLE_STATUS, tablesParams.toJson(), true));
    var result = ApiResponse.returnResponse(
        response, PublicEntity.fromJson(response.data));
    if (result.status == ApiStatus.COMPLETED && result?.data?.status) {
      Get.back();
    } else {
      showSnackBar(text: result?.data?.message ?? '');
    }
  }
}
