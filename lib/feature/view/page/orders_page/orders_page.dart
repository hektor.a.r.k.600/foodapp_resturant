import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:food_resturant/core/config/config.dart';
import 'package:food_resturant/core/config/routes.dart';
import 'package:food_resturant/feature/entities/get_orders_entity.dart';
import 'package:food_resturant/library/enums/enums.dart';
import 'package:food_resturant/library/extention/enum_extention.dart';
import 'package:food_resturant/feature/view/page/orders_page/orders_page_controller.dart';
import 'package:food_resturant/feature/view/page/profile_page/profile_page_controller.dart';
import 'package:food_resturant/feature/view/page/stocks_page/stocks_page_controller.dart';
import 'package:food_resturant/feature/view/page/tables_page/tables_page_controller.dart';
import 'package:food_resturant/feature/view/widget/app_bar.dart';
import 'package:food_resturant/feature/view/widget/box_shadow_widget.dart';
import 'package:food_resturant/library/widgets/button_widget.dart';
import 'package:food_resturant/library/widgets/cached_network_image_widget.dart';
import 'package:food_resturant/library/widgets/custom_drop_list/custom_drop_down_list.dart';
import 'package:food_resturant/library/widgets/text_widget.dart';
import 'package:get/get.dart';
import 'package:qr_flutter/qr_flutter.dart';
import 'package:food_resturant/library/extention/money_extention.dart';
import 'package:food_resturant/library/extention/enum_extention.dart';
import 'package:intl/intl.dart';
import '../../../../library/widgets/text_field_widget.dart';

class OrdersPage extends StatelessWidget {
  final controller = Get.put(OrdersPageController());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          iconTheme: IconThemeData(opacity: 0.0),
          toolbarHeight: 190.0,
          backgroundColor: Get.theme.scaffoldBackgroundColor,
          elevation: 0.0,
          bottom: TabBar(
              isScrollable: false,
              controller: controller.tabController,
              onTap: (index) {
                switch (index) {
                  case 0:
                    controller.getOrdersApi(
                        orderTypes: OrderTypes.ALL,
                        orderStatus: controller.selectedOrderStatus);
                    break;
                  case 1:
                    controller.getOrdersApi(
                        orderTypes: OrderTypes.DELIVERY,
                        orderStatus: controller.selectedOrderStatus);

                    break;
                  case 2:
                    controller.getOrdersApi(
                        orderTypes: OrderTypes.SERVEIN,
                        orderStatus: controller.selectedOrderStatus);

                    break;
                  case 3:
                    controller.getOrdersApi(
                        orderTypes: OrderTypes.TAKEOUT,
                        orderStatus: controller.selectedOrderStatus);

                    break;
                }
              },
              indicatorColor: Get.theme.primaryColor,
              tabs: controller.postTabBuilder()),
          flexibleSpace: BuildAppBarWidget(
            title: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                CustomText(
                  'All Orders',
                  size: 25.0,
                  fontWeight: FontWeight.bold,
                ),
                CustomText(
                  'View All Orders',
                  size: 19.0,
                  fontWeight: FontWeight.normal,
                )
              ],
            ),
          ),
        ),
        body: Obx(
          () => TabBarView(
            physics: NeverScrollableScrollPhysics(),
            controller: controller.tabController,
            children: [
              tabViewBuilder(context, OrderTypes.ALL),
              tabViewBuilder(context, OrderTypes.DELIVERY),
              tabViewBuilder(context, OrderTypes.SERVEIN),
              tabViewBuilder(context, OrderTypes.TAKEOUT),
            ],
          ),
        ));
  }

  RefreshIndicator tabViewBuilder(BuildContext context, OrderTypes orderTypes) {
    return RefreshIndicator(
      backgroundColor: Get.theme.primaryColorDark,
      onRefresh: () {
        return controller.getOrdersApi(
            orderTypes: orderTypes,
            orderStatus: controller.selectedOrderStatus);
      },
      child: ListView(
        children: [
          Padding(
            padding: const EdgeInsets.only(top: 8.0, left: 8.0, right: 8.0),
            child: Row(
              children: [
                Expanded(
                  child: SizedBox(
                      height: 60,
                      child: CustomTextField(
                        hint: "Search by Mobile and Serial Nomber",
                        fontSize: 20,
                        onChange: controller.filter,
                      )),
                ),
                SizedBox(
                  width: 8.0,
                ),
                Obx(
                  () => SizedBox(
                      height: 40,
                      child: CustomButton(
                        onTap: controller.searchOrders,
                        color: controller.filter.isEmpty
                            ? Colors.grey
                            : Get.theme.primaryColor,
                        child: CustomText(
                          "Search",
                          size: 20,
                        ),
                      )),
                )
              ],
            ),
          ),
          CustomDropDownList(
            padding: EdgeInsets.all(8),
            context: context,
            dropdownColor: Get.theme.primaryColorLight.withAlpha(150),
            onChange: (c) {
              controller.selectedOrderStatus = OrderStatus?.values
                  ?.firstWhere((element) => element?.orderStatusToString == c);

              controller.getOrdersApi(
                  orderTypes: orderTypes,
                  orderStatus: controller.selectedOrderStatus);
            },
            valueItem: controller.selectedOrderStatus.orderStatusToString,
            items: OrderStatus?.values
                ?.map((e) => e.orderStatusToString)
                ?.toList(),
          ),
          ListView.builder(
            shrinkWrap: true,
            physics: NeverScrollableScrollPhysics(),
            itemCount: controller.order.length ?? 0,
            itemBuilder: (context, index) => orderItem(controller.order[index]),
          )
        ],
      ),
    );
  }

  GestureDetector orderItem(GetOrdersObject order) {
    return GestureDetector(
      onTap: () {
        Get.offNamed(Routes.OrderStatusPage, arguments: order);
      },
      child: Padding(
        padding: const EdgeInsets.all(5.0),
        child: Container(
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(20.0),
              boxShadow: [buildBoxShadow()],
              color: Get.theme.accentColor),
          child: Row(
            children: [
              Container(
                height: 150.0,
                width: 40.0,
                decoration: BoxDecoration(
                    color: Color(0xff0065D9),
                    borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(20.0),
                        bottomLeft: Radius.circular(20.0))),
                child: Center(
                  child: RotatedBox(
                    quarterTurns: -45,
                    child: CustomText(
                      order.status.orderStatusToString,
                      size: 20.0,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
              ),
              // Container(
              //    height: 140.0,
              //    width: 40.0,
              //    decoration: BoxDecoration(
              //        color: Colors.green,
              //        borderRadius: BorderRadius.only(
              //            topLeft: Radius.circular(20.0),
              //            bottomLeft: Radius.circular(20.0))),
              //    child: Center(
              //      child: RotatedBox(
              //        quarterTurns: -45,
              //        child: CustomText(
              //          'Cooking',
              //          size: 20.0,
              //          fontWeight: FontWeight.bold,
              //        ),
              //      ),
              //    ),
              //  ),
              Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.end,
                children: [
                  SizedBox(
                    width: Get.width * 0.8,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        order.chashAprovalStatus ==
                                ChashAprovalStatus.Pending.index
                            ? Padding(
                                padding: const EdgeInsets.only(
                                    right: 12.0, left: 12.0),
                                child: CustomText(
                                  'Need to Approval',
                                  color: Colors.black,
                                  size: 16.0,
                                  fontWeight: FontWeight.bold,
                                ),
                              )
                            : SizedBox(),
                        Padding(
                          padding:
                              const EdgeInsets.only(right: 12.0, left: 12.0),
                          child: CustomText(
                            '#${order.serialNo ?? ''}',
                            color: Colors.black,
                            size: 18.0,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ],
                    ),
                  ),
                  SizedBox(
                    width: Get.width - 84.0,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Row(
                          children: [
                            Padding(
                              padding: const EdgeInsets.only(
                                  top: 8.0, left: 4.0, right: 4.0),
                              child: BuildCachedImageWidget(
                                imageUrl: order?.user?.profileImage ?? '',
                                height: 70.0,
                                width: 70.0,
                                borderRadius: 60.0,
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(top: 5.0),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  CustomText(
                                    order?.user?.name ?? '',
                                    color: Colors.black,
                                    size: 18.0,
                                    fontWeight: FontWeight.bold,
                                  ),
                                  CustomText(
                                    order?.user?.userName ?? '',
                                    color: Colors.black,
                                    size: 18.0,
                                    fontWeight: FontWeight.normal,
                                  )
                                ],
                              ),
                            ),
                          ],
                        ),
                        // Padding(
                        //   padding: const EdgeInsets.only(top: 5.0),
                        //   child: Icon(
                        //     Icons.announcement,
                        //     color: Colors.red,
                        //   ),
                        // )
                      ],
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(right: 12.0, left: 12.0),
                    child: CustomText(
                      DateFormat("y/M/d").add_jm().format(
                          DateTime.fromMillisecondsSinceEpoch(
                              order.createdAt * 1000)),
                      color: Colors.black,
                      size: 18.0,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      SizedBox(
                        width: Get.width - 120,
                        child: Divider(
                          thickness: 1,
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(
                            top: 6.0, right: 12.0, left: 12.0),
                        child: SizedBox(
                          width: Get.width - 84.0,
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              CustomText(
                                order.orderType.orderTypeToString ?? '',
                                color: Colors.black,
                                size: 18.0,
                                fontWeight: FontWeight.bold,
                              ),
                              CustomText(
                                'Total : ${order.totalPrice.moneyFormatter ?? ''}',
                                color: Colors.black,
                                size: 18.0,
                                fontWeight: FontWeight.bold,
                              ),
                            ],
                          ),
                        ),
                      )
                    ],
                  )
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}
