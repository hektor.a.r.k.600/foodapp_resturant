import 'package:flutter/material.dart';
import 'package:food_resturant/core/config/api_address.dart';
import 'package:food_resturant/core/config/global_setting.dart';
import 'package:food_resturant/feature/depenency_injection.dart';
import 'package:food_resturant/feature/entities/get_orders_entity.dart';
import 'package:food_resturant/feature/entities/login_entity.dart';
import 'package:food_resturant/feature/server/api_helper.dart';
import 'package:food_resturant/feature/server/api_response.dart';
import 'package:food_resturant/feature/server/params/get_orders_params.dart';
import 'package:food_resturant/feature/server/params/search_orders_params.dart';
import 'package:food_resturant/library/enums/enums.dart';
import 'package:food_resturant/library/extention/enum_extention.dart';
import 'package:food_resturant/library/extention/list_extention.dart';
import 'package:food_resturant/library/platforms/loading_overlay.dart';
import 'package:food_resturant/library/widgets/snackbar.dart';
import 'package:food_resturant/library/widgets/text_widget.dart';
import 'package:get/get.dart';

class OrdersPageController extends GetxController
    with SingleGetTickerProviderMixin {
  List<String> tabsTitle = List<String>(4);
  final overlay = LoadingOverlay.of(Get.context);

  TabController tabController;
  TabController cashTabController;
  LoginObject user;
  ScrollController controller;
  RxString filter = "".obs;
  RxList<GetOrdersObject> order = <GetOrdersObject>[].obs;
  // RxList<GetOrdersObject> deliveryOrder = <GetOrdersObject>[].obs;
  // RxList<GetOrdersObject> servinOrder = <GetOrdersObject>[].obs;
  // RxList<GetOrdersObject> takeoutOrder = <GetOrdersObject>[].obs;

  OrderStatus selectedOrderStatus;

  addItemsToTabs() {
    tabsTitle[0] = OrderTypes.ALL.orderTypeToString;
    tabsTitle[1] = OrderTypes.DELIVERY.orderTypeToString;
    tabsTitle[2] = OrderTypes.SERVEIN.orderTypeToString;
    tabsTitle[3] = OrderTypes.TAKEOUT.orderTypeToString;
  }

  List<Widget> postTabBuilder() {
    List<Widget> listTabs = [];
    tabsTitle.forEach((title) {
      listTabs.add(
        Padding(
          padding: const EdgeInsets.only(bottom: 12.0, top: 11.0),
          child: CustomText(
            title,
            color: Colors.black,
            size: 20.0,
            fontWeight: FontWeight.normal,
          ),
        ),
      );
    });
    return listTabs;
  }

  @override
  void onInit() {
    user = sl<GlobalSetting>().user;
    cashTabController = TabController(length: 2, vsync: this);
    tabController = TabController(length: 4, vsync: this);
    selectedOrderStatus = OrderStatus.ALL;
    addItemsToTabs();

    controller = new ScrollController()..addListener(_scrollListener);

    super.onInit();
  }

  @override
  void dispose() {
    super.dispose();
    controller.dispose();
  }

  @override
  void onReady() {
    getOrdersApi(orderTypes: OrderTypes.ALL, orderStatus: OrderStatus.ALL);
  }

  getOrdersApi(
      {OrderStatus orderStatus = OrderStatus.ALL,
      OrderTypes orderTypes = OrderTypes.ALL}) async {
    // if (sl<GlobalSetting>().user == null) return;
    // var user = sl<GlobalSetting>().user;
    // var orderSParams = GetOrdersParams(storeId: user.store.id, userId: user.id);
    var orderSParams = GetOrdersParams(
      status: orderStatus,
      orderType: orderTypes,
      pageNo: -1,
      chashAprovalStatus: user.roleId == UserRole.STORE_OWNER.index ||
              user.roleId == UserRole.WAITER.index
          ? 2
          : -1,
      storeId: user?.store?.id,
    );

    final response = await overlay.during(sl<ApiHelper>()
        .getWithParam(ApiAddress.GET_STORE_ORDERS, orderSParams.toJson()));
    var result = ApiResponse.returnResponse(
        response, GetOrdersEntity.fromJson(response.data));
    if (result.status == ApiStatus.COMPLETED && result.data.status) {
      // showSnackBar(text: result?.data?.message ?? '');

      order.value = result.data.getOrdersObject;
      // deliveryOrder.value = result.data.getOrdersObject
      //     .where((arg1) => arg1.orderType == OrderTypes.DELIVERY)
      //     .toList();
      // // filteredList.value = allOrder.value;
      //
      // servinOrder.value = result.data.getOrdersObject
      //     .where((arg1) => arg1.orderType == OrderTypes.SERVEIN)
      //     .toList();
      //
      // takeoutOrder.value = result.data.getOrdersObject
      //     .where((arg1) => arg1.orderType == OrderTypes.TAKEOUT)
      //     .toList();
    } else {
      showSnackBar(text: result?.data?.message ?? '');
    }
  }

  searchOrders() async {
    if (filter.value.isNotEmpty) {
      // if (sl<GlobalSetting>().user == null) return;
      // var user = sl<GlobalSetting>().user;
      // var orderSParams = GetOrdersParams(storeId: user.store.id, userId: user.id);
      var orderSParams = SearchOrdersParams(filter: filter.value);

      final response = await overlay.during(sl<ApiHelper>()
          .getWithParam(ApiAddress.SEARCH_ORDERS, orderSParams.toJson()));
      var result = ApiResponse.returnResponse(
          response, GetOrdersEntity.fromJson(response.data));
      if (result.status == ApiStatus.COMPLETED && result.data.status) {
        // showSnackBar(text: result?.data?.message ?? '');

        order.value = result.data?.getOrdersObject;
        // deliveryOrder.value = result.data.getOrdersObject
        //     .where((arg1) => arg1.orderType == OrderTypes.DELIVERY)
        //     .toList();
        // // filteredList.value = allOrder.value;
        //
        // servinOrder.value = result.data.getOrdersObject
        //     .where((arg1) => arg1.orderType == OrderTypes.SERVEIN)
        //     .toList();
        //
        // takeoutOrder.value = result.data.getOrdersObject
        //     .where((arg1) => arg1.orderType == OrderTypes.TAKEOUT)
        //     .toList();
      } else {
        showSnackBar(text: result?.data?.message ?? '');
      }
    }
  }

  void _scrollListener() {
    print(controller.position.extentAfter);
    if (controller.position.extentAfter < 500) {}
  }
}
