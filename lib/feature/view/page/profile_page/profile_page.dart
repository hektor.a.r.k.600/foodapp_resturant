import 'package:flutter/material.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:food_resturant/core/config/config.dart';
import 'package:food_resturant/core/config/routes.dart';
import 'package:food_resturant/feature/entities/file.dart';
import 'package:food_resturant/feature/view/page/profile_page/profile_page_controller.dart';
import 'package:food_resturant/feature/view/page/stocks_page/stocks_page_controller.dart';
import 'package:food_resturant/feature/view/page/tables_page/tables_page_controller.dart';
import 'package:food_resturant/feature/view/widget/app_bar.dart';
import 'package:food_resturant/feature/view/widget/box_shadow_widget.dart';
import 'package:food_resturant/library/platforms/image_picker.dart';
import 'package:food_resturant/library/platforms/upload_file.dart';
import 'package:food_resturant/library/widgets/button_widget.dart';
import 'package:food_resturant/library/widgets/cached_network_image_widget.dart';
import 'package:food_resturant/library/widgets/custom_drop_list/custom_drop_down_list.dart';
import 'package:food_resturant/library/widgets/text_widget.dart';
import 'package:get/get.dart';
import 'package:qr_flutter/qr_flutter.dart';

import '../../../../library/widgets/text_field_widget.dart';

class ProfilePage extends StatelessWidget {
  final controller = Get.put(ProfilePageController());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: SingleChildScrollView(
          child: GetBuilder<ProfilePageController>(
              init: ProfilePageController(),
              builder: (controller) {
                if (controller.user.loginObject.store == null) {
                  return Column(
                    children: [
                      BuildAppBarWidget(
                        appBarHeight: 200,
                        onBack: () {
                          Navigator.pop(context);
                        },
                        padding: EdgeInsets.all(0.0),
                        title: Text(
                          "Profile",
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: 20.0,
                              fontWeight: FontWeight.bold),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.all(16.0),
                        child: Form(
                          key: controller.profileFormKey,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Center(
                                child: InkWell(
                                  onTap: () {
                                    pickImage().then((file) => {
                                          if (file != null)
                                            {
                                              controller.storeProfileFile =
                                                  file,
                                              controller.update(),
                                            }
                                        });
                                  },
                                  child: controller.storeProfileFile != null
                                      ? CircleAvatar(
                                          radius: 50,
                                          backgroundImage: FileImage(
                                              controller.storeProfileFile),
                                        )
                                      : BuildCachedImageWidget(
                                          imageUrl: controller?.getStoreFood
                                                  ?.icon?.location ??
                                              '',
                                          height: 50.0,
                                          width: 50.0,
                                          borderRadius: 50.0,
                                        ),
                                ),
                              ),
                              SizedBox(
                                height: 10.0,
                              ),
                              CustomText(
                                'User Name',
                                color: Colors.black,
                                size: 20.0,
                                fontWeight: FontWeight.bold,
                              ),
                              CustomTextField(
                                  hint: controller.user.loginObject.name,
                                  height: 40.0,
                                  maxLine: 1,
                                  textEditingController:
                                      controller.txtStoreName,
                                  fontSize: 18.0,
                                  isBold: false),
                              SizedBox(
                                height: 10.0,
                              ),
                              CustomText(
                                'Email',
                                color: Colors.black,
                                size: 20.0,
                                fontWeight: FontWeight.bold,
                              ),
                              CustomTextField(
                                  hint: controller.user.loginObject.email,
                                  height: 40.0,
                                  maxLine: 1,
                                  textEditingController:
                                      controller.txtStoreName,
                                  fontSize: 18.0,
                                  isBold: false),
                              SizedBox(
                                height: 10.0,
                              ),
                              CustomText(
                                'Phone',
                                color: Colors.black,
                                size: 20.0,
                                fontWeight: FontWeight.bold,
                              ),
                              CustomTextField(
                                  hint: controller.user.loginObject.mobile,
                                  height: 40.0,
                                  maxLine: 1,
                                  textEditingController:
                                      controller.txtStoreName,
                                  fontSize: 18.0,
                                  isBold: false),
                              SizedBox(
                                height: 10.0,
                              ),
                            ],
                          ),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(bottom: 20),
                        child: Center(
                          child: SizedBox(
                              height: 40.0,
                              width: Get.width * .4,
                              child: CustomButton(
                                onTap: () {
                                  controller.changeLoginStatus();
                                },
                                color: Colors.red,
                                type: 2,
                                borderRadios: 10.0,
                                child: CustomText(
                                  'Log Out',
                                  color: Colors.white,
                                  size: 18.0,
                                  fontWeight: FontWeight.bold,
                                ),
                              )),
                        ),
                      ),
                    ],
                  );
                }
                return Column(
                  // crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    BuildAppBarWidget(
                      appBarHeight: 200,
                      padding: EdgeInsets.all(0.0),
                      title: Text(
                        "Profile",
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: 20.0,
                            fontWeight: FontWeight.bold),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(16.0),
                      child: Form(
                        key: controller.profileFormKey,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                CustomText(
                                  'Store Profile',
                                  color: Colors.black,
                                  size: 22.0,
                                  fontWeight: FontWeight.bold,
                                ),
                              ],
                            ),
                            SizedBox(
                              height: 15.0,
                            ),
                            Center(
                              child: InkWell(
                                onTap: () {
                                  pickImage().then((file) => {
                                        if (file != null)
                                          {
                                            controller.storeProfileFile = file,
                                            controller.update(),
                                          }
                                      });
                                },
                                child: controller.storeProfileFile != null
                                    ? CircleAvatar(
                                        radius: 50,
                                        backgroundImage: FileImage(
                                            controller.storeProfileFile),
                                      )
                                    : BuildCachedImageWidget(
                                        imageUrl: controller?.getStoreFood?.icon
                                                ?.location ??
                                            '',
                                        height: 50.0,
                                        width: 50.0,
                                        borderRadius: 50.0,
                                      ),
                              ),
                            ),
                            SizedBox(
                              height: 10.0,
                            ),
                            CustomText(
                              'Store Name',
                              color: Colors.black,
                              size: 20.0,
                              fontWeight: FontWeight.bold,
                            ),
                            CustomTextField(
                                hint: "Name Here",
                                height: 40.0,
                                maxLine: 1,
                                textEditingController: controller.txtStoreName,
                                fontSize: 18.0,
                                isBold: false),
                            SizedBox(
                              height: 10.0,
                            ),
                            CustomText(
                              'Opening Time',
                              color: Colors.black,
                              size: 20.0,
                              fontWeight: FontWeight.bold,
                            ),

                            // SizedBox(
                            //   height: 50.0,
                            //   child: CustomDropDownList(
                            //     padding: EdgeInsets.only(top: 8.0),
                            //     context: context,
                            //     dropdownColor: Get.theme.primaryColorLight.withAlpha(150),
                            //     onChange: (c) {
                            //       controller.openingTime = c;
                            //     },
                            //     valueItem: "9 AM",
                            //     items: ["9 AM", "2 PM"],
                            //   ),
                            // ),
                            SizedBox(
                              height: 5,
                            ),

                            SizedBox(
                                height: 40.0,
                                width: Get.width,
                                child: CustomButton(
                                  color: Get.theme.primaryColorLight
                                      .withAlpha(150),
                                  onTap: () {
                                    DatePicker.showTimePicker(context,
                                        showTitleActions: true,
                                        showSecondsColumn: false,
                                        onConfirm: (date) {
                                      controller.openingTime.first = date?.hour;
                                      controller.openingTime.last =
                                          date?.minute;
                                      controller.update();
                                    },
                                        currentTime: DateTime.now(),
                                        locale: LocaleType.en);
                                  },
                                  type: 1,
                                  borderRadios: 10.0,
                                  child: Align(
                                    alignment: Alignment.centerLeft,
                                    child: CustomText(
                                      controller.openingTime
                                              .map((e) => e.toString())
                                              .join(':') ??
                                          'Set Opening Time',
                                      textAlign: TextAlign.left,
                                      color: Colors.black,
                                      size: 18.0,
                                    ),
                                  ),
                                )),

                            SizedBox(
                              height: 10.0,
                            ),
                            CustomText(
                              'Closing Time',
                              color: Colors.black,
                              size: 20.0,
                              fontWeight: FontWeight.bold,
                            ),
                            SizedBox(
                              height: 5,
                            ),

                            SizedBox(
                                height: 40.0,
                                width: Get.width,
                                child: CustomButton(
                                  color: Get.theme.primaryColorLight
                                      .withAlpha(150),
                                  onTap: () {
                                    DatePicker.showTimePicker(context,
                                        showTitleActions: true,
                                        showSecondsColumn: false,
                                        onConfirm: (date) {
                                      controller.closingTime.first = date?.hour;
                                      controller.closingTime.last =
                                          date?.minute;
                                      controller.update();
                                    },
                                        currentTime: DateTime.now(),
                                        locale: LocaleType.en);
                                  },
                                  type: 1,
                                  borderRadios: 10.0,
                                  child: Align(
                                    alignment: Alignment.centerLeft,
                                    child: CustomText(
                                      controller.closingTime
                                              .map((e) => e.toString())
                                              .join(':') ??
                                          'Set Closing Time',
                                      textAlign: TextAlign.left,
                                      color: Colors.black,
                                      size: 18.0,
                                    ),
                                  ),
                                )),
                            SizedBox(
                              height: 10.0,
                            ),
                            CustomText(
                              'Store Description',
                              color: Colors.black,
                              size: 20.0,
                              fontWeight: FontWeight.bold,
                            ),
                            CustomTextField(
                                hint: "Describe your thought",
                                height: 100.0,
                                maxLine: 2,
                                textEditingController:
                                    controller.txtDescription,
                                fontSize: 18.0,
                                isBold: false),
                          ],
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(bottom: 20),
                      child: Center(
                        child: SizedBox(
                            height: 40.0,
                            width: Get.width * .4,
                            child: CustomButton(
                              onTap: () {
                                controller.editStoreProfile();
                              },
                              type: 2,
                              borderRadios: 10.0,
                              child: CustomText(
                                'Save Change',
                                color: Colors.black,
                                size: 18.0,
                                fontWeight: FontWeight.bold,
                              ),
                            )),
                      ),
                    ),
                    Divider(),
                    Padding(
                      padding: const EdgeInsets.all(16.0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          SizedBox(
                            height: 15.0,
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              CustomText(
                                'User Profile',
                                color: Colors.black,
                                size: 22.0,
                                fontWeight: FontWeight.bold,
                              ),
                            ],
                          ),
                          SizedBox(
                            height: 15.0,
                          ),
                          CustomText(
                            'First Name',
                            color: Colors.black,
                            size: 20.0,
                            fontWeight: FontWeight.bold,
                          ),
                          CustomTextField(
                              hint: "Name Here",
                              height: 40.0,
                              maxLine: 1,
                              textEditingController: controller.txtFirstName,
                              fontSize: 18.0,
                              isBold: false),
                          CustomText(
                            'Last Name',
                            color: Colors.black,
                            size: 20.0,
                            fontWeight: FontWeight.bold,
                          ),
                          CustomTextField(
                              hint: "Name Here",
                              height: 40.0,
                              maxLine: 1,
                              textEditingController: controller.txtLastName,
                              fontSize: 18.0,
                              isBold: false),
                          CustomText(
                            'Phone Number',
                            color: Colors.black,
                            size: 20.0,
                            fontWeight: FontWeight.bold,
                          ),
                          CustomTextField(
                              hint: "Name Here",
                              height: 40.0,
                              maxLine: 1,
                              textInputType: TextInputType.phone,
                              textEditingController: controller.txtPhoneNumber,
                              fontSize: 18.0,
                              isBold: false),
                          CustomText(
                            'Email',
                            color: Colors.black,
                            size: 20.0,
                            fontWeight: FontWeight.bold,
                          ),
                          CustomTextField(
                              hint: "Name Here",
                              height: 40.0,
                              maxLine: 1,
                              textEditingController: controller.txtEmail,
                              fontSize: 18.0,
                              isBold: false),
                          CustomText(
                            'Username',
                            color: Colors.black,
                            size: 20.0,
                            fontWeight: FontWeight.bold,
                          ),
                          SizedBox(
                            height: 8.0,
                          ),
                          Container(
                            height: 40.0,
                            width: Get.width,
                            decoration: BoxDecoration(
                                color:
                                    Get.theme.primaryColorLight.withAlpha(150),
                                borderRadius: BorderRadius.circular(10.0)),
                            child: Padding(
                              padding: const EdgeInsets.only(left: 12.0),
                              child: Row(
                                children: [
                                  CustomText(
                                    controller.txtUsername.text ?? "",
                                    color: Get.theme.primaryColorDark,
                                    size: 20.0,
                                    fontWeight: FontWeight.w500,
                                  ),
                                ],
                              ),
                            ),
                          ),
                          SizedBox(
                            height: 24.0,
                          ),
                          Padding(
                            padding: const EdgeInsets.only(bottom: 10),
                            child: Center(
                              child: SizedBox(
                                  height: 40.0,
                                  width: Get.width * .4,
                                  child: CustomButton(
                                    onTap: () {
                                      controller.editUserProfile();
                                    },
                                    type: 2,
                                    borderRadios: 10.0,
                                    child: CustomText(
                                      'Save Change',
                                      color: Colors.black,
                                      size: 18.0,
                                      fontWeight: FontWeight.bold,
                                    ),
                                  )),
                            ),
                          ),
                        ],
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(bottom: 20),
                      child: Center(
                        child: SizedBox(
                            height: 40.0,
                            width: Get.width * .4,
                            child: CustomButton(
                              onTap: () {
                                controller.changeLoginStatus();
                              },
                              color: Colors.red,
                              type: 2,
                              borderRadios: 10.0,
                              child: CustomText(
                                'Log Out',
                                color: Colors.white,
                                size: 18.0,
                                fontWeight: FontWeight.bold,
                              ),
                            )),
                      ),
                    ),
                  ],
                );
              }),
        ),
      ),
    );
  }
}
