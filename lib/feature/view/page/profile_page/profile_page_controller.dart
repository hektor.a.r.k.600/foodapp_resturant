import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:food_resturant/core/config/api_address.dart';
import 'package:food_resturant/core/config/global_setting.dart';
import 'package:food_resturant/core/config/keys.dart';
import 'package:food_resturant/core/config/routes.dart';
import 'package:food_resturant/feature/depenency_injection.dart';
import 'package:food_resturant/feature/entities/change_order_status_entity.dart';
import 'package:food_resturant/feature/entities/cover_image.dart';
import 'package:food_resturant/feature/entities/get_store_by_id_entity.dart';
import 'package:food_resturant/feature/entities/get_user_by_id_entity.dart';
import 'package:food_resturant/feature/entities/icon_object.dart';
import 'package:food_resturant/feature/entities/login_entity.dart';
import 'package:food_resturant/feature/entities/public_entity.dart';
import 'package:food_resturant/feature/server/api_helper.dart';
import 'package:food_resturant/feature/server/api_response.dart';
import 'package:food_resturant/feature/server/params/edit_store_profile.dart';
import 'package:food_resturant/feature/server/params/edit_user_profile_params.dart';
import 'package:food_resturant/feature/server/params/get_user_by_id_params.dart';
import 'package:food_resturant/library/platforms/loading_overlay.dart';
import 'package:food_resturant/library/platforms/shared_preferences.dart';
import 'package:food_resturant/library/platforms/upload_file.dart';
import 'package:food_resturant/library/widgets/snackbar.dart';
import 'package:food_resturant/library/widgets/text_widget.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';

class ProfilePageController extends GetxController {
  File coverPageFile;
  File storeProfileFile;
  final overlay = LoadingOverlay.of(Get.context);
  final profileFormKey = GlobalKey<FormState>();

  TextEditingController txtStoreName = TextEditingController();
  TextEditingController txtDescription = TextEditingController();
  TextEditingController txtFirstName = TextEditingController();
  TextEditingController txtLastName = TextEditingController();
  TextEditingController txtPhoneNumber = TextEditingController();
  TextEditingController txtEmail = TextEditingController();
  TextEditingController txtUsername = TextEditingController();
  TextEditingController txtPassword = TextEditingController();

  List<int> openingTime = List(2);
  List<int> closingTime = List(2);

  GetStoreByIdObject getStoreFood;

  LoginEntity user;

  @override
  void onInit() {
    if (sl<DataStorage>().getData(key: USER) != null) {
      user = LoginEntity.fromJson(
          json.decode(sl<DataStorage>().getData(key: USER)));
      sl<GlobalSetting>().user = user?.loginObject;
    }
    super.onInit();
  }

  @override
  void onReady() {
    getStoreById();
    super.onReady();
  }

  editStoreProfile() async {
    if (profileFormKey.currentState.validate()) {
      var store = sl<GlobalSetting>().user?.store;
      IconObject storeProfileImage;
      CoverImage storeCoverImage;

      if (storeProfileFile != null) {
        overlay.show(isUpload: true);
        storeProfileImage = await uploadFile(imageFile: storeProfileFile);
        overlay.hide();
      }
      if (coverPageFile != null) {
        overlay.show(isUpload: true);
        IconObject iconObject = await uploadFile(imageFile: coverPageFile);
        overlay.hide();
        if (iconObject != null) {
          storeCoverImage = CoverImage(
              id: iconObject.id,
              type: iconObject?.type,
              location: iconObject.location);
        }
      }

      var storeProfileParams = EditStoreProfile(
          storeId: store.id,
          closingHour: closingTime.first,
          closingMinute: closingTime.last,
          openingHour: openingTime.first,
          openingMinute: openingTime.last,
          name: txtStoreName.text,
          icon: storeProfileImage ?? getStoreFood?.icon,
          coverImage: storeCoverImage ?? getStoreFood?.coverImage,
          descriptions: txtDescription.text);

      final response = await overlay.during(sl<ApiHelper>()
          .post(ApiAddress.EDIT_STORE_PROFILE, storeProfileParams.toJson()));
      var result = ApiResponse.returnResponse(
          response, PublicEntity.fromJson(response.data));
      if (result.status == ApiStatus.COMPLETED && result?.data?.status) {
        LoginEntity user = LoginEntity.fromJson(
            json.decode(sl<DataStorage>().getData(key: USER)));
        user.loginObject.store.name = txtStoreName.text;
        user.loginObject.store.icon = storeProfileImage ?? getStoreFood?.icon;
        sl<DataStorage>().setData(key: USER, value: json.encode(user.toJson()));
        sl<GlobalSetting>().user = user?.loginObject;

        Get.back();
      } else {
        showSnackBar(text: result?.data?.message ?? '');
      }
    }
  }

  editUserProfile() async {
    var storeProfileParams = UserEditProfileParam(
      userId: user.loginObject.id,
      email: txtEmail.text ?? "",
      familyName: txtLastName.text ?? "",
      name: txtFirstName.text ?? "",
      mobile: txtPhoneNumber.text ?? "",
    );

    final response = await overlay.during(sl<ApiHelper>()
        .post(ApiAddress.USER_EDIT_PROFILE, storeProfileParams.toJson()));
    var result = ApiResponse.returnResponse(
        response, PublicEntity.fromJson(response.data));
    if (result.status == ApiStatus.COMPLETED && result?.data?.status) {
      LoginEntity user = LoginEntity.fromJson(
          json.decode(sl<DataStorage>().getData(key: USER)));
      user.loginObject.email = txtEmail.text ?? "";
      user.loginObject.name = txtFirstName.text ?? "";
      user.loginObject.familyName = txtLastName.text ?? "";
      user.loginObject.mobile = txtPhoneNumber.text ?? "";
      sl<DataStorage>().setData(key: USER, value: json.encode(user.toJson()));
      sl<GlobalSetting>().user = user?.loginObject;
      update();
      Get.back();
    } else {
      showSnackBar(text: result?.data?.message ?? '');
    }
  }

  getStoreById() async {
    var store = sl<GlobalSetting>().user?.store;
    print(sl<GlobalSetting>().user?.id);
    if (store?.id != null) {
      var getStore = {"Id": store.id};
      final response = await overlay.during(
          sl<ApiHelper>().getWithParam(ApiAddress.GET_STORE_BY_ID, getStore));
      var result = ApiResponse.returnResponse(
          response, GetStoreByIdEntity.fromJson(response.data));
      if (result.status == ApiStatus.COMPLETED && result?.data?.status) {
        getStoreFood = result?.data?.getStoreByIdObject;
        txtStoreName.text = getStoreFood?.name ?? '';
        txtDescription.text = getStoreFood?.descriptions ?? '';
        openingTime.first = getStoreFood?.openingHour;
        openingTime.last = getStoreFood?.openingMinute;

        closingTime.first = getStoreFood?.closingHour;
        closingTime.last = getStoreFood?.closingMinute;

        update();
        // showSnackBar(text: result?.data?.message ?? '');
        // Get.back();
        getUserById();
      } else {
        showSnackBar(text: result?.data?.message ?? '');
      }
    }
  }

  getUserById() async {
    var getUserById = GetUserByIdParams(userId: user.loginObject.id);

    final responseUser = await overlay.during(sl<ApiHelper>()
        .getWithParam(ApiAddress.GET_USERS_BY_ID, getUserById.toJson()));
    var resultUser = ApiResponse.returnResponse(
        responseUser, GetUserByIdEntity.fromJson(responseUser.data));
    if (resultUser.status == ApiStatus.COMPLETED && resultUser.data.status) {
      txtFirstName.text = resultUser?.data?.getUsersObject?.name ?? "";
      txtLastName.text = resultUser?.data?.getUsersObject?.familyName ?? "";
      txtUsername.text = resultUser?.data?.getUsersObject?.userName ?? "";
      txtPhoneNumber.text = resultUser?.data?.getUsersObject?.mobile ?? "";
      txtEmail.text = resultUser?.data?.getUsersObject?.email ?? "";
      update();
    }
  }

  changeLoginStatus() async {
    final response = await overlay.during(sl<ApiHelper>().post(
        ApiAddress.CHANGE_LOGIN_STATUS,
        ChangeLoginStatusModel(
                loginStatus: 2, userId: sl<GlobalSetting>().user?.id)
            .toJson()));
    var result = ApiResponse.returnResponse(
        response, GetStoreByIdEntity.fromJson(response.data));
    if (result.status == ApiStatus.COMPLETED && result?.data?.status) {
      logOut();
    } else {
      showSnackBar(text: result?.data?.message ?? '');
    }
  }

  void logOut() {
    final box = GetStorage();
    overlay
        .during(box.erase())
        .whenComplete(() => Get.offAllNamed(Routes.SplashScreen));
    //   overlay.show();
    // sl<DataStorage>().erase().then((value) => {
    //   overlay.hide(),
    //
    // });
  }
}
