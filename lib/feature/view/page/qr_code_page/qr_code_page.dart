import 'package:flutter/material.dart';
import 'package:food_resturant/core/config/routes.dart';
import 'package:food_resturant/feature/view/page/food_list_page/food_list_page_controller.dart';
import 'package:food_resturant/feature/view/page/order_history_page/order_history_page_controller.dart';
import 'package:food_resturant/feature/view/page/qr_code_page/qr_code_page_controller.dart';
import 'package:food_resturant/feature/view/page/restaurant_notification_page/restaurant_notification_page_controller.dart';
import 'package:food_resturant/feature/view/page/tables_page/tables_page_controller.dart';
import 'package:food_resturant/feature/view/page/withdraw_page/withdraw_page_controller.dart';
import 'package:food_resturant/feature/view/widget/app_bar.dart';
import 'package:food_resturant/feature/view/widget/box_shadow_widget.dart';
import 'package:food_resturant/library/widgets/button_widget.dart';
import 'package:food_resturant/library/widgets/custom_drop_list/custom_drop_down_list.dart';
import 'package:food_resturant/library/widgets/text_widget.dart';
import 'package:get/get.dart';
import 'package:qr_code_scanner/qr_code_scanner.dart';
import 'package:qr_flutter/qr_flutter.dart';

class QRCodePage extends StatelessWidget {
  final controller = Get.put(QRCodePageController());

  @override
  Widget build(BuildContext context) {
    return GetBuilder<QRCodePageController>(
        init: QRCodePageController(),
        builder: (controller) {
          return Scaffold(
              body: Column(
            children: [
              BuildAppBarWidget(
                title: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    CustomText(
                      'Scan QR Code',
                      color: Colors.white,
                      size: 25.0,
                      fontWeight: FontWeight.bold,
                    ),
                    SizedBox(
                      height: 2.0,
                    ),
                    CustomText(
                      'Scan Your Attendance QR Code',
                      color: Colors.white,
                      size: 18.0,
                      fontWeight: FontWeight.w500,
                    ),
                  ],
                ),
              ),
              Expanded(flex: 10, child: _buildQrView(context)),
              // Expanded(
              //   flex: 1,
              //   child: Padding(
              //       padding: const EdgeInsets.all(8.0),
              //       child: CustomText(
              //         controller.result != null
              //             ? 'Barcode Type: ${controller.result.format} Data: ${controller.result.code}'
              //             : 'Place Code inside the box',
              //         color: Colors.black,
              //       )),
              // )
            ],
          ));
        });
  }

  Widget _buildQrView(BuildContext context) {
    // For this example we check how width or tall the device is and change the scanArea and overlay accordingly.
    var scanArea = (Get.width < 500 || Get.height < 500) ? 250.0 : 500.0;
    // To ensure the Scanner view is properly sizes after rotation
    // we need to listen for Flutter SizeChanged notification and update controller
    return QRView(
      key: controller.qrKey,
      cameraFacing: CameraFacing.back,
      onQRViewCreated: _onQRViewCreated,
      formatsAllowed: [BarcodeFormat.qrcode],
      overlay: QrScannerOverlayShape(
        borderColor: Get.theme.primaryColor,
        borderRadius: 10,
        borderLength: 25,
        borderWidth: 2,
        cutOutSize: scanArea,
      ),
    );
  }

  void _onQRViewCreated(QRViewController qrViewController) {
    controller.qrCodeViewcontroller = qrViewController;
    controller.update();

    qrViewController.scannedDataStream.listen((scanData) {
      controller.barCodeResult = scanData;
      if(!controller?.callAttendance) {
        controller.addAttendence();
        controller.update();
      }
    });
  }
}
