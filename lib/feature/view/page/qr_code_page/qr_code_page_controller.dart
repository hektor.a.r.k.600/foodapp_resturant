import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:food_resturant/core/config/api_address.dart';
import 'package:food_resturant/core/config/keys.dart';
import 'package:food_resturant/feature/depenency_injection.dart';
import 'package:food_resturant/feature/entities/login_entity.dart';
import 'package:food_resturant/feature/entities/public_entity.dart';
import 'package:food_resturant/feature/entities/user.dart';
import 'package:food_resturant/feature/server/api_helper.dart';
import 'package:food_resturant/feature/server/api_response.dart';
import 'package:food_resturant/feature/server/params/add_attendence_params.dart';
import 'package:food_resturant/library/platforms/loading_overlay.dart';
import 'package:food_resturant/library/platforms/shared_preferences.dart';
import 'package:food_resturant/library/widgets/snackbar.dart';
import 'package:food_resturant/library/widgets/text_widget.dart';
import 'package:get/get.dart';
import 'package:get_mac/get_mac.dart';
import 'package:qr_code_scanner/qr_code_scanner.dart';

class QRCodePageController extends GetxController {
  Barcode barCodeResult;
  QRViewController qrCodeViewcontroller;
  final GlobalKey qrKey = GlobalKey(debugLabel: 'QR');
  final overlay = LoadingOverlay.of(Get.context);

  bool callAttendance = false;

  @override
  void onClose() {
    qrCodeViewcontroller.dispose();
    super.onClose();
  }

  addAttendence() async {
    if (sl<DataStorage>().getData(key: USER) != null) {
      var user = LoginEntity.fromJson(
          json.decode(sl<DataStorage>().getData(key: USER)));

      var user2 = User(
        id: user?.loginObject?.id,
        name: user?.loginObject?.name,
        userName: user?.loginObject?.userName,
        familyName: user?.loginObject?.familyName,
        profileImage: user?.loginObject?.profileImage?.address,
      );
      String macAddress;

      try {
        macAddress = await GetMac.macAddress;
      } on PlatformException {
        showSnackBar(text: 'Failed to get Device MAC Address' ?? '');
      }

      var addAttendenceParams = AddAttendenceParams(
          macAddress: macAddress,
          user: user2,
          status: 1,
          qrCodeId: barCodeResult?.code?.contains('http')
              ? barCodeResult?.code?.split('id=')[1]
              : barCodeResult?.code);
      callAttendance = true;
      final response = await overlay.during(sl<ApiHelper>()
          .post(ApiAddress.ADD_ATTENDENCE, addAttendenceParams.toJson()));
      var result = ApiResponse.returnResponse(
          response, PublicEntity.fromJson(response.data));
      callAttendance = false;
      if (result.status == ApiStatus.COMPLETED) {
        Get.back();
        showSnackBar(text: result?.data?.message ?? '',snackBarType: SnackBarType.Report);
      } else {
        showSnackBar(text: result?.data?.message ?? '');
      }
    }
  }
}
