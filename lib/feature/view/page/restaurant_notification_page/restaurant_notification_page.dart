import 'package:flutter/material.dart';
import 'package:food_resturant/core/config/routes.dart';
import 'package:food_resturant/feature/entities/get_notification_entity.dart';
import 'package:food_resturant/feature/view/page/food_list_page/food_list_page_controller.dart';
import 'package:food_resturant/feature/view/page/order_history_page/order_history_page_controller.dart';
import 'package:food_resturant/feature/view/page/restaurant_notification_page/restaurant_notification_page_controller.dart';
import 'package:food_resturant/feature/view/page/tables_page/tables_page_controller.dart';
import 'package:food_resturant/feature/view/page/withdraw_page/withdraw_page_controller.dart';
import 'package:food_resturant/feature/view/widget/app_bar.dart';
import 'package:food_resturant/feature/view/widget/box_shadow_widget.dart';
import 'package:food_resturant/library/widgets/button_widget.dart';
import 'package:food_resturant/library/widgets/custom_drop_list/custom_drop_down_list.dart';
import 'package:food_resturant/library/widgets/text_field_widget.dart';
import 'package:food_resturant/library/widgets/text_widget.dart';
import 'package:get/get.dart';
import 'package:qr_flutter/qr_flutter.dart';

class RestaurantNotificationPage extends StatelessWidget {
  final controller = Get.put(RestaurantNotificationPageController());

  @override
  Widget build(BuildContext context) {
    return GetBuilder<RestaurantNotificationPageController>(
        init: RestaurantNotificationPageController(),
        builder: (x) {
          return Scaffold(
          appBar: AppBar(
              iconTheme: IconThemeData(opacity: 0.0),
              toolbarHeight: 170.0,
              backgroundColor: Get.theme.scaffoldBackgroundColor,
              elevation: 0.0,
              // bottom: TabBar(
              //     controller: controller.tabController,
              //     indicatorColor: Get.theme.primaryColor,
              //     tabs: controller.tabBuilder()),
              flexibleSpace: BuildAppBarWidget(
                title: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    CustomText(
                      'Store Notification',
                      color: Colors.white,
                      size: 25.0,
                      fontWeight: FontWeight.bold,
                    ),
                    SizedBox(
                      height: 2.0,
                    ),
                    CustomText(
                      'View Your Notification',
                      color: Colors.white,
                      size: 18.0,
                      fontWeight: FontWeight.w500,
                    ),
                  ],
                ),
              ),
            ),
            body: RefreshIndicator(
              backgroundColor: Get.theme.primaryColorDark,

              onRefresh: () {
                return controller.getNotification();
              },
              child: Obx(() =>ListView.builder(
                  padding: EdgeInsets.all(5),
                  itemCount:
                      controller.getNotificationObject?.value?.length ?? 0,
                  itemBuilder: (context, index) {
                    final item = controller.getNotificationObject?.value[index];
                    return buildListItem(item);
                  })),
            ),

            // TabBarView(
            //   controller: controller.tabController,
            //   children: [
            //
            //     ListView.builder(
            //       padding: EdgeInsets.all(5),
            //       itemCount: 3,
            //       itemBuilder: (context, index) => GestureDetector(
            //         onTap: () {
            //
            //         },
            //         child: buildListItem(index),
            //       ),
            //     ),
            //     ListView.builder(
            //       padding: EdgeInsets.all(5),
            //       itemCount: 1,
            //       itemBuilder: (context, index) => GestureDetector(
            //         onTap: () {
            //
            //         },
            //         child: buildListItem(index),
            //       ),
            //     ),
            //
            //   ],
            // )
          );
        });
  }

  //todo add item object instead of index
  Widget buildListItem(GetNotificationObject item) {
    return InkWell(
      child: Card(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(15.0),
        ),
        color: Get.theme.accentColor,
        elevation: 4,
        margin: EdgeInsets.all(5.0),
        child: Container(
          height: 80,
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 15),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                Flexible(
                  child: CustomText(
                    item?.title ?? '',
                    color: Colors.black,
                    size: 18.0,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                // SizedBox(
                  // width: 20.0,
                // ),
                Flexible(
                  child: CustomText(
                    item?.description ?? '',
                    color: Colors.black,
                    size: 16.0,
                    fontWeight: FontWeight.normal,
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }


  showTicketWidget(BuildContext context) {
    return showModalBottomSheet<String>(
      enableDrag: true,
      context: context,
      builder: (context) => Container(
        child: Padding(
          padding: const EdgeInsets.all(15.0),
          child: Column(
            children: [
              CustomText(
                'Add Ticket',
                color: Colors.black,
                size: 30.0,
                fontWeight: FontWeight.bold,
              ),
              SizedBox(
                height: 10,
              ),
              CustomTextField(
                  hint: "description",
                  height: 40.0,
                  maxLine: 1,
                  textEditingController:
                  controller.txtControllerDescriptionTicket,
                  fontSize: 18.0,
                  isBold: false),
              Expanded(
                child: Align(
                  alignment: Alignment.bottomCenter,
                  child: SizedBox(
                      height: 40.0,
                      width: Get.width * .4,
                      child: CustomButton(
                        onTap: () {
                          // controller.addTicket();
                        },
                        type: 2,
                        borderRadios: 10.0,
                        child: CustomText(
                          'Add Ticket',
                          color: Colors.black,
                          size: 18.0,
                          fontWeight: FontWeight.bold,
                        ),
                      )),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }



}
