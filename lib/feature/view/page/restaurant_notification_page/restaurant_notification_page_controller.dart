import 'package:flutter/material.dart';
import 'package:food_resturant/core/config/api_address.dart';
import 'package:food_resturant/core/config/global_setting.dart';
import 'package:food_resturant/feature/depenency_injection.dart';
import 'package:food_resturant/feature/entities/get_notification_entity.dart';
import 'package:food_resturant/feature/entities/login_entity.dart';
import 'package:food_resturant/feature/server/api_helper.dart';
import 'package:food_resturant/feature/server/api_response.dart';
import 'package:food_resturant/feature/server/params/get_notification_params.dart';
import 'package:food_resturant/library/platforms/loading_overlay.dart';
import 'package:food_resturant/library/widgets/snackbar.dart';
import 'package:food_resturant/library/widgets/text_widget.dart';
import 'package:get/get.dart';

class RestaurantNotificationPageController extends GetxController
    with SingleGetTickerProviderMixin {
  final overlay = LoadingOverlay.of(Get.context);

  // int selectedIndex = 0;
  // List<String> tabsTitle = List<String>(2);
  // TabController tabController;

  LoginObject user;

  RxList<GetNotificationObject> getNotificationObject = <GetNotificationObject>[].obs;

  TextEditingController txtControllerDescriptionTicket = TextEditingController(text: '');

  addItemsToTabs() {
    // tabsTitle[0] = ("UnSeen");
    // tabsTitle[1] = ("Seen");
  }

  // List<Widget> tabBuilder() {
  //   List<Widget> listTabs = List<Widget>();
  //   tabsTitle.forEach((title) {
  //     listTabs.add(
  //       Padding(
  //         padding: const EdgeInsets.only(bottom: 12.0, top: 11.0),
  //         child: Row(
  //           mainAxisSize: MainAxisSize.min,
  //           mainAxisAlignment: MainAxisAlignment.spaceEvenly,
  //           children: [
  //             CustomText(
  //               title,
  //               color: Colors.black,
  //               size: 20.0,
  //               fontWeight: FontWeight.normal,
  //             ),
  //             SizedBox(width: 5,),
  //             listTabs.length == 0
  //                 ? Container(
  //                     padding: const EdgeInsets.all(3.0),
  //                     decoration: new BoxDecoration(
  //                       color: Colors.red,
  //                       borderRadius: BorderRadius.circular(15),
  //                     ),
  //                     constraints: BoxConstraints(
  //                       minWidth: 20,
  //                       minHeight: 20,
  //                     ),
  //                     child: Center(
  //                       child: CustomText('10', color: Colors.white),
  //                     ),
  //                   )
  //                 : SizedBox(),
  //           ],
  //         ),
  //       ),
  //     );
  //   });
  //   return listTabs;
  // }

  @override
  void onInit() {
    user = sl<GlobalSetting>().user;
    // tabController = TabController(length: 2, vsync: this);
    // addItemsToTabs();
    // tabController.addListener(() {
    //   selectedIndex = tabController.index;
    //   update();
    // });
    super.onInit();
  }

  @override
  void onReady() {
    getNotification();
    super.onReady();
  }

  getNotification() async{
    var notification = GetNotificationParams(roleId: user?.roleId);

    final response = await overlay.during(sl<ApiHelper>()
        .getWithParam(ApiAddress.GET_NOTIFICATION, notification.toJson()));
    var result = ApiResponse.returnResponse(
        response, GetNotificationEntity.fromJson(response.data));
    if (result.status == ApiStatus.COMPLETED && result?.data?.status) {
      // showSnackBar(text: result?.data?.message ?? '');
      getNotificationObject.assignAll(result.data?.getNotificationObject);

      // storeFood.value = result.data?.getStoreFoodObject;
    }else{
      showSnackBar(text: result?.data?.message ?? '');

    }


  }
}
