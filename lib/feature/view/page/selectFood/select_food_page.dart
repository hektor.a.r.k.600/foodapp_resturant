import 'package:custom_radio_grouped_button/CustomButtons/ButtonTextStyle.dart';
import 'package:custom_radio_grouped_button/CustomButtons/CustomRadioButton.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:food_resturant/feature/entities/get_store_food_entity.dart';
import 'package:food_resturant/feature/view/page/selectFood/selecte_food_page_controller.dart';
import 'package:food_resturant/feature/view/page/statistics_page/statistics_page_controller.dart';
import 'package:food_resturant/feature/view/page/stocks_page/stocks_page_controller.dart';
import 'package:food_resturant/feature/view/page/tables_page/tables_page_controller.dart';
import 'package:food_resturant/feature/view/widget/app_bar.dart';
import 'package:food_resturant/feature/view/widget/box_shadow_widget.dart';
import 'package:food_resturant/library/widgets/button_widget.dart';
import 'package:food_resturant/library/widgets/custom_drop_list/custom_drop_down_list.dart';
import 'package:food_resturant/library/widgets/text_widget.dart';
import 'package:get/get.dart';
import 'package:qr_flutter/qr_flutter.dart';

import '../../../../library/widgets/text_field_widget.dart';

class SelectFoodPage extends StatelessWidget {
  final controller = Get.put(SelectFoodPageController());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      floatingActionButton:  Padding(
        padding: const EdgeInsets.all(15.0),
        child: SizedBox(
            height: 40.0,
            width: Get.width * .4,
            child: CustomButton(
              onTap: () {
                Get.back(canPop: false,
                    result: controller.selectedFoodItem);
              },
              type: 2,
              borderRadios: 10.0,
              child: CustomText(
                'Done',
                color: Colors.black54,
                size: 18.0,
                fontWeight: FontWeight.bold,
              ),
            )),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
      body: SingleChildScrollView(
        child: GetBuilder<SelectFoodPageController>(
            init: SelectFoodPageController(),
            builder: (x) {
              return Container(
                height: Get.height,
                child: Column(
                  children: [
                    BuildAppBarWidget(
                      title: Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          CustomText(
                            'Select Food',
                            color: Colors.white,
                            size: 25.0,
                            fontWeight: FontWeight.bold,
                          ),
                          SizedBox(
                            height: 2.0,
                          ),
                          CustomText(
                            'Select Food For Discount',
                            color: Colors.white,
                            size: 18.0,
                            fontWeight: FontWeight.w500,
                          ),
                        ],
                      ),
                    ),
                    // Expanded(
                    //   child: Container(
                    //     margin: const EdgeInsets.only(
                    //         right: 16.0, left: 16.0, top: 16.0, bottom: 16.0),
                    //     decoration: BoxDecoration(
                    //       color: Get.theme.accentColor,
                    //       boxShadow: [buildBoxShadow()],
                    //       borderRadius: BorderRadius.circular(10.0),
                    //     ),
                    //     child: Obx(() => ListView.builder(
                    //           itemCount:
                    //               controller.storeFood?.value?.length ?? 0,
                    //           shrinkWrap: true,
                    //           padding: EdgeInsets.all(8.0),
                    //           // physics: NeverScrollableScrollPhysics(),
                    //           itemBuilder: (context, index) {
                    //             final item = controller.storeFood?.value[index];
                    //             return storeFoodItem(item);
                    //           },
                    //         )),
                    //   ),
                    // ),
                    Expanded(
                      child: Container(
                        margin: const EdgeInsets.only(
                            right: 16.0, left: 16.0, top: 16.0, bottom: 16.0),
                        decoration: BoxDecoration(
                          color: Get.theme.accentColor,
                          boxShadow: [buildBoxShadow()],
                          borderRadius: BorderRadius.circular(10.0),
                        ),
                        child: Obx(() =>
                            SingleChildScrollView(
                              child: CustomRadioButton(
                                height: 40,
                                horizontal: true,
                                enableShape: true,
                                elevation: 5,
                                selectedColor: Get.theme.primaryColor,
                                unSelectedColor: Get.theme.accentColor,
                                buttonLables: controller.storeFood?.value?.map((e) => e.name)?.toList(),
                                buttonValues: controller.storeFood?.value?.toList(),
                                buttonTextStyle: ButtonTextStyle(
                                    selectedColor: Get.theme.accentColor,
                                    unSelectedColor: Colors.black,
                                    textStyle: TextStyle(fontSize: 16)),
                                radioButtonValue: (value) {
                               controller.selectedFoodItem = value;
                                },
                              ),
                            ),




                        ),
                      ),
                    ),

                  ],
                ),
              );
            }),
      ),
    );
  }

  storeFoodItem(GetStoreFoodObject item) {
    return SizedBox(
      height: 40,
      child: CheckboxListTile(
        contentPadding: EdgeInsets.all(0),
        dense: true,
        checkColor: Get.theme.primaryColorDark,
        activeColor: Get.theme.primaryColorLight,
        title: CustomText(
          item?.name ?? '',
          color: Colors.black,
          size: 18.0,
          fontWeight: FontWeight.bold,
          textAlign: TextAlign.left,
        ),
        value: item?.isItemChecked,
        onChanged: (isCheck) {
          item?.isItemChecked = isCheck;
          if(isCheck){
           controller.selectedFood.add(item);
          }else{
            if(controller.selectedFood.any((element) => element ==item)){
              controller.selectedFood.removeWhere((element) => element ==item);
            }
          }
          controller.update();
        },
        controlAffinity:
            ListTileControlAffinity.leading, //  <-- leading Checkbox
      ),
    );
  }
}
