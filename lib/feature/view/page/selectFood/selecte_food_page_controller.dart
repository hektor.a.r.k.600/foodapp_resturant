import 'package:flutter/material.dart';
import 'package:food_resturant/core/config/api_address.dart';
import 'package:food_resturant/core/config/global_setting.dart';
import 'package:food_resturant/feature/depenency_injection.dart';
import 'package:food_resturant/feature/entities/get_store_food_entity.dart';
import 'package:food_resturant/feature/entities/login_entity.dart';
import 'package:food_resturant/feature/server/api_helper.dart';
import 'package:food_resturant/feature/server/api_response.dart';
import 'package:food_resturant/feature/server/params/get_store_food_params.dart';
import 'package:food_resturant/library/platforms/loading_overlay.dart';
import 'package:food_resturant/library/widgets/snackbar.dart';
import 'package:food_resturant/library/widgets/text_widget.dart';
import 'package:get/get.dart';

class SelectFoodPageController extends GetxController {
  final overlay = LoadingOverlay.of(Get.context);

  int selectedFoodId;


  RxList<GetStoreFoodObject> storeFood = <GetStoreFoodObject>[].obs;
  List<GetStoreFoodObject> selectedFood = new List();
  GetStoreFoodObject selectedFoodItem;

  LoginObject user;

  // List<String> pizzaItemTitle = List<String>();
  // List<String> sandwichItemTitle = List<String>();
  // List<String> bargerItemTitle = List<String>();
  //
  // bool _isCheckedPizza = false;
  // bool _isCheckedSandwich = false;
  // bool _isCheckedBarger = false;

  // addPizzaItemToCheckBox() {
  //   pizzaItemTitle.add('Spicy Chicken Pizza');
  //   pizzaItemTitle.add("Cheese Pizza");
  //   pizzaItemTitle.add("Vegetable Pizza");
  //   pizzaItemTitle.add("Pepperoni Pizza");
  // }

  // addSandwichToCheckBox() {
  //   sandwichItemTitle.add("Hotdog");
  //   sandwichItemTitle.add("Roast Beef");
  // }

  // addBargerToCheckBox() {
  //   bargerItemTitle.add("Special Barger");
  //   bargerItemTitle.add("Cheese Barger");
  //   bargerItemTitle.add("Pepperoni Pizza");
  // }

  // List<Widget> pizzaCheckBoxBuilder(List<String> checkBoxTitles) {
  //   List<Widget> listWidget = List<Widget>();
  //
  //   listWidget.add(SizedBox(
  //     height: 40,
  //     child: CheckboxListTile(
  //       contentPadding: EdgeInsets.all(0),
  //       dense: true,
  //       checkColor: Get.theme.primaryColorDark,
  //       activeColor: Get.theme.primaryColorLight,
  //       title: CustomText(
  //         'Pizza',
  //         color: Colors.black,
  //         size: 25.0,
  //         fontWeight: FontWeight.bold,
  //         textAlign: TextAlign.left,
  //       ),
  //       value: _isCheckedPizza,
  //       onChanged: (isCheck) {
  //         _isCheckedPizza = isCheck;
  //         update();
  //       },
  //       controlAffinity: ListTileControlAffinity.leading, //  <-- leading Checkbox
  //     ),
  //   ));
  //
  //   listWidget.add(Divider(
  //     color: Colors.grey[500],
  //   ));
  //   checkBoxTitles.forEach((title) {
  //     listWidget.add(SizedBox(
  //       height: 30,
  //       child: CheckboxListTile(
  //         contentPadding: EdgeInsets.zero,
  //         checkColor: Get.theme.primaryColorDark,
  //         activeColor: Get.theme.primaryColorLight,
  //         title: CustomText(
  //           title,
  //           color: Colors.black,
  //           size: 18.0,
  //           fontWeight: FontWeight.bold,
  //           textAlign: TextAlign.left,
  //         ),
  //         value: _isCheckedPizza ? _isCheckedPizza : false,
  //         onChanged: (isCheck) {
  //           _isCheckedPizza = isCheck;
  //           update();
  //         },
  //         controlAffinity:
  //             ListTileControlAffinity.leading, //  <-- leading Checkbox
  //       ),
  //     ));
  //   });
  //   listWidget.add(SizedBox(
  //     height: 30,
  //   ));
  //   return listWidget;
  //
  //   //  checkBoxTitles.map((title) => {
  //   //   CheckboxListTile(
  //   //     contentPadding: EdgeInsets.zero,
  //   //     isThreeLine: false,
  //   //     checkColor: Get.theme.primaryColorDark,
  //   //     activeColor:
  //   //     Get.theme.primaryColorLight,
  //   //     title: CustomText(title,
  //   //       color: Colors.black,
  //   //       size: 18.0,
  //   //       fontWeight: FontWeight.bold,
  //   //       textAlign: TextAlign.left,
  //   //     ),
  //   //     value: _isChecked,
  //   //     onChanged: (isCheck) {
  //   //       _isChecked = isCheck;
  //   //       update();
  //   //     },
  //   //     controlAffinity: ListTileControlAffinity
  //   //         .leading, //  <-- leading Checkbox
  //   //   )
  //   // }).toList().toList();
  // }

  // List<Widget> sandwichCheckBoxBuilder(List<String> checkBoxTitles) {
  //   List<Widget> listWidget = List<Widget>();
  //
  //   listWidget.add(SizedBox(
  //     height: 40,
  //     child: CheckboxListTile(
  //       contentPadding: EdgeInsets.all(0),
  //       dense: true,
  //       checkColor: Get.theme.primaryColorDark,
  //       activeColor: Get.theme.primaryColorLight,
  //       title: CustomText(
  //         'Sandwich',
  //         color: Colors.black,
  //         size: 25.0,
  //         fontWeight: FontWeight.bold,
  //         textAlign: TextAlign.left,
  //       ),
  //       value: _isCheckedSandwich,
  //       onChanged: (isCheck) {
  //         _isCheckedSandwich = isCheck;
  //         update();
  //       },
  //       controlAffinity: ListTileControlAffinity.leading, //  <-- leading Checkbox
  //     ),
  //   ));
  //   listWidget.add(Divider(
  //     color: Colors.grey[500],
  //   ));
  //   checkBoxTitles.forEach((title) {
  //     listWidget.add(SizedBox(
  //       height: 30,
  //       child: CheckboxListTile(
  //         contentPadding: EdgeInsets.zero,
  //         checkColor: Get.theme.primaryColorDark,
  //         activeColor: Get.theme.primaryColorLight,
  //         title: CustomText(
  //           title,
  //           color: Colors.black,
  //           size: 18.0,
  //           fontWeight: FontWeight.bold,
  //           textAlign: TextAlign.left,
  //         ),
  //         value: _isCheckedSandwich ? _isCheckedSandwich : false,
  //         onChanged: (isCheck) {
  //           _isCheckedSandwich = isCheck;
  //           update();
  //         },
  //         controlAffinity:
  //             ListTileControlAffinity.leading, //  <-- leading Checkbox
  //       ),
  //     ));
  //   });
  //   listWidget.add(SizedBox(
  //     height: 30,
  //   ));
  //   return listWidget;
  //
  // }

  // List<Widget> bargerCheckBoxBuilder(List<String> checkBoxTitles) {
  //   List<Widget> listWidget = List<Widget>();
  //
  //   listWidget.add(SizedBox(
  //     height: 40,
  //     child: CheckboxListTile(
  //       contentPadding: EdgeInsets.all(0),
  //       dense: true,
  //       checkColor: Get.theme.primaryColorDark,
  //       activeColor: Get.theme.primaryColorLight,
  //       title: CustomText(
  //         'Barger',
  //         color: Colors.black,
  //         size: 25.0,
  //         fontWeight: FontWeight.bold,
  //         textAlign: TextAlign.left,
  //       ),
  //       value: _isCheckedBarger,
  //       onChanged: (isCheck) {
  //         _isCheckedBarger = isCheck;
  //         update();
  //       },
  //       controlAffinity: ListTileControlAffinity.leading, //  <-- leading Checkbox
  //     ),
  //   ));
  //   listWidget.add(Divider(
  //     color: Colors.grey[500],
  //   ));
  //   checkBoxTitles.forEach((title) {
  //     listWidget.add(SizedBox(
  //       height: 30,
  //       child: CheckboxListTile(
  //         contentPadding: EdgeInsets.zero,
  //         checkColor: Get.theme.primaryColorDark,
  //         activeColor: Get.theme.primaryColorLight,
  //         title: CustomText(
  //           title,
  //           color: Colors.black,
  //           size: 18.0,
  //           fontWeight: FontWeight.bold,
  //           textAlign: TextAlign.left,
  //         ),
  //         value: _isCheckedBarger ? _isCheckedBarger : false,
  //         onChanged: (isCheck) {
  //           _isCheckedBarger = isCheck;
  //           update();
  //         },
  //         controlAffinity:
  //             ListTileControlAffinity.leading, //  <-- leading Checkbox
  //       ),
  //     ));
  //   });
  //   listWidget.add(SizedBox(
  //     height: 30,
  //   ));
  //   return listWidget;
  //
  // }

  @override
  void onInit() {
    user = sl<GlobalSetting>().user;
    super.onInit();
  }

  @override
  void onReady() {
    getAllStoreFood();
    super.onReady();
  }

  radioButtonOnChange(int food) {
    selectedFoodId = food;
    update();
  }

  getAllStoreFood() async {
    var getStoreFoodParams = GetStoreFoodParams(soreId: user?.store?.id ?? '');

    final response = await overlay.during(sl<ApiHelper>()
        .getWithParam(ApiAddress.GET_STORE_FOOD, getStoreFoodParams.toJson()));
    var result = ApiResponse.returnResponse(
        response, GetStoreFoodEntity.fromJson(response.data));
    if (result.status == ApiStatus.COMPLETED && result?.data?.status) {
      // showSnackBar(text: result?.data?.message ?? '');
      storeFood.value = result.data?.getStoreFoodObject;
      // storeFood.value = [
      //   GetStoreFoodObject(name: '111', status: 1),
      //   GetStoreFoodObject(name: '2222', status: 2),
      //   GetStoreFoodObject(name: '2222', status: 2),
      //   GetStoreFoodObject(name: '2222', status: 2),
      // ];
      // pizzaStoreFood.value = result.data?.getStoreFoodObject?.where((element) => element.)

    } else {
      showSnackBar(text: result?.data?.message ?? '');
    }
  }
}
