
import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_animator/widgets/animator_widget.dart';
import 'package:food_resturant/feature/depenency_injection.dart';
import 'package:food_resturant/library/platforms/notification_service.dart';
import 'package:get/get.dart';

class SplashScreenController extends GetxController {
 
  @override
  void onInit() {
    NotificationService().initialise();
    SystemChrome.setEnabledSystemUIOverlays([]);
    super.onInit();
  }

  @override
  void onReady() async{
    var pushId =await sl<NotificationService>()?.getPushId();
    log('PUSH ID : ' + pushId??'');
  }
}
