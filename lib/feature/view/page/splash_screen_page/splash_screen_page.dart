import 'package:flutter/material.dart';
import 'package:flutter_animator/widgets/fading_entrances/fade_in_up.dart';
import 'package:food_resturant/core/config/config.dart';
import 'package:food_resturant/feature/view/page/splash_screen_page/splash_screen_controller.dart';
import 'package:food_resturant/library/platforms/splash_handler.dart';
import 'package:food_resturant/library/widgets/text_widget.dart';
import 'package:get/get.dart';

class SplashScreen extends StatelessWidget {
  final controller = Get.put(SplashScreenController());
  @override
  Widget build(BuildContext context) {
    splashHandler(context,);
    return SafeArea(
      child: Scaffold(
        backgroundColor: Colors.grey[50],
        body: Padding(
          padding: const EdgeInsets.all(90.0),
          child: Center(child: Image.asset(Images.icon)),
        ),
      ),
    );
  }
}
