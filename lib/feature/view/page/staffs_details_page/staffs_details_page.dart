import 'package:flutter/material.dart';
import 'package:food_resturant/feature/view/page/staffs_details_page/staffs_details_page_controller.dart';
import 'package:food_resturant/feature/view/widget/app_bar.dart';
import 'package:food_resturant/library/widgets/button_widget.dart';
import 'package:food_resturant/library/widgets/cached_network_image_widget.dart';
import 'package:food_resturant/library/widgets/custom_drop_list/custom_drop_down_list.dart';
import 'package:food_resturant/library/widgets/text_widget.dart';
import 'package:get/get.dart';
import 'package:http/http.dart';

class StaffDetailsPage extends StatelessWidget {
  final controller = Get.put(StaffsDetailsPageController());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Column(
          children: [
            BuildAppBarWidget(
              title: BuildCachedImageWidget(
                imageUrl: "",
                height: 80.0,
                width: 80.0,
                borderRadius: 40.0,
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(16.0),
              child: Container(
                height: 120.0,
                width: MediaQuery.of(context).size.width,
                decoration: BoxDecoration(
                    color: Get.theme.primaryColorLight.withAlpha(150),
                    borderRadius: BorderRadius.circular(10.0)),
                child: Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          CustomText(
                            'First Name',
                            color: Colors.black,
                            size: 18.0,
                            fontWeight: FontWeight.bold,
                          ),
                          CustomText(
                            'Daisy',
                            color: Get.theme.primaryColorDark,
                            size: 18.0,
                            fontWeight: FontWeight.w500,
                          ),
                        ],
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          CustomText(
                            'Last Name',
                            color: Colors.black,
                            size: 18.0,
                            fontWeight: FontWeight.bold,
                          ),
                          CustomText(
                            'Ford',
                            color: Get.theme.primaryColorDark,
                            size: 18.0,
                            fontWeight: FontWeight.w500,
                          ),
                        ],
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          CustomText(
                            'Phone Number',
                            color: Colors.black,
                            size: 18.0,
                            fontWeight: FontWeight.bold,
                          ),
                          CustomText(
                            '+44 770 5270 8055',
                            color: Get.theme.primaryColorDark,
                            size: 18.0,
                            fontWeight: FontWeight.w500,
                          ),
                        ],
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          CustomText(
                            'Age',
                            color: Colors.black,
                            size: 18.0,
                            fontWeight: FontWeight.bold,
                          ),
                          CustomText(
                            '22',
                            color: Get.theme.primaryColorDark,
                            size: 18.0,
                            fontWeight: FontWeight.w500,
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 0.0, right: 16.0, left: 16.0),
              child: Container(
                width: MediaQuery.of(context).size.width,
                decoration: BoxDecoration(
                    color: Get.theme.primaryColorLight.withAlpha(150),
                    borderRadius: BorderRadius.circular(10.0)),
                child: Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      CustomText(
                        'Address',
                        color: Colors.black,
                        size: 18.0,
                        fontWeight: FontWeight.bold,
                      ),
                      SizedBox(
                        height: 10.0,
                      ),
                      CustomText(
                        '4972  Logan Lane, Lahania \n Hawaii',
                        color: Get.theme.primaryColorDark,
                        textAlign: TextAlign.start,
                        size: 18.0,
                        fontWeight: FontWeight.w500,
                      ),
                    ],
                  ),
                ),
              ),
            ),
            Padding(
              padding:
                  const EdgeInsets.only(top: 16.0, right: 16.0, left: 16.0),
              child: Container(
                width: MediaQuery.of(context).size.width,
                decoration: BoxDecoration(
                    color: Get.theme.primaryColorLight.withAlpha(150),
                    borderRadius: BorderRadius.circular(10.0)),
                child: Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      CustomText(
                        'Post',
                        color: Colors.black,
                        size: 18.0,
                        fontWeight: FontWeight.bold,
                      ),
                      CustomText(
                        'Cashier',
                        color: Get.theme.primaryColorDark,
                        textAlign: TextAlign.start,
                        size: 18.0,
                        fontWeight: FontWeight.w500,
                      ),
                    ],
                  ),
                ),
              ),
            ),
            Padding(
              padding:
                  const EdgeInsets.only(top: 16.0, right: 16.0, left: 16.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  CustomText(
                    'Working Time',
                    color: Colors.black,
                    size: 20.0,
                    fontWeight: FontWeight.bold,
                  ),
                  SizedBox(
                    height: 10.0,
                  ),
                  GetBuilder<StaffsDetailsPageController>(builder: (x) {
                    return Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Expanded(
                          child: GestureDetector(
                            onTap: () {
                              x.changeWorkingTime(true);
                            },
                            child: Container(
                              height: 42.0,
                              decoration: BoxDecoration(
                                  color: x.isFullTime
                                      ? Get.theme.primaryColor
                                      : Get.theme.primaryColorLight
                                          .withAlpha(150),
                                  borderRadius: BorderRadius.only(
                                      topLeft: Radius.circular(10.0),
                                      bottomLeft: Radius.circular(10.0))),
                              child: Center(
                                child: CustomText(
                                  'Full Time',
                                  color: Get.theme.primaryColorDark,
                                  size: 18.0,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                            ),
                          ),
                        ),
                        Expanded(
                          child: GestureDetector(
                            onTap: () {
                              x.changeWorkingTime(false);
                            },
                            child: Container(
                              height: 42.0,
                              decoration: BoxDecoration(
                                  color: x.isFullTime
                                      ? Get.theme.primaryColorLight
                                          .withAlpha(150)
                                      : Get.theme.primaryColor,
                                  borderRadius: BorderRadius.only(
                                      topRight: Radius.circular(10.0),
                                      bottomRight: Radius.circular(10.0))),
                              child: Center(
                                child: CustomText(
                                  'Part Time',
                                  color: Get.theme.primaryColorDark,
                                  size: 18.0,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                            ),
                          ),
                        ),
                      ],
                    );
                  }),
                ],
              ),
            ),
            Padding(
              padding:
                  const EdgeInsets.only(top: 16.0, right: 16.0, left: 16.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  CustomText(
                    'Working Shift',
                    color: Colors.black,
                    size: 20.0,
                    fontWeight: FontWeight.bold,
                  ),
                  SizedBox(
                    height: 10.0,
                  ),
                  CustomDropDownList(
                    context: context,
                    padding: EdgeInsets.all(0.0),
                    onChange: (e) {},
                    items: ["Nights", "Days"],
                    valueItem: "Nights",
                  )
                ],
              ),
            ),
            SizedBox(
              height: 50.0,
            ),
            Padding(
              padding: const EdgeInsets.all(16.0),
              child: Row(
                children: [
                  Expanded(
                      flex: 5,
                      child: SizedBox(
                          height: 40.0,
                          child: CustomButton(
                            onTap: () {
                              showChangeStaffStatusButtomSheet(context);
                            },
                            type: 2,
                            splashColor: Colors.redAccent,
                            borderRadios: 10.0,
                            color: Colors.red,
                            child: CustomText(
                              'Change Staff Status',
                              color: Get.theme.accentColor,
                              size: 18.0,
                              fontWeight: FontWeight.bold,
                            ),
                          ))),
                  SizedBox(
                    width: 16.0,
                  ),
                  Expanded(
                      flex: 3,
                      child: SizedBox(
                          height: 40.0,
                          child: CustomButton(
                            onTap: () {
                              showTodayStaffAttendanceButtomSheet(context);
                            },
                            type: 2,
                            borderRadios: 10.0,
                            child: CustomText(
                              'Attendance',
                              color: Colors.black,
                              size: 18.0,
                              fontWeight: FontWeight.bold,
                            ),
                          )))
                ],
              ),
            )
          ],
        ),
      ),
    );
  }

  showChangeStaffStatusButtomSheet(BuildContext context) {
    return showModalBottomSheet<String>(
      context: context,
      builder: (context) => Container(
        child: Column(
          children: [
            Expanded(
              child: Column(
                children: [
                  SizedBox(
                    height: 20.0,
                  ),
                  CustomText(
                    'Change Staff Status',
                    color: Colors.black,
                    size: 25.0,
                    fontWeight: FontWeight.bold,
                  ),
                  SizedBox(
                    height: 20.0,
                  ),
                  GetBuilder<StaffsDetailsPageController>(builder: (x) {
                    return Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Column(
                        children:
                            x.staffStatusRadioBuilder(x.staffStatusRadiosTitle),
                      ),
                    );
                  })
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(16.0),
              child: CustomButton(
                onTap: () {
                  navigator.pop();
                },
                type: 2,
                child: CustomText(
                  'Change Status',
                  color: Colors.black,
                  size: 18.0,
                  fontWeight: FontWeight.bold,
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  showTodayStaffAttendanceButtomSheet(BuildContext context) {
    return showModalBottomSheet<String>(
      context: context,
      builder: (context) => Container(
        child: Column(
          children: [
            Expanded(
              child: Column(
                children: [
                  SizedBox(
                    height: 10.0,
                  ),
                  CustomText(
                    'Today Staff Attendance',
                    color: Colors.black,
                    size: 25.0,
                    fontWeight: FontWeight.bold,
                  ),
                  SizedBox(
                    height: 20.0,
                  ),
                  GetBuilder<StaffsDetailsPageController>(builder: (x) {
                    return Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Column(
                        children: x.staffStatusRadioBuilder(
                            x.staffAttendanceRadiosTitle),
                      ),
                    );
                  }),
                  Padding(
                    padding: const EdgeInsets.only(left: 15.0, top: 10.0),
                    child: Row(
                      children: [
                        CustomText(
                          'Leave Type',
                          color: Colors.black,
                          size: 18.0,
                          fontWeight: FontWeight.bold,
                        ),
                      ],
                    ),
                  ),
                  CustomDropDownList(
                    context: context,
                    valueItem: "All Day Long",
                    onChange: (e) {},
                  ),
                  CustomText(
                    'Advanced Attendance',
                    color: Get.theme.primaryColorDark,
                    size: 18.0,
                    fontWeight: FontWeight.bold,
                  ),
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(16.0),
              child: CustomButton(
                onTap: () {
                  navigator.pop();
                },
                type: 2,
                child: CustomText(
                  'Set Attendance',
                  color: Colors.black,
                  size: 16.0,
                  fontWeight: FontWeight.bold,
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
