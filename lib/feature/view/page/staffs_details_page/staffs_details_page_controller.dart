import 'package:flutter/material.dart';
import 'package:food_resturant/library/widgets/text_widget.dart';
import 'package:get/get.dart';

class StaffsDetailsPageController extends GetxController {
  bool isFullTime = true;
  List<String> staffStatusRadiosTitle = List<String>(3);
  List<String> staffAttendanceRadiosTitle = List<String>(3);
  int selectedStaffStatus = 0;

  addStaffsStatusToRadios() {
    staffStatusRadiosTitle[0] = ("Working");
    staffStatusRadiosTitle[1] = ("Suspended");
    staffStatusRadiosTitle[2] = ("Fired");
  }

  addstaffAttendanceToRadios() {
    staffAttendanceRadiosTitle[0] = ("Present");
    staffAttendanceRadiosTitle[1] = ("Absent");
    staffAttendanceRadiosTitle[2] = ("Leave");
  }

  List<Widget> staffStatusRadioBuilder(List<String> radiosTitles) {
    List<Widget> listRadios = List<Widget>();
    for (var i = 0; i < radiosTitles.length; i++) {
      listRadios.add(
        Row(
          children: [
            SizedBox(
              height: 30.0,
              width: 30.0,
              child: Radio(
                activeColor: Get.theme.primaryColor,
                value: i,
                groupValue: selectedStaffStatus,
                onChanged: radioButtonOnChange,
              ),
            ),
            CustomText(
              radiosTitles[i],
              color: Colors.black,
              size: 18.0,
              fontWeight: FontWeight.normal,
            )
          ],
        ),
      );
    }

    return listRadios;
  }

  radioButtonOnChange(var selectedId) {
    selectedStaffStatus = selectedId;
    update();
  }

  changeWorkingTime(bool value) {
    isFullTime = value;
    update();
  }

  @override
  void onInit() {
    addStaffsStatusToRadios();
    addstaffAttendanceToRadios();
    super.onInit();
  }
}
