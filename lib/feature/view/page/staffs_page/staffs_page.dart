import 'package:flutter/material.dart';
import 'package:food_resturant/core/config/routes.dart';
import 'package:food_resturant/feature/entities/get_user_object.dart';
import 'package:food_resturant/feature/entities/get_users_entity.dart';
import 'package:food_resturant/feature/view/page/staffs_page/staffs_page_controller.dart';
import 'package:food_resturant/feature/view/widget/app_bar.dart';
import 'package:food_resturant/library/widgets/cached_network_image_widget.dart';
import 'package:food_resturant/library/widgets/text_widget.dart';
import 'package:get/get.dart';

class StaffsPage extends StatelessWidget {
  final controller = Get.put(StaffsPageController());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        iconTheme: IconThemeData(opacity: 0.0),
        toolbarHeight: 190.0,
        backgroundColor: Get.theme.scaffoldBackgroundColor,
        elevation: 0.0,
        bottom: TabBar(
            isScrollable: true,
            controller: controller.tabController,
            indicatorColor: Get.theme.primaryColor,
            tabs: controller.postTabBuilder()),
        flexibleSpace: BuildAppBarWidget(
          title: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              CustomText(
                'Store Staffs',
                size: 25.0,
                fontWeight: FontWeight.bold,
              ),
              CustomText(
                'View, Edit, Add, Remove Staffs',
                size: 19.0,
                fontWeight: FontWeight.normal,
              )
            ],
          ),
        ),
      ),
      body: TabBarView(
        controller: controller.tabController,
        children: [
          Container(
            child: Column(
              children: [
                // Padding(
                //   padding: const EdgeInsets.only(right: 10.0),
                //   child: GetBuilder<StaffsPageController>(
                //     builder: (x) => Row(
                //         mainAxisAlignment: MainAxisAlignment.spaceBetween,
                //         children: x.staffStatusRadioBuilder()),
                //   ),
                // ),
                Expanded(
                  child: RefreshIndicator(
                    backgroundColor: Get.theme.primaryColorDark,
                    onRefresh: () {
                      return controller.getUsersApi();
                    },
                    child: ListView.builder(
                      itemCount: controller?.all?.length??0,
                      itemBuilder: (context, index) => GestureDetector(
                        onTap: () {
                          Get.toNamed(Routes.StaffsDetailsPage);
                        },
                        child: staffsItem(context , controller?.all[index]),
                      ),
                    ),
                  ),
                )
              ],
            ),
          ),
          RefreshIndicator(
            backgroundColor: Get.theme.primaryColorDark,
            onRefresh: () {
              return controller.getUsersApi();
            },
            child: ListView.builder(
              itemCount: controller?.cashiers?.length??0,
              itemBuilder: (context, index) => GestureDetector(
                onTap: () {
                  Get.toNamed(Routes.StaffsDetailsPage);
                },
                child: staffsItem(context , controller?.cashiers[index]),
              ),
            ),
          ),
          RefreshIndicator(
            backgroundColor: Get.theme.primaryColorDark,
            onRefresh: () {
              return controller.getUsersApi();
            },
            child: ListView.builder(
              itemCount: controller?.waiters?.length??0,
              itemBuilder: (context, index) => GestureDetector(
                onTap: () {
                  Get.toNamed(Routes.StaffsDetailsPage);
                },
                child: staffsItem(context , controller?.waiters[index]),
              ),
            ),
          ),
          RefreshIndicator(
            backgroundColor: Get.theme.primaryColorDark,
            onRefresh: () {
              return controller.getUsersApi();
            },
            child: ListView.builder(
              itemCount: controller?.deliverys?.length??0,
              itemBuilder: (context, index) => GestureDetector(
                onTap: () {
                  Get.toNamed(Routes.StaffsDetailsPage);
                },
                child: staffsItem(context , controller?.deliverys[index]),
              ),
            ),
          ),
          RefreshIndicator(
            backgroundColor: Get.theme.primaryColorDark,
            onRefresh: () {
              return controller.getUsersApi();
            },
            child: ListView.builder(
              itemCount: controller?.workers?.length??0,
              itemBuilder: (context, index) => GestureDetector(
                onTap: () {
                  Get.toNamed(Routes.StaffsDetailsPage);
                },
                child: staffsItem(context , controller?.workers[index]),
              ),
            ),
          ),
        ],
      ),
    );
  }

  Card staffsItem(BuildContext context, GetUsersObject getUsersObject) {
    return Card(
                      color: Get.theme.accentColor,
                      elevation: 4,
                      margin: EdgeInsets.all(8.0),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          // Container(
                          //   height: 80.0,
                          //   width: MediaQuery.of(context).size.width / 4.5,
                          //   decoration: BoxDecoration(
                          //       color: Get.theme.primaryColor,
                          //       borderRadius: BorderRadius.only(
                          //           topLeft: Radius.circular(5),
                          //           bottomLeft: Radius.circular(5))),
                          //   child: Center(
                          //     child: BuildCachedImageWidget(
                          //       imageUrl: "",
                          //       height: 50.0,
                          //       width: 50.0,
                          //       borderRadius: 50.0,
                          //     ),
                          //   ),
                          // ),
                          BuildCachedImageWidget(
                            imageUrl: "",
                            height: 50.0,
                            width: 50.0,
                            borderRadius: 50.0,
                          ),
                          SizedBox(
                            width: 10.0,
                          ),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              CustomText(
                                getUsersObject?.name??'',
                                color: Colors.black,
                                size: 18.0,
                                fontWeight: FontWeight.bold,
                              ),
                              SizedBox(
                                width: 10.0,
                              ),
                              CustomText(
                                getUsersObject?.roleId.toString()??'',
                                color: Colors.black,
                                size: 16.0,
                                fontWeight: FontWeight.normal,
                              ),
                            ],
                          )
                        ],
                      ),
                    );
  }
}
