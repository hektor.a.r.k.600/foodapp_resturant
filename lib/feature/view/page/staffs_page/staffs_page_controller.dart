import 'package:flutter/material.dart';
import 'package:food_resturant/core/config/api_address.dart';
import 'package:food_resturant/feature/depenency_injection.dart';
import 'package:food_resturant/feature/entities/get_user_object.dart';
import 'package:food_resturant/feature/entities/get_users_entity.dart';
import 'package:food_resturant/feature/server/api_helper.dart';
import 'package:food_resturant/feature/server/api_response.dart';
import 'package:food_resturant/feature/server/params/get_users_params.dart';
import 'package:food_resturant/library/enums/enums.dart';
import 'package:food_resturant/library/extention/int_extention.dart';
import 'package:food_resturant/library/platforms/loading_overlay.dart';
import 'package:food_resturant/library/widgets/snackbar.dart';
import 'package:food_resturant/library/widgets/text_widget.dart';
import 'package:get/get.dart';

class StaffsPageController extends GetxController
    with SingleGetTickerProviderMixin {
  final overlay = LoadingOverlay.of(Get.context);

  List<String> tabsTitle = List<String>(5);
  List<String> radiosTitle = List<String>(3);
  int selectedRadio = 0;
  TabController tabController;

  RxList<GetUsersObject> all = <GetUsersObject>[].obs;
  RxList<GetUsersObject> cashiers = <GetUsersObject>[].obs;
  RxList<GetUsersObject> waiters = <GetUsersObject>[].obs;
  RxList<GetUsersObject> chefs = <GetUsersObject>[].obs;
  RxList<GetUsersObject> deliverys = <GetUsersObject>[].obs;
  RxList<GetUsersObject> workers = <GetUsersObject>[].obs;

  addItemsToTabs() {
    tabsTitle[0] = ("All");
    tabsTitle[1] = ("Cashiers");
    tabsTitle[2] = ("Waiters");
    // tabsTitle[3] = ("Chefs");
    tabsTitle[3] = ("Deliverys");
    tabsTitle[4] = ("Workers");
  }

  addItemsToRadios() {
    radiosTitle[0] = ("Working");
    radiosTitle[1] = ("Suspended");
    radiosTitle[2] = ("Fired");
  }

  List<Widget> postTabBuilder() {
    List<Widget> listTabs = List<Widget>();
    tabsTitle.forEach((title) {
      listTabs.add(
        Padding(
          padding: const EdgeInsets.only(bottom: 12.0, top: 11.0),
          child: CustomText(
            title,
            color: Colors.black,
            size: 20.0,
            fontWeight: FontWeight.normal,
          ),
        ),
      );
    });
    return listTabs;
  }

  List<Widget> staffStatusRadioBuilder() {
    List<Widget> listRadios = List<Widget>();
    for (var i = 0; i < radiosTitle.length; i++) {
      listRadios.add(
        InkWell(
          onTap: () {},
          child: Row(
            children: [
              Radio(
                activeColor: Get.theme.primaryColor,
                value: i,
                groupValue: selectedRadio,
                onChanged: radioButtonOnChange,
              ),
              CustomText(
                radiosTitle[i],
                color: Colors.black,
                size: 18.0,
                fontWeight: FontWeight.normal,
              )
            ],
          ),
        ),
      );
    }

    return listRadios;
  }

  radioButtonOnChange(var selectedId) {
    selectedRadio = selectedId;
    update();
  }

  @override
  void onInit() {
    tabController = TabController(length: 5, vsync: this);
    addItemsToTabs();
    addItemsToRadios();
    super.onInit();
  }

  @override
  void onReady() {
    getUsersApi();
  }

  getUsersApi() async {
    var usersParams = GetUsersParams();

    final usersResponse = await overlay.during(sl<ApiHelper>()
        .getWithParam(ApiAddress.GET_USERS, usersParams.toJson()));
    var usersResult = ApiResponse.returnResponse(
        usersResponse, GetUsersEntity.fromJson(usersResponse.data));
    if (usersResult.status == ApiStatus.COMPLETED &&
        usersResult?.data?.status) {
      all.value = usersResult?.data?.object;
      cashiers.value = usersResult?.data?.object
          ?.where((element) => element.roleId == UserRole.CASHIER)?.toList();
      waiters.value = usersResult?.data?.object
          ?.where((element) => element.roleId == UserRole.WAITER)?.toList();
      deliverys.value = usersResult?.data?.object
          ?.where((element) => element.roleId == UserRole.DELIVERY)?.toList();
      workers.value = usersResult?.data?.object
          ?.where((element) => element.roleId == UserRole.USER)?.toList();
    } else {
      showSnackBar(text: usersResult?.data?.message ?? '');
    }
  }
}
