import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:food_resturant/feature/view/page/statistics_page/statistics_page_controller.dart';
import 'package:food_resturant/feature/view/page/stocks_page/stocks_page_controller.dart';
import 'package:food_resturant/feature/view/page/tables_page/tables_page_controller.dart';
import 'package:food_resturant/feature/view/widget/app_bar.dart';
import 'package:food_resturant/feature/view/widget/box_shadow_widget.dart';
import 'package:food_resturant/library/widgets/button_widget.dart';
import 'package:food_resturant/library/widgets/custom_drop_list/custom_drop_down_list.dart';
import 'package:food_resturant/library/widgets/text_widget.dart';
import 'package:get/get.dart';
import 'package:qr_flutter/qr_flutter.dart';

import '../../../../library/widgets/text_field_widget.dart';

class StatisticsPage extends StatelessWidget {
  final controller = Get.put(StatisticsPageController());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        iconTheme: IconThemeData(opacity: 0.0),
        toolbarHeight: 190.0,
        backgroundColor: Get.theme.scaffoldBackgroundColor,
        elevation: 0.0,
        bottom: TabBar(
            controller: controller.tabController,
            indicatorWeight: 3,
            indicatorColor: Get.theme.primaryColor,
            indicatorSize: TabBarIndicatorSize.tab,
            indicatorPadding: EdgeInsets.symmetric(horizontal: 10),
            tabs: controller.postTabBuilder()),
        flexibleSpace: BuildAppBarWidget(
          title: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              CustomText(
                'Store Statistics',
                size: 25.0,
                fontWeight: FontWeight.bold,
              ),
              CustomText(
                'View Your Statistics',
                size: 19.0,
                fontWeight: FontWeight.normal,
              )
            ],
          ),
        ),
      ),
      body: TabBarView(
        controller: controller.tabController,
        children: [
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                // Row(
                //   children: [
                //     CustomText(
                //       'Sort By',
                //       color: Colors.black,
                //       size: 18.0,
                //       fontWeight: FontWeight.bold,
                //     ),
                //     SizedBox(
                //       width: Get.width * .4,
                //       child: CustomDropDownList(
                //         context: context,
                //         dropdownColor: Get.theme.accentColor.withAlpha(150),
                //         onChange: (c) {},
                //         valueItem: "All",
                //         items: ["All"],
                //       ),
                //     )
                //   ],
                // ),
                Expanded(
                  child: Container(
                    margin: const EdgeInsets.only(
                        right: 16.0, left: 16.0, top: 16.0, bottom: 16.0),
                    decoration: BoxDecoration(
                      color: Get.theme.accentColor,
                      boxShadow: [buildBoxShadow()],
                      borderRadius: BorderRadius.circular(10.0),
                    ),
                    child: GetBuilder<StatisticsPageController>(
                        init: StatisticsPageController(),
                        builder: (x) {
                          return RefreshIndicator(
                            backgroundColor: Get.theme.primaryColorDark,
                            onRefresh: () {
                              return controller.getTransactionApi();
                            },
                            child: ListView.builder(
                                // physics: NeverScrollableScrollPhysics(),
                                shrinkWrap: true,
                                padding: EdgeInsets.only(top: 8.0),
                                itemCount:
                                    controller?.transaction?.value?.length ?? 0,
                                itemBuilder: (BuildContext ctx, int index) {
                                  final item =
                                      controller?.transaction?.value[index];
                                  return Padding(
                                    padding: const EdgeInsets.only(
                                        left: 8.0, right: 8.0, top: 0.0),
                                    child: Column(
                                      children: [
                                        Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                          children: [
                                            CustomText(
                                              item?.description ?? '',
                                              color: Colors.black,
                                              size: 18.0,
                                              fontWeight: FontWeight.bold,
                                              textAlign: TextAlign.left,
                                            ),
                                            CustomText(
                                              "RM" +
                                                  item?.totalPrice.toString(),
                                              color: Colors.black,
                                              size: 18.0,
                                              fontWeight: FontWeight.bold,
                                              textAlign: TextAlign.left,
                                            ),
                                          ],
                                        ),
                                        Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                          children: [
                                            CustomText(
                                              "Total discount :",
                                              color: Colors.black,
                                              size: 17.0,
                                              fontWeight: FontWeight.normal,
                                              textAlign: TextAlign.left,
                                            ),
                                            CustomText(
                                              "RM" +
                                                      item?.totalDiscount
                                                          .toString() ??
                                                  "0.0",
                                              color: Colors.black,
                                              size: 17.0,
                                              fontWeight: FontWeight.normal,
                                              textAlign: TextAlign.left,
                                            ),
                                          ],
                                        ),
                                        Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                          children: [
                                            CustomText(
                                              "Store name :",
                                              color: Colors.black,
                                              size: 17.0,
                                              fontWeight: FontWeight.normal,
                                              textAlign: TextAlign.left,
                                            ),
                                            CustomText(
                                              item?.store?.name ?? "",
                                              color: Colors.black,
                                              size: 17.0,
                                              fontWeight: FontWeight.normal,
                                              textAlign: TextAlign.left,
                                            ),
                                          ],
                                        ),
                                        Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                          children: [
                                            CustomText(
                                              "Status :",
                                              color: Colors.black,
                                              size: 17.0,
                                              fontWeight: FontWeight.normal,
                                              textAlign: TextAlign.left,
                                            ),
                                            CustomText(
                                              item?.isSuccessful ?? false
                                                  ? "Successful"
                                                  : "Failed",
                                              color: Colors.black,
                                              size: 17.0,
                                              fontWeight: FontWeight.normal,
                                              textAlign: TextAlign.left,
                                            ),
                                          ],
                                        ),
                                        Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                          children: [
                                            CustomText(
                                              "Created at :",
                                              color: Colors.black,
                                              size: 17.0,
                                              fontWeight: FontWeight.normal,
                                              textAlign: TextAlign.left,
                                            ),
                                            CustomText(
                                              DateTime.fromMillisecondsSinceEpoch(
                                                      item.createdAt * 1000)
                                                  .toString(),
                                              color: Colors.black,
                                              size: 17.0,
                                              fontWeight: FontWeight.normal,
                                              textAlign: TextAlign.left,
                                            ),
                                          ],
                                        ),
                                        SizedBox(
                                          height: 8,
                                        ),
                                        Divider(),
                                      ],
                                    ),
                                  );
                                }),
                          );
                        }),
                  ),
                )
              ],
            ),
          ),
          // Padding(
          //   padding: const EdgeInsets.all(8.0),
          //   child: Column(
          //     // mainAxisAlignment: MainAxisAlignment.spaceBetween,
          //     // crossAxisAlignment: CrossAxisAlignment.stretch,
          //     children: [
          //       // Row(
          //       // mainAxisAlignment: MainAxisAlignment.spaceBetween,
          //       //   children: [
          //       //     Row(
          //       //       children: [
          //       //         CustomText(
          //       //           'Sort By',
          //       //           color: Colors.black,
          //       //           size: 18.0,
          //       //           fontWeight: FontWeight.bold,
          //       //         ),
          //       //         SizedBox(
          //       //           width: Get.width * .35,
          //       //           child: CustomDropDownList(
          //       //             context: context,
          //       //             dropdownColor: Get.theme.accentColor.withAlpha(150),
          //       //             onChange: (c) {},
          //       //             valueItem: "Date",
          //       //             items: ["Date"],
          //       //           ),
          //       //         )
          //       //       ],
          //       //     ),
          //       //     Row(
          //       //       children: [
          //       //         Row(
          //       //           children: [
          //       //             CustomText(
          //       //               'Month',
          //       //               color: Colors.black,
          //       //               size: 18.0,
          //       //               fontWeight: FontWeight.bold,
          //       //             ),
          //       //             SizedBox(
          //       //               width: Get.width * .35,
          //       //               child: CustomDropDownList(
          //       //                 context: context,
          //       //                 dropdownColor: Get.theme.accentColor.withAlpha(150),
          //       //                 onChange: (c) {},
          //       //                 valueItem: "November",
          //       //                 items: ["November"],
          //       //               ),
          //       //             ),
          //       //           ],
          //       //         ),
          //       //
          //       //       ],
          //       //     ),
          //       //   ],
          //       // ),
          //       // Row(
          //       //   children: [
          //       //     CustomText(
          //       //       'Day',
          //       //       color: Colors.black,
          //       //       size: 18.0,
          //       //       fontWeight: FontWeight.bold,
          //       //     ),
          //       //     SizedBox(
          //       //       width: Get.width * .4,
          //       //       child: CustomDropDownList(
          //       //         context: context,
          //       //         dropdownColor: Get.theme.accentColor.withAlpha(150),
          //       //         onChange: (c) {},
          //       //         valueItem: "12",
          //       //         items: ["12"],
          //       //       ),
          //       //     ),
          //       //   ],
          //       // ),
          //
          //
          //       Expanded(
          //         child: Container(
          //           margin: const EdgeInsets.only(
          //               right: 16.0, left: 16.0, top: 16.0, bottom: 16.0),
          //           decoration: BoxDecoration(
          //             color: Get.theme.accentColor,
          //             boxShadow: [buildBoxShadow()],
          //             borderRadius: BorderRadius.circular(10.0),
          //           ),
          //           child: GetBuilder<StatisticsPageController>(
          //               init: StatisticsPageController(),
          //               builder: (x) {
          //                 return ListView(
          //                   physics: AlwaysScrollableScrollPhysics(),
          //                   padding: EdgeInsets.all(10.0),
          //                   shrinkWrap: true,
          //                   // mainAxisSize: MainAxisSize.max,
          //                   // crossAxisAlignment: CrossAxisAlignment.stretch,
          //                   children: [
          //                     Column(
          //                       crossAxisAlignment: CrossAxisAlignment.start,
          //                       mainAxisAlignment: MainAxisAlignment.spaceBetween,
          //                       children: [
          //                         CustomText(
          //                           'Stocks',
          //                           color: Colors.black,
          //                           size: 25.0,
          //                           fontWeight: FontWeight.bold,
          //                           textAlign: TextAlign.left,
          //                         ),
          //                         Divider(
          //                           color: Colors.grey[500],
          //                         ),
          //                         ListView.builder(
          //                             shrinkWrap: true,
          //                             physics: NeverScrollableScrollPhysics(),
          //                             itemCount: 10,
          //                             itemBuilder: (BuildContext ctx, int index) {
          //                               return Padding(
          //                                 padding: const EdgeInsets.all(5.0),
          //                                 child: Row(
          //                                   mainAxisAlignment:
          //                                   MainAxisAlignment.spaceBetween,
          //                                   children: [
          //                                     CustomText(
          //                                       'Lettue',
          //                                       color: Colors.black,
          //                                       size: 18.0,
          //                                       fontWeight: FontWeight.bold,
          //                                       textAlign: TextAlign.left,
          //                                     ),
          //                                     CustomText(
          //                                       '1 KG',
          //                                       color: Colors.black,
          //                                       size: 18.0,
          //                                       fontWeight: FontWeight.bold,
          //                                       textAlign: TextAlign.left,
          //                                     ),
          //                                   ],
          //                                 ),
          //                               );
          //                             }),
          //                         SizedBox(
          //                           height: 50,
          //                         ),
          //
          //                       ],
          //                     ),
          //                     Row(
          //                       mainAxisAlignment:
          //                       MainAxisAlignment.spaceBetween,
          //                       children: [
          //                         CustomText(
          //                           'Total',
          //                           color: Colors.black,
          //                           size: 18.0,
          //                           fontWeight: FontWeight.bold,
          //                           textAlign: TextAlign.left,
          //                         ),
          //                         CustomText(
          //                           '454',
          //                           color: Colors.black,
          //                           size: 18.0,
          //                           fontWeight: FontWeight.bold,
          //                           textAlign: TextAlign.left,
          //                         ),
          //                       ],
          //                     ),
          //                   ],
          //                 );
          //               }),
          //         ),
          //       )
          //     ],
          //   ),
          // ),
        ],
      ),
    );
  }
}
