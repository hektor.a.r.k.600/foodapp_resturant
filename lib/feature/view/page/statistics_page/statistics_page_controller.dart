import 'package:flutter/material.dart';
import 'package:food_resturant/core/config/api_address.dart';
import 'package:food_resturant/core/config/global_setting.dart';
import 'package:food_resturant/feature/depenency_injection.dart';
import 'package:food_resturant/feature/entities/login_entity.dart';
import 'package:food_resturant/feature/entities/store_get_store_transactions_entity.dart';
import 'package:food_resturant/feature/server/api_helper.dart';
import 'package:food_resturant/feature/server/api_response.dart';
import 'package:food_resturant/feature/server/params/get_store_transaction_params.dart';
import 'package:food_resturant/library/platforms/loading_overlay.dart';
import 'package:food_resturant/library/widgets/snackbar.dart';
import 'package:food_resturant/library/widgets/text_widget.dart';
import 'package:get/get.dart';

class StatisticsPageController extends GetxController
    with SingleGetTickerProviderMixin {
  List<String> tabsTitle = List<String>(1);
  TabController tabController;
  final overlay = LoadingOverlay.of(Get.context);

  LoginObject user;

  RxList<StoreGetStoreTransactionsObject> transaction =
      <StoreGetStoreTransactionsObject>[].obs;
  addItemsToTabs() {
    tabsTitle[0] = ("Sales");
    // tabsTitle[1] = ("Purchases");
  }

  List<Widget> postTabBuilder() {
    List<Widget> listTabs = List<Widget>();
    tabsTitle.forEach((title) {
      listTabs.add(
        Padding(
          padding: const EdgeInsets.only(bottom: 12.0, top: 11.0),
          child: CustomText(
            title,
            color: Colors.black,
            size: 20.0,
            fontWeight: FontWeight.normal,
          ),
        ),
      );
    });
    return listTabs;
  }

  @override
  void onInit() {
    user = sl<GlobalSetting>().user;
    // var argument = Get.arguments;

    tabController = TabController(length: 1, vsync: this);
    addItemsToTabs();
    super.onInit();
  }

  @override
  void onReady() {
    getTransactionApi();
    super.onReady();
  }

  getTransactionApi() async {
    var transactionParams = GetStoreTransactionsParams(
        storeId: user?.store?.id,
        from: (DateTime(2010, 1, 1).millisecondsSinceEpoch ~/ 100),
        to: DateTime.now().millisecondsSinceEpoch ~/ 100);
    final response = await overlay.during(sl<ApiHelper>().getWithParam(
        ApiAddress.GET_STORE_TRANSACTION, transactionParams.toJson()));
    var result = ApiResponse.returnResponse(
        response, StoreGetStoreTransactionsEntity.fromJson(response.data));
    if (result.status == ApiStatus.COMPLETED && result?.data?.status) {
      transaction.value = result.data?.storeGetStoreTransactionsEntityObject;
      update();
    } else {
      showSnackBar(text: result?.data?.message ?? '');
    }
  }
}
