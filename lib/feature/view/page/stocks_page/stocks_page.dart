import 'package:flutter/material.dart';
import 'package:food_resturant/feature/entities/get_food_ingredients_entity.dart';
import 'package:food_resturant/feature/view/page/stocks_page/stocks_page_controller.dart';
import 'package:food_resturant/feature/view/page/tables_page/tables_page_controller.dart';
import 'package:food_resturant/feature/view/widget/app_bar.dart';
import 'package:food_resturant/feature/view/widget/box_shadow_widget.dart';
import 'package:food_resturant/library/widgets/button_widget.dart';
import 'package:food_resturant/library/widgets/custom_drop_list/custom_drop_down_list.dart';
import 'package:food_resturant/library/widgets/show_bottom_sheet.dart';
import 'package:food_resturant/library/widgets/text_widget.dart';
import 'package:get/get.dart';
import 'package:qr_flutter/qr_flutter.dart';

import '../../../../library/widgets/text_field_widget.dart';

class StocksPage extends StatelessWidget {
  final controller = Get.put(StocksPageController());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      floatingActionButton: FloatingActionButton(
        onPressed: () {

          showCustomBottomSheet(bottomSheetAddStock(context));
          // showAddStockBottomSheet(context, controller);
        },
        backgroundColor: Get.theme.primaryColor,
        child: Center(
          child: Icon(
            Icons.add,
            size: 30.0,
          ),
        ),
      ),
      body: RefreshIndicator(
        backgroundColor: Get.theme.primaryColor,
        onRefresh: (){

          return controller.getFoodIngredientsApi();

        },
        child: Column(
          children: [
            BuildAppBarWidget(
              title: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  CustomText(
                    'Store Stocks',
                    color: Colors.white,
                    size: 25.0,
                    fontWeight: FontWeight.bold,
                  ),
                  SizedBox(
                    height: 2.0,
                  ),
                  CustomText(
                    'View, Edit, Add, Remove Tables',
                    color: Colors.white,
                    size: 18.0,
                    fontWeight: FontWeight.w500,
                  ),
                ],
              ),
            ),
            Expanded(
              child: Obx(
                () => ListView.builder(
                    itemCount: controller.foodIngredients?.length ?? 0,
                    shrinkWrap: true,
                    itemBuilder: (context, index) =>
                        stockItem(controller.foodIngredients[index])),
              ),
            ),
          ],
        ),
      ),
    );
  }

  ListView bottomSheetAddStock(BuildContext context) {
    return ListView(
                padding: const EdgeInsets.all(15.0),
                children: [
                  CustomText(
                    'Add Stocks',
                    color: Colors.black,
                    size: 30.0,
                    fontWeight: FontWeight.bold,
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      CustomText(
                        'Stock Name',
                        color: Colors.black,
                        size: 20.0,
                        fontWeight: FontWeight.bold,
                        textAlign: TextAlign.left,
                      ),
                      CustomTextField(
                          hint: "Stock Name",
                          height: 40.0,
                          textEditingController: controller.txtStockName,
                          maxLine: 1,
                          fontSize: 18.0,
                          isBold: false),
                      SizedBox(
                        height: 10,
                      ),
                      CustomText(
                        'Amount',
                        color: Colors.black,
                        size: 20.0,
                        fontWeight: FontWeight.bold,
                        textAlign: TextAlign.left,
                      ),
                      CustomTextField(
                          hint: "Amount",
                          height: 40.0,
                          textInputType: TextInputType.number,
                          textEditingController: controller.txtAmount,
                          maxLine: 1,
                          fontSize: 18.0,
                          isBold: false),
                      SizedBox(
                        height: 10,
                      ),
                      CustomText(
                        'Unit',
                        color: Colors.black,
                        size: 20.0,
                        fontWeight: FontWeight.bold,
                        textAlign: TextAlign.left,
                      ),
                      SizedBox(
                        height: 50.0,
                        child: CustomDropDownList(
                          padding: EdgeInsets.only(top: 8.0),
                          context: context,
                          dropdownColor: Get.theme.primaryColorLight.withAlpha(150),
                          onChange: (c) {
                            controller.selectedUnit = '';

                          },
                          valueItem: "select",
                          items: ["select"],
                        ),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      CustomText(
                        'Price (Per Unit)',
                        color: Colors.black,
                        size: 20.0,
                        fontWeight: FontWeight.bold,
                        textAlign: TextAlign.left,
                      ),
                      CustomTextField(
                          hint: "Price",
                          height: 40.0,
                          textEditingController: controller.txtPricePerunit,
                          textInputType: TextInputType.number,

                          maxLine: 1,
                          fontSize: 18.0,
                          isBold: false),
                      SizedBox(
                        height: 10,
                      ),
                    ],
                  ),
                  SizedBox(
                      height: 40.0,
                      width: Get.width * .4,
                      child: CustomButton(
                        onTap: () {
                          controller.addStockApi();
                        },
                        type: 2,
                        borderRadios: 10.0,
                        child: CustomText(
                          'Add Stock',
                          color: Colors.black,
                          size: 18.0,
                          fontWeight: FontWeight.bold,
                        ),
                      ))
                ],
              );
  }

  GestureDetector stockItem(GetFoodIngredientsObject foodIngredient) {
    return GestureDetector(
      onTap: () {
        // Get.toNamed(Routes.StaffsDetailsPage);
      },
      child: Card(
        color: Get.theme.accentColor,
        elevation: 4,
        margin: EdgeInsets.all(8.0),
        child: Container(
          height: 80.0,
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    CustomText(
                      foodIngredient.ingredient?.ingredient?.name ?? '',
                      color: Colors.black,
                      size: 20.0,
                      fontWeight: FontWeight.bold,
                    ),
                    CustomText(
                      foodIngredient?.value?.toString() ??
                          '' ??
                          foodIngredient?.value.toString(),
                      color: Colors.black,
                      size: 20.0,
                      fontWeight: FontWeight.normal,
                    ),
                  ],
                ),
                IconButton(
                  iconSize: 32,
                  icon: Icon(Icons.delete),
                  onPressed: () {
                    controller.removeFoodIngredientApi(foodIngredient?.id);


                  },
                  color: Colors.red,
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

}
