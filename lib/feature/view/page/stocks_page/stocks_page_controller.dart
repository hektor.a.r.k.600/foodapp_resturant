import 'package:flutter/material.dart';
import 'package:food_resturant/core/config/api_address.dart';
import 'package:food_resturant/feature/depenency_injection.dart';
import 'package:food_resturant/feature/entities/get_food_ingredients_entity.dart';
import 'package:food_resturant/feature/entities/ingredient.dart';
import 'package:food_resturant/feature/entities/public_entity.dart';
import 'package:food_resturant/feature/server/api_helper.dart';
import 'package:food_resturant/feature/server/api_response.dart';
import 'package:food_resturant/feature/server/params/add_food_ingredient_params.dart';
import 'package:food_resturant/feature/server/params/public_params.dart';
import 'package:food_resturant/feature/server/params/remove_food_ingredients_params.dart';
import 'package:food_resturant/library/platforms/loading_overlay.dart';
import 'package:food_resturant/library/widgets/snackbar.dart';
import 'package:food_resturant/library/widgets/text_widget.dart';
import 'package:get/get.dart';
import 'package:uuid/uuid.dart';

class StocksPageController extends GetxController {
  final overlay = LoadingOverlay.of(Get.context);

  RxList<GetFoodIngredientsObject> foodIngredients =
      <GetFoodIngredientsObject>[].obs;

  TextEditingController txtStockName = TextEditingController(text: '');
  TextEditingController txtAmount = TextEditingController(text: '');
  TextEditingController txtPricePerunit = TextEditingController(text: '');

  String selectedUnit;

  @override
  void onReady() {
    getFoodIngredientsApi();
  }

  Future getFoodIngredientsApi() async {
    var publicParams = PublicParams();

    final response = await overlay.during(sl<ApiHelper>()
        .getWithParam(ApiAddress.GET_FOOD_INGREDIENT, publicParams.toJson()));
    var result = ApiResponse.returnResponse(
        response, GetFoodIngredientsEntity.fromJson(response.data));
    if (result.status == ApiStatus.COMPLETED && result?.data?.status) {
      foodIngredients.value = result.data?.getFoodIngredientsObject;
    } else {
      showSnackBar(text: result?.data?.message ?? '');
    }
  }

  addStockApi() async {
    var addStockParams = AddFoodIngredientParams(
      value: int.tryParse(txtAmount.text),
      ingredient: Ingredients(
          id: Uuid().v4(),
          createdAt: 111111111,
          ingredient: Ingredient(name: txtStockName.text ?? '')),
      unit: 'test',
    );

    final response = await overlay.during(sl<ApiHelper>()
        .post(ApiAddress.ADD_FOOD_INGREDIENT, addStockParams.toJson()));
    var result = ApiResponse.returnResponse(
        response, GetFoodIngredientsEntity.fromJson(response.data));
    if (result.status == ApiStatus.COMPLETED) {
      getFoodIngredientsApi();
      // showSnackBar(text: result?.data?.message ?? '');
      Get.back();
    } else {
      showSnackBar(text: result?.data?.message ?? '');
    }
  }

  removeFoodIngredientApi(String id) async {
    var addStockParams = RemoveFoodIngredientsParams(foodIngredientId: id);

    final response = await overlay.during(sl<ApiHelper>()
        .post(ApiAddress.REMOVE_FOOD_INGREDIENT, addStockParams.toJson()));
    var result = ApiResponse.returnResponse(
        response, PublicEntity.fromJson(response.data));
    if (result.status == ApiStatus.COMPLETED && result?.data?.status) {
      // showSnackBar(text: result?.data?.message ?? '');
      getFoodIngredientsApi();
    } else {
      showSnackBar(text: result?.data?.message ?? '');
    }
  }
}
