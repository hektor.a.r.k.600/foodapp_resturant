import 'package:flutter/material.dart';
import 'package:food_resturant/core/config/config.dart';
import 'package:food_resturant/core/config/routes.dart';
import 'package:food_resturant/feature/entities/get_tables_entity.dart';
import 'package:food_resturant/library/enums/enums.dart';
import 'package:food_resturant/library/extention/enum_extention.dart';
import 'package:food_resturant/library/extention/int_extention.dart';
import 'package:food_resturant/feature/view/page/tables_page/tables_page_controller.dart';
import 'package:food_resturant/feature/view/widget/app_bar.dart';
import 'package:food_resturant/feature/view/widget/box_shadow_widget.dart';
import 'package:food_resturant/library/widgets/button_widget.dart';
import 'package:food_resturant/library/widgets/cached_network_image_widget.dart';
import 'package:food_resturant/library/widgets/custom_drop_list/custom_drop_down_list.dart';
import 'package:food_resturant/library/widgets/text_widget.dart';
import 'package:get/get.dart';
import 'package:qr_flutter/qr_flutter.dart';

class TablesPage extends StatelessWidget {
  final controller = Get.put(TablesPageController());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // floatingActionButton: FloatingActionButton(
      //   onPressed: () {
      //     showAddTableButtomSheet(context, controller);
      //   },
      //   backgroundColor: Get.theme.primaryColor,
      //   child: Center(
      //     child: Icon(
      //       Icons.add,
      //       size: 30.0,
      //     ),
      //   ),
      // ),
      body: Column(
        children: [
          BuildAppBarWidget(
            title: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                CustomText(
                  'Store Tables',
                  color: Colors.white,
                  size: 25.0,
                  fontWeight: FontWeight.bold,
                ),
                SizedBox(
                  height: 2.0,
                ),
                // CustomText(
                //   'View, Edit, Add, Remove Tables',
                //   color: Colors.white,
                //   size: 18.0,
                //   fontWeight: FontWeight.w500,
                // ),
              ],
            ),
          ),
          Expanded(
            child: Container(
              margin: const EdgeInsets.only(
                  right: 16.0, left: 16.0, top: 16.0, bottom: 90.0),
              decoration: BoxDecoration(
                color: Get.theme.accentColor,
                boxShadow: [buildBoxShadow()],
                borderRadius: BorderRadius.circular(10.0),
              ),
              child: GetBuilder<TablesPageController>(
                  init: TablesPageController(),
                  builder: (controller) {
                    return Obx(() => GridView.builder(
                          shrinkWrap: true,
                          padding: EdgeInsets.all(8.0),
                          gridDelegate:
                              SliverGridDelegateWithFixedCrossAxisCount(
                                  crossAxisCount: 3),
                          itemCount: controller?.tableObjects?.length ?? 0,
                          itemBuilder: (context, index) {
                            return Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: buildTableWidget(
                                  controller, controller.tableObjects[index]),
                            );
                          },
                        ));
                  }),
            ),
          )
        ],
      ),
    );
  }

  buildTableWidget(TablesPageController controller, TableObject tableObject) {
    return InkWell(
      onTap: () {
        // Get.toNamed(Routes.OrderByTable , arguments: tableObject);
        showTableInformation(tableObject);
      },
      child: Container(
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(10.0),
              border: Border.all(
                  color: tableObject.status.colorTableStatus, width: 2)),
          child: Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                CustomText(
                  tableObject.name.toString(),
                  color: tableObject.status.colorTableStatus,
                  size: 18.0,
                ),
                CustomText(
                  'no:' + tableObject.no.toString(),
                  color: tableObject.status.colorTableStatus,
                  size: 18.0,
                  fontWeight: FontWeight.bold,
                ),
                CustomText(
                  'cap:' + tableObject.sitsCount.toString(),
                  color: tableObject.status.colorTableStatus,
                  size: 18.0,
                ),
                SizedBox(height: 5,),
                CustomText(
                  tableObject.status.stringTableStatus?.toUpperCase(),
                  color: tableObject.status.colorTableStatus,
                  size: 18.0,
                ),
              ],
            ),
          )),
    );
  }

  changeStatusButton(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 15.0),
      child: SizedBox(
          width: Get.width * 0.5,
          height: 40.0,
          child: CustomButton(
            borderRadios: 10.0,
            onTap: () {
              // showTableInformation(context);
            },
            type: 2,
            color: Colors.red,
            splashColor: Colors.redAccent,
            child: CustomText(
              'Change Table Status',
              size: 18.0,
              fontWeight: FontWeight.bold,
            ),
          )),
    );
  }

  showTableInformation(TableObject tableObject) {
    return showModalBottomSheet<String>(
      context: Get.context,
      builder: (context) => Container(
        child: Padding(
          padding: const EdgeInsets.only(top: 30.0),
          child: Column(
            children: [
              CustomText(
                'Table Information',
                color: Colors.black,
                size: 25.0,
                fontWeight: FontWeight.bold,
              ),
              SizedBox(
                height: 10.0,
              ),
              Container(
                  height: 200,
                  width: 200,
                  child: BuildCachedImageWidget(
                    defult:Icon(Icons.image_not_supported , size: 42,) ,
                    imageUrl: tableObject?.qrCodeImage?.location ?? '',
                    loadingWidth: 10,
                    height: 70.0,
                    width: 70.0,
                  )),

              // QrImage(
              //   data: tableObject.qrCode??'',
              //   version: QrVersions.auto,
              //   size: Get.width * 0.6,
              // ),
              SizedBox(
                height: 20.0,
              ),

              controller?.user?.loginObject?.roleId?.enumUserRole ==
                      UserRole.STORE_OWNER
                  ? SizedBox()
                  : SizedBox(
                      width: Get.width * 0.5,
                      height: 40.0,
                      child: CustomButton(
                        borderRadios: 10.0,
                        onTap: () {
                          showChangeTableStatusBottomSheet(
                              context, tableObject);
                        },
                        type: 2,
                        color: Colors.red,
                        splashColor: Colors.redAccent,
                        child: CustomText(
                          'Change Table Status',
                          size: 18.0,
                          fontWeight: FontWeight.bold,
                        ),
                      )),
              SizedBox(
                height: 10.0,
              ),

              SizedBox(
                  width: Get.width * 0.5,
                  height: 40.0,
                  child: CustomButton(
                    borderRadios: 10.0,
                    onTap: () {
                      Get.toNamed(Routes.OrderByTable, arguments: tableObject);
                    },
                    type: 2,
                    color: Colors.green,
                    splashColor: Colors.greenAccent,
                    child: CustomText(
                      'Orders',
                      size: 18.0,
                      fontWeight: FontWeight.bold,
                    ),
                  )),
            ],
          ),
        ),
      ),
    );
  }

  showChangeTableStatusBottomSheet(
      BuildContext context, TableObject tableObject) {
    return showModalBottomSheet<String>(
      context: context,
      builder: (context) => Container(
        child: Padding(
          padding: const EdgeInsets.only(top: 30.0),
          child: Column(
            children: [
              CustomText(
                'Change Table Status',
                color: Colors.black,
                size: 25.0,
                fontWeight: FontWeight.bold,
              ),
              GetBuilder<TablesPageController>(
                  init: TablesPageController(),
                  builder: (x) {
                    return Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Row(
                        children: [
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: controller
                                .tableStatusRadioWidgetBuilder(context),
                          ),
                        ],
                      ),
                    );
                  }),
              SizedBox(
                height: 50.0,
              ),
              SizedBox(
                  width: Get.width * 0.4,
                  height: 40.0,
                  child: CustomButton(
                    borderRadios: 10.0,
                    onTap: () {
                      controller.changeStatus(tableObject);
                    },
                    type: 2,
                    child: CustomText(
                      'Change Status',
                      color: Colors.black,
                      size: 18.0,
                      fontWeight: FontWeight.bold,
                    ),
                  ))
            ],
          ),
        ),
      ),
    );
  }
}
