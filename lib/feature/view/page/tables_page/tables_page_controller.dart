import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:food_resturant/core/config/api_address.dart';
import 'package:food_resturant/core/config/config.dart';
import 'package:food_resturant/feature/depenency_injection.dart';
import 'package:food_resturant/feature/entities/get_tables_entity.dart';
import 'package:food_resturant/feature/entities/login_entity.dart';
import 'package:food_resturant/feature/entities/public_entity.dart';
import 'package:food_resturant/feature/server/params/change_table_status_params.dart';
import 'package:food_resturant/library/extention/enum_extention.dart';
import 'package:food_resturant/feature/server/api_helper.dart';
import 'package:food_resturant/feature/server/api_response.dart';
import 'package:food_resturant/feature/server/params/get_tables_params.dart';
import 'package:food_resturant/library/enums/enums.dart';
import 'package:food_resturant/feature/server/params/change_table_status_params.dart';
import 'package:food_resturant/library/platforms/loading_overlay.dart';
import 'package:food_resturant/library/platforms/shared_preferences.dart';
import 'package:food_resturant/library/widgets/snackbar.dart';
import 'package:food_resturant/library/widgets/text_widget.dart';
import 'package:food_resturant/library/extention/int_extention.dart';
import 'package:get/get.dart';

class TablesPageController extends GetxController {
  final overlay = LoadingOverlay.of(Get.context);
  final tableObjects = [].obs;
  List<String> tableCapacity = [
    "2 Person",
    "3 Person",
    "4 Person",
    "5 Person",
    "6 Person",
    "7 Person",
    "8 Person",
  ];

  var tableStatusId;

  LoginEntity user;


  List<Widget> tableStatusRadioWidgetBuilder(BuildContext context) {
    List<Widget> tableStatusRadioWidgets = List<Widget>();
    for (TableStatus item in TableStatus.values) {
      if (user?.loginObject?.roleId?.enumUserRole == UserRole.WAITER ) {
        if (item == TableStatus.NOT_CLEAN || item == TableStatus.ACTIVE) {
          tableStatusRadioWidgets.add(tableStatusRadioWidget(context, item));
        }
      } else if(user?.loginObject?.roleId?.enumUserRole == UserRole.CASHIER){
        if (item == TableStatus.FULL || item == TableStatus.NOT_CLEAN) {
          tableStatusRadioWidgets.add(tableStatusRadioWidget(context, item));
        }
      }
    }
    return tableStatusRadioWidgets;
  }



  Widget tableStatusRadioWidget(
      BuildContext context, TableStatus tablesStatus) {
    return SizedBox(
        height: 30.0,
        width: MediaQuery.of(context).size.width * 0.5,
        child: Row(
          children: [
            Radio(
              groupValue: tableStatusId,
              onChanged: onTableStatusRadioChange,
              value: tablesStatus.index,
              activeColor: Get.theme.primaryColor,
            ),
            CustomText(
              tablesStatus.stringTableStatus,
              color: Colors.black,
              size: 18.0,
              fontWeight: FontWeight.bold,
            ),
          ],
        ));
  }


  void onTableStatusRadioChange(id) {
    tableStatusId = id;
    print(tableStatusId);
    update();
  }


  @override
  void onInit() {
    user = LoginEntity.fromJson(json.decode(sl<DataStorage>().getData(key: USER)));    
    super.onInit();
  }

  @override
  void onReady() {
    callGetTables();
  }

  callGetTables() async{
    tableObjects.value = [];
    // var tablesParams = GetTablesParams();

    final response = await overlay.during(sl<ApiHelper>()
        .get(ApiAddress.Get_TABLE));
    var result = ApiResponse.returnResponse(
        response, GetTablesEntity.fromJson(response.data));
    if (result.status == ApiStatus.COMPLETED &&
        result?.data?.status) {
      tableObjects.value = result?.data?.tableObject;
    } else {
      showSnackBar(text: result?.data?.message ?? '');
    }


  }

  changeStatus(TableObject tablesModel) async {
    var tablesParams = ChangeTableStatusParams(tableId: tablesModel?.id ,status: (tableStatusId + 1) ??1 );

    final response = await overlay.during(sl<ApiHelper>()
        .post(ApiAddress.CHANGE_TABLE_STATUS , tablesParams.toJson() , true));
    var result = ApiResponse.returnResponse(
        response, PublicEntity.fromJson(response.data));
    if (result.status == ApiStatus.COMPLETED &&
        result?.data?.status) {
        Get.back();
        callGetTables();

    } else {
      showSnackBar(text: result?.data?.message ?? '');
    }





  }







}