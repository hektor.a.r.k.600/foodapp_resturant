import 'package:flutter/material.dart';
import 'package:food_resturant/feature/entities/get_store_withdraw_requests_entity.dart';
import 'package:food_resturant/feature/view/page/withdraw_page/withdraw_page_controller.dart';
import 'package:food_resturant/feature/view/widget/app_bar.dart';
import 'package:food_resturant/library/platforms/conver_date.dart';
import 'package:food_resturant/library/widgets/button_widget.dart';
import 'package:food_resturant/library/widgets/text_field_widget.dart';
import 'package:food_resturant/library/widgets/text_widget.dart';
import 'package:food_resturant/library/extention/enum_extention.dart';
import 'package:get/get.dart';

class WithdrawPage extends StatelessWidget {
  final controller = Get.put(WithdrawPageController());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        floatingActionButton: FloatingActionButton(
          onPressed: () {
            showChangeOrderStatusBottomSheet(context, controller);
          },
          backgroundColor: Get.theme.primaryColor,
          child: Center(
            child: Icon(Icons.add),
          ),
        ),
        appBar: AppBar(
          iconTheme: IconThemeData(opacity: 0.0),
          toolbarHeight: 190.0,
          backgroundColor: Get.theme.scaffoldBackgroundColor,
          elevation: 0.0,
          bottom: TabBar(
              controller: controller.tabController,
              indicatorColor: Get.theme.primaryColor,
              tabs: controller.tabBuilder()),
          flexibleSpace: BuildAppBarWidget(
            title: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                CustomText(
                  'Withdrawal',
                  color: Colors.white,
                  size: 25.0,
                  fontWeight: FontWeight.bold,
                ),
                SizedBox(
                  height: 2.0,
                ),
                CustomText(
                  'View & Withdrawal your Money',
                  color: Colors.white,
                  size: 18.0,
                  fontWeight: FontWeight.w500,
                ),
              ],
            ),
          ),
        ),
        body: Obx(() =>TabBarView(
          controller: controller.tabController,
          children: [
            RefreshIndicator(
              backgroundColor: Get.theme.primaryColorDark,
              onRefresh: () {
                return controller.getWithdrawRequest();
              },
              child: ListView.builder(
                padding: EdgeInsets.all(5),
                itemCount: controller.allWithdraw?.value?.length ?? 0,
                itemBuilder: (context, index) =>
                    buildListItem(controller.allWithdraw?.value[index]),
              ),
            ),
            RefreshIndicator(
              backgroundColor: Get.theme.primaryColorDark,
              onRefresh: () {
                return controller.getWithdrawRequest();
              },
              child: ListView.builder(
                padding: EdgeInsets.all(5),
                itemCount: controller?.pending?.value?.length ?? 0,
                itemBuilder: (context, index) =>
                    buildListItem(controller?.pending?.value[index]),
              ),
            ),
            RefreshIndicator(
              backgroundColor: Get.theme.primaryColorDark,
              onRefresh: () {
                return controller.getWithdrawRequest();
              },
              child: ListView.builder(
                padding: EdgeInsets.all(5),
                itemCount: controller?.paid?.value?.length ?? 0,
                itemBuilder: (context, index) =>
                    buildListItem(controller?.paid?.value[index]),
              ),
            ),
            RefreshIndicator(
              backgroundColor: Get.theme.primaryColorDark,
              onRefresh: () {
                return controller.getWithdrawRequest();
              },
              child: ListView.builder(
                padding: EdgeInsets.all(5),
                itemCount: controller?.rejected?.value?.length ?? 0,
                itemBuilder: (context, index) =>
                    buildListItem(controller?.rejected?.value[index]),
              ),
            ),
          ],
        )));
  }

  //todo add item object instead of index
  InkWell buildListItem(WithDrawGetStoreWithDrawRequestsObject object) {
    return InkWell(
      onTap: () {},
      child: Card(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(15.0),
        ),
        color: Get.theme.accentColor,
        elevation: 4,
        margin: EdgeInsets.all(5.0),
        child: Row(
          mainAxisSize: MainAxisSize.min,
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Container(
              height: 80.0,
              width: Get.width / 10.5,
              decoration: BoxDecoration(
                  color: object?.status?.colorWithdrawStatus,
                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(15),
                      bottomLeft: Radius.circular(15))),
              child: Center(
                child: RotatedBox(
                  quarterTurns: -45,
                  child: CustomText(
                    object.status.withdrawStatusToString??'',
                    size: 18.0,
                    color: Colors.black,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
            ),
            SizedBox(
              width: 10.0,
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                CustomText(
                  object?.value?.toString(),
                  color: Colors.black,
                  size: 16.0,
                  fontWeight: FontWeight.normal,
                ),
                SizedBox(
                  width: 10.0,
                ),
                CustomText(
                  convertMilliSecToDate(object?.createdAt),
                  color: Colors.black,
                  size: 18.0,
                  fontWeight: FontWeight.bold,
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }

  showChangeOrderStatusBottomSheet(
      BuildContext context, WithdrawPageController controller) {
    return showModalBottomSheet<String>(
      enableDrag: true,
      context: context,
      builder: (context) => Container(
        padding: const EdgeInsets.all(15.0),
        child: Column(
          children: [
            Expanded(
              child: ListView(
                children: [
                  CustomText(
                    'Create Withdrawal Request',
                    color: Colors.black,
                    size: 30.0,
                    fontWeight: FontWeight.bold,
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  GetBuilder<WithdrawPageController>(builder: (x) {
                    return Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Column(
                        children: x
                            .withdrawRequestRadioBuilder(x.withdrawRequestRadioTitle),




                      ),
                    );
                  }),

                  CustomTextField(
                      hint: "Value",
                      height: 40.0,
                      textInputType: TextInputType.number,
                      maxLine: 1,
                      textEditingController: controller.txtValueController,
                      fontSize: 18.0,
                      isBold: false),

                ],
              ),
            ),
            SizedBox(
                height: 40.0,
                width: Get.width * .5,
                child: CustomButton(
                  onTap: () {
                    controller.addWithdrawRequest();
                  },
                  type: 2,
                  borderRadios: 10.0,
                  child: CustomText(
                    'Add Withdraw Request',
                    color: Colors.black54,
                    size: 18.0,
                    fontWeight: FontWeight.bold,
                  ),
                ))
          ],
        ),
      ),
    );
  }
}
