import 'package:flutter/material.dart';
import 'package:food_resturant/core/config/api_address.dart';
import 'package:food_resturant/core/config/global_setting.dart';
import 'package:food_resturant/feature/depenency_injection.dart';
import 'package:food_resturant/feature/entities/get_store_withdraw_requests_entity.dart';
import 'package:food_resturant/feature/entities/login_entity.dart';
import 'package:food_resturant/feature/entities/public_entity.dart';
import 'package:food_resturant/feature/entities/user.dart';
import 'package:food_resturant/feature/server/api_helper.dart';
import 'package:food_resturant/feature/server/api_response.dart';
import 'package:food_resturant/feature/server/params/get_withdraw_store_request.dart';
import 'package:food_resturant/feature/server/params/with_draw_add_store_with_draw_request_params.dart';
import 'package:food_resturant/library/enums/enums.dart';
import 'package:food_resturant/library/extention/enum_extention.dart';
import 'package:food_resturant/library/extention/list_extention.dart';
import 'package:food_resturant/library/platforms/loading_overlay.dart';
import 'package:food_resturant/library/widgets/snackbar.dart';
import 'package:food_resturant/library/widgets/text_widget.dart';
import 'package:get/get.dart';

class WithdrawPageController extends GetxController
    with SingleGetTickerProviderMixin {
  final overlay = LoadingOverlay.of(Get.context);

  List<String> withdrawRequestRadioTitle = List<String>(2);
  int selectedWithdrawRequest = 0;
  LoginObject user;

  List<String> tabsTitle = List<String>(4);
  TabController tabController;

  RxList<WithDrawGetStoreWithDrawRequestsObject> allWithdraw =
      <WithDrawGetStoreWithDrawRequestsObject>[].obs;
  RxList<WithDrawGetStoreWithDrawRequestsObject> pending =
      <WithDrawGetStoreWithDrawRequestsObject>[].obs;
  RxList<WithDrawGetStoreWithDrawRequestsObject> rejected =
      <WithDrawGetStoreWithDrawRequestsObject>[].obs;
  RxList<WithDrawGetStoreWithDrawRequestsObject> paid =
      <WithDrawGetStoreWithDrawRequestsObject>[].obs;

  TextEditingController txtValueController = TextEditingController(text: '');

  addItemsToTabs() {
    tabsTitle[0] = WithdrawStatus.ALL.withdrawStatusToString;
    tabsTitle[1] = WithdrawStatus.PENDING.withdrawStatusToString;
    tabsTitle[2] = WithdrawStatus.PAID.withdrawStatusToString;
    tabsTitle[3] = WithdrawStatus.REJECTED.withdrawStatusToString;
  }

  List<Widget> tabBuilder() {
    List<Widget> listTabs = List<Widget>();
    tabsTitle.forEach((title) {
      listTabs.add(
        Padding(
          padding: const EdgeInsets.symmetric(vertical: 10),
          child: CustomText(
            title,
            color: Colors.black,
            size: 18.0,
            fontWeight: FontWeight.bold,
          ),
        ),
      );
    });
    return listTabs;
  }

  addWithdrawRequestToRadios() {
    withdrawRequestRadioTitle[0] = ("Cheque");
    withdrawRequestRadioTitle[1] = ("Cash");
  }

  List<Widget> withdrawRequestRadioBuilder(List<String> radiosTitles) {
    List<Widget> listRadios = List<Widget>();
    for (var i = 0; i < radiosTitles.length; i++) {
      listRadios.add(
        Row(
          children: [
            SizedBox(
              height: 30.0,
              width: 30.0,
              child: Radio(
                activeColor: Get.theme.primaryColor,
                value: i,
                groupValue: selectedWithdrawRequest,
                onChanged: radioButtonOnChange,
              ),
            ),
            CustomText(
              radiosTitles[i],
              color: Colors.black,
              size: 18.0,
              fontWeight: FontWeight.normal,
            )
          ],
        ),
      );
    }

    return listRadios;
  }

  radioButtonOnChange(var selectedId) {
    selectedWithdrawRequest = selectedId;
    update();
  }

  @override
  void onInit() {
    user = sl<GlobalSetting>()?.user;

    tabController = TabController(length: 4, vsync: this);
    addItemsToTabs();
    addWithdrawRequestToRadios();

    super.onInit();
  }

  @override
  void onReady() {
    getWithdrawRequest();
  }

  getWithdrawRequest() async {
    var getWithdrawStoreRequestParams =
        GetWithdrawStoreRequestParams(storeId: user?.store?.id);

    final response = await overlay.during(sl<ApiHelper>().getWithParam(
        ApiAddress.GET_STORE_WITHDRAW_REQUEST,
        getWithdrawStoreRequestParams.toJson()));
    var result = ApiResponse.returnResponse(
        response, GetStoreWithDrawRequestsEntity.fromJson(response.data));
    if (result.status == ApiStatus.COMPLETED && result?.data?.status) {
      allWithdraw.value = [];
      paid.value = [];
      rejected.value = [];
      pending.value = [];

      allWithdraw.value = result.data?.withDrawGetStoreWithDrawRequestsObject;
      pending.value = result.data?.withDrawGetStoreWithDrawRequestsObject
          ?.where((arg1) => arg1.status == WithdrawStatus.PENDING)
          ?.toList();

      rejected.value = result.data?.withDrawGetStoreWithDrawRequestsObject
          ?.where((arg1) => arg1.status == WithdrawStatus.REJECTED)
          ?.toList();

      paid.value = result.data?.withDrawGetStoreWithDrawRequestsObject
          ?.where((arg1) => arg1.status == WithdrawStatus.PAID)
          ?.toList();
      // allWithdraw = result.data?.withDrawGetStoreWithDrawRequestsObject;
      // allWithdraw = result.data?.withDrawGetStoreWithDrawRequestsObject;

      // return result?.data;
    }
  }

  addWithdrawRequest() async {
    if (txtValueController.text.isEmpty) {
      showSnackBar(text: 'please enter value');
      return;
    }
    var withDrawAddStoreWithDrawRequestParams =
        WithDrawAddStoreWithDrawRequestParams(
            storId: user?.store?.id,
            user: User(
                id: user?.id ?? "",
                userName: user?.userName ?? "",
                familyName: user?.mobile ?? "",
                name: user?.familyName ?? "",
                profileImage: user?.profileImage?.address ?? ""),
            value: int.tryParse(txtValueController.text),
            reason: withdrawRequestRadioTitle[selectedWithdrawRequest]);

    final response = await overlay.during(sl<ApiHelper>().post(
        ApiAddress.ADD_STORE_WITHDRAW_REQUEST,
        withDrawAddStoreWithDrawRequestParams.toJson()));
    var result = ApiResponse.returnResponse(
        response, PublicEntity.fromJson(response.data));
    if (result.data.status == true) {
      Get.back();
      getWithdrawRequest();
    } else {
      showSnackBar(
          text: result?.data?.message ?? '', snackBarType: SnackBarType.Report);
    }
  }
}
