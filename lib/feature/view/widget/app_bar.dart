import 'package:flutter/material.dart';
import 'package:get/get.dart';

class BuildAppBarWidget extends StatelessWidget {
  final Widget title;
  final Function onBack;
  final EdgeInsets padding;
  final double appBarHeight;

  const BuildAppBarWidget(
      {Key key,
      @required this.title,
      this.onBack,
      this.padding,
      this.appBarHeight})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: appBarHeight ?? 150.0,
      width: MediaQuery.of(context).size.width,
      color: Get.theme.primaryColor,
      padding: padding ?? EdgeInsets.only(top: 20.0, left: 15.0, right: 15.0),
      child: Stack(
        children: [
          GestureDetector(
            onTap: onBack ??
                () {
                  Get.back();
                },
            child: Align(
              alignment: Alignment.topLeft,
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Icon(
                  Icons.arrow_back,
                  color: Get.theme.accentColor,
                ),
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(16.0),
            child: Center(child: title),
          ),
        ],
      ),
    );
  }
}
