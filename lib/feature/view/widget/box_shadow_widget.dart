import 'package:flutter/material.dart';
import 'package:get/get.dart';

BoxShadow buildBoxShadow() {
  return BoxShadow(
      color: Get.theme.primaryColorDark.withOpacity(0.2),
      blurRadius: 6.0,
      offset: Offset(0.0, 3.0));
}
