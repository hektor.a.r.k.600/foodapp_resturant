enum UserRole {
  ALL,
  USER,
  ADMIN,
  STORE_OWNER,
  WAITER,
  CASHIER,
  DELIVERY,
}

enum TableStatus {
  ACTIVE,
  DEACTIVE,
  FULL,
  NOT_CLEAN,
}

enum StoreStatus {
  DEACTIVE,
  ACTIVE,
  SUSPENDED,
}

enum OrderTypes {
  ALL,
  DELIVERY,
  SERVEIN,
  TAKEOUT,
}
enum OrderStatus {
  ALL,
  PENDING,
  COOKING,
  SERVED,
  INDELIVERY,
  PICKEDUP,
  DELIVERED,
  COMPLETED,
  CANCELED,
  PAID
}
enum ChashAprovalStatus {
  OrderIsNotCash,
  Pending,
  Aproved,
}
enum FoodStatus {
  ACTIVE,
  FINISHED,
}

enum WithdrawStatus {
  ALL,
  PENDING,
  REJECTED,
  PAID,
}
