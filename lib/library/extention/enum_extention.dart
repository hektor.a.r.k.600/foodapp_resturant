import 'package:flutter/material.dart';
import 'package:food_resturant/feature/view/page/tables_page/tables_page.dart';
import 'package:food_resturant/library/enums/enums.dart';
import 'package:food_resturant/library/enums/enums.dart';

extension UserRoleEnum on UserRole {
  int get userRoleToInt {
    switch (this) {
      case UserRole.ALL:
        return -1;
        break;
      case UserRole.USER:
        return 1;
        break;
      case UserRole.ADMIN:
        return 2;
      case UserRole.STORE_OWNER:
        return 3;
      case UserRole.WAITER:
        return 4;
      case UserRole.CASHIER:
        return 5;
      case UserRole.DELIVERY:
        return 6;
        break;
    }
  }
}

extension TableStatusEnum on TableStatus {
  Color get colorTableStatus {
    switch (this) {
      case TableStatus.ACTIVE:
        return Colors.green;
        break;
      case TableStatus.DEACTIVE:
        return Colors.red;
        break;
      case TableStatus.NOT_CLEAN:
        return Colors.orange;
        break;
      case TableStatus.FULL:
        return Colors.yellow;
        break;
      default:
        return Colors.grey;
        break;
    }
  }

  String get stringTableStatus {
    switch (this) {
      case TableStatus.ACTIVE:
        return 'Active';
        break;
      case TableStatus.DEACTIVE:
        return 'DeActive';
        break;
      case TableStatus.NOT_CLEAN:
        return 'Not Clean';
        break;
      case TableStatus.FULL:
        return 'Full';
        break;
    }
  }
}

extension OrderTypeEnum on OrderTypes {
  int get orderTypeToInt {
    switch (this) {
      case OrderTypes.ALL:
        return -1;
      case OrderTypes.DELIVERY:
        return 1;
        break;
      case OrderTypes.SERVEIN:
        return 2;
        break;
      case OrderTypes.TAKEOUT:
        return 3;
        break;
    }
  }

  String get orderTypeToString {
    switch (this) {
      case OrderTypes.ALL:
        return 'All';
        break;
      case OrderTypes.DELIVERY:
        return 'Delivery';
        break;
      case OrderTypes.SERVEIN:
        return 'Serve In';
        break;
      case OrderTypes.TAKEOUT:
        return 'Takeout';
        break;
        defalut:
        return 'All';
        break;
    }
  }
}

extension OrderStatusEnum on OrderStatus {
  int get orderStatusToInt {
    switch (this) {
      case OrderStatus.ALL:
        return -1;
      case OrderStatus.PENDING:
        return 1;
        break;
      case OrderStatus.COOKING:
        return 2;
        break;
      case OrderStatus.SERVED:
        return 3;
      case OrderStatus.INDELIVERY:
        return 4;
      case OrderStatus.PICKEDUP:
        return 5;
      case OrderStatus.DELIVERED:
        return 6;
      case OrderStatus.COMPLETED:
        return 7;
      case OrderStatus.CANCELED:
        return 8;
      case OrderStatus.PAID:
        return 9;
        break;
    }
  }

  String get orderStatusToString {
    switch (this) {
      case OrderStatus.PAID:
        return 'Paid';
      case OrderStatus.PENDING:
        return 'Pending';
        break;
      case OrderStatus.COOKING:
        return 'Cooking';
        break;
      case OrderStatus.SERVED:
        return 'Served';
        break;
      case OrderStatus.INDELIVERY:
        return 'InDelivery';
      case OrderStatus.PICKEDUP:
        return 'Picked Up';
      case OrderStatus.DELIVERED:
        return 'Deliverd';
      case OrderStatus.COMPLETED:
        return 'Completed';
      case OrderStatus.CANCELED:
        return 'Cancelled';
      case OrderStatus.ALL:
        return 'All';
    }
    return '';
  }
}

extension WithdrawStatusEnum on WithdrawStatus {
  int get withdrawStatusToInt {
    switch (this) {
      case WithdrawStatus.ALL:
        return 0;
      case WithdrawStatus.PENDING:
        return 1;
        break;
      case WithdrawStatus.PAID:
        return 2;
        break;
      case WithdrawStatus.REJECTED:
        return 3;
        break;
    }
  }

  String get withdrawStatusToString {
    switch (this) {
      case WithdrawStatus.ALL:
        return 'All';
      case WithdrawStatus.PENDING:
        return 'Pending';
        break;
      case WithdrawStatus.REJECTED:
        return 'Rejected';
        break;
      case WithdrawStatus.PAID:
        return 'Paid';
        break;
    }
    return '';
  }

  Color get colorWithdrawStatus {
    switch (this) {
      case WithdrawStatus.ALL:
        return Colors.grey;
      case WithdrawStatus.PAID:
        return Colors.green;
        break;
      case WithdrawStatus.REJECTED:
        return Colors.red;
        break;
      case WithdrawStatus.PENDING:
        return Colors.orange;
        break;
    }
    return null;
  }
}

extension FoodStatusEnum on FoodStatus {
  int get foodStatusToInt {
    switch (this) {
      case FoodStatus.ACTIVE:
        return 1;
      case FoodStatus.FINISHED:
        return 2;
        break;
    }
  }

  String get foodStatusToString {
    switch (this) {
      case FoodStatus.ACTIVE:
        return 'Active';
      case FoodStatus.FINISHED:
        return 'Finished';
        break;
    }
    return '';
  }
}

extension PeymentType on int {
  String get peymentTypeToString {
    switch (this) {
      case 1:
        return "Cash";
      case 2:
        return "Online";
        break;
    }
  }
}
