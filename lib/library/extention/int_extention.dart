import 'package:food_resturant/library/enums/enums.dart';

extension UserRoleId on int {
  UserRole get enumUserRole {
    switch (this) {
      case 0:
        return UserRole.ALL;
        break;
      case 1:
        return UserRole.USER;
        break;
      case 2:
        return UserRole.ADMIN;
        break;
      case 3:
        return UserRole.STORE_OWNER;
        break;
      case 4:
        return UserRole.WAITER;
        break;
      case 5:
        return UserRole.CASHIER;
        break;
      case 6:
        return UserRole.DELIVERY;
        break;
    }
  }
}

extension TableStatusId on int {
  TableStatus get enumTableStatus {
    switch (this) {
      case 1:
        return TableStatus.ACTIVE;
        break;
      case 2:
        return TableStatus.DEACTIVE;
        break;
      case 3:
        return TableStatus.FULL;
        break;
      case 4:
        return TableStatus.NOT_CLEAN;
        break;
    }
  }
}

extension OrderTypesId on int {
  OrderTypes get enumOrderTypes {
    switch (this) {
      case -1:
        return OrderTypes.ALL;
      case 1:
        return OrderTypes.DELIVERY;
        break;
      case 2:
        return OrderTypes.SERVEIN;
        break;
      case 3:
        return OrderTypes.TAKEOUT;
        break;
    }
  }
}

extension StoreStatusId on int {
  StoreStatus get enumStoreStatus {
    switch (this) {
      case 1:
        return StoreStatus.DEACTIVE;
        break;
      case 2:
        return StoreStatus.ACTIVE;
        break;
      case 3:
        return StoreStatus.SUSPENDED;
        break;
    }
  }
}

extension OrderStatusId on int {
  OrderStatus get enumOrderStatus {
    switch (this) {
      case 1:
        return OrderStatus.PENDING;
        break;
      case 2:
        return OrderStatus.COOKING;
        break;
      case 3:
        return OrderStatus.SERVED;
      case 4:
        return OrderStatus.INDELIVERY;
      case 5:
        return OrderStatus.PICKEDUP;
      case 6:
        return OrderStatus.DELIVERED;
      case 7:
        return OrderStatus.COMPLETED;
      case 8:
        return OrderStatus.CANCELED;
      case 9:
        return OrderStatus.PAID;
        break;
    }
  }
}

extension FoodStatusId on int {
  FoodStatus get enumFoodStatus {
    switch (this) {
      case 1:
        return FoodStatus.ACTIVE;
        break;
      case 2:
        return FoodStatus.FINISHED;
        break;
    }
  }
}

extension WithdrawStatusId on int {
  WithdrawStatus get enumWithdrawStatus {
    switch (this) {
      case 0:
        return WithdrawStatus.ALL;
      case 1:
        return WithdrawStatus.PENDING;
        break;
      case 2:
        return WithdrawStatus.PAID;
        break;
      case 3:
        return WithdrawStatus.REJECTED;
        break;
    }
  }
}
