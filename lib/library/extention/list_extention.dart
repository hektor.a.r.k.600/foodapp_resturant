import 'package:queries/collections.dart';

extension ListExtention<T> on List<T>{
  Collection<T> get listCollection =>Collection(this);

}