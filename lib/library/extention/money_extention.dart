import 'package:flutter_icons/flutter_icons.dart';
import 'package:intl/intl.dart';

extension MoneyExtention on num{
  String get moneyFormatter {
    String moneyFormatter;

    moneyFormatter = NumberFormat.currency(name: 'RM').format(this);
    return moneyFormatter;

    // moneyFormatter = 'RM${this}'

  }

}