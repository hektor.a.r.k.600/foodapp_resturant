class GenderTypes {
  static final int female = 1;
  static final int male = 2;
  static final int maleAndFemale = 3;
}
