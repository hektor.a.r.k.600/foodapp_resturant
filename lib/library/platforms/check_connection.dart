import 'dart:io';

import 'package:data_connection_checker/data_connection_checker.dart';
import 'package:flutter/cupertino.dart';

class CheckConnection {
  checkConnection({
    @required void Function() onConnected,
    @required void Function() onDisconnected,
  }) {
    const int DEFAULT_PORT = 53;

    const Duration DEFAULT_TIMEOUT = const Duration(seconds: 10);
    DataConnectionChecker().addresses = List.unmodifiable([
      AddressCheckOptions(
        InternetAddress('8.8.8.8'),
        port: DEFAULT_PORT,
        timeout: DEFAULT_TIMEOUT,
      ),
    ]);
    DataConnectionChecker().onStatusChange.listen((status) {
      switch (status) {
        case DataConnectionStatus.connected:
          onConnected();
          break;
        case DataConnectionStatus.disconnected:
          onDisconnected();
          break;
      }
    }, onDone: () {
      print("done");
    });
  }
}
