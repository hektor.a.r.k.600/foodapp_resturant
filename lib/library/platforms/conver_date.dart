import 'package:intl/intl.dart';

String convertMilliSecToDate(int second) {
  DateTime dateTime = DateTime.fromMillisecondsSinceEpoch(second * 1000).toLocal();
  return dateTime.year.toString() +
      "/" +
      dateTime.month.toString() +
      "/" +
      dateTime.day.toString();
}

String convertMilliSecToDateTime(num milliSec) {
  DateTime dateTime =
      DateTime.fromMillisecondsSinceEpoch(milliSec.toInt()).toLocal();
  DateTime now = DateTime.now().toLocal();
  Duration dif = now.difference(dateTime);
  if (dif.inDays < 1) {
    String dateFormat = DateFormat('hh:mm aaa').format(dateTime);
    return "Today at " + dateFormat;
  } else if (dif.inDays < 2) {
    String dateFormat = DateFormat('hh:mm aaa').format(dateTime);
    return "Yesterday at " + dateFormat;
  } else if (dif.inDays > 7) {
    String dateFormat = DateFormat('EEEEEEEEE - ' 'hh:mm aaa').format(dateTime);
    return dateFormat;
  } else {
    String dateFormatTime = DateFormat('hh:mm aaa').format(dateTime);
    String dateFormatDate = DateFormat('yyyy/MM/dd').format(dateTime);
    return dateFormatTime + " at " + dateFormatDate;
  }
}

String convertMilliSecToTime(int milliSec) {
  DateTime dateTime =
      DateTime.fromMillisecondsSinceEpoch(milliSec.toInt()).toLocal();
  String dateFormat = DateFormat("jm").format(dateTime);
  return dateFormat;
}
