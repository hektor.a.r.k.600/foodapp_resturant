import 'package:food_resturant/core/config/config.dart';
import 'package:food_resturant/core/error/exception.dart';
import 'package:food_resturant/core/error/failure.dart';

String failureToMassage(Failure failure) {
  switch (failure.runtimeType) {
    case ServerFailure:
      return MassageError.SERVER_FAILURE_MASSAGE;
    case OfflineFailure:
      return MassageError.OFFLINE_FAILURE_MASSAGE;
    case AuthenticationException:
      return MassageError.Authentication_Massage;
    case UnKnownUser:
      return MassageError.UNKNOWN_MASSAGE;
    default:
      return "Unexpected error";
  }
}
