import 'package:food_resturant/core/config/config.dart';
import 'package:food_resturant/feature/depenency_injection.dart';
import 'package:food_resturant/library/platforms/shared_preferences.dart';

class HeaderImpl {
  static Map<String, String> headersWithOutToken = {
    "Content-type": "application/json"
  };
  static Map<String, String> headersWithToken = {
    "Content-type": "application/json",
    "Authorization":
        "Bearer ${sl<DataStorage>().getData(key: USER_TOKEN)}"
  };
}
