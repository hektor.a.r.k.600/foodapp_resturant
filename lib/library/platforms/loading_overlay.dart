import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:data_connection_checker/data_connection_checker.dart';
import 'package:food_resturant/library/widgets/text_widget.dart';
import 'package:get/get.dart';
import 'dart:math';

class LoadingOverlay {
  BuildContext _context;

  void hide() {
    Navigator.of(_context).pop();
  }

  void show({bool isUpload = false}) {
    showDialog(
        context: _context,
        barrierDismissible: false,
        builder: (context) => _FullScreenLoader(
              isUpload: isUpload,
            ));
  }

  Future<T> during<T>(Future<T> future) {
    show();
    return future.whenComplete(() => hide());
  }

  LoadingOverlay._create(this._context);

  factory LoadingOverlay.of(BuildContext context) {
    return LoadingOverlay._create(context);
  }
}

class _FullScreenLoader extends StatelessWidget {
  bool isUpload;

  _FullScreenLoader({this.isUpload = false});

  @override
  Widget build(BuildContext context) {
    return Container(
        decoration: BoxDecoration(),
        child: Center(
          child: !isUpload
              ? CupertinoActivityIndicator(
                  animating: true,
                  radius: 10,
                )
              : Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    CustomText(
                      "Uploading Image...",
                      fontWeight: FontWeight.bold,
                      size: 18.0,
                    ),
                    SizedBox(height: 10,),
                    CircularProgressIndicator(
                    valueColor:AlwaysStoppedAnimation<Color>(Get.theme.primaryColor),),
                  ],
                ),
        ));
  }
}

Color overlayColor(BuildContext context, double elevation) {
  final ThemeData theme = Theme.of(context);
  final double opacity = (4.5 * log(elevation + 1) + 2) / 100.0;
  return theme.colorScheme.onSurface.withOpacity(opacity);
}
