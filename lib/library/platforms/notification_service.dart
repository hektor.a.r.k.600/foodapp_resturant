import 'dart:async';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:food_resturant/core/config/routes.dart';
import 'package:food_resturant/library/widgets/snackbar.dart';
import 'package:get/get.dart';

class NotificationService {
  final FirebaseMessaging firebaseMessaging = FirebaseMessaging.instance;

  Future<String> getPushId({String vapidKey}) async {
    return await firebaseMessaging.getToken(vapidKey: vapidKey);
  }

  Future initialise(
      {void Function(RemoteMessage) onMessage,
        void Function(RemoteMessage) onMessageOpenedApp,
        Future<void> Function(RemoteMessage) onBackgroundMessage}) async {
    await firebaseMessaging.requestPermission(

      alert: true,
      announcement: false,
      badge: true,
      carPlay: false,
      criticalAlert: false,
      provisional: false,
      sound: true,
    );


    FirebaseMessaging.onMessage.listen(onMessage ??
            (RemoteMessage message) {
          print('Got a message whilst in the foreground!');
          print('Message data: ${message.data}');
          // showSnackBar(text: message?.data?.);
          Get.toNamed(Routes.OrdersPage);

          if (message.notification != null) {
            print(
                'Message also contained a notification: ${message.notification}');

          }
        });
    FirebaseMessaging.onBackgroundMessage(
        onBackgroundMessage ?? _firebaseMessagingBackgroundHandler);
    FirebaseMessaging.onMessageOpenedApp.listen(onMessageOpenedApp ??
            (RemoteMessage message) {
      // Get.toNamed(Routes.SplashScreen);
          print('Got a message whilst in the onMessageOpenedApp!');
          print('Message data: ${message.data}');
            Get.toNamed(Routes.OrdersPage);

          if (message.notification != null) {
            print(
                'Message also contained a notification: ${message.notification}');
          }
        });
  }
}

Future<dynamic> _firebaseMessagingBackgroundHandler(
    RemoteMessage message) async {
  print("Handling a background message: ${message.messageId}");
}