import 'package:flutter/cupertino.dart';

String paramsToUrl({@required Map params, @required String url}) {
  String newUrl = url + "?";
  Map simpleValue = Map();
  Map listValue = Map();

  params.forEach((key, value) {
    if (value != null && checkIsListOrNot(value)) {
      listValue.addAll({key: value});
    } else if (value != null) {
      simpleValue.addAll({key: value});
    }
  });

  simpleValue.forEach((key, value) {
    newUrl += (key.toString() + "=" + value.toString() + "&");
  });
  listValue.forEach((key, value) {
    String listParams = "";
    value.forEach((element) {
      listParams += "$key=$element&";
    });
    newUrl += (listParams);
  });
  if (newUrl.endsWith("&")) {
    newUrl = newUrl.substring(0, newUrl.length - 1);
  }

  return newUrl[newUrl.length -1] == "?"
      ? newUrl.substring(0, newUrl.length - 1)
      : newUrl;
}

bool checkIsListOrNot(value) {
  return value.runtimeType.toString().contains("List");
}
