

int parseStringToInt(String value) {
  int parse;
  try {
    parse = int.parse(value);
    return parse;
  } catch (e) {
    return null;
  }
}
