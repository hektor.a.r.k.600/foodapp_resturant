import 'package:flutter/cupertino.dart';
import 'package:get_storage/get_storage.dart';
import 'package:shared_preferences/shared_preferences.dart';

abstract class DataStorage {
  Future<void> setData({@required String key, @required String value});
  String getData({@required String key});
  Future<void> removeData({@required String key});
  Future<void> erase();
}

class DataStorageGetXImpl implements DataStorage {
  final GetStorage getStorage;

  DataStorageGetXImpl({@required this.getStorage});

  @override
  String getData({String key}) {
    return getStorage.read<String>(key);
  }

  @override
  Future<void> removeData({String key}) {
    return getStorage.remove(key);
  }

  @override
  Future<void> setData({String key, String value}) {
    return getStorage.write(key, value);
  }

  @override
  Future<void> erase() {
    return getStorage.erase();

  }
}

class DataStorageSPImpl implements DataStorage {
  final SharedPreferences sharedPreferences;

  DataStorageSPImpl({@required this.sharedPreferences});

  @override
  String getData({String key}) {
    return sharedPreferences.getString(key);
  }

  @override
  Future<void> removeData({String key}) {
    return sharedPreferences.remove(key);
  }

  @override
  Future<void> setData({String key, String value}) {
    return sharedPreferences.setString(key, value);
  }


  @override
  Future<void> erase() {
    // TODO: implement erase
    throw UnimplementedError();
  }
}

