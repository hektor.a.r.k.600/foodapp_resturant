import 'dart:async';
import 'dart:convert';
import 'package:dartz/dartz.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:food_resturant/core/config/api_address.dart';
import 'package:food_resturant/core/config/config.dart';
import 'package:food_resturant/core/config/global_setting.dart';
import 'package:food_resturant/core/config/routes.dart';
import 'package:food_resturant/feature/depenency_injection.dart';
import 'package:food_resturant/feature/entities/check_update.dart';
import 'package:food_resturant/feature/entities/get_food_categories_entity.dart';
import 'package:food_resturant/feature/entities/login_entity.dart';
import 'package:food_resturant/feature/server/api_helper.dart';
import 'package:food_resturant/feature/server/api_response.dart';
import 'package:food_resturant/feature/server/params/public_params.dart';
import 'package:food_resturant/feature/view/page/home_page/home_page.dart';
import 'package:food_resturant/feature/view/page/login_page/login_page.dart';
import 'package:food_resturant/feature/view/page/splash_screen_page/splash_screen_controller.dart';
import 'package:food_resturant/library/platforms/shared_preferences.dart';
import 'package:food_resturant/library/widgets/snackbar.dart';
import 'package:get/get.dart';
import 'package:giffy_dialog/giffy_dialog.dart';
import 'package:dio/dio.dart';
import 'package:package_info/package_info.dart';
import 'package:url_launcher/url_launcher.dart';

// getData(context) async {
//   GetDataFromSharedPreferences sharedPreferences =
//       new GetDataFromSharedPreferences();
//   String userJson =
//       await sharedPreferences.getString(key: USER_SHARED_PREFERENCES_KEY);
//   Timer(Duration(seconds: 2), () async {
//     if (userJson == null) {
//       Navigator.pushReplacement(context, NavigatorSliderBTT(page: LoginPage()));
//     } else {
//       Navigator.pushReplacement(context, NavigatorSliderBTT(page: Homepage()));
//     }
//   });
// }

// setNotification() async {
//   final NotificationService notificationService = sl<NotificationService>();
//   await notificationService.initialise();
//   String pushID = await notificationService.getPushId();
//   print("pushID : " + pushID);
// }

// Future<String> getPushId() async {
//   final NotificationService notificationService = sl<NotificationService>();
//   String pushId = await notificationService.getPushId();
//   return pushId;
// }

Future noInternetDialog(BuildContext context) {
  if (!Get.isDialogOpen) {
    return Get.dialog(
        WillPopScope(
          onWillPop: () async {
            return false;
          },
          child: NetworkGiffyDialog(
            onlyOkButton: true,
            onlyCancelButton: true,
            image: Padding(
              padding: const EdgeInsets.only(top: 15.0),
              child: Image.asset(
                Images.no_internet,
                fit: BoxFit.contain,
              ),
            ),
            title: Text('Waiting For Connection...',
                textAlign: TextAlign.center,
                style: TextStyle(fontSize: 22.0, fontWeight: FontWeight.w600)),
            description: Text(
              'Check your internet connection \n and try again',
              textAlign: TextAlign.center,
              style: TextStyle(fontSize: 16.0),
            ),
            entryAnimation: EntryAnimation.BOTTOM,
            buttonOkColor: Colors.white,
          ),
        ),
        barrierDismissible: false,
        useSafeArea: true);
  } else {
    return null;
  }
}

splashHandler(
  BuildContext context,
) async {
  try {
    LoginEntity user;
    await getFoodCategoryApi();
    if (sl<DataStorage>().getData(key: USER) != null) {
      user = LoginEntity.fromJson(
          json.decode(sl<DataStorage>().getData(key: USER)));
      sl<GlobalSetting>().user = user?.loginObject;
    }

    //  setNotification();
    //  setLocalNotification(context);
    print(user);
    if (user == null) {
      SystemChrome.setEnabledSystemUIOverlays(SystemUiOverlay.values);

      SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
        statusBarColor: Colors.transparent,
      ));
      Get.offAndToNamed(Routes.LoginPage);
      // Get.offAndToNamed(Routes.HomePage);
    } else {
      Get.offAndToNamed(Routes.HomePage);
    }
  } catch (e) {
    print(e);
    SystemChrome.setEnabledSystemUIOverlays(SystemUiOverlay.values);

    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
      statusBarColor: Get.theme.primaryColor,
    ));
    Get.offAndToNamed(Routes.LoginPage);
  }
}

Future<UpdateModel> getUpdate() async {
  Dio _dio = Dio();
  print("object");
  print(ApiAddress.BASE_URL + ApiAddress.CHECK_UPDATE);
  var res = await _dio.get(
    ApiAddress.BASE_URL + ApiAddress.CHECK_UPDATE,
    options: Options(headers: {
      "content-type": "application/json",
      "accept": "application/json",
    }),
  );

  Map result = res.data;
  print(res.data);
  if (res.statusCode == 200) {
    final UpdateModel storeFoodModel =
        UpdateModel.fromJson(result as Map<String, dynamic>);
    return storeFoodModel;
  } else {
    Get.snackbar("Server Error", result['message']);
    return null;
  }
}

getFoodCategoryApi() async {
  var publicParams = PublicParams(pageNo: 1);

  final response = await sl<ApiHelper>()
      .getWithParam(ApiAddress.GET_FOOD_CATEGORIES, publicParams.toJson());
  var result = ApiResponse.returnResponse(
      response, GetFoodCategoriesEntity.fromJson(response.data));
  if (result.status == ApiStatus.COMPLETED && result?.data?.status) {
    sl<GlobalSetting>().foodCategoriesObject =
        result?.data?.getFoodCategoriesObject;
  } else {
    showSnackBar(text: result?.data?.message ?? '');
  }
}
