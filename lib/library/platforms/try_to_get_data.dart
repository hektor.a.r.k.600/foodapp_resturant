import 'package:food_resturant/core/error/exception.dart';
import 'package:food_resturant/feature/server/connections/get_data.dart';
import 'package:http/http.dart';

class TryToGetData implements IServer {
  final IServer decorator;
  final maximumDuration = Duration(seconds: 6);

  TryToGetData(this.decorator);

  @override
  Future<Response> getDataFromServer() async {
    Response data;
    Duration retryToGetData = Duration(seconds: 0);
    while (retryToGetData < maximumDuration) {
      try {
        data = await decorator.getDataFromServer();
        return data;
      } catch (e) {
        if (e.runtimeType == AuthenticationException) {
          throw ServerException();
        } else {
          retryToGetData += Duration(seconds: 2);
          if (retryToGetData >= maximumDuration) {
            throw ServerException();
          }
        }
      }
    }
    return data;
  }

  @override
  Future<LocalResponse> getDataFromLocal() async {
    return await decorator.getDataFromLocal();
  }
}
