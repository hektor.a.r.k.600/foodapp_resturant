import 'dart:convert';
import 'dart:io';
import 'package:dio/dio.dart';
import 'package:food_resturant/core/config/config.dart';
import 'package:food_resturant/feature/depenency_injection.dart';
import 'package:food_resturant/feature/entities/icon_object.dart';
import 'package:food_resturant/feature/entities/image_obj.dart';
import 'package:food_resturant/library/platforms/authentication.dart';
import 'package:food_resturant/library/platforms/shared_preferences.dart';

Future<IconObject> uploadFile(
    {File imageFile, int type, onSendProgress(int count, int total)}) async {
  String token =
      sl<DataStorage>().getData(key: USER_TOKEN);

  Map<String, String> headers = {
    "accept": "text/plain",
    "content-type": "multipart/form-data",
    // "authorization": "Bearer $token"
  };

  FormData formData = FormData.fromMap({
    "type": type??1,
    "file":
        await MultipartFile.fromFile(imageFile.path, filename: imageFile.path.split('/').last)
  });
  final response = await Dio().post("http://fileapi.makanavenue.com/Document/SendDocument",
      data: formData,
      onSendProgress: onSendProgress,
      options: Options(headers: headers));
  print(response.data.toString());
  if (response.statusCode == 200) {
    // String imageId = json.decode(response.data)['id'];
   IconObject iconObject =IconObject.fromJson(response.data);
    return iconObject;
  } else {
    authentication();
    print(response.statusCode.toString());
    return null;
  }
  // ByteStream stream =
  //     // ignore: deprecated_member_use
  //     new http.ByteStream(DelegatingStream.typed(imageFile.openRead()));
  // int length = await imageFile.length();
  // Uri uri = Uri.parse(Address.sendDocumentUrl);
  // MultipartRequest request = new http.MultipartRequest("POST", uri);
  // MultipartFile multiPartFile = new http.MultipartFile('image', stream, length,
  //     filename: basename(imageFile.path));
  // request.files.add(multiPartFile);
  // request.fields['type'] = type.toString();
  // request.headers.addAll(headers);
  // StreamedResponse respond = await request.send();
  // String response = await respond.stream.bytesToString();

  // if (respond.statusCode == 200) {
  //   String imageId = json.decode(json.decode(response))['Images'];
  //   return imageId;
  // } else {
  //   authentication();
  //   print(respond.statusCode.toString());
  //   return "401";
  // }
}
