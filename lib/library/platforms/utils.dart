import 'dart:io';

import 'package:multi_image_picker/multi_image_picker.dart';
import 'package:path_provider/path_provider.dart';

Future<File> getImageFileFromAssets(Asset asset) async {
  var byteData =await  asset.getByteData();
  final file = File('${(await getTemporaryDirectory()).path}/${asset.name}');
  await file.writeAsBytes(byteData.buffer.asUint8List(byteData.offsetInBytes, byteData.lengthInBytes));

  return file;
}