import 'package:auto_animated/auto_animated.dart';
import 'package:flutter/material.dart';

class AnimatedListItems extends StatefulWidget {
  final int index;
  final Widget child;
  final String baseKey;
  final Offset begin;
  final int speed;
  final bool isReverse;
  final bool reAnimateOnVisibility;
  final IAnimationsType transitionType;
  final Alignment scaleAligment;

  AnimatedListItems(
      {@required this.child,
      @required this.index,
      this.begin,
      this.transitionType = AnimationsType.sliderTransitionType,
      this.baseKey,
      this.isReverse = false,
      this.reAnimateOnVisibility = false,
      this.scaleAligment,
      this.speed});
  @override
  _AnimatedListItemsState createState() => _AnimatedListItemsState();
}

class _AnimatedListItemsState extends State<AnimatedListItems> {
  @override
  Widget build(BuildContext context) {
    AnimationsParams animationsParams = AnimationsParams(
      index: widget.index,
      child: widget.child,
      baseKey: widget.baseKey,
      begin: widget.begin,
      speed: widget.speed,
      reAnimateOnVisibility: widget.reAnimateOnVisibility,
      isReverse: widget.isReverse,
      scaleAligment: widget.scaleAligment,
    );
    return animationsDic(animationsParams)[widget.transitionType];
  }
}

Map<IAnimationsType, Widget> animationsDic(AnimationsParams animationsParams) =>
    {
      AnimationsType.sliderTransitionType: SliderAnimation(
        animationsParams: animationsParams,
      ),
      AnimationsType.scaleTransitionType: ScaleAnimation(
        animationsParams: animationsParams,
      ),
    };

class AnimationsParams {
  final int index;
  final Widget child;
  final String baseKey;
  final Offset begin;
  final int speed;
  final bool isReverse;
  final bool reAnimateOnVisibility;
  final Alignment scaleAligment;

  AnimationsParams(
      {this.index,
      this.child,
      this.baseKey,
      this.isReverse,
      this.begin,
      this.reAnimateOnVisibility,
      this.speed,
      this.scaleAligment});
}

class SliderAnimation extends StatelessWidget {
  const SliderAnimation({
    @required this.animationsParams,
  });

  final AnimationsParams animationsParams;

  @override
  Widget build(BuildContext context) {
    return AnimateIfVisible(
        visibleFraction: 0.5,
        reAnimateOnVisibility: animationsParams.reAnimateOnVisibility,
        key: Key((animationsParams.baseKey ?? "item ") +
            animationsParams.index.toString()),
        duration: Duration(
            milliseconds:
                (animationsParams.index * (animationsParams.speed ?? 35))),
        builder: (
          BuildContext context,
          Animation<double> animation,
        ) {
          if (animationsParams.isReverse) {
            return buildSliderTransitionReverse(animation);
          } else {
            return buildSliderTransitionForward(animation);
          }
        });
  }

  FadeTransition buildSliderTransitionForward(Animation<double> animation) {
    return FadeTransition(
      opacity: Tween<double>(
        begin: 0,
        end: 1,
      ).animate(animation),
      child: SlideTransition(
        position: Tween<Offset>(
          begin: animationsParams.begin ?? Offset(0, 0.4),
          end: Offset.zero,
        ).animate(animation),
        child: animationsParams.child,
      ),
    );
  }

  FadeTransition buildSliderTransitionReverse(Animation<double> animation) {
    return FadeTransition(
      opacity: Tween<double>(
        begin: 1,
        end: 0,
      ).animate(animation),
      child: SlideTransition(
        position: Tween<Offset>(
          begin: animationsParams.begin ?? Offset(0.4, 0),
          end: Offset.zero,
        ).animate(animation),
        child: animationsParams.child,
      ),
    );
  }
}

class ScaleAnimation extends StatelessWidget {
  const ScaleAnimation({
    @required this.animationsParams,
  });

  final AnimationsParams animationsParams;

  @override
  Widget build(BuildContext context) {
    return AnimateIfVisible(
        reAnimateOnVisibility: animationsParams.reAnimateOnVisibility,
        key: Key((animationsParams.baseKey ?? "item ") +
            animationsParams.index.toString()),
        duration: Duration(
            milliseconds:
                (animationsParams.index * (animationsParams.speed ?? 35))),
        builder: (
          BuildContext context,
          Animation<double> animation,
        ) {
          if (animationsParams.isReverse) {
            return buildScaleTransitionReverse(animation);
          } else {
            return buildScaleTransitionForward(animation);
          }
        });
  }

  FadeTransition buildScaleTransitionForward(Animation<double> animation) {
    return FadeTransition(
      opacity: Tween<double>(
        begin: 0,
        end: 1,
      ).animate(animation),
      child: ScaleTransition(
        alignment: animationsParams.scaleAligment ?? Alignment.center,
        scale: Tween<double>(begin: 0.7, end: 1).animate(animation),
        child: animationsParams.child,
      ),
    );
  }

  FadeTransition buildScaleTransitionReverse(Animation<double> animation) {
    return FadeTransition(
      opacity: Tween<double>(
        begin: 1,
        end: 0,
      ).animate(animation),
      child: ScaleTransition(
        alignment: animationsParams.scaleAligment ?? Alignment.center,
        scale: Tween<double>(begin: 1, end: 0.7).animate(animation),
        child: animationsParams.child,
      ),
    );
  }
}

abstract class IAnimationsType {
  const IAnimationsType();
}

class ScaleTransitionType extends IAnimationsType {
  const ScaleTransitionType();
}

class SliderTransitionType extends IAnimationsType {
  const SliderTransitionType();
}

class AnimationsType {
  static const ScaleTransitionType scaleTransitionType = ScaleTransitionType();
  static const SliderTransitionType sliderTransitionType =
      SliderTransitionType();
}
