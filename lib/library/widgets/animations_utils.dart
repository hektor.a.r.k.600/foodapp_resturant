import 'package:flutter/material.dart';
import 'package:flutter_animator/flutter_animator.dart';

class AnimationsUtils {
  static Future reverseAllAnimations(
      List<GlobalKey<AnimatorWidgetState>> animationsKey) async {
    animationsKey = animationsKey.reversed.toList();
    for (var i = 0; i < animationsKey.length; i++) {
      animationsKey[i].currentState.animator.controller.duration =
          Duration(milliseconds: 600);
          
      animationsKey[i].currentState.animator.reverse();
      await Future.delayed(Duration(milliseconds: 150));
    }
    await Future.delayed(Duration(milliseconds: 500));
  }

  static Future forwardAllAnimations(
      List<GlobalKey<AnimatorWidgetState>> animationsKey) async {
    for (var i = 0; i < animationsKey.length; i++) {
      animationsKey[i].currentState.animator.controller.duration =
          Duration(milliseconds: 600);
      animationsKey[i].currentState.animator.forward();
      await Future.delayed(Duration(milliseconds: 150));
    }
    await Future.delayed(Duration(milliseconds: 500));
  }

  static AnimationPreferences animationPreferences(
      { int offsetMilliseconds = 0, int durationMilliseconds = 550}) {
    return AnimationPreferences(
        offset: Duration(milliseconds: offsetMilliseconds),
        duration: Duration(milliseconds: durationMilliseconds));
  }
}
