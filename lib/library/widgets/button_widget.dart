import 'package:flutter/material.dart';
import 'package:food_resturant/core/config/config.dart';
import 'package:food_resturant/library/widgets/text_widget.dart';
import 'package:get/get.dart';

class CustomButton extends StatelessWidget {
  final Widget child;
  final Color color;
  final Color outlineColor;
  final Color splashColor;
  final int type;
  final double borderRadios;
  final double elevation;
  final EdgeInsets padding;
  final EdgeInsets insidepadding;

  final Function onTap;

  const CustomButton(
      {Key key,
      this.color,
      this.outlineColor,
      this.splashColor,
      this.type = 1,
      this.borderRadios,
      this.elevation,
      this.padding,
      this.insidepadding,
      this.onTap,
      this.child})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return buttonsList(
        child: child,
        color: color,
        outlineColor: outlineColor,
        splashColor: splashColor,
        borderRadios: borderRadios,
        elevation: elevation,
        padding: padding,
        insidepadding: insidepadding,
        onTap: onTap)[type - 1];
  }
}

List<Widget> buttonsList(
        {Widget child,
        Color color,
        Color outlineColor,
        Color splashColor,
        double borderRadios,
        double elevation,
        EdgeInsets padding,
        EdgeInsets insidepadding,
        Function onTap}) =>
    [
      FlatButton(
        splashColor: splashColor ?? Get.theme.primaryColorDark,
        padding: padding ?? EdgeInsets.only(top: 7.0, right: 15.0, left: 15.0),
        onPressed: onTap ?? () {},
        child: Padding(
          padding: insidepadding ??
              EdgeInsets.only(left: 8.0, right: 8.0, bottom: 4.0),
          child: child ?? CustomText('MyButton'),
        ),
        color: color ?? Get.theme.primaryColor,
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(borderRadios ?? 5.0)),
      ),
      RaisedButton(
        splashColor: splashColor ?? Get.theme.primaryColorDark,
        padding: padding ?? EdgeInsets.only(top: 7.0, right: 15.0, left: 15.0),
        elevation: elevation ?? 3.0,
        onPressed: onTap ?? () {},
        child: Padding(
          padding: insidepadding ??
              EdgeInsets.only(left: 8.0, right: 8.0, bottom: 4.0),
          child: child ?? CustomText('MyButton'),
        ),
        color: color ?? Get.theme.primaryColor,
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(borderRadios ?? 5.0)),
      ),
      OutlineButton(
        splashColor: splashColor ?? Get.theme.primaryColorDark,
        highlightedBorderColor: ColorPallet.white,
        padding: padding ?? EdgeInsets.only(top: 7.0, right: 15.0, left: 15.0),
        borderSide: BorderSide(color: outlineColor ?? Get.theme.primaryColor),
        onPressed: onTap ?? () {},
        child: Padding(
          padding: insidepadding ??
              EdgeInsets.only(left: 8.0, right: 8.0, bottom: 4.0),
          child: child ?? CustomText('MyButton'),
        ),
        color: color ?? Get.theme.primaryColor,
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(
          borderRadios ?? 5.0,
        )),
      ),
      MaterialButton(
        splashColor: splashColor ?? Get.theme.primaryColorDark,
        padding: padding ?? EdgeInsets.only(top: 7.0, right: 15.0, left: 15.0),
        onPressed: onTap ?? () {},
        child: Padding(
          padding: insidepadding ??
              EdgeInsets.only(left: 8.0, right: 8.0, bottom: 4.0),
          child: child ?? CustomText('MyButton'),
        ),
        color: color ?? Get.theme.primaryColor,
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(borderRadios ?? 5.0)),
      ),
    ];
