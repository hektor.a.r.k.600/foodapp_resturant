import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:food_resturant/library/widgets/custom_loading_widget.dart';
import 'package:get/get.dart';

class BuildCachedImageWidget extends StatelessWidget {
  final String imageUrl;
  final double width;
  final double height;
  final Widget defult;
  final double borderRadius;
  final double loadingWidth;
  const BuildCachedImageWidget(
      {Key key,
      @required this.imageUrl,
      this.width,
      this.height,
      this.defult,
      this.loadingWidth,
      this.borderRadius})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ClipRRect(
      borderRadius: BorderRadius.circular(borderRadius ?? 0.0),
      child: imageUrl == null || imageUrl.isEmpty
          ? SizedBox(
              width: width ?? 30.0,
              height: height ?? 30.0,
              child: defult ??
                  // Image.asset(
                  //   Images.empty_Image,
                  //   fit: BoxFit.cover,
                  // )
                  Container(
                    color: Get.theme.primaryColorLight,
                    
                    child: Center(
                      child: Icon(Icons.person,color: Get.theme.primaryColor,size: 50.0,),
                    ),
                  ))
          : CachedNetworkImage(
              width: width ?? 30.0,
              height: height ?? 30.0,
              fit: BoxFit.cover,
              fadeInCurve: Curves.easeIn,
              fadeInDuration: Duration(milliseconds: 100),
              imageUrl: imageUrl,
              placeholder: (context, url) => customLoadingWidget(loadingWidth??width)),
    );
  }
}
