import 'dart:core';

import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'custom_drop_down.dart';

class CustomDropDownList extends StatelessWidget {
  final List<String> items;
  final ValueChanged<String> onChange;
  final String valueItem;
  final BuildContext context;
  final String hint;
  final EdgeInsets padding;
  final EdgeInsets iconPadding;
  final Color iconColor;
  final Color dropdownColor;
  final Color textColor;
  final bool hideIcon;
  final bool showSearchBox;
  final double textSize;
  final double elevation;
  final double iconSize;
  final double borderRadious;
  final Future<List<String>> Function(String) onFind;
  const CustomDropDownList(
      { this.items,
      @required this.onChange,
      @required this.valueItem,
      @required this.context,
      this.hint,
      this.textSize,
      this.padding,
      this.iconColor,
      this.dropdownColor,
      this.iconPadding,
      this.textColor,
      this.showSearchBox = false,
      this.borderRadious,
      this.iconSize,
      this.onFind,
      this.elevation,
      this.hideIcon});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: padding ??
          EdgeInsets.only(right: 10.0, left: 10.0, top: 8.0, bottom: 8.0),
      child: Material(
        elevation: elevation ?? 0.0,
        color: dropdownColor ?? Get.theme.primaryColorLight,
        borderRadius: BorderRadius.all(Radius.circular(borderRadious ?? 10.0)),
        child: DropdownSearch<String>(
            mode: Mode.MENU,
            showSelectedItem: true,
            textcolor: textColor ?? Get.theme.primaryColorDark,
            textSize: textSize ?? 18,
            iconSize: iconSize ?? 24,
            iconColor: iconColor ?? Get.theme.primaryColorDark,
            iconPadding: iconPadding,
            onFind: onFind ,
            showClearButton: hideIcon ?? false,
            items: items,
            label: hint ?? "",
            showSearchBox: showSearchBox,
            hint: hint ?? "Search",
            
            searchBoxDecoration: InputDecoration(
                hintText: "Search",
                hintStyle: TextStyle(color: Colors.grey),
                border: OutlineInputBorder(),
                contentPadding: EdgeInsets.only(left: 10.0, right: 10.0)),
            onChanged: onChange,
            selectedItem: valueItem),
      ),
    );
  }
}
