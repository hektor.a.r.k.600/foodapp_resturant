import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:food_resturant/core/config/config.dart';
import 'package:food_resturant/library/widgets/text_widget.dart';

Widget customEmptyWidget({text}) {
  return Center(child: Text(text??'There is No Data For showing'));
}
// Widget customEmptyWidget(){
//   return Image.asset(Images.emptyPage,fit: BoxFit.cover);
// }
