import 'package:flutter/material.dart';
import 'package:food_resturant/core/config/config.dart';

class CustomFadeInImage extends StatelessWidget {
  const CustomFadeInImage({
    Key key,
    @required this.imagteUrl,
  }) : super(key: key);

  final String imagteUrl;

  @override
  Widget build(BuildContext context) {
    return FadeInImage(
        fit: BoxFit.cover,
        fadeInCurve: Curves.easeIn,
        fadeInDuration: Duration(milliseconds: 100),
        placeholder: AssetImage(Images.loading_Image),
        image: imagteUrl == null
            ? AssetImage(Images.empty_Image)
            : NetworkImage(imagteUrl));
  }
}
