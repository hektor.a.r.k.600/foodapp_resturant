import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:food_resturant/core/config/config.dart';

Widget customLoadingWidget(double radios) {
  return SizedBox(
    height: radios,
    width: radios,
    child: CircularProgressIndicator(
      valueColor: AlwaysStoppedAnimation(ColorPallet.red),
    ),
  );
}
// Widget customEmptyWidget(){
//   return Image.asset(Images.emptyPage,fit: BoxFit.cover);
// }
