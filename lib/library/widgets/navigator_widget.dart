
import 'package:flutter/material.dart';

class NavigatorSliderBTT extends PageRouteBuilder{

  Widget page;

  NavigatorSliderBTT({@required this.page})
      :super(
      pageBuilder: (BuildContext context, Animation<double> animation, Animation<double> secondaryAnimation){
        return page;
      },
      transitionDuration: Duration(milliseconds: 350),
      transitionsBuilder:(BuildContext context, Animation<double> animation, Animation<double> secondaryAnimation, Widget child){
        Animation<Offset> custom = Tween<Offset>(begin: Offset(0.0,1.0),end: Offset(0.0, 0.0)).animate(animation);
        return SlideTransition(position: custom,child: child);
      }
  );
}
class NavigatorSliderRTL extends PageRouteBuilder{

  Widget page;

  NavigatorSliderRTL({@required this.page})
      :super(
      pageBuilder: (BuildContext context, Animation<double> animation, Animation<double> secondaryAnimation){
        return page;
      },
      transitionDuration: Duration(milliseconds: 500),
      transitionsBuilder:(BuildContext context, Animation<double> animation, Animation<double> secondaryAnimation, Widget child){
        Animation<Offset> custom = Tween<Offset>(begin: Offset(1.0,0.0),end: Offset(0.0, 0.0)).animate(animation);
        return SlideTransition(position: custom,child: child);
      }
  );
}
