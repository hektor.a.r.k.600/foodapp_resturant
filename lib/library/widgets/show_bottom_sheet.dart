import 'package:flutter/material.dart';
import 'package:get/get.dart';

Future showCustomBottomSheet(Widget widget) {
  return Get.bottomSheet(widget,
      backgroundColor: Get.theme.accentColor,
      enableDrag: true,
  );
}
