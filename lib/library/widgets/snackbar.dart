import 'package:flutter/material.dart';
import 'package:food_resturant/core/config/config.dart';
import 'package:food_resturant/library/widgets/text_widget.dart';
import 'package:get/get.dart';
import 'package:meta/meta.dart';

enum SnackBarType { Error, Hint, Report }

void showSnackBar(
    {@required String text,
    Color color,
    SnackBarType snackBarType = SnackBarType.Error}) {
  return Get.snackbar("Skills Massasge",
      text.substring(0, text.length > 200 ? 200 : text.length),
      snackPosition: SnackPosition.BOTTOM,
      duration: Duration(seconds: 5),
      titleText: Padding(
        padding: const EdgeInsets.only(top: 4.0),
        child: CustomText(
          text.substring(0, text.length > 200 ? 200 : text.length),
          fontWeight: FontWeight.bold,
          textAlign: TextAlign.left,
        ),
      ),
      messageText: SizedBox(),
      colorText: Colors.black,
      margin: EdgeInsets.all(0.0),
      padding: EdgeInsets.all(8.0),
      boxShadows: [BoxShadow(color: Colors.black45, blurRadius: 4)],
      borderRadius: 0.0,
      dismissDirection: SnackDismissDirection.HORIZONTAL,
      backgroundColor: snackBarType == SnackBarType.Report ? Colors.green : ColorPallet.red);
}
