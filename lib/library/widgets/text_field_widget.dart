import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';

class CustomTextField extends StatelessWidget {
  final String hint;
  final String lable;
  final String error;
  final Function(String) onChange;
  final Color backgroundColor;
  final Color textColor;
  final double fontSize;
  final TextAlign textAlign;
  final double height;
  final double width;
  final EdgeInsets padding;
  final bool isValid;
  final double borderRadius;
  final bool withInputFormatters;
  final Icon suffixIcon;
  final double elevation;
  final Icon prefixIcon;
  final EdgeInsets outSidePadding;
  final TextEditingController textEditingController;
  final List<TextInputFormatter> inputFormatters;
  final bool isBold;
  final Color hintColor;
  final int maxLine;
  final int maxLenght;
  final bool enableBorder;
  final FormFieldValidator<String> validator;
  final TextInputType textInputType;

  const CustomTextField(
      {this.hint,
      this.onChange,
      this.backgroundColor,
      this.textColor,
      this.fontSize,
      this.textAlign,
      this.height,
      this.width,
      this.padding,
      this.lable,
      this.isValid,
      this.borderRadius,
      this.withInputFormatters,
      this.suffixIcon,
      this.elevation,
      this.prefixIcon,
      this.outSidePadding,
      this.textEditingController,
      this.hintColor,
      this.isBold = true,
      this.maxLine = 1,
      this.maxLenght,
      this.enableBorder = false,
      this.validator,
      this.inputFormatters,
      this.error,
      this.textInputType});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: outSidePadding ??
          EdgeInsets.only(right: 0.0, left: 0.0, top: 8.0, bottom: 8.0),
      child: Container(
        height: height,
        decoration: BoxDecoration(
            color:
                backgroundColor ?? Get.theme.primaryColorLight.withAlpha(150),
            borderRadius: BorderRadius.circular(borderRadius ?? 10.0)),
        child: Center(
          child: Padding(
            padding: const EdgeInsets.only(top: 5.0),
            child: TextFormField(
              maxLength: maxLenght ?? null,
              controller: textEditingController,
              inputFormatters: inputFormatters,
              onChanged: onChange,
              validator: validator ??
                  (value) {
                    if (value.isEmpty) {
                      return 'Please Enter Something';
                    }
                    return null;
                  },
              keyboardType: textInputType ?? TextInputType.text,
              textAlign: textAlign ?? TextAlign.left,
              maxLines: maxLine,
              style: TextStyle(
                  //      backgroundColor: backgroundColor ?? Colors.transparent,
                  color: textColor ?? Get.theme.primaryColorDark,
                  fontSize: fontSize ?? 18.0,
                  fontWeight: isBold ? FontWeight.bold : FontWeight.normal),
              decoration: InputDecoration(
                counterText: "",
                enabledBorder: enableBorder
                    ? null
                    : OutlineInputBorder(borderSide: BorderSide.none),
                focusedBorder: enableBorder
                    ? null
                    : OutlineInputBorder(borderSide: BorderSide.none),
                prefixIcon: prefixIcon ?? null,
                suffixIcon: suffixIcon ?? null,
                labelText: lable ?? null,
                hintStyle: TextStyle(
                    color: hintColor ?? Get.theme.primaryColor.withAlpha(150),
                    fontSize: fontSize == null ? 18.0 : fontSize - 3.0,
                    fontWeight: FontWeight.normal),
                hintText: hint ?? "",
              ),
            ),
          ),
        ),
      ),
    );
  }
}
