import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:marquee/marquee.dart';

class CustomText extends StatelessWidget {
  final String text;
  final Color color;
  final double size;
  final FontWeight fontWeight;
    final TextAlign textAlign;
  final int characterCount;
  final bool isScrolling;
  const CustomText(this.text,
      {this.color,
      this.size,
      this.fontWeight,
      this.textAlign,
      this.characterCount,
      this.isScrolling = false});
  @override
  Widget build(BuildContext context) {
    return Material(
      color: Colors.transparent,
      child: Directionality(
        textDirection: TextDirection.ltr,
        child: isScrolling
            ? Marquee(
                text: text,
                style: TextStyle(
                    color: color ?? Colors.white,
                    fontSize: size == null ? 14.0 : size - 4.0,
                    fontWeight: fontWeight ?? FontWeight.normal),
                scrollAxis: Axis.horizontal,
                crossAxisAlignment: CrossAxisAlignment.center,
                blankSpace: 20.0,
                velocity: 50.0,
                pauseAfterRound: Duration(seconds: 1),
                accelerationDuration: Duration(seconds: 1),
                accelerationCurve: Curves.linear,
                decelerationDuration: Duration(milliseconds: 500),
                decelerationCurve: Curves.linear,
              )
            : Text(
                characterCount == null
                    ? text
                    : text.substring(
                            0,
                            text.length < characterCount
                                ? text.length
                                : characterCount) +
                        (text.length < characterCount ? "" : "..."),
                style: TextStyle(
                    color: color ?? Get.theme.accentColor,
                    fontSize: size == null ? 14.0 : size - 4.0,
                    fontWeight: fontWeight ?? FontWeight.normal),
                textAlign: textAlign ?? TextAlign.center,
              ),
      ),
    );
  }
}
