import 'package:animations/animations.dart';
import 'package:flutter/material.dart';
import 'package:food_resturant/core/config/config.dart';
import 'package:food_resturant/library/widgets/text_widget.dart';

class AnimationTransitionContainer extends StatelessWidget {
  const AnimationTransitionContainer({
    @required this.nextPage,
    this.color,
    @required this.child,
    this.borderRadios,
    this.elevation,
    this.transitionType,
  });
  final Widget nextPage;
  final Color color;
  final Widget child;
  final double borderRadios;
  final double elevation;
  final ContainerTransitionType transitionType;

  @override
  Widget build(BuildContext context) {
    return OpenContainer<bool>(
      transitionType: transitionType ?? ContainerTransitionType.fade,
      onClosed: (c) {},
      transitionDuration: Duration(milliseconds: 800),
      openBuilder: (BuildContext _, VoidCallback openContainer) {
        return nextPage;
      },
      tappable: true,
      closedElevation: 0.0,
      closedColor: color ?? ColorPallet.red,
      openColor: color ?? ColorPallet.red,
      closedShape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(borderRadios ?? 5.0)),
      closedBuilder: (BuildContext _, VoidCallback openContainer) {
        return child ?? CustomText('MyButton');
      },
    );
  }
}
