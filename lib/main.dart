import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:food_resturant/feature/view/page/add_discount_page/add_discount_page.dart';
import 'package:food_resturant/feature/view/page/add_food_page/add_food_page.dart';
import 'package:food_resturant/feature/view/page/balance_page/balance_page.dart';
import 'package:food_resturant/feature/view/page/chat_page/chat_page.dart';
import 'package:food_resturant/feature/view/page/createTicket_page/create_ticket_page.dart';
import 'package:food_resturant/feature/view/page/discount_page/restaurant_discounts_page.dart';
import 'package:food_resturant/feature/view/page/food_category_page/food_category_page.dart';
import 'package:food_resturant/feature/view/page/food_list_page/food_list_page.dart';
import 'package:food_resturant/feature/view/page/home_page/home_page.dart';
import 'package:food_resturant/feature/view/page/login_page/login_page.dart';
import 'package:food_resturant/feature/view/page/menu_page/menu_page.dart';
import 'package:food_resturant/feature/view/page/order_history_page/order_history_page.dart';
import 'package:food_resturant/feature/view/page/order_status_page/order_status_page.dart';
import 'package:food_resturant/feature/view/page/orders_by_Table_page/orders_by_table_page.dart';
import 'package:food_resturant/feature/view/page/orders_page/orders_page.dart';
import 'package:food_resturant/feature/view/page/profile_page/profile_page.dart';
import 'package:food_resturant/feature/view/page/qr_code_page/qr_code_page.dart';
import 'package:food_resturant/feature/view/page/selectFood/select_food_page.dart';
import 'package:food_resturant/feature/view/page/splash_screen_page/splash_screen_page.dart';
import 'package:food_resturant/feature/view/page/staffs_details_page/staffs_details_page.dart';
import 'package:food_resturant/feature/view/page/staffs_page/staffs_page.dart';
import 'package:food_resturant/feature/view/page/stocks_page/stocks_page.dart';
import 'package:food_resturant/feature/view/page/tables_page/tables_page.dart';
import 'package:food_resturant/feature/view/page/withdraw_page/withdraw_page.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'core/config/routes.dart';
import 'feature/depenency_injection.dart' as di;
import 'feature/view/page/all_tickets/all_tickets_page.dart';
import 'feature/view/page/restaurant_notification_page/restaurant_notification_page.dart';
import 'feature/view/page/statistics_page/Statistics_page.dart';



Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  await GetStorage.init();
  await di.init();

  runApp(GetMaterialApp(
    debugShowCheckedModeBanner: false,
    theme: ThemeData(
      fontFamily: 'Segoe_UI',
      scaffoldBackgroundColor: Color(0xffF8F7F7),
      visualDensity: VisualDensity.comfortable,
      primaryColorDark: Color(0xff338253),
      primaryColor: Color(0xff00D589),
      primaryColorLight: Color(0xffAFFBE0),
      accentColor: Colors.white,
      // primarySwatch: Colors.blue,

    ),
    themeMode: ThemeMode.light,
    getPages: pages,
    initialRoute: Routes.SplashScreen,
  ));
}

List<GetPage> pages = [
  GetPage(
    name: Routes.SplashScreen,
    page: () => SplashScreen(),
  ),
  GetPage(
    name: Routes.HomePage,
    page: () => HomePage(),
  ),
  GetPage(
    name: Routes.TablesPage,
    page: () => TablesPage(),
  ),
  GetPage(
    name: Routes.StaffsPage,
    page: () => StaffsPage(),
  ),
  GetPage(
    name: Routes.StaffsDetailsPage,
    page: () => StaffDetailsPage(),
  ),
  GetPage(
    name: Routes.MenuPage,
    page: () => MenuPage(),
  ),
  GetPage(
    name: Routes.AddFoodPage,
    page: () => AddFoodPage(),
  ),

  GetPage(
    name: Routes.StocksPage,
    page: () => StocksPage(),
  ),

  GetPage(
    name: Routes.StatisticsPage,
    page: () => StatisticsPage(),
  ),
  GetPage(
    name: Routes.ProfilePage,
    page: () => ProfilePage(),
  ),
  GetPage(
    name: Routes.OrdersPage,
    page: () => OrdersPage(),
  ),
  GetPage(
    name: Routes.OrderStatusPage,
    page: () => OrderStatusPage(),
  ),
  GetPage(
    name: Routes.FoodListPage,
    page: () => FoodListPage(),
  ),
  GetPage(
    name: Routes.OrderHistoryPage,
    page: () => OrderHistoryPage(),
  ),
  GetPage(
    name: Routes.WithdrawPage,
    page: () => WithdrawPage(),
  ),

  GetPage(
    name: Routes.WithdrawPage,
    page: () => WithdrawPage(),
  ),
  GetPage(
    name: Routes.DiscountsPage,
    page: () => RestaurantDiscountsPage(),
  ),
  GetPage(
    name: Routes.AddDiscountsPage,
    page: () => AddDiscountPage(),
  ),

  GetPage(
    name: Routes.SelectFoodPage,
    page: () => SelectFoodPage(),
  ),

  GetPage(
    name: Routes.AllTicketsPages,
    page: () => AllTicketsPage(),
  ),
  GetPage(
    name: Routes.CreateTicketPage,
    page: () => CreateTicketPage(),
  ),
  GetPage(
    name: Routes.ChatPage,
    page: () => ChatPage(),
  ),
  GetPage(
    name: Routes.RestaurantNotificationPage,
    page: () => RestaurantNotificationPage(),
  ),
  GetPage(
    name: Routes.QRCodePage,
    page: () => QRCodePage(),
  ),
  GetPage(
    name: Routes.LoginPage,
    page: () => LoginPage(),
  ),
  GetPage(
    name: Routes.FoodCategory,
    page: () => FoodCategoryPage(),
  ),
  GetPage(
    name: Routes.OrderByTable,
    page: () => OrdersByTablePage(),
  ),
  GetPage(
    name: Routes.BalancePage,
    page: () => BalancePage(),
  ),
];
